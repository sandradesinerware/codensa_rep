
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_CONDICION" 
  before insert on "CONDICION"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "CONDICION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_CONDICION" ENABLE;