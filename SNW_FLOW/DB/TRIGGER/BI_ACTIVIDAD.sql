
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_ACTIVIDAD" 
  before insert on "ACTIVIDAD"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "ACTIVIDAD_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_ACTIVIDAD" ENABLE;