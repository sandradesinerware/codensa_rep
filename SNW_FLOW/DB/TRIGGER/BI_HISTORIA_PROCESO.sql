
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_HISTORIA_PROCESO" 
  before insert on "HISTORIA_PROCESO"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "HISTORIA_PROCESO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_HISTORIA_PROCESO" ENABLE;