
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_ESTADO_FINAL" 
  BEFORE INSERT ON "SNW_FLOW"."ESTADO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "ESTADO_FINAL_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_ESTADO_FINAL" ENABLE;