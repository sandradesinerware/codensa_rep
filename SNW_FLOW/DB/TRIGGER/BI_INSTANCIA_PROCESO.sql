
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_INSTANCIA_PROCESO" 
  before insert on "INSTANCIA_PROCESO"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "INSTANCIA_PROCESO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_INSTANCIA_PROCESO" ENABLE;