
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_PROCESO" 
  before insert on "PROCESO"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "PROCESO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_PROCESO" ENABLE;