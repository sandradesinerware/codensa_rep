
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_TRANSICION" 
  before insert on "TRANSICION"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "TRANSICION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_TRANSICION" ENABLE;