
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BI_VALIDACION" 
  before insert on "VALIDACION"               
  for each row  
begin   
  if :NEW."ID" is null then 
    select "VALIDACION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;




/
ALTER TRIGGER "SNW_FLOW"."BI_VALIDACION" ENABLE;