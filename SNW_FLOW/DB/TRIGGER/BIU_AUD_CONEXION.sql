
  CREATE OR REPLACE TRIGGER "SNW_FLOW"."BIU_AUD_CONEXION" 
BEFORE INSERT OR UPDATE ON CONEXION FOR EACH ROW BEGIN
    --  Copyright (c) Sinerware SAS 2005-2016. All Rights Reserved.
    --
    --    MODIFIED   (YYYY/MM/DD)
    --      jsalamanca 2015-05-05 - Created
    --
    --    VERSION 1.1
    --    Trigger que pobla las columnas de auditoria
    ----------------------------------------------------------------------------------------------------------------
  IF inserting THEN
   :NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);
   :NEW.AUD_FECHA_CREACION := SYSDATE;   
  END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
begin
SELECT owa_util.get_cgi_env ('REMOTE_ADDR') into :NEW.AUD_TERMINAL_ACTUALIZACION FROM DUAL;
exception
when others then
:NEW.AUD_TERMINAL_ACTUALIZACION:=SYS_CONTEXT('USERENV','IP_ADDRESS');
end;
END;



/
ALTER TRIGGER "SNW_FLOW"."BIU_AUD_CONEXION" ENABLE;