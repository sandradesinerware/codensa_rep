
  CREATE OR REPLACE PROCEDURE "SNW_FLOW"."LOG_HISTORIA_PROCESO" 
(
  i_ID_INSTANCIA_PROCESO IN NUMBER 
, i_ID_ACTIVIDAD IN NUMBER 
, i_REQUEST IN varchar2 
) AS
PRAGMA AUTONOMOUS_TRANSACTION;
l_terminal varchar2(400);
BEGIN

 begin
SELECT owa_util.get_cgi_env ('REMOTE_ADDR') into l_terminal FROM DUAL;
exception
when others then
    l_terminal := SYS_CONTEXT('USERENV','IP_ADDRESS');
end;
  INSERT INTO evento_historia_proceso (
    request,
    id_instancia_proceso,
    id_actividad,
    usuario,
    terminal
) VALUES (
    i_REQUEST,
    i_ID_INSTANCIA_PROCESO,
    i_ID_ACTIVIDAD,
     NVL(V('APP_USER'),USER),
    l_terminal
);
commit;

END LOG_HISTORIA_PROCESO;

/