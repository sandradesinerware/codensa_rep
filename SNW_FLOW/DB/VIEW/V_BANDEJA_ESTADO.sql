
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_BANDEJA_ESTADO" ("ID_INSTANCIA_PROCESO", "ID_GRUPO", "FECHA_SOLICITUD", "ID_PROCESO", "ID_BANDEJA", "ID_ACTIVIDAD_VIGENTE", "ID_ESTADO", "ID_INSTANCIA_ACTIVA", "FINALIZADO") AS 
  select v_bandeja.id_instancia_proceso,
v_bandeja.id_grupo,
v_bandeja.fecha_solicitud,
v_bandeja.id_proceso,
v_bandeja.id_bandeja,
v_bandeja.id_actividad_vigente,
ESTADO_INTERMEDIO.id_estado,
id_instancia_activa,
v_bandeja.finalizado
from v_bandeja 
left join ESTADO_INTERMEDIO on (v_bandeja.id_actividad_vigente=ESTADO_INTERMEDIO.id_actividad and ESTADO_INTERMEDIO.ID_PROCESO=v_bandeja.id_proceso);