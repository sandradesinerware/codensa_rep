
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_INSTANCIA_VIGENTE" ("ID_INSTANCIA_BASE", "ID_INSTANCIA_PROCESO", "ID_PROCESO", "FECHA_INICIO", "FECHA_FIN", "OBSERVACION", "ID_ESTADO_FINAL", "HEREDADO", "ID_INSTANCIA_PROCESO_PADRE", "ID_ACTIVIDAD_VIGENTE", "ID_INSTANCIA_ACTIVA", "ID_ESTADO") AS 
  select instancia_proceso.id id_instancia_base, 
instancia_proceso2.id id_instancia_proceso,
instancia_proceso2.id_proceso,
instancia_proceso2.fecha_inicio,
instancia_proceso2.fecha_fin,
instancia_proceso2.observacion,
instancia_proceso2.id_estado_final,
instancia_proceso2.heredado,
instancia_proceso2.id_instancia_proceso_padre,
instancia_proceso2.id_actividad_vigente,
instancia_proceso2.id_instancia_activa,
ESTADO_INTERMEDIO.id_estado
from instancia_proceso inner join instancia_proceso instancia_proceso2 on (instancia_proceso.id_instancia_activa=instancia_proceso2.id)
left join ESTADO_INTERMEDIO on (instancia_proceso2.id_actividad_vigente=ESTADO_INTERMEDIO.id_actividad and ESTADO_INTERMEDIO.ID_PROCESO=instancia_proceso2.id_proceso);