
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_VALIDACION" ("PROCESO_ID", "PROCESO_NOMBRE", "ACTIVIDAD_ID", "ACTIVIDAD_NOMBRE", "TRANSICION_ID", "VALIDACION_ID", "PAQUETE", "FUNCION", "CODIGO_PARAM", "VALIDACION_NOMBRE", "NOMBRE_CASO_NEGATIVO", "RECOMENDACION") AS 
  select 
 proceso.id proceso_id, proceso.nombre proceso_nombre,
 actividad.id actividad_id, actividad.nombre actividad_nombre,
 transicion.id transicion_id,
 validacion.ID validacion_id,
 validacion.PAQUETE,
 validacion.FUNCION,
 validacion.CODIGO_PARAM,
 validacion.NOMBRE validacion_nombre,
 validacion.NOMBRE_CASO_NEGATIVO,
 validacion.RECOMENDACION 
from validacion
join condicion on (validacion.id = condicion.id_validacion)
join transicion on (condicion.id_transicion=transicion.id)
join actividad on (transicion.id_actividad_inicial=actividad.id)
join proceso on (transicion.id_proceso=proceso.id);