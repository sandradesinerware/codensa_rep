
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_PROCESO_TERMINADO" ("INSTANCIA_BASE", "INSTANCIA_ACTIVA", "ACTIVIDAD_ACTUAL", "ESTADO") AS 
  select
 INSTANCIA_PROCESO.id instancia_base,
 INSTANCIA_PROCESO.id_instancia_activa instancia_activa,
 snw_flow.flujo.nombre_actividad(INSTANCIA_PROCESO.id_instancia_activa) ACTIVIDAD_ACTUAL, 
 snw_flow.flujo.ver_estado_actual(INSTANCIA_PROCESO.id_instancia_activa) ESTADO 
from INSTANCIA_PROCESO 
where fecha_fin IS NOT NULL OR id_estado_final IS NOT NULL;