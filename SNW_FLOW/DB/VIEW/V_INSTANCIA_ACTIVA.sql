
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_INSTANCIA_ACTIVA" ("INSTANCIA_BASE", "INSTANCIA_ACTIVA") AS 
  select
 INSTANCIA_PROCESO.id instancia_base,
 INSTANCIA_PROCESO.id_instancia_activa instancia_activa
from INSTANCIA_PROCESO 
where fecha_fin IS NOT NULL OR id_estado_final IS NOT NULL;