
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_PERMISO_ACTIVIDAD" ("ID", "INICIO", "FIN", "ES_INICIAL", "ID_PROCESO", "NOMBRE", "APLICACION", "PAGINAS", "GRUPO", "NOMBRE_PUBLICO", "PRIVILEGIO", "NOMBRE_GRUPO", "ID_USUARIO") AS 
  select v_actividad_grupo."ID",
v_actividad_grupo."INICIO",
v_actividad_grupo."FIN",
v_actividad_grupo."ES_INICIAL",
v_actividad_grupo."ID_PROCESO",
v_actividad_grupo."NOMBRE",
v_actividad_grupo."APLICACION",
v_actividad_grupo."PAGINAS",
v_actividad_grupo."GRUPO",
v_actividad_grupo."NOMBRE_PUBLICO",
v_actividad_grupo."PRIVILEGIO",
v_actividad_grupo."NOMBRE_GRUPO",
snw_auth.v_usuario_grupo.id_usuario from  v_actividad_grupo
left join snw_auth.v_usuario_grupo  on (snw_auth.v_usuario_grupo.id_grupo=v_actividad_grupo.grupo);