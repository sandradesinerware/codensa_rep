
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_BANDEJA" ("ID_INSTANCIA_PROCESO", "ID_PROCESO", "FECHA_SOLICITUD", "ID_ACTIVIDAD_VIGENTE", "ID_INSTANCIA_ACTIVA", "FINALIZADO") AS 
  SELECT
    bandeja.id_instancia_proceso,
    instancia_proceso.ID_PROCESO,
    bandeja.fecha fecha_solicitud,
    instancia_proceso.id_actividad_vigente,
    instancia_proceso.id_instancia_activa,
    bandeja.finalizado
FROM
    bandeja
    LEFT JOIN instancia_proceso ON ( bandeja.id_instancia_proceso = instancia_proceso.id )
GROUP BY (bandeja.id_instancia_proceso , 
bandeja.fecha , bandeja.fecha , instancia_proceso.id_proceso ,
instancia_proceso.id_actividad_vigente , instancia_proceso.id_instancia_activa , bandeja.finalizado);