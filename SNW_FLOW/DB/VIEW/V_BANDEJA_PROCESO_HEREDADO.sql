
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_BANDEJA_PROCESO_HEREDADO" ("INSTANCIA_BASE", "ID_INSTANCIA_PROCESO", "ID_GRUPO", "FECHA_SOLICITUD", "ID_PROCESO", "ID_BANDEJA", "ID_ACTIVIDAD_VIGENTE") AS 
  select 
instancia_base,
id_instancia_proceso,
id_grupo,
fecha_solicitud,
id_proceso,
id_bandeja,
id_actividad_vigente
from (select * from v_instancia_activa where instancia_base <> instancia_activa) vt_proceso_tronco_activo
inner join v_bandeja on (vt_proceso_tronco_activo.instancia_activa=v_bandeja.id_instancia_proceso);