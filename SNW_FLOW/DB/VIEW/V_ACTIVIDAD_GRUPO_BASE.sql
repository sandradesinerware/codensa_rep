
  CREATE OR REPLACE FORCE VIEW "SNW_FLOW"."V_ACTIVIDAD_GRUPO_BASE" ("ID", "INICIO", "FIN", "ES_INICIAL", "ID_PROCESO", "NOMBRE", "APLICACION", "PAGINAS", "GRUPO", "NOMBRE_PUBLICO", "PRIVILEGIO", "NOMBRE_GRUPO") AS 
  SELECT actividad."ID",actividad."INICIO",actividad."FIN",actividad."ES_INICIAL",actividad."ID_PROCESO",actividad."NOMBRE",actividad."APLICACION",actividad."PAGINAS",actividad."GRUPO",actividad."NOMBRE_PUBLICO",actividad."PRIVILEGIO",
  grupo.nombre nombre_grupo
FROM actividad
LEFT JOIN snw_auth.grupo
ON (actividad.grupo    =snw_auth.grupo.id)
WHERE actividad.grupo IS NOT NULL
UNION ALL
SELECT actividad."ID",actividad."INICIO",actividad."FIN",actividad."ES_INICIAL",actividad."ID_PROCESO",actividad."NOMBRE",actividad."APLICACION",actividad."PAGINAS",grupo_privilegios.GRUPO GRUPO,actividad."NOMBRE_PUBLICO",actividad."PRIVILEGIO",
  grupo.nombre nombre_grupo
FROM actividad
LEFT JOIN snw_auth.grupo_privilegios
ON (actividad.privilegio         =snw_auth.grupo_privilegios.FUNCION)
left join snw_auth.grupo on (snw_auth.grupo.id=snw_auth.grupo_privilegios.GRUPO)
WHERE actividad.privilegio IS NOT NULL;