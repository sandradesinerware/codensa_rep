
  CREATE OR REPLACE PACKAGE "SNW_FLOW"."REGLAS" 
AS
TYPE RESULTADO_VALIDACION
IS
  RECORD
  (
    ID      NUMBER,
    ESTADO  NUMBER,
    MENSAJE VARCHAR2(2000));
TYPE RESULTADOS_VALIDACIONES
IS
  TABLE OF RESULTADO_VALIDACION;
  FUNCTION validar(
      i_id_validacion IN validacion.id%type,
      i_parametro     IN parametro)
    RETURN RESULTADO_VALIDACION ;
  FUNCTION validar_transicion(
      i_id_transicion IN NUMBER,
      i_parametros    IN parametros)
    RETURN RESULTADOS_VALIDACIONES PIPELINED;
  FUNCTION nombre_validacion(
      i_id_validacion IN validacion.id%type)
    RETURN VARCHAR2;
  FUNCTION nombre_validacion(
      i_id_validacion   IN validacion.id%type,
      i_estado_esperado IN NUMBER)
    RETURN VARCHAR2 ;
  FUNCTION recomendacion_validacion(
      i_id_validacion IN validacion.id%type)
    RETURN VARCHAR2 ;
END;

/
CREATE OR REPLACE PACKAGE BODY "SNW_FLOW"."REGLAS" 
IS
  --------------------------------------------------------------------------------
  FUNCTION validar(
      i_id_validacion IN validacion.id%type,
      i_parametro     IN parametro)
    RETURN RESULTADO_VALIDACION
  AS
    o_resultado RESULTADO_VALIDACION;
    l_validacion VALIDACION%rowtype;
    l_paquete validacion.paquete%type;
  BEGIN
    o_resultado.id:=i_id_validacion;
    SELECT * INTO l_validacion FROM validacion WHERE id=i_id_validacion;
    IF(l_validacion.paquete IS NULL) THEN
      l_paquete             :='';
    ELSE
      l_paquete:=l_validacion.paquete||'.';
    END IF;
    BEGIN
      EXECUTE immediate 'DECLARE val boolean; BEGIN val := '||l_paquete|| l_validacion.funcion||'(:1, :2, :3); IF(val) THEN :VALOR:=1; ELSE :VALOR:=0;END IF;END;' USING IN i_parametro.numero,
      i_parametro.fecha,
      i_parametro.texto,
      OUT o_resultado.estado ;
    EXCEPTION
    WHEN OTHERS THEN
      o_resultado.mensaje:=SQLERRM||'';
      o_resultado.estado :=0;
    END;
    RETURN o_resultado;
  EXCEPTION
  WHEN no_data_found THEN
    o_resultado.mensaje:='Validacion NO EXISTE!';
    o_resultado.estado :=0;
    RETURN o_resultado;
  END validar;
--------------------------------------------------------------------------------
  FUNCTION validar_transicion(
      i_id_transicion IN NUMBER,
      i_parametros    IN parametros)
    RETURN RESULTADOS_VALIDACIONES PIPELINED
  AS
    l_conteo_condiciones NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_conteo_condiciones
    FROM transicion
    LEFT JOIN condicion
    ON (transicion.id=condicion.id_transicion)
    RIGHT JOIN validacion
    ON (condicion.id_validacion=validacion.id)
    WHERE transicion.id        =i_id_transicion;
    IF (l_conteo_condiciones   =0) THEN
      return;
    END IF;
    FOR validacion IN
    (SELECT validacion.*,
      condicion.salida_esperada
    FROM transicion
    LEFT JOIN condicion
    ON (transicion.id=condicion.id_transicion)
    RIGHT JOIN validacion
    ON (condicion.id_validacion=validacion.id)
    WHERE transicion.id        =i_id_transicion
    )
    LOOP
      DECLARE
        l_resultado_validacion RESULTADO_VALIDACION;
        l_parametro parametro;
      BEGIN
        BEGIN
          SELECT parametro(p.fecha,p.numero, p.texto, p.codigo)
          INTO l_parametro
          FROM TABLE(i_parametros) p
          WHERE p.codigo=validacion.codigo_param;
        EXCEPTION
        WHEN no_data_found THEN
          l_resultado_validacion.mensaje:='No se ha encontrado el parametro: '|| validacion.codigo_param;
          l_resultado_validacion.estado :=0;
          l_resultado_validacion.id     :=validacion.id;
          pipe row(l_resultado_validacion);
        WHEN too_many_rows THEN
          l_resultado_validacion.mensaje:='Se han encontrado varias coincidencias para el parametro: '||validacion.codigo_param;
          l_resultado_validacion.estado :=0;
          l_resultado_validacion.id     :=validacion.id;
          pipe row(l_resultado_validacion);
        END;
        IF (l_parametro                  IS NOT NULL) THEN
          l_resultado_validacion         :=validar(validacion.id, l_parametro);
          IF(l_resultado_validacion.estado=validacion.salida_esperada) THEN
            l_resultado_validacion.estado:=1;
          ELSE
            l_resultado_validacion.estado:=0;
          END IF;
          pipe row (l_resultado_validacion);
        END IF;
      EXCEPTION
      WHEN no_data_needed THEN
        RETURN;
      END;
    END LOOP;
    RETURN;
  END validar_transicion;
--------------------------------------------------------------------------------
  FUNCTION nombre_validacion(
      i_id_validacion IN validacion.id%type)
    RETURN VARCHAR2
  AS
    o_nombre validacion.nombre%type;
  BEGIN
    SELECT NVL(nombre, id) INTO o_nombre FROM validacion WHERE id=i_id_validacion;
    RETURN o_nombre;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN '';
  WHEN OTHERS THEN
    RETURN '';
  END nombre_validacion;
--------------------------------------------------------------------------------
  FUNCTION nombre_validacion(
      i_id_validacion   IN validacion.id%type,
      i_estado_esperado IN NUMBER)
    RETURN VARCHAR2
  AS
    o_nombre validacion.nombre%type;
  BEGIN
    SELECT DECODE(i_estado_esperado,1,NVL(nombre, nombre),0,NVL(nombre_caso_negativo,nombre))
    INTO o_nombre
    FROM validacion
    WHERE id=i_id_validacion;
    RETURN o_nombre;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN '';
  WHEN OTHERS THEN
    RETURN '';
  END nombre_validacion;
--------------------------------------------------------------------------------
  FUNCTION recomendacion_validacion(
      i_id_validacion IN validacion.id%type)
    RETURN VARCHAR2
  AS
    o_recomendacion validacion.recomendacion%type;
  BEGIN
    SELECT recomendacion
    INTO o_recomendacion
    FROM validacion
    WHERE id=i_id_validacion;
    RETURN o_recomendacion;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
  WHEN OTHERS THEN
    RETURN NULL;
  END recomendacion_validacion;
--------------------------------------------------------------------------------
END "REGLAS";

/