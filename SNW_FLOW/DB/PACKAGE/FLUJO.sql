
  CREATE OR REPLACE PACKAGE "SNW_FLOW"."FLUJO" AS
    PROCEDURE actualizar_actividad (
        i_instancia_proceso    IN NUMBER,
        i_actividad            IN NUMBER,
        i_actualizar_act_vig   IN VARCHAR2 DEFAULT 'S'
    );

    PROCEDURE finalizar_como (
        i_instancia_proceso   IN NUMBER,
        i_estado_final        IN NUMBER,
        i_actividad           IN NUMBER,
        i_observacion         IN VARCHAR2
    );

    PROCEDURE forzar_actividad (
        i_instancia_proceso   IN NUMBER,
        i_actividad           IN NUMBER,
        i_reg_historia        IN VARCHAR2 DEFAULT 'S'
    );

    FUNCTION iniciar_proceso (
        i_id_proceso           IN proceso.id%TYPE,
        i_id_instancia_padre   IN proceso.id%TYPE DEFAULT NULL,
        i_heredado             IN VARCHAR2 DEFAULT 'N'
    ) RETURN instancia_proceso.id%TYPE;

    PROCEDURE eliminar_proceso (
        i_id_instancia_proceso IN NUMBER
    );

    PROCEDURE retroceder_proceso (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE
    );

    FUNCTION calcular_rama (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_app_id                 IN VARCHAR2,
        i_session                IN VARCHAR2,
        i_request                IN VARCHAR2,
        i_debug                  IN VARCHAR2,
        i_items                  IN VARCHAR2,
        i_item_values            IN VARCHAR2,
        i_clear_cache            IN VARCHAR2 DEFAULT 'N',
        i_usuario                IN VARCHAR2,
        i_bandeja                IN NUMBER
    ) RETURN VARCHAR2;

    FUNCTION nombre_actividad (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN VARCHAR2;

    PROCEDURE validar_estado_proceso (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_parametros             IN parametros,
        i_transicion             IN transicion.id%TYPE,
        i_es_admin               IN BOOLEAN
    );

    PROCEDURE validar_estado_proceso_pagina (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_parametros             IN parametros,
        i_pag_final              IN VARCHAR2,
        i_app_id                 IN VARCHAR2,
        i_usuario                IN VARCHAR2
    );

    FUNCTION agrupacion_pendiente (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN BOOLEAN;

    FUNCTION evaluar_transicion (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_parametros             IN parametros,
        i_transicion             IN transicion.id%TYPE
    ) RETURN BOOLEAN;

    PROCEDURE avanzar_proceso (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_parametros             IN parametros,
        i_pag_final              IN VARCHAR2,
        i_app_id                 IN VARCHAR2,
        io_msg IN OUT VARCHAR2
    );

    PROCEDURE registrar_observacion (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_observacion            instancia_proceso.observacion%TYPE
    );

    FUNCTION cargar_observacion (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN VARCHAR2;

    PROCEDURE inyectar_actividad (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_id_tarea_siguiente     IN actividad.id%TYPE
    );

    FUNCTION determinar_instancia_activa (
        i_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN NUMBER;

    FUNCTION determinar_instancia_raiz (
        i_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_nivel               IN NUMBER DEFAULT NULL
    ) RETURN NUMBER;

    FUNCTION iniciar_subproceso (
        i_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_proceso             IN proceso.id%TYPE
    ) RETURN NUMBER;

    PROCEDURE heredar_proceso (
        i_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_estado_final        IN instancia_proceso.id_estado_final%TYPE,
        i_obligatoriedad      IN VARCHAR2 DEFAULT 'S'
    );

    PROCEDURE heredar_proceso2 (
        i_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_estado_final        IN instancia_proceso.id_estado_final%TYPE,
        i_proceso_hijo        IN instancia_proceso.id_proceso%TYPE
    );

    PROCEDURE finalizar_proceso (
        i_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_estado_final        IN instancia_proceso.id_estado_final%TYPE
    );

    FUNCTION proceso_finalizado (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN VARCHAR2;

    FUNCTION proceso_finalizado (
        i_id_instancia_proceso   IN instancia_proceso.id%TYPE,
        i_id_estado_final        IN instancia_proceso.id_estado_final%TYPE
    ) RETURN VARCHAR2;

    FUNCTION cargar_paginas (
        i_actividad IN NUMBER
    ) RETURN t_paginas
        PIPELINED;

    FUNCTION validar_permiso_actividad (
        i_pagina              IN NUMBER,
        i_usuario             IN VARCHAR2,
        i_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN BOOLEAN;

    FUNCTION validar_permiso_actividad (
        i_pagina              IN NUMBER,
        i_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN BOOLEAN;

    FUNCTION validar_permiso_actividad2 (
        i_pagina              IN NUMBER,
        i_usuario             IN VARCHAR2,
        i_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN BOOLEAN;

    FUNCTION grupo_actividad_actual_proceso (
        i_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN NUMBER;

    FUNCTION priv_actividad_actual_proceso (
        i_instancia_proceso   IN instancia_proceso.id%TYPE
    ) RETURN NUMBER;

    FUNCTION es_actividad_actual (
        i_id_instancia_proceso   IN NUMBER,
        i_actividad              IN NUMBER
    ) RETURN VARCHAR2;

    PROCEDURE ejecutar_tareas (
        i_transicion   IN transicion.id%TYPE,
        i_parametros   IN parametros
    );

    FUNCTION ver_estado_final (
        i_id_instancia_proceso IN NUMBER
    ) RETURN VARCHAR2;

    FUNCTION cargar_estado_actual (
        i_id_instancia_proceso IN NUMBER
    ) RETURN NUMBER;

    FUNCTION ver_estado_actual (
        i_id_instancia_proceso IN NUMBER
    ) RETURN VARCHAR2;

    FUNCTION cargar_proceso (
        i_id_instancia_proceso IN NUMBER
    ) RETURN NUMBER;

    FUNCTION ver_proceso (
        i_id_instancia_proceso IN NUMBER
    ) RETURN VARCHAR2;

    FUNCTION indentificar_instancia (
        i_instancia_proceso      IN NUMBER,
        i_instancia_subproceso   IN NUMBER,
        i_pagina                 IN NUMBER
    ) RETURN NUMBER;

    FUNCTION fecha_actividad_actual (
        i_instancia_proceso      IN NUMBER,
        i_instancia_subproceso   IN NUMBER,
        i_pagina                 IN NUMBER
    ) RETURN DATE;

    FUNCTION fecha_actividad_actual (
        i_instancia_proceso IN NUMBER
    ) RETURN DATE;

    PROCEDURE cambiar_subproceso_padre (
        l_id_subproceso_ant   IN NUMBER,
        l_id_subproceso_nue   IN NUMBER
    );

END;

/
CREATE OR REPLACE PACKAGE BODY "SNW_FLOW"."FLUJO" 
IS
--------------------------------------------------------------------------------
procedure actualizar_actividad(I_INSTANCIA_PROCESO IN NUMBER, I_ACTIVIDAD IN NUMBER, i_actualizar_act_vig in varchar2 default 'S')
as
begin
delete bandeja where id_instancia_proceso = I_INSTANCIA_PROCESO;
for l_grupo in (select grupo from v_actividad_grupo where id=I_ACTIVIDAD and grupo is not null)
    loop
        insert into bandeja (id_instancia_proceso, id_grupo, finalizado) values (I_INSTANCIA_PROCESO, l_grupo.grupo, decode(proceso_finalizado(I_INSTANCIA_PROCESO),'TRUE',1,0));
    end loop;
    if(i_actualizar_act_vig='S') then
        update instancia_proceso set id_actividad_vigente=I_ACTIVIDAD where id=I_INSTANCIA_PROCESO;
    end if;
end actualizar_actividad;

--------------------------------------------------------------------------------

  PROCEDURE forzar_actividad(I_INSTANCIA_PROCESO IN NUMBER, I_ACTIVIDAD IN NUMBER, i_reg_historia in varchar2 default 'S')
  as
  begin
  if(I_INSTANCIA_PROCESO is null or I_ACTIVIDAD is null) then
    raise_application_error(-20000,'Parametros nulos!.');
  end if;
  if(i_reg_historia='S') then
      INSERT
        INTO HISTORIA_PROCESO
          (
            ID_INSTANCIA_PROCESO,
            ID_ACTIVIDAD,
            FECHA
          )
          VALUES
          (
            I_INSTANCIA_PROCESO,
            I_ACTIVIDAD,
            SYSDATE
          );
  end if;
  actualizar_actividad(I_INSTANCIA_PROCESO,I_ACTIVIDAD);
      for l_proceso_padre in (
        select * from (SELECT level nivel,
                        id_proceso,
                        id,
                        ID_INSTANCIA_PROCESO_PADRE,
                        heredado
                        FROM instancia_proceso
                        START WITH ID = I_INSTANCIA_PROCESO
                        CONNECT BY prior ID_INSTANCIA_PROCESO_PADRE=id
                        order by nivel) 
        where nivel>1)
        loop
            if(flujo.determinar_instancia_activa(l_proceso_padre.id) = I_INSTANCIA_PROCESO) then
                actualizar_actividad(l_proceso_padre.id,I_ACTIVIDAD,i_actualizar_act_vig =>'N');
                update instancia_proceso set id_instancia_activa = I_INSTANCIA_PROCESO where id = l_proceso_padre.id;
                else
                    exit;
            end if;
        end loop;
  end forzar_actividad;
--------------------------------------------------------------------------------
  FUNCTION iniciar_proceso(
      i_id_proceso IN proceso.id%type,
      i_id_instancia_padre in proceso.id%type default null,
      i_heredado in varchar2 default 'N' )
    RETURN instancia_proceso.id%type
  AS
    o_id_instancia_proceso instancia_proceso.id%type;
    l_id_actividad_inicial actividad.id%type;
  BEGIN

  SELECT MAX(id)
    INTO l_id_actividad_inicial
    FROM actividad
    WHERE id_proceso=i_id_proceso
    AND es_inicial  ='S';

    INSERT
    INTO INSTANCIA_PROCESO
      (
        id_proceso,
        FECHA_INICIO,
        id_instancia_proceso_padre,
        heredado
      )
      VALUES
      (
        i_id_proceso,
        sysdate,
        i_id_instancia_padre,
        i_heredado
      )
    RETURNING id
    INTO o_id_instancia_proceso ;
    update instancia_proceso set id_instancia_activa=o_id_instancia_proceso where id=o_id_instancia_proceso;
    forzar_actividad(o_id_instancia_proceso,l_id_actividad_inicial);
    RETURN o_id_instancia_proceso;
  EXCEPTION
  WHEN no_data_found THEN
    raise_application_error(-20000,'No se ha parametrizado la actividad inicial para el proceso: '||i_id_proceso);
  END iniciar_proceso;
--------------------------------------------------------------------------------
  PROCEDURE eliminar_proceso
    (
      i_id_instancia_proceso IN NUMBER
    )
  IS
    l_conteo_hijos       NUMBER;
    l_conteo_actividades NUMBER;
  BEGIN
    --Verificar que no tenga procesos hijos
    SELECT COUNT(*)
    INTO l_conteo_hijos
    FROM instancia_proceso
    WHERE id_instancia_proceso_padre=i_id_instancia_proceso;

    --Eliminar proceso.
    IF(l_conteo_hijos=0) THEN
      DELETE historia_proceso WHERE id_instancia_proceso = i_id_instancia_proceso;
      DELETE bandeja WHERE  id_instancia_proceso = i_id_instancia_proceso;
      DELETE instancia_proceso WHERE id=i_id_instancia_proceso;
    ELSE
      raise_application_error(-20002,'No se puede eliminar un proceso con subprocesos activos.');
    END IF;
    for proceso_predecesor in (select id, determinar_instancia_activa(id) instancia_activa from instancia_proceso where id_instancia_activa=i_id_instancia_proceso)
    loop
        update instancia_proceso set id_instancia_activa=proceso_predecesor.instancia_activa where id=proceso_predecesor.id;
    end loop;
  END eliminar_proceso;
--------------------------------------------------------------------------------
PROCEDURE retroceder_proceso ( i_id_instancia_proceso   IN instancia_proceso.id%TYPE ) AS
    l_conteo_estados   NUMBER;
    l_ultima_tarea     historia_proceso.id%TYPE;
    l_penultima_actividad actividad.id%type;
    l_mismo_proceso    BOOLEAN;
BEGIN
    SELECT COUNT(*) INTO l_conteo_estados
    FROM historia_proceso
    WHERE id_instancia_proceso = i_id_instancia_proceso;
    IF
        ( l_conteo_estados > 1 )
    THEN
        select id into l_ultima_tarea 
            from (select id, id_actividad, fecha, max(fecha) over (partition by id_instancia_proceso) max_fecha 
                    from historia_proceso where ID_INSTANCIA_PROCESO=i_id_instancia_proceso) 
            where fecha=max_fecha;
        l_penultima_actividad := actividad_pck.actividad_anterior(i_id_instancia_proceso);
        l_mismo_proceso := flujo.determinar_instancia_activa(i_id_instancia_proceso) = i_id_instancia_proceso;
        IF ( l_mismo_proceso ) THEN
            DELETE historia_proceso WHERE id = l_ultima_tarea;
            forzar_actividad(i_id_instancia_proceso, l_penultima_actividad, 'N');
        ELSE
            raise_application_error(-20000,'Un proceso nuevo ha iniciado a partir del anterior por lo tanto no es posible volver a una actividad pasada');
        END IF;
    END IF;
    if(proceso_finalizado(i_id_instancia_proceso)='TRUE') then
                UPDATE instancia_proceso
                SET fecha_fin = NULL,
                    id_estado_final = NULL
            WHERE id = i_id_instancia_proceso;
    end if;

END retroceder_proceso;
--------------------------------------------------------------------------------
  FUNCTION subproceso_activo(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_id_proceso           IN instancia_proceso.id_proceso%type)
    RETURN BOOLEAN
  IS
    l_conteo NUMBER;
  BEGIN
    FOR l_instancia IN
    (SELECT          *
    FROM instancia_proceso
    WHERE ID_INSTANCIA_PROCESO_PADRE = i_id_instancia_proceso
    AND id_proceso                   = i_id_proceso
    )
    LOOP
      IF(flujo.proceso_finalizado(l_instancia.id)='FALSE') THEN
        RETURN true;
      elsif(agrupacion_pendiente(l_instancia.id)) THEN
        RETURN true;
      END IF;
    END LOOP;
    RETURN false;
  END subproceso_activo;
--------------------------------------------------------------------------------
  FUNCTION agrupacion_pendiente(
      i_id_instancia_proceso IN instancia_proceso.id%type)
    RETURN BOOLEAN
  IS
    l_conteo_agrupacion NUMBER;
    l_estado_actual     NUMBER;
  BEGIN
    l_estado_actual:= flujo.cargar_estado_actual(i_id_instancia_proceso);
    FOR l_conexion IN
    (SELECT conexion.id_proceso_hijo,
      instancia_proceso.id id_instancia
    FROM instancia_proceso
    LEFT JOIN conexion
    ON (instancia_proceso.ID_PROCESO = conexion.id_proceso_padre)
    WHERE instancia_proceso.id       = i_id_instancia_proceso
    AND conexion.tipo                ='A'
    AND conexion.id_estado           =l_estado_actual
    AND conexion.obligatorio         ='S'
    )
    LOOP
      IF(subproceso_activo(i_id_instancia_proceso, l_conexion.id_proceso_hijo)) THEN
        RETURN true;
      END IF;
    END LOOP;
    RETURN false;
  END agrupacion_pendiente;
--------------------------------------------------------------------------------
  FUNCTION evaluar_transicion(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_transicion           IN transicion.id%type)
    RETURN BOOLEAN
  IS
    l_actividad_actual actividad.id%type;
    l_transicion transicion.id%type;
    l_conteo_condicion NUMBER;
    l_cumple_condicion BOOLEAN:=false;
    l_id_tarea_siguiente transicion.id_actividad_siguiente%type;
    l_id_estado_final estado.id%type;
  BEGIN
    SELECT COUNT(*)
    INTO l_conteo_condicion
    FROM transicion
    LEFT JOIN condicion
    ON (transicion.id=condicion.id_transicion)
    RIGHT JOIN validacion
    ON (condicion.id_validacion=validacion.id)
    WHERE transicion.id        =i_transicion
    AND condicion.id          IS NOT NULL;
    SELECT id_actividad_siguiente,
      id_estado_final
    INTO l_id_tarea_siguiente,
      l_id_estado_final
    FROM transicion
    WHERE id=i_transicion;
    DECLARE
      l_conteo NUMBER:=0;
    BEGIN
      SELECT COUNT(*)
      INTO l_conteo
      FROM TABLE( REGLAS.VALIDAR_TRANSICION(i_transicion , i_parametros)) p
      WHERE p.estado      =1;
      l_cumple_condicion := (l_conteo = l_conteo_condicion);
    END;
    RETURN l_cumple_condicion;
  END evaluar_transicion;
--------------------------------------------------------------------------------
  PROCEDURE ejecutar_transicion(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_transicion           IN transicion.id%type)
  IS
    l_id_tarea_siguiente transicion.id_actividad_siguiente%type;
    l_id_estado_final estado.id%type;
  BEGIN
    SELECT id_actividad_siguiente,
      id_estado_final
    INTO l_id_tarea_siguiente,
      l_id_estado_final
    FROM transicion
    WHERE id=i_transicion;
    forzar_actividad(i_id_instancia_proceso,l_id_tarea_siguiente);
    flujo.ejecutar_tareas(i_transicion,i_parametros);
    IF(l_id_estado_final IS NOT NULL) THEN
      flujo.finalizar_proceso(i_id_instancia_proceso,l_id_estado_final);
    END IF;
  END ejecutar_transicion;
--------------------------------------------------------------------------------
  FUNCTION calcular_rama
    (
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_app_id               IN VARCHAR2,
      i_session              IN VARCHAR2,
      i_request              IN VARCHAR2,
      i_debug                IN VARCHAR2,
      i_items                IN VARCHAR2,
      i_item_values          IN VARCHAR2,
      i_clear_cache          IN VARCHAR2 DEFAULT 'N',
      i_usuario              IN VARCHAR2,
      i_bandeja              IN NUMBER
    )
    RETURN VARCHAR2
  IS
    l_actividad_actual actividad.id%type;
    o_target VARCHAR2(500);
    l_permiso_ejecucion boolean;
    l_instancia_actual number;
  BEGIN
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(i_id_instancia_proceso);
    l_instancia_actual := flujo.determinar_instancia_activa(i_id_instancia_proceso);
    l_permiso_ejecucion := actividad_pck.validar_ejecucion(    l_instancia_actual,    i_usuario,    i_app_id,    null);
    if l_permiso_ejecucion then
        if i_bandeja is null then
            SELECT 'f?p='
              ||NVL(aplicacion,i_app_id)
              ||':'
              ||inicio
              ||':'
              ||i_session
              ||':'
              ||i_request
              ||':'
              || i_debug
              ||':'
              ||DECODE(i_clear_cache,'S',inicio,'')
              ||':'
              ||i_items
              ||':'
              ||i_item_values
            INTO o_target
            FROM actividad
            WHERE id=l_actividad_actual;
        else
            SELECT 'f?p='
              ||NVL(aplicacion,i_app_id)
              ||':'
              ||i_bandeja
              ||':'
              ||i_session
              ||':'
              ||i_request
              ||':'
              || i_debug
              ||':'
              ||DECODE(i_clear_cache,'N',i_bandeja,'')
              ||':'
              ||i_items
              ||':'
              ||i_item_values
            INTO o_target
            FROM actividad
            WHERE id=l_actividad_actual;
        end if;
    else
    SELECT 'f?p='
          ||NVL(aplicacion,i_app_id)
          ||':'
          ||i_bandeja
          ||':'
          ||i_session
          ||':'
          ||i_request
          ||':'
          || i_debug
          ||':'
          ||DECODE(i_clear_cache,'N',i_bandeja,'')
          ||':'
          ||i_items
          ||':'
          ||i_item_values
        INTO o_target
        FROM actividad
        WHERE id=l_actividad_actual;
    end if;
    RETURN APEX_UTIL.PREPARE_URL( p_url => o_target);
    exception
    when no_data_found then
    return '';
  END calcular_rama;
--------------------------------------------------------------------------------
  FUNCTION nombre_actividad(
      i_id_instancia_proceso IN instancia_proceso.id%type)
    RETURN VARCHAR2
  IS
    l_actividad_actual actividad.id%type;
    o_nombre_actividad actividad.nombre%type;
  BEGIN
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(i_id_instancia_proceso);
    SELECT nombre_publico
    INTO o_nombre_actividad
    FROM actividad
    WHERE id=l_actividad_actual;
    RETURN o_nombre_actividad;
  END nombre_actividad;
--------------------------------------------------------------------------------
  PROCEDURE mostrar_validacion(
      i_nombre     IN VARCHAR2,
      i_estado     IN NUMBER,
      i_id         IN NUMBER,
      i_info_error IN VARCHAR2 DEFAULT NULL)
  IS
    l_recomendacion validacion.recomendacion%type;
    l_estilo     VARCHAR2(200);
    l_icono      VARCHAR2(200);
    l_cumple     VARCHAR2(200);
    l_mensaje_ID VARCHAR2(200):='';
    l_info_error VARCHAR2(400);
  BEGIN
    IF(i_id          IS NOT NULL) THEN
      l_recomendacion:= reglas.recomendacion_validacion(i_id);
      l_mensaje_ID   := 'Validaci�n (ID '||i_id||')';
    END IF;
    IF(i_estado =1) THEN
      l_estilo := 'color:#4d9c2d';
      l_icono  := 'fa-check';
      l_cumple := 'Cumple';
    ELSE
      l_estilo    := 'color:#fb7703';
      l_icono     := 'fa-times';
      l_cumple    := 'No_cumple';
      l_info_error:= REPLACE(i_info_error,'ORA-20000','');
    END IF;
    htp.p('<li> <strong > <p value ="'||i_nombre||'"><b>'||i_nombre||'</b></p></strong>');
    htp.p('<span ><p id="id_resultado_validacion" name="'||l_cumple||'" >'||l_mensaje_ID||' <span style="'||l_estilo||'"><i class="fa '||l_icono||' fa-2x" aria-hidden="true"></i></span> </p> </span>');
    IF(l_recomendacion IS NOT NULL) THEN
      htp.p('<p>Recomendaci�n: <span class="" value ="" >'||l_recomendacion||'</span></p>');
    END IF;
    IF(l_info_error IS NOT NULL) THEN
      htp.p('<p>Informaci�n adicional <span class="" value ="" >'||l_info_error||'</span></p>');
    END IF;
    htp.p('</li>');
  END mostrar_validacion;
--------------------------------------------------------------------------------
  PROCEDURE validar_estado_proceso(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_transicion           IN transicion.id%type,
      i_es_admin             IN BOOLEAN)
  AS
    l_actividad_actual actividad.id%type;
    l_conteo_condicion NUMBER;
    l_mostrar_resumen  BOOLEAN := true;
  BEGIN
    IF(i_id_instancia_proceso IS NULL) THEN
      raise_application_error(-20000,'El proceso no ha sido instanciado!');
    END IF;
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(i_id_instancia_proceso);
    htp.p('<ul>');
    FOR res_val IN
    (SELECT      *
    FROM TABLE( REGLAS.VALIDAR_TRANSICION(i_transicion , i_parametros))
    ORDER BY 2 DESC
    )
    LOOP
      DECLARE
        l_nombre VARCHAR2(2000);
      BEGIN
        SELECT reglas.nombre_validacion(validacion.id,condicion.salida_esperada)
        INTO l_nombre
        FROM transicion
        LEFT JOIN condicion
        ON (transicion.id=condicion.id_transicion)
        RIGHT JOIN validacion
        ON (condicion.id_validacion                        =validacion.id)
        WHERE transicion.id                                =i_transicion
        AND ID_VALIDACION                                  =res_val.id;
        IF(i_es_admin OR (NOT i_es_admin AND res_val.estado=0) ) THEN
          l_mostrar_resumen                               := false;
          mostrar_validacion(l_nombre, res_val.estado, res_val.id, res_val.mensaje);
        END IF;
      END;
    END LOOP;
    IF(l_mostrar_resumen) THEN
      mostrar_validacion('Se cumplen todas las validaciones.', 1 , NULL);
    END IF;
    htp.p('</ul>');
  EXCEPTION
  WHEN OTHERS THEN
    htp.p(SQLERRM||' depuracion: '||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    --htp.p(SQLERRM);
  END validar_estado_proceso;
--------------------------------------------------------------------------------
  PROCEDURE validar_estado_proceso_pagina(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_pag_final            IN VARCHAR2,
      i_app_id               IN VARCHAR2,
      i_usuario              IN VARCHAR2 )
  IS
    l_nombre_actividad actividad.nombre%type;
    l_cumple_condicion       BOOLEAN:=false;
    l_es_admin               BOOLEAN:=false;
    l_constante_perfil_admin NUMBER :=101;
  BEGIN
    l_es_admin := snw_auth.autorizacion.verificar_grupo(l_constante_perfil_admin, sysdate, i_usuario);
    FOR l_actividad IN
    ( SELECT ID_ACTIVIDAD_SIGUIENTE AS id,
      transicion.ID                AS id_transicion
    FROM actividad
    LEFT JOIN transicion
    ON (transicion.id_actividad_inicial=actividad.id)
    WHERE actividad.aplicacion         = i_app_id and actividad.id=actividad_pck.actividad_actual(i_id_instancia_proceso)
    AND transicion.ID                 IS NOT NULL)
    LOOP
      SELECT nombre_publico
      INTO l_nombre_actividad
      FROM actividad
      WHERE id=l_actividad.id;
      htp.p('<div >');
      htp.p('<span id="Actividad" value="Siguiente actividad: '||l_nombre_actividad||'">      
<a> Siguiente actividad: '||l_nombre_actividad||'</a></span>');
      validar_estado_proceso(i_id_instancia_proceso, i_parametros, l_actividad.id_transicion, l_es_admin);
      htp.p('</div>');
    END LOOP;
  END validar_estado_proceso_pagina;
--------------------------------------------------------------------------------
  PROCEDURE avanzar_proceso(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_pag_final            IN VARCHAR2,
      i_app_id               IN VARCHAR2,
      io_msg                 IN OUT VARCHAR2)
  IS
    l_cumple               BOOLEAN :=false;
    l_agrupacion_pendiente BOOLEAN :=false ;
  BEGIN
    io_msg                 :='';
    l_agrupacion_pendiente := agrupacion_pendiente(i_id_instancia_proceso) ;
    FOR posible_transicion IN
    (SELECT                 *
    FROM
      (SELECT transicion.*,
        (SELECT COUNT(*) FROM condicion WHERE id_transicion=transicion.id
        ) conteo
      FROM actividad 
      LEFT JOIN transicion  ON (transicion.id_actividad_inicial=actividad.id)
      WHERE (actividad.fin               = i_pag_final 
      OR actividad.inicio                = i_pag_final )
      AND actividad.aplicacion           =i_app_id 
      and actividad.id_proceso=cargar_proceso2(i_id_instancia_proceso) 
      and actividad.id=actividad_pck.actividad_actual(i_id_instancia_proceso)
      and transicion.id is not null
      ) V_ACTIVIDADES
    ORDER BY conteo DESC )
    LOOP
      l_cumple:= evaluar_transicion(i_id_instancia_proceso, i_parametros, posible_transicion.ID);
      IF(l_cumple AND NOT l_agrupacion_pendiente ) THEN
        ejecutar_transicion(i_id_instancia_proceso, i_parametros, posible_transicion.ID);
        EXIT;
      END IF;
    END LOOP;
    IF (NOT l_cumple) THEN
      io_msg:='No se cumple al menos una transici�n de negocio para ir a la siguiente actividad, revise las validaciones y complete la informaci�n en la pesta�a correspondiente';
    elsif(l_agrupacion_pendiente) THEN
      io_msg:='A�n hay sub-procesos pendientes!';
    ELSE
      io_msg:='';
    END IF;
  END avanzar_proceso;
--------------------------------------------------------------------------------
  PROCEDURE registrar_observacion(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_observacion instancia_proceso.observacion%type)
  IS
  BEGIN
    UPDATE instancia_proceso
    SET OBSERVACION=i_observacion
    WHERE id     =i_id_instancia_proceso;
  END registrar_observacion;
--------------------------------------------------------------------------------
  FUNCTION cargar_observacion(
      i_id_instancia_proceso IN instancia_proceso.id%type)
    RETURN VARCHAR2
  IS
    o_observacion instancia_proceso.observacion%type;
  BEGIN
    SELECT OBSERVACION
    INTO o_observacion
    FROM instancia_proceso
    WHERE id=i_id_instancia_proceso;
    RETURN o_observacion;
  END cargar_observacion;
--------------------------------------------------------------------------------
  PROCEDURE inyectar_actividad(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_id_tarea_siguiente   IN actividad.id%type)
  IS
    l_actividad_actual actividad.id%type;
    l_transicion transicion.id%type;
    l_estado_final NUMBER;
  BEGIN
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(i_id_instancia_proceso);
    BEGIN
      SELECT transicion.id,
        id_estado_final
      INTO l_transicion,
        l_estado_final
      FROM transicion
      WHERE NVL(id_actividad_inicial,l_actividad_actual)=l_actividad_actual
      AND id_actividad_siguiente                        =i_id_tarea_siguiente;
    EXCEPTION
    WHEN too_many_rows THEN
      raise_application_error(-20000,'Hay m�s de una transicion para la actividad '|| l_actividad_actual||' y '||i_id_tarea_siguiente||'. [id_instancia_proceso: '|| i_id_instancia_proceso||']' );
    WHEN no_data_found THEN
      raise_application_error(-20000,'No se ha parametrizado la transicion para las actividades ' ||l_actividad_actual||' y '||i_id_tarea_siguiente||'. [id_instancia_proceso: '|| i_id_instancia_proceso||']' );
    END;
    forzar_actividad(i_id_instancia_proceso,i_id_tarea_siguiente);
    IF(l_estado_final IS NOT NULL) THEN
      flujo.finalizar_proceso(i_id_instancia_proceso,l_estado_final);
    END IF;
  END inyectar_actividad;
--------------------------------------------------------------------------------
  FUNCTION determinar_instancia_activa
    (
      i_instancia_proceso IN instancia_proceso.id%type
    )
    RETURN NUMBER AS 
    l_instancia_tronco NUMBER;
    l_instancia_padre instancia_proceso%rowtype;
    l_conteo NUMBER:=0;
    l_anterior NUMBER;
   BEGIN
    if(i_instancia_proceso is null) then
    return null;
  end if;
  if(flujo.proceso_finalizado(i_instancia_proceso)='FALSE') then
    return i_instancia_proceso;
  end if;
    FOR instancia IN
( SELECT level nivel,
          id_proceso,
          id,
          ID_INSTANCIA_PROCESO_PADRE,
          heredado
        FROM instancia_proceso
        WHERE level=1 or heredado='S'
          START WITH ID      =i_instancia_proceso
          CONNECT BY prior id=ID_INSTANCIA_PROCESO_PADRE
          ORDER BY level
    )
    LOOP
       IF (instancia.nivel=1)
        THEN
        l_anterior:= instancia.id_instancia_proceso_padre;
        l_instancia_tronco:= instancia.id;
      END IF;


      IF(instancia.nivel=2 AND instancia.heredado='S') THEN
        l_anterior := l_instancia_tronco;
        l_instancia_tronco:= instancia.id;
      END IF;

      IF(instancia.nivel>2) THEN
        l_anterior := l_instancia_tronco;
        l_instancia_tronco:= instancia.id;
        SELECT *
        INTO l_instancia_padre
        FROM instancia_proceso
        WHERE id                     =instancia.id_instancia_proceso_padre;
        IF not(l_instancia_padre.heredado='S' AND instancia.heredado='S' 
        and l_anterior=instancia.id_instancia_proceso_padre) THEN
            return l_anterior;
        END IF;
      END IF;

      IF ( flujo.proceso_finalizado(instancia.id)='FALSE' AND l_instancia_tronco = instancia.id ) THEN
        RETURN instancia.id;
      END IF;

    END LOOP;
    RETURN l_instancia_tronco;
  END determinar_instancia_activa;
--------------------------------------------------------------------------------
  FUNCTION determinar_instancia_raiz(
      i_instancia_proceso IN instancia_proceso.id%type,
      i_nivel             IN NUMBER DEFAULT NULL )
    RETURN NUMBER
  IS
  BEGIN
    FOR instancia IN
    (SELECT nivel,
      id_proceso,
      id,
      ID_INSTANCIA_PROCESO_PADRE,
      heredado
    FROM
      (SELECT level nivel,
        id_proceso,
        id,
        ID_INSTANCIA_PROCESO_PADRE,
        heredado
      FROM instancia_proceso
        START WITH ID                              =i_instancia_proceso
        CONNECT BY prior ID_INSTANCIA_PROCESO_PADRE=id
      ) procesos
    WHERE ID_INSTANCIA_PROCESO_PADRE IS NULL
    OR procesos.nivel                 =i_nivel
    )
    LOOP
      RETURN instancia.id;
    END LOOP;
    RETURN NULL;
  END determinar_instancia_raiz;
--------------------------------------------------------------------------------
  FUNCTION iniciar_subproceso(
      i_proceso         IN proceso.id%type,
      i_instancia_padre IN instancia_proceso.id%type,
      i_heredado        IN instancia_proceso.heredado%type )
    RETURN NUMBER
  IS
    o_nuevo_proceso NUMBER;
  BEGIN
    o_nuevo_proceso := flujo.iniciar_proceso(i_proceso,i_instancia_padre,i_heredado);
    RETURN o_nuevo_proceso;
  END iniciar_subproceso;
--------------------------------------------------------------------------------
  FUNCTION iniciar_subproceso(
      i_instancia_proceso IN instancia_proceso.id%type,
      i_proceso           IN proceso.id%type )
    RETURN NUMBER
  IS
    l_instancia_actual   NUMBER;
    l_estado_actual      NUMBER;
    l_conteo_subprocesos NUMBER;
  BEGIN
    l_instancia_actual    := determinar_instancia_activa(i_instancia_proceso);
    IF(l_instancia_actual IS NULL) THEN
      raise_application_error(-20010, 'No se puede iniciar subproceso, no se encuentra instancia de proceso activa. InstanciaBase:'||i_instancia_proceso||' Proceso:'||i_proceso);
    END IF;
    l_estado_actual    := cargar_estado_actual(l_instancia_actual);
    IF(l_estado_actual IS NULL) THEN
      raise_application_error(-20010, 'No se puede determinar el estado actual del proceso. InstanciaBase:'||i_instancia_proceso||' Proceso:'||i_proceso);
    END IF;
    SELECT COUNT(*)
    INTO l_conteo_subprocesos
    FROM instancia_proceso
    LEFT JOIN conexion
    ON (instancia_proceso.ID_PROCESO = conexion.id_proceso_padre)
    WHERE instancia_proceso.id       = l_instancia_actual
    AND conexion.tipo                ='E'
    AND (conexion.id_estado          =l_estado_actual)
    AND conexion.id_proceso_hijo     = i_proceso
    AND conexion.obligatorio         ='N';
    IF(l_conteo_subprocesos          >0) THEN
      RETURN iniciar_subproceso( i_proceso , l_instancia_actual, 'N' );
    ELSE
      raise_application_error(-20010,'No se puede instanciar un proceso que no ha sido parametrizado');
    END IF;
    RETURN NULL;
  END iniciar_subproceso;
--------------------------------------------------------------------------------
  PROCEDURE heredar_proceso(
      i_instancia_proceso IN instancia_proceso.id%type,
      i_estado_final      IN instancia_proceso.id_estado_final%type,
      i_obligatoriedad    IN VARCHAR2 DEFAULT 'S')
  IS
    l_conexion conexion%rowtype;
    l_nueva_instancia NUMBER;
  BEGIN
    SELECT conexion.*
    INTO l_conexion
    FROM instancia_proceso
    LEFT JOIN conexion
    ON (instancia_proceso.id_proceso=conexion.ID_PROCESO_PADRE)
    WHERE instancia_proceso.id      =i_instancia_proceso
    AND conexion.id_estado          =i_estado_final
    AND conexion.obligatorio        = i_obligatoriedad
    AND tipo                        = 'E';
    l_nueva_instancia              := iniciar_subproceso(l_conexion.id_proceso_hijo,i_instancia_proceso,'S');
  EXCEPTION
  WHEN no_data_found THEN
    NULL;
  WHEN too_many_rows THEN
    raise_application_error(-20010,'Error en la parametrizacion del proceso, solo un proceso puede heredar a otro. InstanciaProceso: '||i_instancia_proceso||' Estado: '||i_estado_final);
  END heredar_proceso;
--------------------------------------------------------------------------------
  PROCEDURE heredar_proceso2(
      i_instancia_proceso IN instancia_proceso.id%type,
      i_estado_final      IN instancia_proceso.id_estado_final%type,
      i_proceso_hijo      IN instancia_proceso.id_proceso%type)
  IS
    l_conexion conexion%rowtype;
    l_nueva_instancia NUMBER;
  BEGIN
    SELECT conexion.*
    INTO l_conexion
    FROM instancia_proceso
    LEFT JOIN conexion
    ON (instancia_proceso.id_proceso=conexion.ID_PROCESO_PADRE)
    WHERE instancia_proceso.id      =i_instancia_proceso
    AND conexion.id_proceso_hijo    = i_proceso_hijo
    AND conexion.id_estado          =i_estado_final
    AND conexion.obligatorio        = 'N'
    AND tipo                        = 'E';
    l_nueva_instancia              := iniciar_subproceso(l_conexion.id_proceso_hijo,i_instancia_proceso,'S');
  EXCEPTION
  WHEN no_data_found THEN
    NULL;
  WHEN too_many_rows THEN
    raise_application_error(-20010,'Error en la parametrizacion del proceso, solo un proceso puede heredar a otro. InstanciaProceso: '||i_instancia_proceso||' Estado: '||i_estado_final);
  END heredar_proceso2;
--------------------------------------------------------------------------------
  PROCEDURE finalizar_proceso(
      i_instancia_proceso IN instancia_proceso.id%type,
      i_estado_final      IN instancia_proceso.id_estado_final%type )
  IS
  BEGIN
    UPDATE instancia_proceso
    SET fecha_fin    =sysdate,
      id_estado_final=i_estado_final
    WHERE id         =i_instancia_proceso;
    heredar_proceso(i_instancia_proceso,i_estado_final);
  END finalizar_proceso;
--------------------------------------------------------------------------------
  FUNCTION proceso_finalizado(
      i_id_instancia_proceso IN instancia_proceso.id%type )
    RETURN VARCHAR2
  IS
    l_instancia_proceso instancia_proceso%rowtype;
  BEGIN
    IF(i_id_instancia_proceso IS NULL) THEN
      RETURN 'NULL';
    END IF;
    SELECT *
    INTO l_instancia_proceso
    FROM instancia_proceso
    WHERE id                          =i_id_instancia_proceso;
    IF(l_instancia_proceso.fecha_fin IS NULL OR l_instancia_proceso.id_estado_final IS NULL) THEN
      RETURN 'FALSE';
    END IF;
    RETURN 'TRUE';
  END proceso_finalizado;
--------------------------------------------------------------------------------
  FUNCTION proceso_finalizado(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_id_estado_final      IN instancia_proceso.id_estado_final%type)
    RETURN VARCHAR2
  IS
    l_instancia_proceso instancia_proceso%rowtype;
  BEGIN
    SELECT *
    INTO l_instancia_proceso
    FROM instancia_proceso
    WHERE id                          =i_id_instancia_proceso;
    IF(l_instancia_proceso.fecha_fin IS NOT NULL AND l_instancia_proceso.id_estado_final=i_id_estado_final) THEN
      RETURN 'TRUE';
    END IF;
    RETURN 'FALSE';
  END proceso_finalizado;
--------------------------------------------------------------------------------
  FUNCTION cargar_paginas(
      i_actividad IN NUMBER)
    RETURN t_paginas pipelined
  AS
    l_paginas APEX_APPLICATION_GLOBAL.VC_ARR2;
    l_pag_str actividad.paginas%type;
  BEGIN
    SELECT
      CASE
        WHEN paginas IS NULL
        THEN ''
        ELSE paginas
          ||':'
      END
      ||inicio
      ||':'
      ||fin
    INTO l_pag_str
    FROM actividad
    WHERE id   =i_actividad;
    l_paginas := APEX_UTIL.STRING_TO_TABLE(l_pag_str);
    FOR i IN 1..l_paginas.count
    LOOP
      pipe row(l_paginas(i));
    END LOOP;
    RETURN ;
  EXCEPTION
  WHEN no_data_needed THEN
    RETURN;
  WHEN no_data_found THEN
    RETURN;
  END cargar_paginas;
--------------------------------------------------------------------------------
  FUNCTION validar_permiso_actividad(
      i_pagina            IN NUMBER,
      i_usuario           IN VARCHAR2,
      i_instancia_proceso IN instancia_proceso.id%type)
    RETURN BOOLEAN
  AS
  BEGIN
    IF(i_instancia_proceso IS NULL) THEN
      RETURN false;
    END IF;
    FOR l_actividad IN
    (SELECT actividad.id
    FROM actividad
    LEFT JOIN snw_auth.usu_grupo
    ON (actividad.GRUPO=usu_grupo.GRUPO)
    LEFT JOIN snw_auth.USUARIO
    ON (usuario.id=usu_grupo.USUARIO)
    WHERE upper(usuario) LIKE upper(i_usuario)
    AND usu_grupo.grupo        IS NOT NULL
    AND actividad.id IN (ACTIVIDAD_PCK.actividad_actual(i_instancia_proceso))
    )
    LOOP
      DECLARE
        l_conteo NUMBER;
      BEGIN
        SELECT COUNT(*)
        INTO l_conteo
        FROM TABLE(cargar_paginas(l_actividad.id))
        WHERE COLUMN_VALUE = i_pagina ;
        IF(l_conteo        >0) THEN
          RETURN true;
        END IF;
      END;
    END LOOP;
    RETURN false;
  END validar_permiso_actividad;
------------------------------------------------------------------------------
  FUNCTION validar_permiso_actividad(
      i_pagina            IN NUMBER,
      i_instancia_proceso IN instancia_proceso.id%type)
    RETURN BOOLEAN
  AS
  BEGIN
    IF(i_instancia_proceso IS NULL) THEN
      RETURN false;
    END IF;
    FOR l_actividad IN
    (SELECT ACTIVIDAD_PCK.actividad_actual(i_instancia_proceso) id FROM dual
    )
    LOOP
      DECLARE
        l_conteo NUMBER;
      BEGIN
        SELECT COUNT(*)
        INTO l_conteo
        FROM TABLE(cargar_paginas(l_actividad.id))
        WHERE COLUMN_VALUE = i_pagina ;
        IF(l_conteo        >0) THEN
          RETURN true;
        END IF;
      END;
    END LOOP;
    RETURN false;
  END validar_permiso_actividad;
--------------------------------------------------------------------------------
  FUNCTION validar_permiso_actividad2(
      i_pagina            IN NUMBER,
      i_usuario           IN VARCHAR2,
      i_instancia_proceso IN instancia_proceso.id%type)
    RETURN BOOLEAN
  AS
  BEGIN
    IF(i_instancia_proceso IS NULL) THEN
      RETURN false;
    END IF;
    FOR l_actividad IN
    (SELECT actividad.id
    FROM actividad
    LEFT JOIN snw_auth.grupo_privilegios
    ON (snw_auth.grupo_privilegios.funcion=actividad.privilegio)
    LEFT JOIN snw_auth.usu_grupo
    ON (snw_auth.grupo_privilegios.GRUPO=usu_grupo.GRUPO)
    LEFT JOIN snw_auth.USUARIO
    ON (usuario.id=usu_grupo.USUARIO)
    WHERE upper(user_name) LIKE upper(i_usuario)
    AND actividad.privilegio IS NOT NULL
    AND actividad.id         IN (ACTIVIDAD_PCK.actividad_actual(i_instancia_proceso))
    )
    LOOP
      DECLARE
        l_conteo NUMBER;
      BEGIN
        SELECT COUNT(*)
        INTO l_conteo
        FROM TABLE(cargar_paginas(l_actividad.id))
        WHERE COLUMN_VALUE = i_pagina ;
        IF(l_conteo        >0) THEN
          RETURN true;
        END IF;
      END;
    END LOOP;
    RETURN false;
  END validar_permiso_actividad2;
--------------------------------------------------------------------------------
  FUNCTION grupo_actividad_actual_proceso(
      i_instancia_proceso IN instancia_proceso.id%type)
    RETURN NUMBER
  AS
    o_grupo            NUMBER;
    l_actividad_actual NUMBER;
  BEGIN
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(i_instancia_proceso);
    SELECT actividad.grupo
    INTO o_grupo
    FROM actividad
    WHERE id=l_actividad_actual;
    RETURN o_grupo;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN -1;
  END grupo_actividad_actual_proceso;
--------------------------------------------------------------------------------
  FUNCTION priv_actividad_actual_proceso(
      i_instancia_proceso IN instancia_proceso.id%type)
    RETURN NUMBER
  AS
    o_privilegio       NUMBER;
    l_actividad_actual NUMBER;
  BEGIN
    l_actividad_actual:= ACTIVIDAD_PCK.actividad_actual(i_instancia_proceso);
    SELECT actividad.privilegio
    INTO o_privilegio
    FROM actividad
    WHERE id = l_actividad_actual;
    RETURN o_privilegio;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN -1;
  END priv_actividad_actual_proceso;
--------------------------------------------------------------------------------
  FUNCTION es_actividad_actual(
      i_id_instancia_proceso IN NUMBER,
      i_actividad            IN NUMBER)
    RETURN VARCHAR2
  IS
    l_conteo           NUMBER;
    l_actividad_actual NUMBER;
  BEGIN
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(i_id_instancia_proceso);
    IF( i_actividad     = l_actividad_actual) THEN
      RETURN 'TRUE';
    ELSE
      RETURN 'FALSE';
    END IF;
  END es_actividad_actual;
--------------------------------------------------------------------------------
  PROCEDURE ejecutar_tarea(
      i_paquete       IN tarea_automatica.paquete%type,
      i_procedimiento IN tarea_automatica.procedimiento%type,
      i_parametro     IN parametro)
  AS
    l_paquete tarea_automatica.paquete%type;
  BEGIN
    IF(i_paquete IS NULL) THEN
      l_paquete  :='';
    ELSE
      l_paquete:=i_paquete||'.';
    END IF;
    BEGIN
      EXECUTE immediate 'BEGIN '||l_paquete||i_procedimiento||'(:1, :2, :3); END;' USING IN i_parametro.numero,
      i_parametro.fecha,
      i_parametro.texto ;
    END;
  END ejecutar_tarea;
--------------------------------------------------------------------------------
  PROCEDURE ejecutar_tareas(
      i_transicion IN transicion.id%type,
      i_parametros IN parametros)
  IS
  BEGIN
    FOR tarea_auto IN
    (SELECT PAQUETE,
      PROCEDIMIENTO,
      parametro(p.fecha,p.numero, p.texto, p.codigo) o_parametro
    FROM tarea_transicion
    LEFT JOIN TAREA_AUTOMATICA
    ON (TAREA_AUTOMATICA.id=tarea_transicion.id_tarea)
    LEFT JOIN TABLE(i_parametros) p
    ON (p.codigo        =TAREA_AUTOMATICA.codigo_param)
    WHERE id_transicion =i_transicion
    )
    LOOP
      ejecutar_tarea(tarea_auto.paquete, tarea_auto.procedimiento, tarea_auto.o_parametro);
    END LOOP;
  END ejecutar_tareas;
--------------------------------------------------------------------------------
  FUNCTION ver_estado_final(
      i_id_instancia_proceso IN NUMBER)
    RETURN VARCHAR2
  IS
    o_estado ESTADO.NOMBRE%type;
  BEGIN
    SELECT nombre
    INTO o_estado
    FROM estado
    WHERE id IN
      (SELECT id_estado_final
      FROM instancia_proceso
      WHERE id=i_id_instancia_proceso
      );
    RETURN o_estado;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN'';
  END ver_estado_final;
--------------------------------------------------------------------------------
  FUNCTION cargar_estado_actual(
      i_id_instancia_proceso IN NUMBER)
    RETURN NUMBER
  IS
    o_estado ESTADO.ID%type;
  BEGIN
    IF(flujo.proceso_finalizado(i_id_instancia_proceso)='TRUE') THEN
      SELECT id_estado_final
      INTO o_estado
      FROM instancia_proceso
      WHERE id=i_id_instancia_proceso;
      RETURN o_estado;
    END IF;
    --TODO: Cambiar manera de determinar ultima actividad
    SELECT estado.id
        into o_estado
    FROM instancia_proceso
    LEFT JOIN estado_intermedio
    ON (estado_intermedio.id_actividad=actividad_pck.actividad_actual(i_id_instancia_proceso)
    AND instancia_proceso.id_proceso =estado_intermedio.id_proceso)
    LEFT JOIN estado
    ON (estado.id             =estado_intermedio.ID_ESTADO)
    where instancia_proceso.id=i_id_instancia_proceso;
    RETURN o_estado;
  EXCEPTION
  WHEN OTHERS THEN
     RETURN NULL;
  END cargar_estado_actual;
--------------------------------------------------------------------------------
  FUNCTION ver_estado_actual(
      i_id_instancia_proceso IN NUMBER)
    RETURN VARCHAR2
  IS
    o_estado ESTADO.NOMBRE%type;
    l_estado NUMBER;
  BEGIN
    l_estado := cargar_estado_actual(i_id_instancia_proceso);
    SELECT nombre INTO o_estado FROM estado WHERE id=l_estado;
    RETURN o_estado;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN'';
  END ver_estado_actual;
--------------------------------------------------------------------------------
  FUNCTION cargar_proceso(
      i_id_instancia_proceso IN NUMBER)
    RETURN NUMBER
  IS
  BEGIN
  return cargar_proceso2(i_id_instancia_proceso);
  END cargar_proceso;
--------------------------------------------------------------------------------
    function ver_proceso(i_id_instancia_proceso IN NUMBER) return varchar2
    is
    l_proceso NUMBER;
    o_proceso proceso.nombre%type;
    begin
        l_proceso := cargar_proceso2(i_id_instancia_proceso);
        select nombre into o_proceso from proceso where id=l_proceso;
        return o_proceso;
    exception
        when no_data_found then
            return null;
    end ver_proceso;
--------------------------------------------------------------------------------
  FUNCTION indentificar_instancia(
      I_INSTANCIA_PROCESO    IN NUMBER ,
      I_INSTANCIA_SUBPROCESO IN NUMBER ,
      i_pagina               IN NUMBER)
    RETURN NUMBER
  AS
    l_instancia_actual NUMBER;
    l_actividad_actual NUMBER;
    l_tiene_permiso    BOOLEAN;
  BEGIN
    IF(I_INSTANCIA_PROCESO IS NULL AND I_INSTANCIA_SUBPROCESO IS NULL) THEN
      RETURN NULL;
    elsif(I_INSTANCIA_SUBPROCESO IS NOT NULL AND snw_flow.flujo.validar_permiso_actividad(i_pagina,I_INSTANCIA_SUBPROCESO) ) THEN
      l_instancia_actual         :=I_INSTANCIA_SUBPROCESO;
    elsif(I_INSTANCIA_PROCESO    IS NOT NULL AND snw_flow.flujo.validar_permiso_actividad(i_pagina,I_INSTANCIA_PROCESO)) THEN
      l_instancia_actual         :=I_INSTANCIA_PROCESO;
    END IF;
    RETURN l_instancia_actual;
  END indentificar_instancia;

--------------------------------------------------------------------------------
  FUNCTION fecha_actividad_actual(
      I_INSTANCIA_PROCESO    IN NUMBER ,
      I_INSTANCIA_SUBPROCESO IN NUMBER ,
      i_pagina               IN NUMBER)
    RETURN DATE
  IS
    l_instancia_actual NUMBER;
    l_actividad_actual NUMBER;
    o_fecha            DATE;
  BEGIN
    l_instancia_actual := indentificar_instancia(I_INSTANCIA_PROCESO,I_INSTANCIA_SUBPROCESO,i_pagina);
    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(l_instancia_actual);
    o_fecha            := ACTIVIDAD_PCK.fecha_actividad(l_instancia_actual,l_actividad_actual);
    RETURN o_fecha;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
  END fecha_actividad_actual;
--------------------------------------------------------------------------------
  FUNCTION fecha_actividad_actual(
      I_INSTANCIA_PROCESO    IN NUMBER )
    RETURN DATE
  IS
    l_actividad_actual NUMBER;
    o_fecha            DATE;
  BEGIN

    l_actividad_actual := ACTIVIDAD_PCK.actividad_actual(I_INSTANCIA_PROCESO);
    o_fecha            := ACTIVIDAD_PCK.fecha_actividad(I_INSTANCIA_PROCESO,l_actividad_actual);
    RETURN o_fecha;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
  END fecha_actividad_actual;
--------------------------------------------------------------------------------
  PROCEDURE FINALIZAR_COMO(
        I_INSTANCIA_PROCESO IN NUMBER,
        I_ESTADO_FINAL IN NUMBER,
        I_ACTIVIDAD IN NUMBER,
        I_OBSERVACION IN VARCHAR2)
  as
  l_instancia_proceso instancia_proceso%rowtype;
  l_conteo NUMBER;
  begin
  select * into l_instancia_proceso from instancia_proceso where id=I_INSTANCIA_PROCESO;

  select count(*) into l_conteo from proceso 
  left join actividad on (proceso.id=actividad.id_proceso) 
  left join estado on (estado.id_proceso=proceso.id) 
  where proceso.id=l_instancia_proceso.id_proceso and estado.id=I_ESTADO_FINAL and actividad.id=I_ACTIVIDAD;

  if( l_conteo = 0 ) then
    raise_application_error(-20000,'Parametros de finalizaci�n del proceso incongruentes!');
  end if;

    forzar_actividad(I_INSTANCIA_PROCESO,I_ACTIVIDAD);
    flujo.finalizar_proceso(I_INSTANCIA_PROCESO,I_ESTADO_FINAL);
    if(I_OBSERVACION is not null) then
        registrar_observacion(I_INSTANCIA_PROCESO,I_OBSERVACION);
    end if;
  end FINALIZAR_COMO;

--------------------------------------------------------------------------------
    PROCEDURE cambiar_subproceso_padre(
        L_ID_SUBPROCESO_ANT IN NUMBER ,
        L_ID_SUBPROCESO_NUE IN NUMBER ) 
    AS
    L_EXISTE_SUB_ANT NUMBER;
    L_EXISTE_SUB_NUE NUMBER;
    BEGIN

    SELECT COUNT(*) INTO L_EXISTE_SUB_ANT 
    FROM INSTANCIA_PROCESO 
    WHERE ID=L_ID_SUBPROCESO_ANT;

    SELECT COUNT(*) INTO L_EXISTE_SUB_NUE 
    FROM INSTANCIA_PROCESO 
    WHERE ID=L_ID_SUBPROCESO_NUE;

    IF (L_EXISTE_SUB_NUE=1 AND L_EXISTE_SUB_ANT=1) THEN

    FOR INSTANCIAS_AFECTADAS IN (SELECT ID FROM INSTANCIA_PROCESO 
                                WHERE ID_INSTANCIA_PROCESO_PADRE=L_ID_SUBPROCESO_ANT ORDER BY ID )
    LOOP
    UPDATE INSTANCIA_PROCESO SET ID_INSTANCIA_PROCESO_PADRE=L_ID_SUBPROCESO_NUE
    WHERE ID=INSTANCIAS_AFECTADAS.ID;
    DBMS_OUTPUT.PUT_LINE('INSTANCIA ID = '||INSTANCIAS_AFECTADAS.ID ||
    ' ---- INSTANCIA ANTERIOR-INSTANCIA NUEVA=  '||L_ID_SUBPROCESO_ANT||' - '||L_ID_SUBPROCESO_NUE);
    END LOOP;
    END IF;
    END cambiar_subproceso_padre;

END "FLUJO";

/