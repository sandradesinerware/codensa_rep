
  CREATE OR REPLACE PACKAGE "SNW_FLOW"."ACTIVIDAD_PCK" 
AS
  FUNCTION actividad_actual(
      i_instancia_proceso IN NUMBER)
    RETURN NUMBER;
  --------------------------------------------------------------------------------
  FUNCTION actividad_anterior(
      i_instancia_proceso IN NUMBER)
    RETURN NUMBER;
  --------------------------------------------------------------------------------
  FUNCTION actividad_siguiente(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros)
    RETURN NUMBER;
  --------------------------------------------------------------------------------
  FUNCTION validar_ejecucion(
      I_USUARIO   IN VARCHAR2,
      i_actividad IN NUMBER,
      i_nivel_acceso in NUMBER default null)
    RETURN BOOLEAN;
--------------------------------------------------------------------------------
  function validar_ejecucion(
      i_instancia_proceso    IN NUMBER,
      i_usuario              IN VARCHAR2,
      i_aplicacion           IN VARCHAR2,
      i_nivel_acceso         IN NUMBER default null)
      return boolean;
  --------------------------------------------------------------------------------
  FUNCTION validar_aplicacion(
      I_APP       IN VARCHAR2,
      i_actividad IN NUMBER)
    RETURN BOOLEAN;
  -------------------------------------------------------------------------------
  FUNCTION fecha_actividad(
      i_instancia_proceso IN NUMBER,
      i_actividad         IN NUMBER)
    RETURN DATE;
  -------------------------------------------------------------------------------
    FUNCTION nombre_grupo_actividad(
      i_id_actividad in number)
    return varchar2;
  -------------------------------------------------------------------------------
END ACTIVIDAD_PCK;

/
CREATE OR REPLACE PACKAGE BODY "SNW_FLOW"."ACTIVIDAD_PCK" 
AS
  FUNCTION actividad_actual(
      i_instancia_proceso IN NUMBER)
    RETURN NUMBER
  IS
    o_actividad NUMBER;
  BEGIN

  select id_actividad_vigente 
  into o_actividad 
  from instancia_proceso where id=i_instancia_proceso;
  if(o_actividad is not null) then
    return o_actividad;
  end if;
    select id_actividad into o_actividad 
            from (select id, id_actividad, fecha, max(fecha) over (partition by id_instancia_proceso) max_fecha 
                    from historia_proceso where ID_INSTANCIA_PROCESO=i_instancia_proceso) 
            where fecha=max_fecha;
    RETURN o_actividad;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
  END actividad_actual;
--------------------------------------------------------------------------------
  FUNCTION actividad_anterior(
      i_instancia_proceso IN NUMBER)
    RETURN NUMBER
  IS
    o_actividad        NUMBER;
    l_actividad_actual NUMBER;
  BEGIN
    l_actividad_actual := actividad_actual(i_instancia_proceso);      
       select id_actividad into o_actividad 
            from (select id, id_actividad, fecha, max(fecha) over (partition by id_instancia_proceso) max_fecha 
                    from historia_proceso where ID_INSTANCIA_PROCESO=i_instancia_proceso and id_actividad <> l_actividad_actual) 
            where fecha=max_fecha;
    RETURN o_actividad;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
  END actividad_anterior;
--------------------------------------------------------------------------------
  FUNCTION actividad_siguiente(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros)
    RETURN NUMBER
  IS
    l_cumple               BOOLEAN :=false;
    l_agrupacion_pendiente BOOLEAN :=false ;
    l_actividad_actual NUMBER;
    l_proceso NUMBER;
  BEGIN
    --l_agrupacion_pendiente := flujo.agrupacion_pendiente(i_id_instancia_proceso) ;
    l_proceso:= cargar_proceso2(i_id_instancia_proceso);
    l_actividad_actual:= actividad_actual(i_id_instancia_proceso);
    FOR posible_transicion IN
    (select transicion.*,(SELECT COUNT(*) FROM condicion WHERE id_transicion=transicion.id) conteo
        from actividad left join transicion on (transicion.ID_ACTIVIDAD_INICIAL=actividad.id) 
        where actividad.id=nvl(l_actividad_actual,-1) and actividad.id_proceso=nvl(l_proceso,-1)
        and transicion.id is not null
        ORDER BY conteo DESC  )
    LOOP
      l_cumple:= flujo.evaluar_transicion(i_id_instancia_proceso, i_parametros, posible_transicion.ID);
      IF(l_cumple) THEN
        RETURN posible_transicion.id_actividad_siguiente;
      END IF;
    END LOOP;
    RETURN NULL;
  END actividad_siguiente;
--------------------------------------------------------------------------------
  FUNCTION validar_ejecucion(
      I_USUARIO   IN VARCHAR2,
      i_actividad IN NUMBER,
      i_nivel_acceso in NUMBER default null)
    RETURN BOOLEAN
  AS
    l_agente_actual   NUMBER;
    l_conteo_permisos NUMBER;
  BEGIN
    if(I_USUARIO is null or i_actividad is null) then
        return false;
    end if;

    if(i_nivel_acceso is null) then
        l_agente_actual := snw_auth.obtener_usuario(I_USUARIO);
        SELECT COUNT(*)
        INTO l_conteo_permisos
        FROM v_permiso_actividad
        WHERE id_usuario        =l_agente_actual
        AND id                  = i_actividad;
        RETURN l_conteo_permisos>0;
    end if;
    return false;
  END validar_ejecucion;

--------------------------------------------------------------------------------
  function validar_ejecucion(
      i_instancia_proceso    IN NUMBER,
      i_usuario              IN VARCHAR2,
      i_aplicacion           IN VARCHAR2,
      i_nivel_acceso         IN NUMBER default null)
      return boolean
  IS
    l_actividad_actual    NUMBER;
    l_pagina_correcta boolean;
    l_proceso_vivo boolean;
  BEGIN
  if(i_instancia_proceso is null or i_aplicacion is null) then
    return false;
  end if;
    l_actividad_actual    := ACTIVIDAD_PCK.actividad_actual(i_instancia_proceso);
    l_pagina_correcta := ACTIVIDAD_PCK.VALIDAR_APLICACION(i_aplicacion ,l_actividad_actual);
    l_proceso_vivo := flujo.proceso_finalizado(i_instancia_proceso)='FALSE';
    IF(l_pagina_correcta and l_proceso_vivo) THEN
      return  actividad_pck.validar_ejecucion(i_usuario,l_actividad_actual, i_nivel_acceso);
    END IF;
    return false;
  END validar_ejecucion;
--------------------------------------------------------------------------------

  FUNCTION validar_aplicacion(
      I_APP   IN VARCHAR2,
      i_actividad IN NUMBER)
    RETURN BOOLEAN
  AS
  l_conteo NUMBER;
  BEGIN
  select count(*) into l_conteo from actividad where id=i_actividad and aplicacion like I_APP;
  return l_conteo>0;
  END validar_aplicacion;
--------------------------------------------------------------------------------
FUNCTION fecha_actividad(
    i_instancia_proceso IN NUMBER,
    i_actividad         IN NUMBER)
  RETURN DATE
AS
  o_fecha DATE;
BEGIN
  SELECT fecha
  INTO o_fecha
  FROM historia_proceso
  RIGHT JOIN
    (select id 
            from (select id, id_actividad, fecha, max(fecha) over (partition by id_instancia_proceso) max_fecha 
                    from historia_proceso where ID_INSTANCIA_PROCESO=i_instancia_proceso AND id_actividad = i_actividad) 
            where fecha=max_fecha
    ) v_historia
  ON (v_historia.id=historia_proceso.id); 
  RETURN o_fecha;
END fecha_actividad;
--------------------------------------------------------------------------------
FUNCTION nombre_grupo_actividad(i_id_actividad in number)
  return varchar2
as
  l_nombre varchar2(200);
  begin
  select nombre_grupo into l_nombre from v_actividad_grupo where id = i_id_actividad;
  return l_nombre;
end nombre_grupo_actividad;
END ACTIVIDAD_PCK;
/