
  CREATE OR REPLACE PACKAGE "SNW_FLOW"."VISUALIZACION_PCK" 
AS


      
  PROCEDURE render_nav_proceso(
      i_instancia_proceso    IN NUMBER,
      i_instancia_subproceso IN NUMBER,
      i_usuario              IN VARCHAR2,
      i_pagina               IN VARCHAR2,
      i_aplicacion           IN VARCHAR2,
      i_parametros           IN parametros,
      i_main_title           IN VARCHAR2,
      i_nivel_acceso         IN NUMBER default null);


END VISUALIZACION_PCK;

/
CREATE OR REPLACE PACKAGE BODY "SNW_FLOW"."VISUALIZACION_PCK" 
AS
  FUNCTION ver_grupos_actividad(
      i_actividad IN NUMBER)
    RETURN VARCHAR2
  IS
    o_grupos VARCHAR2(4000);
  BEGIN
    SELECT LISTAGG(nombre_grupo, ', ') WITHIN GROUP (
    ORDER BY nombre_grupo)
    INTO o_grupos
    FROM v_actividad_grupo
    WHERE id= i_actividad;
    RETURN o_grupos;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
  END ver_grupos_actividad;
--------------------------------------------------------------------------------
  FUNCTION nombre_actividad(
      i_actividad IN NUMBER)
    RETURN VARCHAR2
  IS
    o_actividad actividad.nombre_publico%type;
  BEGIN
    SELECT nombre_publico INTO o_actividad FROM actividad WHERE id=i_actividad;
    RETURN o_actividad;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
  END nombre_actividad;
--------------------------------------------------------------------------------
  PROCEDURE mostrar_validacion(
      i_nombre     IN VARCHAR2,
      i_estado     IN NUMBER,
      i_id         IN NUMBER,
      i_info_error IN VARCHAR2 DEFAULT NULL)
  IS
    l_recomendacion validacion.recomendacion%type;
    l_estilo     VARCHAR2(200);
    l_icono      VARCHAR2(200);
    l_cumple     VARCHAR2(200);
    l_mensaje_ID VARCHAR2(200):='';
    l_info_error VARCHAR2(4000);
  BEGIN
    IF(i_id          IS NOT NULL) THEN
      l_recomendacion:= reglas.recomendacion_validacion(i_id);
      l_mensaje_ID   := 'Validación (ID '||i_id||')';
    END IF;
    IF(i_estado =1) THEN
      l_estilo := 'color:#4d9c2d';
      l_icono  := 'fa-check';
      l_cumple := 'Cumple';
    ELSE
      l_estilo    := 'color:#fb7703';
      l_icono     := 'fa-times';
      l_cumple    := 'No_cumple';
      l_info_error:= REPLACE(i_info_error,'ORA-20000','');
    END IF;
    htp.p(' <strong > <p value ="'||i_nombre||'"><b>'||i_nombre||'</b></p></strong>');
    htp.p('<span ><p id="id_resultado_validacion" name="'||l_cumple||'" >'||'</p> </span>');
    IF(l_recomendacion IS NOT NULL) THEN
      htp.p('<p>Recomendación: <span class="" value ="" >'||l_recomendacion||'</span></p>');
    END IF;
    IF(l_info_error IS NOT NULL) THEN
      htp.p('<p>Información adicional <span class="" value ="" >'||l_info_error||'</span></p>');
    END IF;
  END mostrar_validacion;
--------------------------------------------------------------------------------

  PROCEDURE validar_estado_proceso(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_transicion           IN transicion.id%type,
      i_es_admin             IN BOOLEAN)
  AS
    l_actividad_actual actividad.id%type;
    l_conteo_condicion NUMBER;
    l_mostrar_resumen  BOOLEAN := true;
  BEGIN
    IF(i_id_instancia_proceso IS NULL) THEN
      raise_application_error(-20000,'El proceso no ha sido instanciado!');
    END IF;
    l_actividad_actual:= actividad_pck.actividad_actual(i_id_instancia_proceso);
    htp.p('<ul>');
    FOR res_val IN
    (SELECT      *
    FROM TABLE( REGLAS.VALIDAR_TRANSICION(i_transicion , i_parametros))
    ORDER BY 2 DESC
    )
    LOOP
      DECLARE
        l_nombre VARCHAR2(2000);
      BEGIN
        SELECT reglas.nombre_validacion(validacion.id,condicion.salida_esperada)
        INTO l_nombre
        FROM transicion
        LEFT JOIN condicion
        ON (transicion.id=condicion.id_transicion)
        RIGHT JOIN validacion
        ON (condicion.id_validacion                        =validacion.id)
        WHERE transicion.id                                =i_transicion
        AND ID_VALIDACION                                  =res_val.id;
        IF(i_es_admin OR (NOT i_es_admin AND res_val.estado=0) ) THEN
          l_mostrar_resumen                               := false;
          mostrar_validacion(l_nombre, res_val.estado, res_val.id, res_val.mensaje);
        END IF;
      END;
    END LOOP;
    IF(l_mostrar_resumen) THEN
      mostrar_validacion('Se cumplen todas las validaciones.', 1 , NULL);
    END IF;
    htp.p('</ul>');
  EXCEPTION
  WHEN OTHERS THEN
    htp.p(SQLERRM||' depuracion: '||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
  END validar_estado_proceso;
--------------------------------------------------------------------------------
PROCEDURE validar_estado_proceso_pagina(
      i_id_instancia_proceso IN instancia_proceso.id%type,
      i_parametros           IN parametros,
      i_pag_final            IN VARCHAR2,
      i_app_id               IN VARCHAR2,
      i_usuario              IN VARCHAR2 )
  IS
    l_nombre_actividad actividad.nombre%type;
    l_cumple_condicion       BOOLEAN:=false;
    l_es_admin               BOOLEAN:=false;
    l_constante_perfil_admin NUMBER :=101;
  BEGIN
    l_es_admin := snw_auth.autorizacion.verificar_grupo(l_constante_perfil_admin, sysdate, i_usuario);
    FOR l_actividad IN
    (SELECT ID_ACTIVIDAD_SIGUIENTE AS id,
      transicion.ID                AS id_transicion
    FROM actividad
    LEFT JOIN transicion
    ON (transicion.id_actividad_inicial=actividad.id)
    WHERE actividad.aplicacion         = i_app_id and actividad.id=actividad_pck.actividad_actual(i_id_instancia_proceso)
    AND transicion.ID                 IS NOT NULL    )
    LOOP
      SELECT nombre_publico
      INTO l_nombre_actividad
      FROM actividad
      WHERE id=l_actividad.id;
      htp.p('<div >');
      htp.p('<span id="Actividad" value="Siguiente actividad: '||l_nombre_actividad||'">      
<a> Siguiente actividad: '||l_nombre_actividad||'</a></span>');
      validar_estado_proceso(i_id_instancia_proceso, i_parametros, l_actividad.id_transicion, l_es_admin);
      htp.p('</div>');
    END LOOP;
  END validar_estado_proceso_pagina;
--------------------------------------------------------------------------------
  PROCEDURE region_izquierda(
      i_actividad IN NUMBER,
      i_ejecuta_actual in boolean,
      i_usuario   IN VARCHAR2)
  IS
    l_actividad     VARCHAR2(4000);
    l_roles         VARCHAR2(4000);
    l_ejecuta_ambas BOOLEAN := false;
    l_boton         VARCHAR2(4000):= '';
  BEGIN
    IF(i_actividad   IS NOT NULL) THEN
      l_actividad    := nombre_actividad(i_actividad);
      l_roles        := 'Rol: '||ver_grupos_actividad(i_actividad);
      l_ejecuta_ambas:= actividad_pck.validar_ejecucion(i_usuario,i_actividad) and i_ejecuta_actual;
    END IF;
    IF(l_ejecuta_ambas) THEN
      l_boton := '<button onclick="apex.submit({request:''PREVIOUS'', showWait: true, ignoreChange: true, validate: false});" class="t-Button " type="button" id="P_BOTON_PREVIOUS">          
      <span class="t-Button-label">&lt;&lt; '||l_actividad||'</span>        
      </button>';
    END IF;
    htp.p('<div style="float:left;width:30%;text-align:left;">    
    <div class="t-Region-buttons-left">    
    <table>        
    <tr><td>'||'</td></tr>'||     
    '</table>
    </div>    
    </div>');
  END region_izquierda;
--------------------------------------------------------------------------------
  PROCEDURE region_centro(
      i_actividad IN NUMBER,
      i_titulo    IN VARCHAR2,
      i_usuario   IN VARCHAR2,
      i_ejecuta_actual boolean)
  IS
    l_actividad VARCHAR2(4000);
    l_roles     VARCHAR2(4000);
    l_ejecuta_actual boolean := i_ejecuta_actual;
    l_ejecuta boolean;
    l_proceso_nulo boolean;
    l_mensaje varchar2(4000) := '';
  BEGIN
    IF(i_actividad IS NOT NULL) THEN
      l_actividad  := nombre_actividad(i_actividad);
      l_roles      := 'Rol: '||ver_grupos_actividad(i_actividad);
      l_ejecuta := actividad_pck.validar_ejecucion(i_usuario,i_actividad);
      else
      l_proceso_nulo:=true;
    END IF;
        if (l_proceso_nulo) then
            l_mensaje := '';
        elsif(not l_ejecuta) then
            l_mensaje := '<tr><td><p><b>Estimado usuario, usted no tiene permiso<b></p></td></tr>';
        elsif(not l_ejecuta_actual) then
            l_mensaje := '<tr><td><p><b>Estimado usuario, el proceso ha terminado<b></p></td></tr>';
        end if;
    htp.p('<div style=" float:center; margin:0 auto;width:100%;text-align:center;">    
    <table align="center">    
    <tr><td><div
    ">      
    <p>'||i_titulo||'</p></div>    
    </td></tr>'||
    '</table>        
    </div>');
  END region_centro;
--------------------------------------------------------------------------------
  PROCEDURE region_derecha(
      i_actividad  IN NUMBER,
      i_parametros IN parametros,
      i_usuario    IN VARCHAR2,
      i_instancia in number,
      i_aplicacion in varchar2,
      i_pagina in varchar2,
      i_ejecuta_actual in boolean)
  IS
    l_actividad VARCHAR2(4000);
    l_roles     VARCHAR2(4000);
    l_label varchar2(4000);
    l_ejecuta_siguente boolean:= false;
    l_ejecuta_actual boolean := i_ejecuta_actual;
    l_boton varchar2(4000) := '';
  BEGIN
    IF(i_actividad IS NOT NULL) THEN
      l_actividad  := nombre_actividad(i_actividad);
      l_roles      := 'Rol: '||ver_grupos_actividad(i_actividad);
      l_ejecuta_siguente := actividad_pck.validar_ejecucion(i_usuario,i_actividad);

      if(l_ejecuta_siguente) then
        l_label := 'Actividad siguiente';
      else
        l_label := 'Enviar';
      end if;

      if(l_ejecuta_actual) then
      l_boton := '<button onclick="apex.submit({request:''NEXT'',showWait: true, ignoreChange: true, validate: false});" class="t-Button t-Button--hot " type="button" id="P_BOTON_NEXT">        
      <span class="t-Button-label">'||l_actividad||' &gt;&gt;</span>        
      </button>';
      else
        l_boton := '';
      end if;

    htp.p('<div style="float:right;width:30%;text-align:right;"><div class="t-Region-buttons-right">    
    <table>      
    <tr><td>'||l_boton||'</td></tr>'|| 
    '</table>   
    </div>   
    </div>');
    elsif(i_ejecuta_actual) then
    l_boton := '<button onclick="openModal(''validaciones'')" class="t-Button t-Button--danger " type="button" id="P_VALIDACIONES">
      <span class="t-Icon fa fa-info-circle" aria-hidden="true"></span>
      <span class="t-Button-label">Validaciones</span></button>';
    htp.p('<div style="float:right;width:30%;text-align:right;"><div class="t-Region-buttons-right">    
    <table>      
    <tr><td>'||l_boton||'</td></tr>        
    <tr><td>');
    htp.p('<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog--inline ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-describedby="validaciones" style="display: none; position: fixed;" aria-labelledby="ui-id-1">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">Validaciones</span>
		<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" aria-disabled="false" title="Cerrar">
			<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Cerrar</span>
		</button>
	</div>
	<div id="validaciones" class="t-DialogRegion js-modal js-draggable js-resizable js-dialog-autoheight js-dialog-size900x400 js-regionDialog ui-dialog-content ui-widget-content" style="">
		<div class="t-DialogRegion-wrap">
		<div class="t-DialogRegion-bodyWrapperOut">
			<div class="t-DialogRegion-bodyWrapperIn">
				<div class="t-DialogRegion-body">
          <table summary="" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
            <tr><td align="right">
              <button class="t-Button t-Button--noLabel t-Button--icon t-Button--noUI  t-Button--hot" onclick="closeModal(''validaciones'')" type="button" id="P_BTN_CLOSE" title="Cerrar" aria-label="Cerrar">
                <span class="t-Icon fa fa-window-close fa-2x" aria-hidden="true"></span>
              </button>
              </td></tr>
						</tbody>
					</table>');
        validar_estado_proceso_pagina(i_instancia, i_parametros , i_pagina, i_aplicacion, i_usuario);
        htp.p('					
					<table summary="" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tbody>
            <tr><td align="right">
							<button class="t-Button t-Button--noLabel t-Button--icon t-Button--noUI  t-Button--hot" onclick="closeModal(''validaciones'')" type="button" id="P_BTN_CLOSE_1" title="Cerrar" aria-label="Cerrar">
                <span class="t-Icon fa fa-window-close fa-2x" aria-hidden="true"></span>
              </button>
            </td></tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="t-DialogRegion-buttons">
			<div class="t-ButtonRegion t-ButtonRegion--dialogRegion">
				<div class="t-ButtonRegion-wrap">
					<div class="t-ButtonRegion-col t-ButtonRegion-col--left">
						<div class="t-ButtonRegion-buttons"></div>
					</div>
					<div class="t-ButtonRegion-col t-ButtonRegion-col--right">
						<div class="t-ButtonRegion-buttons"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90;"></div>
	<div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90;"></div>
</div>');

    htp.p('</p></td></tr>        
    <tr><td><p>'||l_roles||'</p></td></tr>     
    </table>   
    </div>   
    </div>');

    END IF;

  END region_derecha;
    -------------------------------------------------------
  PROCEDURE render_flujo(i_instancia_actual in number,i_actividad_actual in number) IS
    l_tablero varchar2(4000);
    l_inicial  number;
    l_id_proceso number;
    l_nombre varchar2(4000);
  BEGIN
      htp.p('<style>
                body {
                    font-family: "Alegreya Sans", sans-serif;
                    float: center;
                }
                .container {
                  width: 100%;
                  text-align: center;
                }
                .progressbar {
                  counter-reset: step;
                }
                .progressbar li {
                  list-style-type: none;
                  width: 25%;
                  float: left;
                  font-size: 12px;
                  position: relative;
                  text-align: center;
                  text-transform: uppercase;
                  color: #7d7d7d;
                }
                .progressbar li:before {
                  width: 30px;
                  height: 30px;
                  content: counter(step);
                  counter-increment: step;
                  line-height: 30px;
                  border: 2px solid #7d7d7d;
                  display: block;
                  text-align: center;
                  margin: 0 auto 10px auto;
                  border-radius: 50%;
                  background-color: white;
                }
                .progressbar li:after {
                  height: 2px;
                  content: '';
                  position: absolute;
                  background-color: #7d7d7d;
                  top: 15px;
                  left: -50%;
                  z-index: -1;
                }
                .progressbar li:first-child:after {
                  content: none;
                }
                .progressbar li.active {
                  color: green;
                }
                .progressbar li.active:before {
                  border-color: #55b776;
                }
                .progressbar li.active + li:after {
                  background-color: #7d7d7d;
                }
            </style>');
    htp.p('<div class="container">
                <ul class="progressbar">');
    select id_proceso into l_id_proceso from instancia_proceso where id = i_instancia_actual;
    select id into l_inicial from actividad where id_proceso= l_id_proceso and es_inicial = 'S';
    select nombre_publico into l_nombre from actividad where id = l_inicial;
    if i_actividad_actual = l_inicial then
        l_tablero := l_tablero||'<li class="active">'||l_nombre||'</li>';
    else
        l_tablero := l_tablero||'<li>'||l_nombre||'</li>';
    end if;
    for i in (select id,nombre_publico from actividad where id_proceso = l_id_proceso) loop
        begin
            if i.id <> l_inicial then
                select max(id_actividad_siguiente) into l_inicial from transicion where
                id_actividad_inicial = l_inicial and principal ='S';
                select nombre_publico into l_nombre from actividad where id = l_inicial;
                if i_actividad_actual = l_inicial then
                    l_tablero := l_tablero||'<li class="active">'||l_nombre||'</li>';
                else
                    l_tablero := l_tablero||'<li>'||l_nombre||'</li>';
                end if;
            end if;
            exception when others then
            null;
         end;
    end loop;
    l_tablero := l_tablero||'</ul></div>';      
    htp.p(l_tablero);
  END;
--------------------------------------------------------------------------------
  PROCEDURE render_nav_proceso(
      i_instancia_proceso    IN NUMBER,
      i_instancia_subproceso IN NUMBER,
      i_usuario              IN VARCHAR2,
      i_pagina               IN VARCHAR2,
      i_aplicacion           IN VARCHAR2,
      i_parametros           IN parametros,
      i_main_title           IN VARCHAR2,
      i_nivel_acceso         IN NUMBER default null)
  IS            
    l_instancia_actual    NUMBER;
    l_actividad_anterior  NUMBER;
    l_actividad_actual    NUMBER;
    l_actividad_siguiente NUMBER;
    l_permiso_ejecucion boolean;
  BEGIN
    l_instancia_actual := flujo.indentificar_instancia(i_instancia_proceso,i_instancia_subproceso,i_pagina);
    l_actividad_anterior := actividad_pck.actividad_anterior(l_instancia_actual);
    l_actividad_actual := actividad_pck.actividad_actual(l_instancia_actual);
    l_actividad_siguiente := actividad_pck.actividad_siguiente(l_instancia_actual,i_parametros);
    l_permiso_ejecucion := actividad_pck.validar_ejecucion(    l_instancia_actual,    i_usuario,    i_aplicacion,    i_nivel_acceso);
    htp.p('<div style="width:100%;">');
    render_flujo(l_instancia_actual,l_actividad_actual);    
    region_izquierda(l_actividad_anterior,l_permiso_ejecucion,i_usuario);
    region_derecha(    l_actividad_siguiente,    i_parametros,    i_usuario,    l_instancia_actual,    i_aplicacion,    i_pagina,    l_permiso_ejecucion);
    region_centro(    l_actividad_actual,    i_main_title,    i_usuario,    l_permiso_ejecucion);
    htp.p('</div>');

  END render_nav_proceso;
--------------------------------------------------------------------------------
END VISUALIZACION_PCK;

/