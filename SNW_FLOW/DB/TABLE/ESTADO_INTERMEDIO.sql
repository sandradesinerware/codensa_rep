
  CREATE TABLE "SNW_FLOW"."ESTADO_INTERMEDIO" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"ID_PROCESO" NUMBER NOT NULL ENABLE, 
	"ID_ACTIVIDAD" NUMBER NOT NULL ENABLE, 
	"ID_ESTADO" NUMBER NOT NULL ENABLE, 
	"AUD_CREADO_POR" VARCHAR2(400) DEFAULT 'HISTORICO' NOT NULL ENABLE, 
	"AUD_FECHA_CREACION" DATE DEFAULT SYSDATE NOT NULL ENABLE, 
	"AUD_FECHA_ACTUALIZACION" DATE DEFAULT SYSDATE NOT NULL ENABLE, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(400) DEFAULT 'HISTORICO' NOT NULL ENABLE, 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(100) DEFAULT 'IP_ADDRESS' NOT NULL ENABLE, 
	 CONSTRAINT "ESTADO_INTERMEDIO_UK1" UNIQUE ("ID_PROCESO", "ID_ACTIVIDAD") ENABLE, 
	 CONSTRAINT "ESTADO_PK" PRIMARY KEY ("ID") ENABLE
   )  ENABLE ROW MOVEMENT ;