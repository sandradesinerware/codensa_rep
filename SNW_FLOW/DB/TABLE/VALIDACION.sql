
  CREATE TABLE "SNW_FLOW"."VALIDACION" 
   (	"ID" NUMBER, 
	"PAQUETE" VARCHAR2(100), 
	"FUNCION" VARCHAR2(32), 
	"CODIGO_PARAM" VARCHAR2(32), 
	"NOMBRE" VARCHAR2(200), 
	"NOMBRE_CASO_NEGATIVO" VARCHAR2(200), 
	"RECOMENDACION" VARCHAR2(2000), 
	"AUD_CREADO_POR" VARCHAR2(400) DEFAULT 'HISTORICO' NOT NULL ENABLE, 
	"AUD_FECHA_CREACION" DATE DEFAULT SYSDATE NOT NULL ENABLE, 
	"AUD_FECHA_ACTUALIZACION" DATE DEFAULT SYSDATE NOT NULL ENABLE, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(400) DEFAULT 'HISTORICO' NOT NULL ENABLE, 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(100) DEFAULT 'IP_ADDRESS' NOT NULL ENABLE, 
	 CONSTRAINT "VALIDACION_PK" PRIMARY KEY ("ID") ENABLE
   )  ENABLE ROW MOVEMENT ;