
  CREATE TABLE "SNW_FLOW"."INSTANCIA_PROCESO" 
   (	"ID" NUMBER, 
	"ID_PROCESO" NUMBER NOT NULL ENABLE, 
	"FECHA_INICIO" DATE NOT NULL ENABLE, 
	"FECHA_FIN" DATE, 
	"OBSERVACION" VARCHAR2(4000), 
	"ID_ESTADO_FINAL" NUMBER, 
	"HEREDADO" VARCHAR2(1) DEFAULT 'N' NOT NULL ENABLE, 
	"ID_INSTANCIA_PROCESO_PADRE" NUMBER, 
	"ID_ACTIVIDAD_VIGENTE" NUMBER, 
	"ID_INSTANCIA_ACTIVA" NUMBER, 
	 CONSTRAINT "INSTANCIA_PROCESO_PK" PRIMARY KEY ("ID") ENABLE
   )  ENABLE ROW MOVEMENT ;