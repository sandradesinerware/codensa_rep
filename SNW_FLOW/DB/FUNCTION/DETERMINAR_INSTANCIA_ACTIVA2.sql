
  CREATE OR REPLACE FUNCTION "SNW_FLOW"."DETERMINAR_INSTANCIA_ACTIVA2" 
    (
      i_instancia_proceso IN instancia_proceso.id%type
    )
    RETURN NUMBER AS 
    l_instancia_tronco NUMBER;
    l_instancia_padre instancia_proceso%rowtype;
    l_conteo NUMBER:=0;
    l_anterior NUMBER;
   BEGIN
   dbms_output.put_line('determinar_instancia_activa');
    if(i_instancia_proceso is null) then
    return null;
  end if;
  if(flujo.proceso_finalizado(i_instancia_proceso)='FALSE') then
    return i_instancia_proceso;
  end if;
    FOR instancia IN
( SELECT level nivel,
          id_proceso,
          id,
          ID_INSTANCIA_PROCESO_PADRE,
          heredado
        FROM instancia_proceso
        WHERE level=1 or heredado='S'
          START WITH ID      =i_instancia_proceso
          CONNECT BY prior id=ID_INSTANCIA_PROCESO_PADRE
          ORDER BY level,id
    )
    LOOP
    dbms_output.put_line('nivel:'||instancia.nivel||',id_proceso:'||instancia.id_proceso||', id:'||instancia.id||', id_padre:'||instancia.id_instancia_proceso_padre||', heredado:'||instancia.heredado);

       IF (instancia.nivel=1 OR (instancia.nivel=2 AND instancia.heredado='S'))
        THEN
        l_anterior:= instancia.id_instancia_proceso_padre;
        l_instancia_tronco:= instancia.id;
      END IF;


      IF(instancia.nivel=2 AND instancia.heredado='S') THEN
        l_anterior := l_instancia_tronco;
        l_instancia_tronco:= instancia.id;
      END IF;

      IF(instancia.nivel>2) THEN
        SELECT *
        INTO l_instancia_padre
        FROM instancia_proceso
        WHERE id                     =instancia.id_instancia_proceso_padre;
        IF(l_instancia_padre.heredado='S' AND instancia.heredado='S' and l_anterior=instancia.id_instancia_proceso_padre) THEN
            l_anterior := l_instancia_tronco;
            l_instancia_tronco        := instancia.id;
        END IF;
        if(l_anterior<>instancia.id_instancia_proceso_padre) then
            return l_instancia_tronco;
        end if;
      END IF;

      IF ( flujo.proceso_finalizado(instancia.id)='FALSE' AND l_instancia_tronco = instancia.id ) THEN
        RETURN instancia.id;
      END IF;

    END LOOP;
    RETURN l_instancia_tronco;
  END determinar_instancia_activa2;

/