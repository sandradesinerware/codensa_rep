
  CREATE OR REPLACE FUNCTION "SNW_FLOW"."INSTANCIA_EN_PROCESOS" (i_id_instacia IN NUMBER, i_id_procesos IN VARCHAR2) RETURN NUMBER
AS
l_resultado NUMBER;
BEGIN

SELECT COUNT(ID) into l_resultado FROM INSTANCIA_PROCESO WHERE ID = i_id_instacia AND ID_PROCESO in (select to_number(column_value) as IDs from xmltable(i_id_procesos));
IF l_resultado > 0 THEN
RETURN 1;
ELSE
RETURN 0;
END IF;
END;

/