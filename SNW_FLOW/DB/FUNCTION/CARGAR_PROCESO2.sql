
  CREATE OR REPLACE FUNCTION "SNW_FLOW"."CARGAR_PROCESO2" 
(
  i_id_instancia_proceso IN NUMBER 
) RETURN NUMBER AS 
o_proceso NUMBER;
BEGIN
 SELECT id_proceso
    INTO o_proceso
    FROM instancia_proceso
    WHERE id=i_id_instancia_proceso;
    RETURN o_proceso;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
  RETURN NULL;
END CARGAR_PROCESO2;

/