
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."MOSTRAR_CURSOS" 
(
  i_id_grupo in NUMBER,
  i_rol in  VARCHAR2
) return number 
as
l_visible PDI_GRUPO.VISIBLE%TYPE;
begin

select visible into l_visible from pdi_grupo where id = i_id_grupo;

if(i_rol = 'admin') then 
    return 1;
elsif (l_visible = 'S') then 
    return 1;
else 
    return 0;
end if;
end MOSTRAR_CURSOS;
/