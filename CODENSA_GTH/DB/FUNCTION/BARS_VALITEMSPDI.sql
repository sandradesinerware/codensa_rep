
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALITEMSPDI" 
(pitem IN VARCHAR2, pencabezado IN BARS_ENCPDI.ID%TYPE) RETURN NUMBER
IS
kplaneado CONSTANT VARCHAR2(30) := 'PLANEADO';
kconcertado CONSTANT VARCHAR2(30) := 'CONCERTADO';
kfinalizado CONSTANT VARCHAR2(30) := 'FINALIZADO';
kanulado CONSTANT VARCHAR2(30) := 'ANULADO';
knoaprobado CONSTANT VARCHAR2(30) := 'NO APROBADO';
kadministradores CONSTANT VARCHAR2(30) := 'ADMINISTRADORES';
kmodoconsulta CONSTANT VARCHAR2(30) := 'CONSULTA';
kmodoedicion CONSTANT VARCHAR2(30) := 'EDICION';
ktipoestadopdi CONSTANT VARCHAR2(30) := 'PDI';
vtipo_estado NUMBER(2,0);
vestado_formulario VARCHAR2(30);
vevaluado BARS_FORMEVAOBJ.EVALUADO%TYPE;
vgestor TRABAJADOR.JEFE%TYPE;
vusuario USUARIO.NUMERO_IDENTIFICACION%TYPE;
vaccion tipo.nombre%type;
flag BOOLEAN;
formulario_inexistente EXCEPTION;

--Colecci�n para almacenar los grupos del usuario
TYPE grupo_array IS TABLE OF GRUPO.NOMBRE%TYPE;
vgrupos grupo_array;
i number(2) := 0;

BEGIN
--Determinar usuario
SELECT numero_identificacion into vusuario
  FROM USUARIO
  WHERE upper(user_name) = upper(v('APP_USER'));
--Determinar grupos (i.e. roles) del usuario
vgrupos := grupo_array();
FOR rec_grupo IN ( SELECT upper(trim(gb.nombre)) nombre
                   from USU_GRUPO ubg inner join GRUPO gb on ubg.grupo = gb.id
                   where ubg.usuario = vusuario ) 
                   LOOP
  i := i + 1;
  vgrupos.EXTEND(1);
  vgrupos(i) := rec_grupo.nombre;
END LOOP;

if pencabezado IS NULL then --El formulario se va a crear
 raise formulario_inexistente;
end if;

--Determinar tipo estado PDI
SELECT id into vtipo_estado
  FROM TIPO_ESTADO_BARS
  WHERE upper(nombre) = ktipoestadopdi;
--Determinar estado del formulario PDI
SELECT upper(trim(E.nombre)) into vestado_formulario
  FROM BARS_PDI F inner join ESTADO_BARS E on F.ID_estado = E.id
  WHERE F.id = NVL(pencabezado,0);
--Determinar evaluado y gestor
SELECT evaluado into vevaluado
  FROM BARS_ENCPDI
  WHERE id = NVL(pencabezado,0);
SELECT jefe into vgestor
  FROM TRABAJADOR
  WHERE numero_identificacion = NVL(vevaluado,0);
--Determinar acci�n de desarrollo
SELECT upper(nombre) into vaccion
  FROM tipo 
  WHERE id = ( SELECT ID_accion FROM BARS_PDI WHERE id = pencabezado );

--Validaci�n del display de los items de objetivos de acuerdo al item y al estado del formulario
CASE upper(pitem)
WHEN 'PDI.ESTADO' THEN --No para visualizaci�n sino para read only
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF (flag) then
    RETURN 0;
  else
    RETURN 1;
  end if;
WHEN 'PDI.ESTFINFORMACION' THEN --Este campo es solo para las acciones de formaci�n
  IF ( vaccion LIKE 'FORMACI_N' AND vestado_formulario in (kconcertado,kfinalizado) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'CREATE' THEN --Aca no deber�a llegar porque el bot�n est� condicionado para que solo aparezca si :P78_ID es NULL, y en ese caso
                   --se lanzar�a excepcion formulario_inexistente
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( pencabezado is null and (flag OR vusuario = v('P78_EVALUADO') ) ) then --Solo administradores y el propio evaluado pueden crear
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'SAVE' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and vevaluado = vusuario )
    OR ( flag ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'DELETE' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and vevaluado = vusuario ) 
    OR ( flag and vestado_formulario in (kplaneado) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'CONCERTAR' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and ( vgestor = vusuario OR flag ) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'REPROBAR_CONCERTACION' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kplaneado) and ( vgestor = vusuario OR flag ) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
WHEN 'FINALIZAR' THEN
  flag := FALSE;
  FOR i IN 1..vgrupos.COUNT LOOP
    if vgrupos(i) = kadministradores then
      flag := TRUE;
    end if;
  end loop;
  IF ( vestado_formulario in (kconcertado) and ( flag ) ) then
    RETURN 1;
  else
    RETURN 0;
  end if;
ELSE
  RETURN 0;
END CASE;

EXCEPTION
  WHEN FORMULARIO_INEXISTENTE THEN
    CASE upper(pitem)
      WHEN 'CREATE' THEN 
        FOR i IN 1..vgrupos.COUNT LOOP
            if vgrupos(i) = kadministradores then
                flag := TRUE;
            end if;
        end loop;
        IF ( pencabezado is null and (flag OR vusuario = v('P78_EVALUADO') ) ) then
            RETURN 1;
        else
            RETURN 0;
        end if;
      WHEN 'NUEVO_PDI' THEN --Bot�n de la p�gina 77 donde nace el proceso de creaci�n 
        FOR i IN 1..vgrupos.COUNT LOOP
            if vgrupos(i) = kadministradores then
                flag := TRUE;
            end if;
        end loop;
        IF ( pencabezado is null and (flag OR vusuario = v('P77_EVALUADO') ) ) then
            RETURN 1;
        else
            RETURN 0;
        end if;
      WHEN 'PDI.ESTADO' /*No display sino readonly*/ THEN RETURN 1; --El estado inicial del formulario se asigna autom�ticamente
      WHEN 'PDI.ESTFINFORMACION' THEN RETURN 0; --A un formulario inexistente no se le muestra este campo
    ELSE RETURN 0;
    END CASE;

  WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_VALITEMSPDI. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
      || SQLCODE || ' - ' || SQLERRM);

END BARS_VALITEMSPDI;

/