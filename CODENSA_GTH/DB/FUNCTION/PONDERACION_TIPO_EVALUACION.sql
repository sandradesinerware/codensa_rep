
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."PONDERACION_TIPO_EVALUACION" 
(i_trabajador in NUMBER,
i_tipo_evaluacion in NUMBER,
i_periodo in NUMBER)
return NUMBER
is
o_ponderacion NUMBER;
begin
select PORCENTAJE_TOTAL into o_ponderacion 
    from restriccion_target right join trabajador on (restriccion_target.id_target=trabajador.id_target_opr) 
    where trabajador.numero_identificacion=i_trabajador and id_tipo_evaluacion=i_tipo_evaluacion and periodo=i_periodo;
return o_ponderacion;
exception
    when no_data_found then
    return 0;
end;

/