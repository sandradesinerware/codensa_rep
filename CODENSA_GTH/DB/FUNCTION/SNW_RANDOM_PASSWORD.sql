
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."SNW_RANDOM_PASSWORD" return varchar2 as 
l_random_password varchar2(30);
begin
select dbms_random.string('x',6) || lpad(to_char(trunc(dbms_random.value(1,99))),2,'0') into l_random_password from dual;
return l_random_password;
end;

/