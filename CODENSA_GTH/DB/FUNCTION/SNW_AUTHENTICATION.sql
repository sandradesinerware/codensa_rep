
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."SNW_AUTHENTICATION" (
    p_username IN VARCHAR2,
    p_password IN VARCHAR2 )
  RETURN BOOLEAN
IS
  l_user usuario.user_name%type := upper(p_username);
  l_pwd usuario.password%type;
  l_id usuario.id%type;
BEGIN
  SELECT id ,
    password
  INTO l_id,
    l_pwd
  FROM usuario
  WHERE user_name = l_user;
  RETURN l_pwd   = rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password||l_id||l_user), 2 ));
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN false;
END;

/