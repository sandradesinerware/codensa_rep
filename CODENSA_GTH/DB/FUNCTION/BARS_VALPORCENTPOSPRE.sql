
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."BARS_VALPORCENTPOSPRE" ( pposicion IN BARS_POSPRE.id%type, pporcentaje IN NUMBER ) RETURN VARCHAR2
is
retV  VARCHAR2(200);
rmin  NUMBER;
rmax  NUMBER;
idPos NUMBER;
begin
  retV := 'Correcto.';

  select RESTRMIN, RESTRMAX into rmin, rmax from BARS_POSPRE where id = pposicion;

  if( rmin is not null and pporcentaje < rmin ) then
              retV := 'El porcentaje es inferior al minimo.';
  elsif( rmax is not null and pporcentaje > rmax ) then
           retV := 'El porcentaje supera el maximo.';
  end if; 

  return retV;
end;

/