
  CREATE OR REPLACE FUNCTION "CODENSA_GTH"."DETERMINAR_PCT_CONSECUCION" (pformulario IN FORMULARIO_BARS.ID%TYPE, 
  ptipo IN SECCION_FORMULARIO.TIPO%TYPE)
RETURN PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE
IS
  vpct_consecucion PCT_CONSECUCION_BARS.PCT_CONSECUCION%TYPE;
  vresultado_encuesta NUMBER;
  vconteo_filas NUMBER;
  vperiodo PERIODO_BARS.ANO%TYPE;
BEGIN
  --Determinar promedio encuesta y normalizar
  vresultado_encuesta := NORMALIZAR_BPR(DETERMINAR_PROMEDIO_ENCUESTA(pformulario, ptipo));


  --Persiste la informacion en FORMULARIO_BARS
  update formulario_bars set media_normalizada = vresultado_encuesta where id = pformulario;

  
  --Determinar pct_consecucion
  vpct_consecucion := OBTENER_PORCENTAJE_CONSECUCION(vresultado_encuesta);

  
  /* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
   --Determinar periodo
  select periodo into vperiodo from FORMULARIO_BARS where id = pformulario;
  
  --Determinar pct_consecucion
  SELECT count(*) into vconteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 61 and periodo = vperiodo AND vresultado_encuesta BETWEEN media_comp_inf AND media_comp_sup;
    
  if vconteo_filas = 1 then --Se determinó el porcentaje de consecución  
    SELECT pct_consecucion into vpct_consecucion
      from PCT_CONSECUCION_BARS
      where tipo_id = 61 and  periodo = vperiodo AND vresultado_encuesta BETWEEN media_comp_inf AND media_comp_sup;
  else --No se determinó un porcentaje de consecución
    vpct_consecucion := NULL;
  end if;
    */
  
  
  RETURN vpct_consecucion;
  
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en DETERMINAR_PCT_CONSECUCION. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
END;
/