
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_TRABAJADOR" ("IDENTIFICACION", "NUMERO_IDENTIFICACION", "NOMBRES", "APELLIDOS", "NOMBRE_COMPLETO", "AREA", "ID_CARGO", "COLECTIVO", "JEFE", "TELETRABAJO", "BLOB_CONTENT", "FILE_NAME", "MIME_TYPE", "LAST_UPDATED", "CHARSET", "FK_TIPO_TRABAJADOR", "ID_ESTADO", "CENCOS", "ZONA", "ID_GERENCIA", "ID_UNIDAD_ORGANIZATIVA", "ID_TARGET_BPR", "ID_TARGET_OPR", "ID_TIPO_REGIMEN", "ID_EMPRESA", "EMAIL", "ID", "USUARIO", "GERENCIA", "TARGET_BPR", "TARGET_OPR", "TIPO_REGIMEN", "UNIDAD_ORGANIZATIVA", "EMPRESA", "CARGO", "ESTADO", "IDENTIFICACION_NOMBRE", "GESTOR", "GESTOR_NRO_IDENTIFICACION") AS 
  SELECT trabajador.identificacion,
  trabajador."NUMERO_IDENTIFICACION",
  trabajador."NOMBRES",
  trabajador. "APELLIDOS" ,
  trabajador.NOMBRES||' '||trabajador.APELLIDOS AS NOMBRE_COMPLETO,
  trabajador."AREA",
  trabajador. "ID_CARGO" ,
  trabajador."COLECTIVO",
  trabajador. "JEFE" ,
  trabajador."TELETRABAJO",
  trabajador. "BLOB_CONTENT" ,
  trabajador."FILE_NAME",
  trabajador. "MIME_TYPE" ,
  trabajador."LAST_UPDATED",
  trabajador. "CHARSET" ,
  trabajador."FK_TIPO_TRABAJADOR",
  trabajador."ID_ESTADO" ,
  trabajador."CENCOS",
  trabajador."ZONA",
  trabajador."ID_GERENCIA" ,
  trabajador."ID_UNIDAD_ORGANIZATIVA",
  trabajador."ID_TARGET_BPR" ,
  trabajador."ID_TARGET_OPR",
  trabajador. "ID_TIPO_REGIMEN" ,
  trabajador."ID_EMPRESA",
  trabajador."EMAIL",
  trabajador.id AS id,
  usuario.user_name usuario ,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_gerencia
  ) AS gerencia,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_TARGET_BPR
  ) AS TARGET_BPR,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_TARGET_OPR
  ) AS TARGET_OPR,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_TIPO_REGIMEN
  ) AS TIPO_REGIMEN,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_UNIDAD_ORGANIZATIVA
  ) AS UNIDAD_ORGANIZATIVA,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_EMPRESA
  ) AS EMPRESA,
  (SELECT nombre FROM tipo WHERE id = trabajador.ID_cargo
  ) AS cargo,
  DECODE(trabajador.id_estado, 1, 'Activo', 0, 'Inactivo') AS estado ,
  (trabajador.numero_identificacion
  || ' '
  || trabajador.apellidos
  || ', '
  || trabajador.nombres) AS IDENTIFICACION_NOMBRE,
  CASE
    WHEN J.nombres IS NOT NULL
    THEN J.nombres
      || ' '
      || J.apellidos
    ELSE NULL
  END AS gestor,
  J.numero_identificacion gestor_nro_identificacion
FROM trabajador
LEFT JOIN TRABAJADOR J
ON ( trabajador.jefe = J.numero_identificacion )
LEFT JOIN usuario
ON (usuario.numero_identificacion= trabajador.numero_identificacion);