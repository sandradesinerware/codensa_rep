
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."BARS_RENDIMIENTO" ("NUMERO_IDENTIFICACION", "LINK_IDENTIFICACION", "NOMBRES", "APELLIDOS", "ACTUACION", "POSICION INICIAL", "RENDIMIENTO", "AJUSTE_VGA", "MESA", "PCT_CALIBRADO", "PUNTOSPCT") AS 
  (
select T.NUMERO_IDENTIFICACION, 
T.NUMERO_IDENTIFICACION as LINK_IDENTIFICACION, 
T.NOMBRES, 
T.APELLIDOS,
nvl(fb.PCT_CONSECUCION, 0) as "ACTUACION",
bp.NOMBRE as "POSICION INICIAL",
nvl(bp.nombre,'Sin definir') as "RENDIMIENTO",
bmt.AJUSTE as AJUSTE_VGA,
bmt.mesa as MESA,
bmt.PCT_CALIBRADO as PCT_CALIBRADO,
(SELECT MEDIA_COMP_SUP from PCT_CONSECUCION_BARS where PCT_CONSECUCION
= fb.PCT_CONSECUCION and periodo = bm.periodo) as "PUNTOSPCT"
from BARS_MESAHOM_TRABAJADOR bmt inner join "TRABAJADOR" T on bmt.trabajador = t.numero_identificacion
 inner join BARS_MESAHOM bm on bmt.mesa = bm.id
 inner join FORMULARIO_BARS fb on (fb.EVALUADO = bmt.trabajador and fb.periodo = bm.periodo) 
 left join BARS_POSICION bp on (bm.periodo = bp.periodo and fb.PCT_CONSECUCION between bp.PCT_INF and bp.PCT_SUP)
where fb.tipo = 'EVALUACION'
);