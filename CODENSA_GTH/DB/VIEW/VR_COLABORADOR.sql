
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."VR_COLABORADOR" ("ID", "ID_PERIODO", "PERIODO", "EVALUADO", "EVALUADOR", "NOMBRE_EVALUADO") AS 
  select
id,
periodo id_periodo,
(select nombre from pdi_periodo where id=periodo) periodo,
evaluado,
evaluador,
(select nombres||' '||apellidos from trabajador where numero_identificacion=evaluado) nombre_evaluado
from bars_encpdi;