
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_CREAR_SERVICIOS" ("ID", "SERVICIO", "codigo servicio", "DIVISION", "SUBGERENCIA", "GERENCIA") AS 
  select servicio.id, servicio.NOMBRE servicio, servicio.CODIGO as "codigo servicio",dep1.NOMBRE division,dep2.nombre subgerencia,dep3.nombre gerencia 
from servicio
join dependencia dep1 on (dep1.id = servicio.division)
join dependencia dep2 on (dep1.tipo = dep2.id)
join dependencia dep3 on (dep2.tipo = dep3.id);