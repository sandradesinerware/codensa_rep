
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_TRABAJADOR_SIN_PEC" ("USUARIO_TRABAJADOR", "IDENTIFICACION_TRABAJADOR", "NOMBRE_TRABAJADOR", "IDENTIFICACION_GESTOR", "NOMBRE_GESTOR") AS 
  select  
  v_trabajador.usuario usuario_trabajador,
  v_trabajador.numero_identificacion identificacion_trabajador,
  v_trabajador.nombre_completo nombre_trabajador,
  v_trabajador.gestor_nro_identificacion identificacion_gestor,
  v_trabajador.gestor nombre_gestor  
  from v_trabajador
  where numero_identificacion not in (select ID_TRABAJADOR from pec);