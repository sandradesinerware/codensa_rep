
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."VR_GRAFICA_GENERAL_COLABORADOR" ("ID_ENCABEZADO", "CONTEO", "PROPORCION", "ESTADO_NEGOCIO") AS 
  Select 
Id_Encabezado,
Sum(Conteo) Conteo,
To_Char(Sum(Proporcion))||'%' Proporcion,
Estado_Negocio
From
(
SELECT 
ID_ENCABEZADO,
count(*) conteo,
round(count(*)/(select count(*) from vr_reporte_pdi where ID_ENCABEZADO = PDI.ID_ENCABEZADO)*100) proporcion,
ESTADO_NEGOCIO
FROM VR_REPORTE_PDI PDI
group by ESTADO_NEGOCIO,ID_ENCABEZADO
Union All
Select 
Id_Encabezado,
0 Conteo,
0 Proporcion,
'Pendiente' Estado_Negocio
From Vr_Reporte_Pdi Pdi
Union All
Select 
Id_Encabezado,
0 Conteo,
0 Proporcion,
'Realizado' Estado_Negocio
From Vr_Reporte_Pdi Pdi)
Group By Id_Encabezado,Estado_Negocio;