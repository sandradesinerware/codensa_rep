
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_INSCRITOS_GRUPO" ("ID_GRUPO", "NO_TRABAJADOR", "TRABAJADOR", "NO_GESTOR", "GESTOR", "ESTADO", "ID_PDI") AS 
  select 
id_grupo,
evaluado No_trabajador,
(select nombres||' '||apellidos from trabajador where numero_identificacion = evaluado) Trabajador,
evaluador No_gestor,
(select nombres||' '||apellidos from trabajador where numero_identificacion = evaluador) Gestor,
estado,
id id_pdi
from v_reporte_pdi where id_grupo is not null;