
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_ENCABEZADO_OPR" ("ID", "CONSECUTIVO", "PERIODO", "PERIODO_VISIBLE", "PERIODO_VISIBLE_FULL", "FECHA_INICIO", "FECHA_FIN", "CARGO", "GERENCIA", "UNIDAD_ORGANIZATIVA", "ID_TARGET_OPR", "TARGET", "ESTADO", "EVALUADO_ASIGNADO", "EVALUADO_NOMBRES", "EVALUADO_APELLIDOS", "DATOS_TRABAJADOR", "GESTOR_VIGENTE_FULL", "GESTOR_VIGENTE_EMAIL", "CONCERTADOR_ASIGNADO", "CONCERTADOR_NOMBRES", "CONCERTADOR_APELLIDOS", "CONCERTADOR_NOMBRE_FULL", "DATOS_GESTOR", "EVALUADOR_ASIGNADO", "EVALUADOR_NOMBRES", "EVALUADOR_APELLIDOS", "EVALUADOR_NOMBRE_FULL", "APROBADOR_RESULTADO", "FECHA_IMPRESION", "FECHEVA", "RESULTADO_PONDERADO", "PCTCONSFORM", "PCTCONSABI", "POSICION") AS 
  SELECT 
 BARS_FORMEVAOBJ.id, 
 consecutivo, 
 --Columnas del periodo
 bars_perobj.ano periodo,
 bars_perobj.nombre_visible periodo_visible,
 bars_perobj.nombre_visible || ' (' ||BARS_FORMEVAOBJ.fecha_inicio || ' - ' || BARS_FORMEVAOBJ.fecha_fin ||  ')' periodo_visible_full,
 BARS_FORMEVAOBJ.fecha_inicio,
 BARS_FORMEVAOBJ.fecha_fin,
 --A partir de 2020 el cargo cambi� de ser un tipo referenciado a la tabla tipo, a un texto copiado de la posici�n
 CASE WHEN bars_perobj.ano < 2020 THEN (select nombre from tipo where id = BARS_FORMEVAOBJ.cargo)
  ELSE BARS_FORMEVAOBJ.NOMBRE_CARGO
 END cargo,
 (select nombre from tipo where id = BARS_FORMEVAOBJ.id_gerencia) gerencia,
 --A partir de 2020 la Unidad Organizativa cambi� de ser un tipo referenciado a la tabla tipo, a un texto copiado de la posici�n
 CASE WHEN bars_perobj.ano < 2020 THEN (select nombre from tipo where id = BARS_FORMEVAOBJ.id_unidad_organizativa)
  ELSE BARS_FORMEVAOBJ.NOMBRE_UNIDAD_ORGANIZATIVA
 END unidad_organizativa,
 BARS_FORMEVAOBJ.id_target_opr,
 (select nombre from tipo where id = BARS_FORMEVAOBJ.id_target_opr) target,
 (select nombre from ESTADO_BARS where id = BARS_FORMEVAOBJ.ESTADO) estado,
 --Columnas del evaluado
 evaluado.numero_identificacion evaluado_asignado,
 evaluado.nombres evaluado_nombres,
 evaluado.apellidos evaluado_apellidos,
 evaluado.apellidos||' '||evaluado.nombres datos_trabajador,
 gestor_vigente.numero_identificacion||' '||gestor_vigente.apellidos||' '||gestor_vigente.nombres gestor_vigente_full,
 gestor_vigente.email gestor_vigente_email,
 --Columnas del aprobador de la concertaci�n (normalmente el gestor)
 concertador.numero_identificacion concertador_asignado,
 concertador.nombres concertador_nombres,
 concertador.apellidos concertador_apellidos,
 concertador.apellidos||' '||concertador.nombres concertador_nombre_full,
 concertador.numero_identificacion||' '||concertador.apellidos||' '||concertador.nombres datos_gestor,
 --Columnas del que diligencia y aprueba el resultado (el gestor que est� en su momento)
 eva_resultado.numero_identificacion evaluador_asignado,
 eva_resultado.nombres evaluador_nombres,
 eva_resultado.apellidos evaluador_apellidos,
 eva_resultado.apellidos||' '||eva_resultado.nombres evaluador_nombre_full,
 eva_resultado.numero_identificacion||' '||eva_resultado.apellidos||' '||eva_resultado.nombres aprobador_resultado,
 sysdate fecha_impresion,
 BARS_FORMEVAOBJ.FECHEVA,
 BARS_FORMEVAOBJ.RESULTADO_PONDERADO,
 BARS_FORMEVAOBJ.PCTCONSFORM,
 BARS_FORMEVAOBJ.PCTCONSABI,
 BARS_FORMEVAOBJ.POSICION
from    BARS_FORMEVAOBJ 
left join bars_perobj on (BARS_FORMEVAOBJ.periodo = bars_perobj.ano)
left join trabajador evaluado on (BARS_FORMEVAOBJ.evaluado=evaluado.numero_identificacion)
left join trabajador concertador on (BARS_FORMEVAOBJ.evaluador=concertador.numero_identificacion)
left join trabajador eva_resultado on (BARS_FORMEVAOBJ.EVALUADOR_RESULTADO = EVA_RESULTADO.numero_identificacion)
left join trabajador gestor_vigente on (evaluado.jefe = gestor_vigente.numero_identificacion);