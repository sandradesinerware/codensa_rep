
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_AGENTE" ("TIPO_AGENTE", "NUMERO_IDENTIFICACION", "NOMBRE", "EMAIL", "CARGO", "CLASE", "ID_DEPARTAMENTO", "ID_NIVEL_CARGO", "GERENCIA", "SUBGERENCIA", "DIVISION", "DOCUMENTO_IDENTIFICACION") AS 
  SELECT 'T' Tipo_agente, 
         t.NUMERO_IDENTIFICACION NUMERO_IDENTIFICACION, 
         t.NOMBRES||' '||t.APELLIDOS AS NOMBRE, 
         t.EMAIL, 
         cargo.nombre cargo, 
         t.CLASE, 
         t.ID_DEPARTAMENTO,
         t.ID_NIVEL_CARGO,
         gerencia.nombre gerencia,
         subgerencia.nombre subgerencia,
         division.nombre division,
         t.identificacion documento_identificacion
  FROM TRABAJADOR t
        left join dependencia departamento on t.id_departamento = departamento.id 
        left join dependencia division on departamento.tipo = division.id 
        left join dependencia subgerencia on division.tipo = subgerencia.id 
        left join dependencia gerencia on subgerencia.tipo = gerencia.id
        left join tipo cargo on t.id_cargo = cargo.id
UNION
SELECT 'C' Tipo_agente, 
        c.NIT NUMERO_IDENTIFICACION, 
        c.NOMBRE, 
        c.EMAIL, 
        c.CARGO, 
        c.CLASE, 
        c.ID_DEPARTAMENTO,
        c.ID_NIVEL_CARGO,
        gerencia.nombre gerencia,
        subgerencia.nombre subgerencia,
        division.nombre division,
        c.nit documento_identificacion
  FROM CONTRATISTA c
        left join dependencia departamento on c.id_departamento = departamento.id 
        left join dependencia division on departamento.tipo = division.id 
        left join dependencia subgerencia on division.tipo = subgerencia.id 
        left join dependencia gerencia on subgerencia.tipo = gerencia.id;