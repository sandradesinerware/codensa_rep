
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_REPORTE_GENERAL_PEC" ("ID", "USUARIO_TRABAJADOR", "IDENTIFICACION_TRABAJADOR", "NOMBRE_TRABAJADOR", "IDENTIFICACION_GESTOR", "NOMBRE_GESTOR", "ESTADO_ACTUAL", "FECHA_CREACION", "MOTIVO_CREACION", "INTENSIDAD_HORAS", "PEC_ESTADO", "PROMEDIO_EVALUACION") AS 
  select
  pec.id,
  v_trabajador.usuario usuario_trabajador,
  pec.id_trabajador identificacion_trabajador,
  v_trabajador.nombre_completo nombre_trabajador,
  v_trabajador.gestor_nro_identificacion identificacion_gestor,
  v_trabajador.gestor nombre_gestor,
  (select nombre from tipo where id = pec.id_estado) estado_actual,
  pec.fecha_creacion,
  (select nombre from tipo where id = PEC.motivo_creacion) motivo_Creacion,
  'Ver actividades' INTENSIDAD_HORAS,
  (select nombre from tipo where id = pec.id_estado) pec_estado,  
  pec_evaluacion.promedio_evaluacion promedio_evaluacion
  from pec
  left join pec_evaluacion
    on pec_evaluacion.id_pec = pec.id
  left join v_trabajador
    on pec.id_trabajador = v_trabajador.numero_identificacion;