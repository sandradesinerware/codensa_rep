
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_ADMIN_ENCABEZADOS_CRUZADOS" ("EVALUADO", "NOMBRE_EVALUADO", "EVALUADOR", "NOMBRE_EVALUADOR", "EVALUADOR_ACTUAL", "NOMBRE_EVALUADOR_ACTUAL", "ID_ENCABEZADO", "PERIODO") AS 
  select
    t.numero_identificacion evaluado,    
    t.nombres || ' ' || t.apellidos nombre_evaluado,     
    enc.evaluador,
    ( select nombres || ' ' || apellidos from trabajador where numero_identificacion = enc.evaluador ) nombre_evaluador,
    t.jefe evaluador_actual,
    ( select nombres || ' ' || apellidos from trabajador where numero_identificacion = t.jefe ) nombre_evaluador_actual,
    enc.id id_encabezado,
    enc.periodo
    from trabajador t 
    left join bars_encpdi enc 
        on ( enc.evaluado = t.numero_identificacion );