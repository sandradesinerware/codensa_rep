
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."VR_GRAFICA_DESARROLLO_COLABORA" ("ID_ENCABEZADO", "CONTEO", "PROPORCION", "ESTADO_NEGOCIO") AS 
  select 
id_encabezado,
sum(conteo) conteo,
to_char(sum(proporcion))||'%' proporcion,
estado_negocio
from
(SELECT 
ID_ENCABEZADO,
count(*) conteo,
round(nvl(count(*),0)/(select count(*) from vr_reporte_pdi where ID_ENCABEZADO = PDI.ID_ENCABEZADO
AND ID_ACCION<> 57)*100) proporcion,
ESTADO_NEGOCIO
FROM VR_REPORTE_PDI PDI
WHERE ID_ACCION <> 57
group by ID_ENCABEZADO,ESTADO_NEGOCIO
union all
SELECT 
ID_ENCABEZADO,
0 conteo,
0 proporcion,
'Pendiente' ESTADO_NEGOCIO
FROM VR_REPORTE_PDI PDI
WHERE ID_ACCION <> 57
union all
SELECT 
ID_ENCABEZADO,
0 conteo,
0 proporcion,
'Realizado' ESTADO_NEGOCIO
FROM VR_REPORTE_PDI PDI
WHERE ID_ACCION <> 57)
group by id_encabezado,estado_negocio;