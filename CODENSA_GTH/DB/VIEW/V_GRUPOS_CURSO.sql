
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_GRUPOS_CURSO" ("ID", "ID_PERIODO", "PERIODO", "FACULTAD", "NEGOCIO", "VISIBLE", "ID_ESTADO", "ESTADO", "FECHA_INICIO", "FECHA_FIN", "ID_CURSO", "CURSO", "OBJETIVO", "ID_TIPO_CURSO", "TIPO_CURSO", "GRUPO", "ENCARGADO", "TIPO_GRUPO", "ID_TIPO_GRUPO", "CUPOS_DISPONIBLES", "LIST_ESPERA", "CUPO_MINIMO", "CUPO_ANALISIS", "CUPO_MAXIMO", "NO_PDI", "INSCRITOS", "ID_ROL", "ID_TIPO_JURISDICCION") AS 
  select 
pdi_grupo.ID ID,
pdi_grupo.id_periodo id_periodo,
(select ano from pdi_periodo where id=pdi_grupo.id_periodo) periodo,
facultad.nombre facultad,
negocio.nombre negocio,
pdi_grupo.visible visible,
pdi_grupo.estado id_estado,
(select nombre from tipo where id = pdi_grupo.estado) estado,
pdi_grupo. fecha_inicio,
pdi_grupo.fecha_fin,
pdi_curso.id id_curso,
pdi_curso.nombre_visible curso,
pdi_curso.objetivo objetivo,
pdi_curso.tipo id_tipo_curso,
(select nombre from tipo where id = pdi_curso.tipo) tipo_curso,
pdi_grupo.nombre_grupo grupo,
(select nombres||' '||apellidos from trabajador where trabajador.numero_identificacion=pdi_curso.id_encargado) encargado,
(select nombre from tipo where id = pdi_grupo.tipo_grupo) tipo_grupo,
pdi_grupo.tipo_grupo id_tipo_grupo,
ADMINISTRACION_PDI_PCK.NRO_CUPOS_DISP_GRUPO(pdi_grupo.ID) cupos_disponibles,
decode(pdi_grupo.tipo_grupo,SNW_CONSTANTES.constante_tipo('CONFORMADO_GRUPO'),'S',ADMINISTRACION_PDI_PCK.LISTA_ESPERA(pdi_grupo.ID )) list_espera,
pdi_grupo.cupo_minimo,
pdi_grupo.cupo_analisis,
pdi_grupo.cupo_maximo,
pdi_curso.no_pdi NO_PDI,
ADMIN_CURSO_PCK.INSCRITOS_GRUPO(pdi_grupo.id) inscritos,
id_rol,
id_tipo_jurisdiccion
from pdi_grupo 
left join pdi_curso on (pdi_curso.id = pdi_grupo.id_pdi_curso)
left join tipo negocio on (negocio.id = pdi_curso.id_negocio)
left join tipo facultad on (facultad.id = negocio.tipo and facultad.tipo = SNW_CONSTANTES.constante_tipo('FACULTAD'));