
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_INSCRITOS" ("ID", "ID_PDI_GRUPO", "ID_PERIODO", "ID_ESTADO", "ESTADO", "NUMERO_ASIGNADO", "EVALUADO", "NOMBRES_EVALUADO", "APELLIDOS_EVALUADO", "EVALUADOR") AS 
  select 
bars_pdi.id,
bars_pdi.id_pdi_grupo,
bars_encpdi.periodo id_periodo,
bars_pdi.id_estado,
(select nombre from estado_bars where id=bars_pdi.id_estado) estado,
bars_encpdi.evaluado numero_asignado,
(select nombres||' '||apellidos from trabajador where trabajador.numero_identificacion=bars_encpdi.evaluado) evaluado,
(select nombres from trabajador where trabajador.numero_identificacion=bars_encpdi.evaluado) nombres_evaluado,
(select apellidos from trabajador where trabajador.numero_identificacion=bars_encpdi.evaluado) apellidos_evaluado,
(select nombres||' '||apellidos from trabajador where trabajador.numero_identificacion=bars_encpdi.evaluador) evaluador
from bars_pdi
left join bars_encpdi on (bars_pdi.id_encabezado = bars_encpdi.id)
where bars_pdi.id in (select id_pdi from pdi_concepto where id_estado_pdi =30) and
id_accion = snw_constantes.constante_tipo('FORMACION');