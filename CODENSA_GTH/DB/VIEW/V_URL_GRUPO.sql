
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_URL_GRUPO" ("ID_GRUPO", "PERIODO", "NOMBRE_CURSO", "TIPO_CURSO", "NOMBRE_GRUPO", "ESTADO_GRUPO", "FECHA_INICIO", "FECHA_FIN", "URL_GRUPO", "JURISDICCION") AS 
  select 
pdi_grupo.ID id_grupo,
(select ano from pdi_periodo where id=pdi_grupo.id_periodo) periodo,
pdi_curso.nombre_visible nombre_curso,
(select nombre from tipo where id = pdi_curso.tipo) tipo_curso,
pdi_grupo.nombre_grupo nombre_grupo,
(select nombre from tipo where id = pdi_grupo.estado) estado_grupo,
pdi_grupo. fecha_inicio,
pdi_grupo.fecha_fin,
(SELECT UTILS_PCK.BUILD_URL_GROUP(pdi_grupo.ID) FROM DUAL) url_grupo,
(select nombre from tipo where id = pdi_grupo.id_tipo_jurisdiccion) jurisdiccion
from pdi_grupo 
left join pdi_curso on (pdi_curso.id = pdi_grupo.id_pdi_curso);