
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_HISTORIAL_PEC" ("ID", "IDENTIFICACION_TRABAJADOR", "NOMBRE_TRABAJADOR", "IDENTIFICACION_GESTOR", "NOMBRE_GESTOR", "ESTADO_ACTUAL", "FECHA_CREACION", "INTENSIDAD_HORAS", "PEC_ESTADO", "FECHA_ESTADO") AS 
  select 
  pec.id,
  pec.id_trabajador identificacion_trabajador,
  (select nombres || ' ' || apellidos from trabajador where numero_identificacion = pec.id_trabajador) nombre_trabajador,
  pec.id_gestor identificacion_gestor,
  (select nombres || ' ' || apellidos from trabajador where numero_identificacion = pec.id_gestor) nombre_gestor,
  (select nombre from tipo where id = pec.id_estado) estado_actual,
  pec.fecha_creacion,
  pec.intensidad_horas,
  (select nombre from tipo where id = pec_estado.id_tipo_estado) pec_estado,
  pec_estado.fecha fecha_estado
from pec_estado
left join pec
  on pec_estado.id_pec = pec.id;