
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_PEC_EVALUACION" ("ID", "ENUNCIADO", "NUMERO", "ESTADO", "CALIFICACION_PREGUNTA", "ID_EVALUACION", "ID_PEC") AS 
  select 
  pec_preguntas.id,
  pec_preguntas.enunciado,
  pec_preguntas.numero,
  pec_preguntas.estado,
  pec_evaluacion_respuestas.calificacion_pregunta,
  pec_evaluacion_respuestas.ID_EVALUACION,
  pec_evaluacion.ID_PEC
from pec_preguntas  
left join pec_evaluacion_respuestas
  on pec_preguntas.id = pec_evaluacion_respuestas.id_pregunta
left join pec_evaluacion
  on pec_evaluacion_respuestas.id_evaluacion = pec_evaluacion.id
  where pec_preguntas.estado <> 'I';