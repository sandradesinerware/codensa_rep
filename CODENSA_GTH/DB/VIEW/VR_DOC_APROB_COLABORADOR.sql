
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."VR_DOC_APROB_COLABORADOR" ("EVALUADO", "ID", "ID_PERIODO", "PERIODO", "NOMBRE_PDI", "ID_ACCION", "TIPO_PDI", "FECHA_INICIO") AS 
  select
v_reporte_pdi.evaluado,
v_reporte_pdi.id,
id_periodo,
(select nombre from pdi_periodo where id=id_periodo) periodo,
nvl(curso,accion) nombre_pdi,
id_accion,
accion tipo_pdi,
fecha_inicio
from v_reporte_pdi
where id_estado=30;