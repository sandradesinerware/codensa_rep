
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."VR_GRAFICA_FORMACION_COLABORA" ("ID_ENCABEZADO", "CONTEO", "PROPORCION", "ESTADO_NEGOCIO") AS 
  Select 
Id_Encabezado,
Sum(Conteo) Conteo,
To_Char(Sum(Proporcion))||'%' Proporcion,
Estado_Negocio
From
(
SELECT 
ID_ENCABEZADO,
count(*) conteo,
round(nvl(count(*),0)/(select count(*) from vr_reporte_pdi where ID_ENCABEZADO = PDI.ID_ENCABEZADO
AND ID_ACCION= 57)*100) proporcion,
ESTADO_NEGOCIO
FROM VR_REPORTE_PDI PDI
WHERE ID_ACCION = 57
group by ID_ENCABEZADO,ESTADO_NEGOCIO
Union All
Select 
Id_Encabezado,
0 Conteo,
0 Proporcion,
'Pendiente' Estado_Negocio
From Vr_Reporte_Pdi Pdi
Where Id_Accion = 57
Union All
Select 
Id_Encabezado,
0 Conteo,
0 Proporcion,
'Realizado' Estado_Negocio
From Vr_Reporte_Pdi Pdi
Where Id_Accion = 57)
Group By Id_Encabezado,Estado_Negocio;