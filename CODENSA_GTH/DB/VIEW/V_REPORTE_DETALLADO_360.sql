
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_REPORTE_DETALLADO_360" ("FORMULARIO", "TIPO", "EVALUADO", "EVALUADOR", "ESTADO", "COMENTARIO", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19") AS 
  select 
FORMULARIO,
(select tipo from formulario_360 where id = formulario) tipo,
(select evaluado from formulario_360 where id = formulario) EVALUADO, 
(select evaluador from formulario_360 where id = formulario) EVALUADOR, 
(select estado_bars.nombre from formulario_360 inner join estado_bars on formulario_360.estado = estado_bars.id where formulario_360.id = formulario) estado,
COMENTARIO, "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"
from 
(
select RESPUESTA_360.formulario formulario, plantilla_preg_360.secuencia_pregunta plant_preg, valor, comentario 
from RESPUESTA_360 inner join FORMULARIO_360 on ( RESPUESTA_360.formulario = FORMULARIO_360.id )
 left join PLANTILLA_PREG_360 on ( RESPUESTA_360.plantilla_pregunta = PLANTILLA_PREG_360.id )
 left join COMENTARIO_360 on ( FORMULARIO_360.id = COMENTARIO_360.formulario )
where FORMULARIO_360.periodo = ( select MAX(ano) from PERIODO_BARS where activo_360 = 'S' )
)
pivot 
(
--TO DO: Hacer din�mico el conjunto de preguntas a la plantilla utilizada
sum(valor) for plant_preg in(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19)
) order by evaluado;