
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_DETALLE_PDI" ("ID", "ID_ENCABEZADO", "JUSTIFICACION", "FECHA_INICIO", "FECHA_FIN", "PROYECTO", "AREA", "NOMBRE_ACCION_LIBRE", "DESCRIPCION", "COMENTARIOS_GESTOR", "COMENTARIOS_RH", "NUEVO_CURSO", "ID_ESTADO", "Estado", "PERIODO", "EVALUADOR", "EVALUADO", "Acci�n", "Comportamiento", "Curso", "AUD_FECHA_CREACION", "AUD_FECHA_ACTUALIZACION", "AUD_CREADO_POR", "AUD_ACTUALIZADO_POR") AS 
  select
 bars_pdi.id,
 bars_pdi.id_encabezado,
 bars_pdi.justificacion,
 /*nvl(bars_pdi.logro_esperado, '-') logro_esperado,*/
 to_char(bars_pdi.fecha_inicio, 'YYYY-MM-DD') fecha_inicio,
 to_char(bars_pdi.fecha_fin, 'YYYY-MM-DD') fecha_fin,
 nvl(bars_pdi.proyecto, '-') proyecto,
 nvl(bars_pdi.area, '-') area,
 nvl(bars_pdi.nombre_accion_libre, '-') nombre_accion_libre,
 nvl(bars_pdi.descripcion, '-') descripcion,
 bars_pdi.comentarios_gestor,
 bars_pdi.comentarios_rh,
 nvl(bars_pdi.nuevo_curso, '-') nuevo_curso,
 bars_pdi.id_estado,
 estado_bars.nombre as "Estado",
 bars_encpdi.periodo,
 bars_encpdi.evaluador,
 bars_encpdi.evaluado,
 /*gestor.identificacion_nombre gestor,
 trabajador.identificacion_nombre trabajador, */
 t1.nombre as "Acci�n",
 t2.nombre as "Comportamiento", 
 bars_compdi.tema as "Curso",
 bars_pdi.AUD_FECHA_CREACION,
 bars_pdi.AUD_FECHA_ACTUALIZACION,
 bars_pdi.AUD_CREADO_POR,
 bars_pdi.AUD_ACTUALIZADO_POR
from bars_pdi
 inner join bars_encpdi
 on (bars_pdi.id_encabezado = bars_encpdi.id)
 /*left join v_trabajador gestor
 on (gestor.numero_identificacion = bars_encpdi.evaluador)
 left join v_trabajador trabajador
 on (trabajador.numero_identificacion = bars_encpdi.evaluado)*/
 left join tipo t1
 on (bars_pdi.id_accion = t1.id)
 left join estado_bars
 on (bars_pdi.id_estado = estado_bars.id)
 left join tipo t2
 on (bars_pdi.id_comportamiento = t2.id)
 left join bars_compdi
 on (bars_pdi.id_curso = bars_compdi.id);