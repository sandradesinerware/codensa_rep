
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_PEC_BASICO" ("ID", "TRABAJADOR_ASIGNADO", "TRABAJADOR_NOMBRES", "TRABAJADOR_APELLIDOS", "TRABAJADOR_FULL", "GESTOR_ASIGNADO", "GESTOR_NOMBRES", "GESTOR_APELLIDOS", "GESTOR_FULL", "PEC_ESTADO_ID", "PEC_ESTADO_NOMBRE", "FECHA_CREACION", "FECHA_PLANEACION", "FECHA_EJECUCION", "MOTIVO_CREACION") AS 
  SELECT 
PEC.id,
T.numero_identificacion trabajador_asignado,
T.nombres trabajador_nombres,
T.apellidos trabajador_apellidos,
T.apellidos||' '||T.nombres trabajador_full,
J.numero_identificacion gestor_asignado,
J.nombres gestor_nombres,
J.apellidos gestor_apellidos,
J.apellidos||' '||J.nombres gestor_full,
PEC.id_estado pec_estado_id,
(select nombre from TIPO where id = PEC.ID_ESTADO) pec_estado_nombre,
PEC.fecha_creacion,
( select fecha from PEC_ESTADO where id_pec = PEC.id and PEC_ESTADO.id_tipo_estado = SNW_CONSTANTES.constante_tipo('PEC_PLANEADO') ) fecha_planeacion,
( select fecha from PEC_ESTADO where id_pec = PEC.id and PEC_ESTADO.id_tipo_estado = SNW_CONSTANTES.constante_tipo('PEC_EJECUTADO') ) fecha_ejecucion,
(select nombre from tipo where id = PEC.motivo_creacion) motivo_Creacion
FROM PEC
inner join TRABAJADOR T on PEC.id_trabajador = T.numero_identificacion
left join TRABAJADOR J on PEC.id_gestor = J.numero_identificacion;