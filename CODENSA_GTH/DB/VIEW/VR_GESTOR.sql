
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."VR_GESTOR" ("TRABAJADOR", "EVALUADOR", "PERIODO") AS 
  select 
(select nombres||' '||apellidos from trabajador where numero_identificacion = evaluador) trabajador,
evaluador,
periodo
from bars_encpdi
group by periodo,evaluador;