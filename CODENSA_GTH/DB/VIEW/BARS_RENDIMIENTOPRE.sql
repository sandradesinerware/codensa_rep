
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."BARS_RENDIMIENTOPRE" ("NUMERO_IDENTIFICACION", "LINK_IDENTIFICACION", "NOMBRES", "APELLIDOS", "POSICION_INICIAL_ID", "POSICION_INICIAL_NOMBRE", "MESA", "POSICION_FINAL_ID", "POSICION_FINAL_NOMBRE") AS 
  (
select T.NUMERO_IDENTIFICACION,
T.NUMERO_IDENTIFICACION as LINK_IDENTIFICACION, 
T.NOMBRES,
T.APELLIDOS,
nvl(BP.id, 0) as posicion_inicial_id,
nvl(BP.nombre,'Sin definir') as posicion_inicial_nombre,
BTP.mesahompre as mesa,
nvl(BTP.pospre_final, 0) as posicion_final_id,
nvl(BPFINAL.nombre,'Sin definir') as posicion_final_nombre
from TRABAJADOR T
 inner join BARS_TRAEVAPRE BTP on (T.numero_identificacion = BTP.trabajador)
 left join BARS_FORMEVAPRE BF on (BTP.trabajador = BF.evaluado  and BTP.perpre = BF.periodo) /*Confirmar, pues al usar left join, un trabajador no evaluado cuenta para el c�lculo del total de la mesa, pero su posici�n ser�a 'Sin definir' porque no tendr�a */
 left join BARS_POSPRE BP on (BF.posicion = BP.id)
 left join BARS_POSPRE BPFINAL on (BTP.pospre_final = BPFINAL.id)
where
 BF.tipo = 'EVALUACION'
);