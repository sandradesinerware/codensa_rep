
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_ADMIN_PDIS_ENCS_CRUZADOS" ("PERIODO", "ID_PDI", "TIPO_PDI", "ID_ENCABEZADO", "EVALUADO", "NOMBRE_EVALUADO", "EVALUADOR", "NOMBRE_EVALUADOR", "EVALUADOR_ACTUAL", "NOMBRE_EVALUADOR_ACTUAL", "ID_CURSO", "ID_PDI_CURSO", "ID_GRUPO", "NOMBRE_GRUPO") AS 
  select     
        enc.periodo,
        pdi.id id_pdi,
        tipo.nombre tipo_pdi,
        enc.id id_encabezado,
        enc.evaluado,
        t.nombres || ' ' || t.apellidos nombre_evaluado,
        enc.evaluador,
        ( select ( nombres || ' ' || apellidos ) from trabajador where numero_identificacion = enc.evaluador ) nombre_evaluador,
        t.jefe evaluador_actual,
        ( select ( nombres || ' ' || apellidos ) from trabajador where numero_identificacion = t.jefe ) nombre_evaluador_actual,
        pdi.id_curso,
        NVL(to_char(pdi_grupo.id_pdi_curso), 'No PDI Formación') id_pdi_curso,
        NVL(to_char(pdi_grupo.id), 'No PDI Formación') id_grupo,
        NVL(pdi_grupo.nombre_grupo, 'No PDI Formación') nombre_grupo
    from bars_encpdi enc inner join trabajador t on (enc.evaluado = t.numero_identificacion)
    left join bars_pdi pdi on ( pdi.id_encabezado = enc.id )
    left join pdi_grupo on ( pdi_grupo.id = pdi.id_pdi_grupo )
    left join tipo on ( tipo.id = pdi.id_accion );