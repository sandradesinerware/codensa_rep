
  CREATE OR REPLACE FORCE VIEW "CODENSA_GTH"."V_ENCABEZADO_PDI" ("ID", "PERIODO", "DATOS_TRABA", "GESTOR", "CARGO", "GERENCIA", "UNIDAD_ORG", "FECHA_IMPRESION") AS 
  select
 bars_encpdi.id,
 pdi_periodo.nombre periodo,
 tra1.numero_identificacion||' '||tra1.apellidos||' '||tra1.nombres datos_traba,
 tra2.numero_identificacion||' '||tra2.apellidos||' '||tra2.nombres gestor,
 t1.nombre cargo,
 t2.nombre gerencia,
 t3.nombre unidad_org,
 sysdate fecha_impresion
from bars_encpdi 
left join pdi_periodo on (bars_encpdi.periodo = pdi_periodo.id)
left join trabajador tra1 on (bars_encpdi.evaluado=tra1.numero_identificacion)
left join trabajador tra2 on (bars_encpdi.evaluador=tra2.numero_identificacion)
left join tipo t1 on (bars_encpdi.id_cargo = t1.id and t1.tipo = SNW_CONSTANTES.constante_tipo('CARGO'))
left join tipo t2 on (bars_encpdi.id_gerencia = t2.id and t2.tipo = SNW_CONSTANTES.constante_tipo('GERENCIA'))
left join tipo t3 on (bars_encpdi.id_unidad_organizativa = t3.id and t3.tipo = SNW_CONSTANTES.constante_tipo('UNIDAD_ORGANIZATIVA'));