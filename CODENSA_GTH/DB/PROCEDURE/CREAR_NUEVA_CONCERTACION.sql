
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CREAR_NUEVA_CONCERTACION" (
    pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
    pano IN BARS_PEROBJ.ANO%type,
    pfecheva IN BARS_FORMEVAOBJ.FECHEVA%type,
    pformulario OUT BARS_FORMEVAOBJ.ID%type)
IS

 l_formulario_anterior NUMBER;
 l_consecutivo NUMBER;
 l_fecha_fin_periodo BARS_PEROBJ.fecha_fin%type;
 l_estado_nuevo NUMBER := 6; -- Estado NUEVO
 l_evaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 l_id_unidad_organizativa bars_formevaobj.id_unidad_organizativa%TYPE;
 l_id_gerencia bars_formevaobj.id_gerencia%TYPE; 
 l_id_target_opr bars_formevaobj.id_target_opr%type;
 l_cargo VARCHAR2(400);
 l_unidad VARCHAR2(400);
 l_fecha_inicio BARS_FORMEVAOBJ.fecha_inicio%type;
 l_fecha_fin BARS_FORMEVAOBJ.fecha_fin%type;


BEGIN

-- Partimos de recibir el evaluado y el periodo para crear una nueva concertacion en el mismo periodo para el mism otrabajador

 -- Se comenta, para que permita crear la concertacion aun en el periodo inactivo
 -- VAL_BARS_PEROBJ(pano, k_estado_activo, vconteo_filas, vmensaje);
 
 -- if (vconteo_filas = 1) then --El periodo es el activo.


 /* 
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'OBJETIVOS_BARS';

 
 
 */
     
     -- Llave primaria formulario a crear
     SELECT NVL( max(id),0 ) + 1 into pformulario
       from BARS_FORMEVAOBJ;

     --Datos del trabajador que determinan el formulario de objetivos
     SELECT jefe ,id_gerencia, id_unidad_organizativa, id_target_opr INTO l_evaluador, l_id_gerencia, l_id_unidad_organizativa, l_id_target_opr
       FROM TRABAJADOR
       WHERE numero_identificacion = pevaluado;

     -- Numero consecutivo correspondiente
     SELECT NVL(max(consecutivo),0) + 1 into l_consecutivo
       from BARS_FORMEVAOBJ 
       where evaluado = pevaluado and periodo = pano;

     -- Obtener la fecha fin del periodo recibido
     select fecha_fin into l_fecha_fin_periodo from BARS_PEROBJ where ANO = pano;

     -- C�lculo del cargo como nombre de la posicion vigente del evaluado y su Unidad vigente
     BEGIN
     select posicion, unidad_organizativa into l_cargo, l_unidad from snw_cargos.V_POSICION_UO
       where numero_identificacion = pevaluado;
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         l_cargo := 'NO DETERMINADO';
         l_unidad := 'NO DETERMINADA';
     END;
     
     -- Obtener el formulario con consecutivo anterior
     select id into l_formulario_anterior from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano and consecutivo = l_consecutivo - 1;

     -- Se actualiza la fecha fin del formulario anterior
     UPDATE bars_formevaobj set fecha_fin = trunc(sysdate) - 1 where id = l_formulario_anterior;

     -- Calculo de la fecha inicio y fecha fin
     -- Se asume que la primera concertaci�n que se crea, siempre va tener el rango completo del periodo
     l_fecha_inicio := trunc(sysdate);
     l_fecha_fin := l_fecha_fin_periodo;

       
     -- Creaci�n formulario en estado nuevo
      INSERT INTO BARS_FORMEVAOBJ (id, evaluado, periodo, estado, fecheva, cargo, nombre_cargo, cencos, zona, id_gerencia, id_unidad_organizativa, nombre_unidad_organizativa, id_target_opr, consecutivo, fecha_inicio, fecha_fin)
        values (pformulario, pevaluado, pano, l_estado_nuevo, pfecheva, null, l_cargo, null, null, l_id_gerencia, l_id_unidad_organizativa, l_unidad, l_id_target_opr, l_consecutivo, l_fecha_inicio, l_fecha_fin);

EXCEPTION
when no_data_found then
    RAISE_APPLICATION_ERROR (-20000,'Error de datos en CREAR_NUEVA_CONCERTACION.' || SQLCODE || ' - ' || SQLERRM);
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en CREAR_NUEVA_CONCERTACION.' || SQLCODE || ' - ' || SQLERRM);

END;
/