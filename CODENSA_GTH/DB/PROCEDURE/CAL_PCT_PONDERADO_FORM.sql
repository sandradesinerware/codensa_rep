
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_PCT_PONDERADO_FORM" 
(i_formulario IN BARS_FORMEVAOBJ.id%type, limite in NUMBER)
is
l_resultado_promedio_crudo NUMBER;
l_resultado_promedio_base100 NUMBER;
l_trabajador trabajador%rowtype;
l_periodo NUMBER;
begin

--Carga la informacion del trabajador
select trabajador.*  into l_trabajador 
from BARS_FORMEVAOBJ left join trabajador on (BARS_FORMEVAOBJ.evaluado=Trabajador.Numero_Identificacion) 
where BARS_FORMEVAOBJ.id_target_opr not in (64) and BARS_FORMEVAOBJ.id=i_formulario;--Si el trabajador no tiene un target opr asociado entonces no aplica.
select periodo into l_periodo from BARS_FORMEVAOBJ where id = i_formulario;

--Si el trabajador tiene un target opr asignado, se procede a calcular el porcentaje de consecucion teniendo como restriccion la ponderacion que no puede ser menos o mas del 80%
select sum( (ponderacion/100)*pctconsind ) into l_resultado_promedio_crudo
    from BARS_OBJETIVO
    where formevaobj = i_formulario;
l_resultado_promedio_base100 := l_resultado_promedio_crudo * (100/limite);

/*20200311 CODIGO ANTERIOR guardado porque no entiendo el objetivo de ese c�digo
--Si el trabajador tiene un target opr asignado, se procede a calcular el porcentaje de consecucion teniendo como restriccion la ponderacion que no puede ser menos o mas del 80%
select 
(sum(bars_objetivo.ponderacion*nvl(bars_objetivo.pctconsind,8))*100)/(100*sum(bars_objetivo.ponderacion)) into l_resultado_promedio_crudo
from BARS_FORMEVAOBJ right join BARS_OBJETIVO on (BARS_OBJETIVO.FORMEVAOBJ=BARS_FORMEVAOBJ.id) 
where BARS_FORMEVAOBJ.id=i_formulario
group by BARS_FORMEVAOBJ.id 
having sum(bars_objetivo.ponderacion) between (limite-0.1) and (limite+0.1);
*/

--actualiza el resultado en el formulario (en 2019 se evidenci� un problema y por eso el parche
if ( substr(l_periodo,4) < 2019 ) then
  update BARS_FORMEVAOBJ set Resultado_Ponderado = l_resultado_promedio_crudo where id = i_formulario;
end if;

exception
when no_data_found then
if(l_trabajador.numero_identificacion is not null) then --Si el trabajador no tiene un target opr asociado entonces no aplica.
raise_application_error(-20000,'La ponderacion de objetivos no alcanza o excede el '||limite||'% para el formulario de objetivos: '||i_formulario);
end if;
end;
/