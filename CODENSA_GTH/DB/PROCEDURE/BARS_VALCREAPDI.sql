
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALCREAPDI" (pperiodo IN PDI_PERIODO.id%type,  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo NUMBER;
 vestado_activo NUMBER;
BEGIN
 --Verificar estado del periodo
 SELECT count(*) into vconteo
  FROM PDI_PERIODO
  WHERE id = pperiodo and ESTADO = 1;

 IF vconteo = 1 then -- El periodo est� en estado ACTIVO
  pmensaje := 'Puede crear un nuevo PDI. El periodo est� activo. ';
  preturn := 1;
 ELSE
  pmensaje := 'El periodo est� inactivo. ';
  preturn := 0;
 END IF;

EXCEPTION
 WHEN OTHERS THEN
    RAISE_APPLICATION_ERROR(-20000, 'Un error inesperado ocurri� en BARS_VALCREAPDI. Por favor, inf�rmelo al administrador de la aplicaci�n. ' 
      || SQLCODE || ' - ' || SQLERRM);

END BARS_VALCREAPDI;

/