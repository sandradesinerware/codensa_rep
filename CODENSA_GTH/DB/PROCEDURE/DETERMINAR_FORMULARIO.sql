
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."DETERMINAR_FORMULARIO" 
(pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_BARS.TIPO%type, pformulario OUT FORMULARIO_BARS.ID%type)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vplantilla PLANTILLA_FORMULARIO.ID%TYPE;
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'FORMULARIO_BARS';

 l_id_cargo TRABAJADOR.ID_CARGO%TYPE;
 l_id_gerencia TRABAJADOR.ID_GERENCIA%TYPE;
 l_id_unidad_organizativa TRABAJADOR.ID_UNIDAD_ORGANIZATIVA%TYPE;
BEGIN
  VAL_ESTADO_PERIODO(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   VAL_ENCUESTA_EXISTE (pevaluado, pano, ptipo, vreturn, vmensaje);

   if (vreturn = 0) then --La evaluaci�n no existe. Crear formulario de acuerdo a la plantilla
     --Escogencia de la plantilla suponiendo que existe solo una para el colectivo (REQUIERE MEJORA EN SIGUIENTE VERSION)
     SELECT PLANTILLA_FORMULARIO.id into vplantilla
       from TRABAJADOR inner join tipo on (TRABAJADOR.ID_TARGET_BPR = tipo.id)
       inner join PLANTILLA_FORMULARIO on (tipo.id = PLANTILLA_FORMULARIO.TARGET_BPR)
       where TRABAJADOR.numero_identificacion = pevaluado;
     --Elecci�n del evaluador
     SELECT TRABAJADOR.jefe into vevaluador
       from TRABAJADOR
       where TRABAJADOR.NUMERO_IDENTIFICACION = pevaluado;
     --Verificaci�n existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = 'FORMULARIO_BARS'
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_formulario;
     if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_formulario and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_formulario );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     --Llave primaria formulario a crear
     SELECT NVL( max(id),0 )+1 into pformulario
       from FORMULARIO_BARS;

       select id_cargo, id_gerencia, id_unidad_organizativa into l_id_cargo, l_id_gerencia, l_id_unidad_organizativa 
       from trabajador where numero_identificacion in (pevaluado);

     --Creaci�n formulario en estado nuevo
     INSERT INTO FORMULARIO_BARS (id, plantilla, evaluador, evaluado, periodo, estado, fecha_evaluacion, tipo, id_cargo, id_gerencia, id_unidad_organizativa)
       values (pformulario, vplantilla, vevaluador, pevaluado, pano, vestado, trunc(sysdate), ptipo,  l_id_cargo, l_id_gerencia, l_id_unidad_organizativa);
   else --La encuesta existe, retornar formulario
     SELECT id into pformulario
       from FORMULARIO_BARS
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;

 else --El periodo no es el activo
   VAL_ENCUESTA_EXISTE (pevaluado, pano, ptipo, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluaci�n no existe. C�mo no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluaci�n existe
     SELECT id into pformulario
       from FORMULARIO_BARS
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;
 end if;
/*EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO.' || SQLCODE || ' - ' || SQLERRM);*/
END;

/