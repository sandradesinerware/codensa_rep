
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_PARAMETRIZA_PERIODO" 
(pano IN NUMBER)
AS
--Procedimiento para parametrizar automáticamente el periodo bars de un año a otro, si las condiciones se mantienen iguales
kestado_inactivo CONSTANT VARCHAR2(100) := 'INACTIVO';
ktipo_estado_periodo CONSTANT VARCHAR2(100) := 'PERIODO_BARS';
vestado_inactivo ESTADO_BARS.id%type;
id_pct NUMBER;
conteo NUMBER;

BEGIN

select ESTADO_BARS.id into vestado_inactivo
 from ESTADO_BARS
 where ESTADO_BARS.tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = ktipo_estado_periodo)
  and upper(ESTADO_BARS.nombre) = kestado_inactivo;

--PARAMETRIZACIÓN PARA EVALUACIÓN Y AUTOEVALUACIÓN

--periodo_bars se crea en estado inactivo si no existe
select count(*) into conteo
 from PERIODO_BARS
 where ano = pano;
if (conteo = 0) then
 insert into periodo_bars (ano, estado) values (pano, vestado_inactivo);
end if;

/* ! ! ! Se elimina: https://sinerware.atlassian.net/browse/GTH2019-81 ! ! ! 
    --PCT_CONSECUCION_BARS
    select count(*) into conteo
     from PCT_CONSECUCION_BARS
     where periodo = pano;
    
    if (conteo = 0) then
    insert into PCT_CONSECUCION_BARS(id, pct_consecucion,media_comp_inf,media_comp_sup,periodo)
      SELECT PCT_CONSECUCION_BARS_SEQ.NEXTVAL,pct_consecucion, media_comp_inf, media_comp_sup, pano
        FROM PCT_CONSECUCION_BARS
        WHERE periodo = (pano-1);
    end if;
*/

--AJUSTE_HOMOGENEIZACION (No implemtado aún)
select count(*) into conteo FROM BARS_AJUHOM
where periodo = pano;
 if (conteo = 0) then
insert into BARS_AJUHOM (id, valor, periodo)
 SELECT id+5, valor, pano
 FROM BARS_AJUHOM
 where periodo = (pano-1);
 end if; 

--bars_posicion

select count(*) into conteo
 from BARS_POSICION
 where periodo = pano;
 if (conteo = 0) then
insert into BARS_POSICION (id, nombre, pct_inf, pct_sup, periodo)
 SELECT id+3, nombre, pct_inf, pct_sup, pano
 from bars_posicion
 where periodo = (pano-1);
 end if;

--BARS_RESTRPOSICION
select count(*) into conteo
 from BARS_RESTRPOSICION
 where posicion in (select id from BARS_POSICION where periodo=pano);
 if (conteo = 0) then
insert into BARS_RESTRPOSICION (posicion, RESTRMIN, RESTRMAX)
 SELECT res.posicion+3, res.restrmin, res.restrmax
 from BARS_RESTRPOSICION res inner join BARS_POSICION pos on (res.posicion = pos.id)
 where pos.periodo = (pano-1);
 end if;
--BARS_TEXTORESPUESTA
select count(*) into conteo
 from BARS_TEXTORESPUESTA
 where periodo=pano;
 if (conteo = 0) then
insert into BARS_TEXTORESPUESTA (id, valorresp, nombre, periodo)
 SELECT id+5, valorresp, nombre, pano
 FROM BARS_TEXTORESPUESTA
 where periodo = (pano-1);
 end if;
 commit;
END BARS_PARAMETRIZA_PERIODO;
/