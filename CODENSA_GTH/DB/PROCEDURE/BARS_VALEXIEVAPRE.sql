
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALEXIEVAPRE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN BARS_PERPRE.ANO%type, ptipo IN BARS_FORMEVAPRE.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 select count(*) into vconteo_filas
   from BARS_FORMEVAPRE
   where evaluado = pevaluado and periodo = pano and ptipo = tipo;
 if vconteo_filas = 0 then --La evaluaci�n no existe
   preturn := 0;
   pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no tiene una evaluaci�n de predictores de potencial de tipo '
     || ptipo|| ' en el periodo ' || pano || '. ';
 else
   preturn := 1;
   pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n de predictores de potencial de tipo ' 
     || ptipo || ' en el periodo ' || pano || '. ';
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en BARS_VALEXIEVAPRE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;

/