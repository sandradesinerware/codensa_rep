
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BORRAR_BPR" (pusuario varchar2, pperiodo number, ptipo varchar2) as 
 lnumero_identificacion trabajador.numero_identificacion%type;
 lformulario number;
begin 
 /*Tipo puede ser EVALUACION O AUTOEVALUACION*/
 --select numero_identificacion into lnumero_identificacion from usuario where upper(user_name) = upper(pusuario);
 determinar_formulario(pusuario, pperiodo, UPPER(ptipo), lformulario);
 if lformulario is not null then
  delete respuesta where seccion_formulario in (select id from seccion_formulario where formulario = lformulario);
  delete seccion_formulario where formulario = lformulario;
  delete comentario_bars where formulario = lformulario;
  delete formulario_bars where id = lformulario;
 end if;
end;

/