
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."NUEVO_TIPO_REGIMEN" IS
l_tipo_regimen  varchar(100);
l_id_tipo_regimen  number;

BEGIN
---actualizar tipo de regimen de trabajadores  de acuerdo a datos en plantilla
    FOR i IN (SELECT ID, identificacion, tipo_regimen FROM TEMP_NUEVO_TIPO_REGIMEN)
    LOOP
    CASE
    WHEN i.tipo_regimen = 'Nuevo Convenio' THEN l_id_tipo_regimen:=75;
    WHEN i.tipo_regimen = 'R�gimen Integral' THEN l_id_tipo_regimen:= 73;
    WHEN i.tipo_regimen = 'Directivos Grupo' THEN l_id_tipo_regimen:= 74;
    ELSE NULL;
    END CASE;
    UPDATE trabajador
    SET id_tipo_regimen = l_id_tipo_regimen
    WHERE numero_identificacion = i.identificacion;
    END LOOP;
 ---actualizar tabla trabajadores   
    FOR i IN (SELECT ID, ID_TIPO_REGIMEN FROM TRABAJADOR)
    LOOP
    IF i.id_tipo_regimen = 1045 THEN  UPDATE trabajador
                                        SET id_tipo_regimen = 75
                                        WHERE id = i.id ;
    END IF;

    END LOOP;
END;

/