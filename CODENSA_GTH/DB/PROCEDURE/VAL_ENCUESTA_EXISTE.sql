
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ENCUESTA_EXISTE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN PERIODO_BARS.ANO%type, ptipo IN FORMULARIO_BARS.TIPO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 --Validar que el periodo no sea superior al a�o actual (REVISAR SI ES NECESARIO EN FASE II)
 if ( pano > extract( YEAR from sysdate ) ) then
   preturn := 0;
   pmensaje := 'Aun no existen encuestas para el periodo ' || pano || '. ';
 else --El periodo es igual o inferior al a�o actual
   select count(*) into vconteo_filas
     from FORMULARIO_BARS
     where evaluado = pevaluado and periodo = pano and ptipo = tipo;
   if vconteo_filas = 0 then --La evaluaci�n no existe
     preturn := 0;
     pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no tiene una evaluaci�n de tipo ' || ptipo || ' en el periodo '
       || pano || '. ';
   else
     preturn := 1;
     pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n de tipo ' || ptipo || ' en el periodo '
       || pano || '. ';
   end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_ENCUESTA_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;

/