
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."DETERMINAR_FORMULARIO_EVAL_OBJ" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,  pano IN BARS_PEROBJ.ANO%type, pformulario OUT BARS_FORMEVAOBJ.ID%type, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO'; k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO'; k_tipo_estado_formulario CONSTANT VARCHAR2(20) := 'OBJETIVOS_BARS'; vcargo TRABAJADOR.id_cargo%type;
 vcencos TRABAJADOR.cencos%type;
 vzona TRABAJADOR.zona%type;

/*DETERMINAR_FORMULARIO_EVAL_OBJ (APROBACION)
-Valida periodo activo
  SI ACTIVO
     -VAL_FORMULARIO_EXISTE
        SI NO EXISTE
             RETORNAR FALSO
        SI EXISTE
             RETORNAR FORMULARIO

  SI NO ACTIVO
      -VAL_FORMULARIO_EXISTE
        SI NO EXISTE
             RETORNAR FALSO
        SI EXISTE
             RETORNAR FORMULARIO
*/

BEGIN
 --Datos del trabajador que determinan el formulario de objetivos
 SELECT id_cargo, cencos, zona, jefe INTO vcargo, vcencos, vzona, vevaluador
   FROM TRABAJADOR
   WHERE numero_identificacion = pevaluado;

 VAL_BARS_PEROBJ(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);
   if (vreturn = 0) then --La evalución no existe.
     pmensaje := 'El formulario no existe. ' || vmensaje;     pformulario := 0;
   else --La evaluación existe, retornar formulario
     SELECT id into pformulario
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano and cargo = vcargo and cencos = vcencos 
         and NVL(zona,0) = NVL(vzona,0);
       pmensaje := 'El formulario existe. ' || vmensaje;   end if;

 else --El periodo no es el activo
   VAL_FORMULARIO_EXISTE (pevaluado, pano, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluación no existe. Como no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluación existe
     SELECT id into pformulario
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano and cargo = vcargo;
   end if;
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en DETERMINAR_FORMULARIO_EVAL_OBJ.' || SQLCODE || ' - ' || SQLERRM);END;

/