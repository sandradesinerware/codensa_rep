
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CARGUE_GRUPOS_PDI" AS 
 l_conteo_curso number; 
 l_id_negocio number; 
 l_tipo number; 
 l_id_facultad number; 
 l_foco_holding number;
 l_foco_interno number;
 l_conteo_grupo number; 
 l_id_curso number;
 l_estado_grupo number; 
 l_tipo_grupo number; 
 l_periodo number;
 l_jurisdiccion number;
 l_rol number;
 
 
BEGIN

                                             -- > > > CARGUE DE CURSOS < < < --
    
-- Se itera sobre cada grupo en la tabla temporal
for i in (SELECT * FROM TEMP_CARGUE_GRUPO) loop
    begin

        select count(*) into l_conteo_curso from pdi_curso where upper(nombre_curso) = upper(i.nombre_curso); 

        if l_conteo_curso = 0 then 

            l_id_negocio := i.id_negocio;

            select tipo into l_id_facultad from tipo where id = l_id_negocio and tipo in (
            select id from tipo where tipo = SNW_CONSTANTES.constante_tipo('FACULTAD'));
                        
            
            select id into l_tipo from tipo where tipo = SNW_CONSTANTES.constante_tipo('TIPO_CURSO_PDI') and upper(nombre) = upper(i.tipo);                             
            select id into l_foco_holding from PDI_FOCO where upper(nombre) = upper(i.foco_holding);            
            select id into l_foco_interno from PDI_FOCO where upper(nombre) = upper(i.foco_interno);        
                        
            /*if l_id_negocio is not null and l_tipo is not null and l_id_facultad is not null then
                insert into PDI_CURSO (id_encargado,id_negocio,nombre_curso,nombre_visible,objetivo,tipo,id_facultad,CALIFICABLE_ASIST,FOCO_HOLDING,FOCO_INTERNO,provedor,horas,no_pdi) 
                values (i.encargado,l_id_negocio,i.nombre_curso,i.nombre_visible,i.objetivo,l_tipo,l_id_facultad,'S',l_foco_holding,l_foco_interno,i.proveedor,i.horas,i.no_pdi); 
            end if; */

        end if; 

        exception when others then 
            dbms_output.put_line('Curso: ' || i.nombre_curso||' - '||i.nombre_grupo||' = = = > '||' - '||sqlerrm);             
        end; 
end loop; 

                                    -- > > > CARGUE DE GRUPOS < < < --


-- Se itera sobre cada grupo en la tabla temporal
for i in (SELECT * FROM TEMP_CARGUE_GRUPO) loop

    begin
    
    select id into l_id_curso from pdi_curso where upper(nombre_curso) = upper(i.nombre_curso); 
    select id into l_periodo from pdi_periodo where ano = i.periodo; 
    select count(*) into l_conteo_grupo from pdi_grupo where upper(nombre_grupo) = upper(i.nombre_grupo) and id_pdi_curso = l_id_curso and id_periodo = l_periodo;

    if l_conteo_grupo = 0 then
        
        select id into l_estado_grupo from tipo where upper(constante) = upper(i.estado); 
        
        l_jurisdiccion := null;
        if i.jurisdiccion is not null then
        
            select id into l_jurisdiccion from tipo where upper(nombre) = upper(i.jurisdiccion);                         
        end if;
        
        l_rol := null;
        if i.rol is not null then --  TODO: !! Gestor por ahora !!. Generalizar para mas roles
            l_rol := 14;
        end if;
        /*
        if l_id_curso is not null then
            
            insert into pdi_grupo (id_pdi_curso,visible,nombre_grupo,estado,tipo_grupo,cupo_maximo,id_periodo,fecha_inicio,fecha_fin,cupo_minimo,cupo_analisis,id_tipo_jurisdiccion,id_rol) 
            values (l_id_curso,trim(i.visible),i.nombre_grupo,l_estado_grupo,decode(i.tipo_grupo,'ABIERTO_GRUPO',
            SNW_CONSTANTES.constante_tipo('CONFORMADO_GRUPO'),SNW_CONSTANTES.constante_tipo('LISTA_ESPERA_GRUPO')),i.cupo_maximo,l_periodo,
            to_date(i.fecha_inicio,'mm/dd/yyyy'),to_date(i.fecha_fin,'mm/dd/yyyy'),i.cupo_minimo,i.cupo_analisis,l_jurisdiccion,l_rol); 
        end if; 
        */
        
    end if; 
    exception when others then 
        dbms_output.put_line('Grupo: ' || i.nombre_curso||' - '||i.nombre_grupo|| ' - ' || i.jurisdiccion ||' = = = > '||' - '||sqlerrm);             
    end; 
end loop; 
END CARGUE_GRUPOS_PDI;
/