
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_CREAMESAHOM" (
    pperiodo IN NUMBER)
IS
  /*Este procedimiento crea las mesas de homogeneizacion de evaluciones de
  actuaci�n (BARS) para un periodo dado,
  con base en una tabla temporal denominada BARS_TEMPCARGUEMESA con los datos,
  siguiendo la siguiente plantilla:
  consecutivo|id_trabajador(NUMBER)|nombres|apellidos|cargo|tipo_trabajador|
  area|id_gestor(NUMBER)|mesa. La tabla tendr� adicionalmente
  un campo resultado*/
  CURSOR ccargue
  IS
    SELECT
      *
    FROM
      BARS_TEMPCARGUEMESA
    ORDER BY
      CONSECUTIVO;
  vconteo                NUMBER;
  vidmesa                NUMBER;
  vidcargo               NUMBER;
  vidcargo_anterior      NUMBER;
  vidtipo_trabajador     NUMBER;
  vidarea                NUMBER;
  vidarea_anterior       NUMBER;
  vidgestor_anterior     NUMBER;
  trabajador_inexistente EXCEPTION;
BEGIN
  FOR reccargue IN ccargue
  LOOP
    BEGIN --Se necesita un bloque adicional, para que al encontrar que el
      -- trabajador no existe, se contin�e con el siguiente registro sin
      -- problema
      --Verificar si la mesa existe
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        BARS_MESAHOM
      WHERE
        periodo                    = pperiodo
      AND trim(upper(descripcion)) = trim(upper(reccargue.mesa));
      IF vconteo                   = 0 THEN --la mesa no existe as� que debe
        -- crearse
        SELECT
          NVL(MAX(id),0)+1
        INTO
          vidmesa
        FROM
          BARS_MESAHOM;
        INSERT
        INTO
          BARS_MESAHOM
          (
            id,
            periodo,
            descripcion
          )
          VALUES
          (
            vidmesa,
            pperiodo,
            trim(initcap(reccargue.mesa))
          );
      ELSE --la mesa s� existe, consultar su id
        SELECT
          id
        INTO
          vidmesa
        FROM
          BARS_MESAHOM
        WHERE
          periodo                    = pperiodo
        AND trim(upper(descripcion)) = trim(upper(reccargue.mesa));
      END IF;
      --Asociar el trabajador a la mesa para el periodo
      --Verificar si el trabajador existe
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        TRABAJADOR
      WHERE
        numero_identificacion = reccargue.id_trabajador;
      IF vconteo              = 0 THEN --el trabajador no existe. Se reporta en
        -- el log y se contin�a con el siguiente registro.
        raise trabajador_inexistente;
      END IF;
      --Asociar el trabajador a la mesa
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        BARS_MESAHOM_TRABAJADOR
      WHERE
        trabajador = reccargue.id_trabajador
      AND mesa     = vidmesa;
      IF vconteo   = 0 THEN --el trabajador no ha sido asociado as� que debe
        -- asociarse
        INSERT
        INTO
          BARS_MESAHOM_TRABAJADOR
          (
            trabajador,
            mesa
          )
          VALUES
          (
            reccargue.id_trabajador,
            vidmesa
          );
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'Trabajador cargado'
        WHERE
          consecutivo = reccargue.consecutivo;
      END IF;
/*      --Actualizar datos del trabajador
      --Cargo
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        CARGO
      WHERE
        trim(upper(nombre)) = trim(upper(reccargue.cargo));
      IF vconteo            = 0 THEN --el cargo no existe. No se crea
        -- autom�ticamente sino se reporta en el log la no coincidencia.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'El cargo reportado no existe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el cargo s� existe. Se procede a compararlo con el que tiene el
        -- trabajador (se toma el primero en caso que est� repetido)
        SELECT
          id
        INTO
          vidcargo
        FROM
          CARGO
        WHERE
          trim(upper(nombre)) = trim(upper(reccargue.cargo))
        AND rownum            = 1;
        SELECT
          cargo
        INTO
          vidcargo_anterior
        FROM
          TRABAJADOR
        WHERE
          numero_identificacion = reccargue.id_trabajador;
        IF vidcargo            != vidcargo_anterior THEN --Se debe actualizar
          -- el cargo
          UPDATE
            TRABAJADOR
          SET
            cargo = vidcargo
          WHERE
            numero_identificacion = reccargue.id_trabajador;
          UPDATE
            BARS_TEMPCARGUEMESA
          SET
            resultado = resultado
            || ' - '
            || 'Cargo actualizado. El anterior fue el id '
            || vidcargo_anterior
            || '.'
          WHERE
            consecutivo = reccargue.consecutivo;
        END IF;
      END IF;
      --�rea
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        AREA
      WHERE
        trim(upper(nombre)) = trim(upper(reccargue.area));
      IF vconteo            = 0 THEN --el �rea no existe. No se crea
        -- autom�ticamente sino se reporta en el log la no coincidencia.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'El �rea reportado no existe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el �rea s� existe. Se procede a compararla con el que tiene el
        -- trabajador
        SELECT
          id
        INTO
          vidarea
        FROM
          AREA
        WHERE
          trim(upper(nombre)) = trim(upper(reccargue.area));
        SELECT
          area
        INTO
          vidarea_anterior
        FROM
          TRABAJADOR
        WHERE
          numero_identificacion = reccargue.id_trabajador;
        IF vidarea             != vidarea_anterior THEN --Se debe actualizar el
          -- �rea
          UPDATE
            TRABAJADOR
          SET
            area = vidarea
          WHERE
            numero_identificacion = reccargue.id_trabajador;
          UPDATE
            BARS_TEMPCARGUEMESA
          SET
            resultado = resultado
            || ' - '
            || '�rea actualizada. La anterior fue el id '
            || vidarea_anterior
            || '.'
          WHERE
            consecutivo = reccargue.consecutivo;
        END IF;
      END IF;
      --tipo_trabajador
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        BARS_TIPO_TRABAJADOR
      WHERE
        trim(upper(nombre)) = trim(upper(reccargue.tipo_trabajador));
      IF vconteo            = 0 THEN --el tipo no existe. No se crea
        -- autom�ticamente sino se reporta en el log la no coincidencia.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'El tipo_trabajador reportado no existe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el tipo s� existe. Se procede a actualizarlo independiente de su
        -- correspondencia
        SELECT
          id_tipo_trabajador
        INTO
          vidtipo_trabajador
        FROM
          BARS_TIPO_TRABAJADOR
        WHERE
          trim(upper(nombre)) = trim(upper(reccargue.tipo_trabajador));
        UPDATE
          TRABAJADOR
        SET
          fk_tipo_trabajador = vidtipo_trabajador
        WHERE
          numero_identificacion = reccargue.id_trabajador;
      END IF;*/
      --jefe
      SELECT
        COUNT(*)
      INTO
        vconteo
      FROM
        TRABAJADOR
      WHERE
        numero_identificacion = reccargue.id_gestor;
      IF vconteo              = 0 THEN --el jefe no existe. Se reporta en el
        -- log.
        UPDATE
          BARS_TEMPCARGUEMESA
        SET
          resultado = resultado
          || ' - '
          || 'No existe el jefe.'
        WHERE
          consecutivo = reccargue.consecutivo;
      ELSE --el jefe si existe. Se verifica su correspondencia.
        SELECT
          jefe
        INTO
          vidgestor_anterior
        FROM
          TRABAJADOR
        WHERE
          numero_identificacion = reccargue.id_trabajador;
        IF reccargue.id_gestor != vidgestor_anterior THEN
          UPDATE
            BARS_TEMPCARGUEMESA
          SET
            resultado = resultado
            || ' - '
            || 'El jefe es diferente. El reportado es el id '
            || vidgestor_anterior
            || '.'
          WHERE
            consecutivo = reccargue.consecutivo;
        END IF;
      END IF;
    EXCEPTION --Para el manejo del trabajador inexistente sin terminar el loop
    WHEN TRABAJADOR_INEXISTENTE THEN
      UPDATE
        BARS_TEMPCARGUEMESA
      SET
        resultado = resultado
        || ' - '
        || 'No existe el trabajador.'
      WHERE
        consecutivo = reccargue.consecutivo;
    END;
  END LOOP;
EXCEPTION
WHEN OTHERS THEN
  RAISE_APPLICATION_ERROR(-20000,
  'Un error inesperado ocurri� en BARS_CREAMESAHOM. Por favor, inf�rmelo al administrador de la aplicaci�n. '
  || SQLCODE || ' - ' || SQLERRM);
END BARS_CREAMESAHOM;

/