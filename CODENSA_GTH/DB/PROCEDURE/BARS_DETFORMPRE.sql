
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_DETFORMPRE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,
  pano IN BARS_PERPRE.ANO%type, ptipo IN BARS_FORMEVAPRE.TIPO%type, pformulario OUT BARS_FORMEVAPRE.ID%type)
IS
 vconteo_filas NUMBER;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
 vevaluador TRABAJADOR.NUMERO_IDENTIFICACION%TYPE;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 k_estado_activo CONSTANT VARCHAR2(10) := 'ACTIVO';
 k_estado_nuevo CONSTANT VARCHAR2(10) := 'NUEVO';
 k_tipo_estado_predictores CONSTANT VARCHAR2(30) := 'PREDICTORES_POTENCIAL';
BEGIN
  BARS_VALESTPERPRE(pano, k_estado_activo, vconteo_filas, vmensaje);
 if (vconteo_filas = 1) then --El periodo es el activo.
   BARS_VALEXIEVAPRE (pevaluado, pano, ptipo, vreturn, vmensaje);

   if (vreturn = 0) then --La evaluaci�n no existe. Crear formulario (la plantilla se determina con base en el periodo)
     --Elecci�n del evaluador
     SELECT TRABAJADOR.jefe into vevaluador
       from TRABAJADOR
       where TRABAJADOR.NUMERO_IDENTIFICACION = pevaluado;
     --Verificaci�n existencia del estado 'NUEVO'
     --Verificar tipo_estado_bars = PREDICTORES_POTENCIAL
     select count(*) into vconteo_filas 
       from TIPO_ESTADO_BARS
       where upper(nombre) = k_tipo_estado_predictores;
     if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
       SELECT id into vtipo_estado
         from TIPO_ESTADO_BARS
         where upper(nombre) = k_tipo_estado_predictores and rownum = 1;
     else --El tipo_estado_bars no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vtipo_estado
         from TIPO_ESTADO_BARS;
       INSERT INTO TIPO_ESTADO_BARS (id, nombre)
         values ( vtipo_estado, k_tipo_estado_predictores );
     end if;
     --Verificar estado deseado
     SELECT count(*) into vconteo_filas
       from ESTADO_BARS
       where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo;
     if vconteo_filas >= 1 then --El estado 'NUEVO' existe
       SELECT id into vestado
         from ESTADO_BARS
         where tipo_estado = vtipo_estado and upper(nombre) = k_estado_nuevo and rownum = 1;
     else --El estado deseado no existe. Debe crearse.
       SELECT NVL( max(id),0 )+1 into vestado
         from ESTADO_BARS;
       INSERT INTO ESTADO_BARS (id, nombre, tipo_estado)
         values ( vestado, k_estado_nuevo, vtipo_estado );
     end if;
     --Llave primaria formulario a crear
     SELECT NVL( max(id),0 )+1 into pformulario
       from BARS_FORMEVAPRE;
     --Creaci�n formulario en estado nuevo
     INSERT INTO BARS_FORMEVAPRE (id, gestor, evaluado, periodo, estado, fecha_actualizacion, tipo)
       values (pformulario, vevaluador, pevaluado, pano, vestado, trunc(sysdate), ptipo);
   else --La encuesta existe, retornar formulario
     SELECT id into pformulario
       from BARS_FORMEVAPRE
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;

 else --El periodo no es el activo
   BARS_VALEXIEVAPRE (pevaluado, pano, ptipo, vreturn, vmensaje);
   if (vreturn = 0) then --La evaluaci�n no existe. C�mo no es periodo activo no se puede crear.
     pformulario := NULL;
   else --La evaluaci�n existe
     SELECT id into pformulario
       from BARS_FORMEVAPRE
       where evaluado = pevaluado and periodo = pano and tipo = ptipo;
   end if;
 end if;
EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000,'Error inesperado en BARS_DETFORMPRE.' || SQLCODE || ' - ' || SQLERRM);
END;

/