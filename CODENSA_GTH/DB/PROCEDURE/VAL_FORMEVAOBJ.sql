
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_FORMEVAOBJ" 
(
  i_formulario in bars_formevaobj.id%type, presultado in out NUMBER, pmsj in out VARCHAR2,
  ptipo in VARCHAR2 DEFAULT 'TOTAL'
) as 
l_formulario bars_formevaobj%rowtype;
esTargetT5 number; --0 No, Otro Si
kTipoCurvaOnOff CONSTANT VARCHAR2(100) := 'ON/OFF';
kTipoCurvaProject CONSTANT VARCHAR2(100) := '2 PROJECT';
vTipoCurva TIPO.nombre%type;
vTipoObjetivoAbierto BARS_OBJETIVO.id_tipo%type;
valPeso number;
valResultado number;
vPonderacionTotal number;

begin

select * into l_formulario from bars_formevaobj where bars_formevaobj.id=i_formulario;
--Obtener el id_tipo ABIERTO VIGENTE para hacer el filtro
select id into vTipoObjetivoAbierto
  FROM TIPO where tipo = ( select id from TIPO where upper(nombre) = 'TIPO_OBJETIVO' )
  and upper(nombre) = 'ABIERTO' and upper(estado) = 'A';
--Determinar si es targetT5
select count(*) into esTargetT5
  FROM TIPO 
    where id = l_formulario.id_target_opr and tipo = (
    SELECT id FROM TIPO WHERE UPPER(NOMBRE) = 'TARGET_OPR' ) and upper(nombre) IN ('T5', 'N/A');

presultado := 1; --Se empieza con la suposici�n que est�n correctos todos los objetivos

for l_bars_objetivo in (select * from bars_objetivo where formevaobj=i_formulario 
                        and id_tipo = CASE ptipo WHEN 'TOTAL' THEN id_tipo
                                                 WHEN 'ABIERTO' THEN vTipoObjetivoAbierto
                                                 ELSE id_tipo
                                      END)
loop

--Determinar tipo curva
BEGIN
select nombre into vTipoCurva
  FROM TIPO
    where id = l_bars_objetivo.id_tipo_curva;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    vTipoCurva := '';
END;
----Validaciones por objetivo---------/*

if ( esTargetT5=0 ) then --No es T5. 

  --El resultado depende del tipo curva. Si es OnOff -> (0,10) TODO: Parametrizar por Target. Como parche, se ajusta manual a 99
  if ( vTipoCurva = kTipoCurvaOnOff ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 99 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 99] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Si tipo de curva es Project -> {8,10} TODO: Parametrizar por Target. Como parche, se ajusta manual a 99
  elsif ( vTipoCurva = kTipoCurvaProject ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 99 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 99] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Otro tipo de curva -> [0,10] TODO: Parametrizar por Target. Como parche, se ajusta manual a 99
  else
      if ( l_bars_objetivo.PCTCONSIND between 0 and 99 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en el intervalo [0 - 99] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  end if;

else --Es T5

  --El resultado depende del tipo curva. Si es OnOff -> (0,10) 
  if ( vTipoCurva = kTipoCurvaOnOff ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 10 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Si tipo de curva es Project -> {8,10}
  elsif ( vTipoCurva = kTipoCurvaProject ) then
      if ( l_bars_objetivo.PCTCONSIND between 0 and 10 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en los posibles valores [0 - 10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  --Otro tipo de curva -> [0,10]
  else
      if ( l_bars_objetivo.PCTCONSIND between 0 and 10 ) then
        valResultado := 1;
      else
        valResultado := 0;
        pmsj := pmsj || 'El resultado debe estar en el intervalo [0 - 10] para el objetivo: ' || l_bars_objetivo.objnum || '. ';
      end if;
  end if;

end if;

----Validaciones por Objetivo---------*/

if ( valResultado = 0 ) then
    presultado := 0;
end if;

end loop;


EXCEPTION
  WHEN OTHERS THEN
    presultado := 0;
    pmsj := 'Error inesperado en VAL_FORMEVAOBJ. ' || SQLCODE || ' - ' || SQLERRM || ' ';

end val_formevaobj;

/