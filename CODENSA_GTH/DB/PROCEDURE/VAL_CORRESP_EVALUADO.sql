
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_CORRESP_EVALUADO" (pevaluador IN TRABAJADOR.numero_identificacion%type,
  pevaluado IN TRABAJADOR.numero_identificacion%type, pano IN PERIODO_BARS.ano%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
vconteo_filas NUMBER;
vmensaje VARCHAR2(4000);
BEGIN
 /*Ana determin� que para formularios antiguos, la edici�n la debe hacer el que actualmente figura como jefe
 --Determinaci�n si el periodo es el activo (o vigente)
 VAL_ESTADO_PERIODO (pano, 'ACTIVO', vconteo_filas, vmensaje);
 if (vconteo_filas = 0) then --El a�o no es el activo. Por tanto, la relaci�n debe buscarse    
                             --en el formulario del a�o suministrado.
   SELECT count(*) into vconteo_filas
     from FORMULARIO_BARS
     where periodo = pano and evaluador = pevaluador and evaluado = pevaluado;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
 else --El a�o es el activo. Por tanto, la relaci�n debe buscarse entre jefes y empleados.
 */
   SELECT count(*) into vconteo_filas
     from TRABAJADOR EMP left join TRABAJADOR JEFE on (EMP.jefe = JEFE.NUMERO_IDENTIFICACION)
     where EMP.NUMERO_IDENTIFICACION = pevaluado and JEFE.NUMERO_IDENTIFICACION = pevaluador;
   if vconteo_filas = 0 then --No hay correspondencia
     preturn := 0;
     pmensaje := 'No hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   else
     preturn := 1;
     pmensaje := 'Hay correspondencia entre el evaluado y el evaluador para el periodo evaluado. ';
   end if;
-- end if;
EXCEPTION
 WHEN NO_DATA_FOUND THEN
   preturn := 0;
   pmensaje := 'Faltan par�metros en la base de datos para verificar la correspondencia entre el evaluado y el evaluador. ';
 WHEN OTHERS THEN
   preturn := 0;
   pmensaje := 'Error inesperado. No se pudo verificar la correspondencia entre el evaluado y el evaluador. ' || SQLCODE
     || ' - ' || SQLERRM || ' ';
END;

/