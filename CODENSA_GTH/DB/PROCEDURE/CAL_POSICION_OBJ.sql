
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_POSICION_OBJ" 
(
  i_formulario in BARS_FORMEVAOBJ.id%type 
) as 
l_periodo BARS_FORMEVAOBJ.periodo%type;
l_conteo_filas number;
l_posicion varchar2(50);
l_resultado_ponderado NUMBER;
begin
--Primero calcula el ponderado a nivel de formulario.
--CAL_PCT_PONDERADO_FORM(i_formulario);

select periodo,resultado_ponderado into l_periodo, l_resultado_ponderado from BARS_FORMEVAOBJ where id = i_formulario;


  SELECT count(*) into l_conteo_filas
    from BARS_POSICION
    where tipo_id = 60 and periodo = l_periodo AND l_resultado_ponderado BETWEEN pct_inf AND pct_sup;

    if l_conteo_filas = 1 then --Se determinů la posicion  
    SELECT nombre into l_posicion
      from BARS_POSICION
      where tipo_id = 60 and periodo = l_periodo AND l_resultado_ponderado BETWEEN pct_inf AND pct_sup;

      update BARS_FORMEVAOBJ set posicion= l_posicion where id=i_formulario;

  else --No se determinů la posicion
    l_posicion := NULL;
  end if;



end cal_posicion_obj;

/