
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SET_GESTOR" 
(
  i_numero_identificacion in number 
, msg out varchar2 
) as 
l_counter NUMBER;
begin
select count(*) into l_counter from usu_grupo where usuario in (i_numero_identificacion) and grupo=14;
if(l_counter<1) then
insert into usu_grupo values ((select max(id)+1 from usu_grupo), 14, i_numero_identificacion);
msg:='El trabajador '||i_numero_identificacion||' ahora tiene perfil de gestor!.';
else
msg:='El trabajador '||i_numero_identificacion||' ya tenia perfil de gestor!.';
end if;

end set_gestor;

/