
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SET_AUTORIZACION" 
(
  i_numero_identificacion in number 
, i_nombre_grupo in varchar2
) as 
l_counter NUMBER;
l_id_grupo VARCHAR2(100);
l_msg VARCHAR2(200);

--Elecci�n del permiso
BEGIN
select id into l_id_grupo
 from GRUPO
 where upper(trim(nombre)) = upper(i_nombre_grupo);

--Registro del permiso
BEGIN
insert into usu_grupo (id, grupo, usuario) values (usu_grupo_seq.nextval, l_id_grupo, i_numero_identificacion);
l_msg:='El trabajador '||i_numero_identificacion||' ahora tiene perfil de ' || i_nombre_grupo;
DBMS_OUTPUT.PUT_LINE(l_msg);

EXCEPTION
 when DUP_VAL_ON_INDEX then 
   l_msg:='El usuario ya tiene el permiso';
   DBMS_OUTPUT.PUT_LINE(l_msg);
 when OTHERS then l_msg:='El usuario no existe';
   DBMS_OUTPUT.PUT_LINE(l_msg);
END;

EXCEPTION --No encontr� el permiso
 when NO_DATA_FOUND then 
   l_msg:='El permiso solicitado no existe';
   DBMS_OUTPUT.PUT_LINE(l_msg);
 when TOO_MANY_ROWS then 
   l_msg:='El permiso solicitado est� duplicado';
   DBMS_OUTPUT.PUT_LINE(l_msg);
END;

/