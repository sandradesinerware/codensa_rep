
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALESTFORMOBJ" (pformulario IN BARS_FORMEVAOBJ.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
BEGIN
 --Validar que el formulario esté en el estado solicitado
 SELECT count(*) into vconteo_filas
   from BARS_FORMEVAOBJ
   where id = pformulario and estado = ( SELECT id from ESTADO_BARS where upper(nombre) = upper(pestado)
     and tipo_estado = (SELECT id from TIPO_ESTADO_BARS where upper(nombre) = 'OBJETIVOS_BARS') );
 if vconteo_filas = 0 then
   preturn := 0;
   pmensaje := 'El formulario no se encuentra en el estado ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El formulario se encuentra en el estado ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar si el formulario se encuentra en el estado ' || pestado || '.
     Hay un problema de parametrización de estados. Contacte al administrador de la aplicación. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALESTFORMOBJ. Contacte al administrador
     de la aplicación. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;

/