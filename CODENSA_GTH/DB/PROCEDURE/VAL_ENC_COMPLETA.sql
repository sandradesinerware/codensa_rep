
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_ENC_COMPLETA" (pformulario IN FORMULARIO_BARS.ID%TYPE,
  preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vnum_preguntas NUMBER;
 vnum_respuestas NUMBER;
 CURSOR csecciones IS SELECT id, seccion from SECCION_FORMULARIO where formulario = pformulario;
 vreturn NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 SELECT COUNT(*) into vnum_preguntas
   from FORMULARIO_BARS inner join PLANTILLA_FORMULARIO on (FORMULARIO_BARS.PLANTILLA = PLANTILLA_FORMULARIO.ID)
   inner join PLANTILLA_PREGUNTAS on (PLANTILLA_FORMULARIO.ID = PLANTILLA_PREGUNTAS.plantilla_formulario)
   where FORMULARIO_BARS.ID = pformulario;
 SELECT COUNT(*) into vnum_respuestas
   from SECCION_FORMULARIO inner join RESPUESTA on (SECCION_FORMULARIO.ID = RESPUESTA.seccion_formulario)
   where SECCION_FORMULARIO.FORMULARIO = pformulario;
 if (vnum_preguntas = vnum_respuestas) then --Encuesta con todas las respuestas
   preturn := 1; --Preasignaci�n para verificar si VAL_MAX_NO_APLICA cambia su valor
   pmensaje := 'Evaluaci�n con todas las secciones diligenciadas. ';
   --Validar m�ximo de No Aplicas
   FOR c1 IN csecciones LOOP
     VAL_MAX_NO_APLICA (c1.id, vreturn, vmensaje);
     if (vreturn = 0) then --No satisface m�ximo n�mero de no aplicas
       preturn := 0;
       pmensaje := pmensaje || 'La secci�n ' || c1.seccion || ' no satisface n�mero m�ximo de "No Aplicas". ';
     end if;
   END LOOP;
 else --Encuesta incompleta en respuestas
   preturn := 0;
   pmensaje := 'Evaluaci�n incompleta. Faltan respuestas por contestar. ';
 end if;
EXCEPTION
 WHEN OTHERS THEN
   preturn := 0;
   pmensaje := pmensaje || 'Error inesperado en VAL_ENC_COMPLETA. ' || SQLCODE || ' - ' || SQLERRM || ' ';
END;

/