
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_ACTUALIZAR_CONSEC_CALIB" (idForm IN NUMBER) IS
    vAjusteCalibrado NUMBER;
    vTrabajador      NUMBER;
    vPeriodo         VARCHAR2(30);
    vPct             NUMBER;
    vIdMesa          NUMBER;
    vAjuste          NUMBER;
    vconteo_filas 	 NUMBER;
BEGIN

 select evaluado, periodo, pct_consecucion into vTrabajador,
vPeriodo, vPct from FORMULARIO_BARS where ID = idForm;

 --Verificar si el trabajador ya tiene mesa de homogeneización
 select count(*) into vconteo_filas
   from BARS_MESAHOM_TRABAJADOR bmt join BARS_MESAHOM bm on bmt.MESA = bm.ID
   where trabajador = vTrabajador and periodo = vPeriodo;
 if vconteo_filas != 0 then -- El trabajador tiene mesa de homogeneización
   select bm.ID, bmt.AJUSTE into vIdMesa, vAjuste from
     BARS_MESAHOM_TRABAJADOR bmt join BARS_MESAHOM bm on bmt.MESA = bm.ID
     where PERIODO = vPeriodo and TRABAJADOR = vTrabajador;

   update BARS_MESAHOM_TRABAJADOR set PCT_CALIBRADO =
     BARS_GET_NIVELES(vPct,vAjuste,vPeriodo) where TRABAJADOR = vTrabajador and MESA
     = vIdMesa and AJUSTE IS NOT NULL;
 end if;

EXCEPTION
WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_ACTUALIZAR_CONSEC_CALIB. ' || SQLCODE || ' - ' 
     || SQLERRM || ' ');
END;
/