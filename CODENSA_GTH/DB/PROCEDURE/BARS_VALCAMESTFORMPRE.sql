
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALCAMESTFORMPRE" (pformulario IN BARS_FORMEVAPRE.id%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 vestado_anterior VARCHAR2(50);
BEGIN
 --Obtener estado actual
 SELECT e.nombre into vestado_anterior
   from BARS_FORMEVAPRE f inner join ESTADO_BARS e on (f.estado = e.id)
   where f.id = pformulario;

 --Validar que el cambio sea a un estado anterior
 CASE upper(vestado_anterior)
   WHEN 'NUEVO' THEN --Imposible hacer el cambio a uno anterior
     preturn := 0;
	 pmensaje := 'No se puede realizar ning�n cambio de estado a un formulario en estado NUEVO. ';
   WHEN 'EN TRATAMIENTO' THEN --Solo a nuevo
     if upper(pestado) = 'NUEVO' then 
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO para dicho formulario. ';
	 end if;
   WHEN 'FACTORES FINALIZADOS' THEN --A nuevo o en tratamiento
     if upper(pestado) IN ('NUEVO','EN TRATAMIENTO') then
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO o EN TRATAMIENTO para dicho formulario. ';
	 end if;
   WHEN 'CERRADO' THEN --A nuevo, en tratamiento o factores finalizados
     if upper(pestado) IN ('NUEVO','EN TRATAMIENTO','FACTORES FINALIZADOS') then
	   preturn := 1;
	   pmensaje := 'Se puede realizar el cambio de estado del formulario. ';
	 else
	   preturn := 0;
	   pmensaje := 'No se puede realizar el cambio de estado del formulario. Solo es v�lido el cambio a NUEVO o EN TRATAMIENTO o FACTORES FINALIZADOS para dicho formulario. ';
	 end if;
   ELSE 
     preturn := 0;
	 pmensaje := 'No se puede realizar el cambio de estado del formulario. Su estado inicial es desconocido. ';
 END CASE;

EXCEPTION
 WHEN TOO_MANY_ROWS THEN
   preturn := 0;
   pmensaje := 'No se pudo validar el cambio de estado. Hay un problema de parametrizaci�n de estados. Contacte al administrador de la aplicaci�n. ';
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALCAMESTFORMPRE. Contacte al administrador
     de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM || ' ');
END;

/