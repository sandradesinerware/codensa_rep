
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_CAMESTPDI" (pformulario IN BARS_PDI.ID%TYPE, pestado IN ESTADO_BARS.NOMBRE%TYPE)
is
 vconteo_filas NUMBER;
 vtipo_estado TIPO_ESTADO_BARS.ID%TYPE;
 vestado ESTADO_BARS.ID%TYPE;
 faltan_parametros EXCEPTION;
 formulario_inexistente EXCEPTION;
 ktipoestadopdi CONSTANT VARCHAR2(30) := 'PDI';
BEGIN
  --Verificar que el formulario exista
  select count(*) into vconteo_filas
  from BARS_PDI
  where id = pformulario;
  if vconteo_filas = 0 then --formulario inexistente
    RAISE formulario_inexistente;
  else
    --Verificar tipo_estado_bars = 'PDI'
    select count(*) into vconteo_filas 
    from TIPO_ESTADO_BARS
    where upper(nombre) = ktipoestadopdi;
    if vconteo_filas >= 1 then --El tipo_estado_bars s� existe
      SELECT id into vtipo_estado
        from TIPO_ESTADO_BARS
        where upper(nombre) =  ktipoestadopdi and rownum = 1;
    else --El tipo_estado_bars no existe. Generar excepci�n
      RAISE faltan_parametros;
    end if;
    --Verificar estado deseado
    SELECT count(*) into vconteo_filas
      from ESTADO_BARS
      where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado);
    if vconteo_filas >= 1 then --El estado deseado existe
      SELECT id into vestado
        from ESTADO_BARS
        where tipo_estado = vtipo_estado and upper(nombre) = upper(pestado) and rownum = 1;
    else --El estado deseado no existe. Generar excepci�n
      RAISE faltan_parametros;
    end if;
    --Actualizar el estado del formulario  
    UPDATE BARS_PDI SET ID_estado = vestado where id = pformulario;
  end if;
EXCEPTION
  when faltan_parametros then
    raise_application_error(-20000,'Falta el estado deseado para el cambio de estado en BARS_CAMESTPDI. 
      Favor contactar al administrador del sistema. ');
  when formulario_inexistente then
    raise_application_error(-20000,'Falta el formulario para el cambio de estado en BARS_CAMESTPDI. 
      Favor contactar al administrador del sistema. ');
  when others then
    raise_application_error(-20000,'Un error inesperado ocurri� en BARS_CAMESTPDI. 
      Favor contactar al administrador del sistema. ' ||SQLCODE||' - '||SQLERRM || ' ');
END;

/