
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."SNW_SCRIPT_AUDITORIA_V1" (
    esquema      IN VARCHAR2 ,
    tabla_modelo IN VARCHAR2)
AS
  trigger_body     VARCHAR2(1000) := ' 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('|| chr(39)||'APP_USER'|| chr(39)||'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
:NEW.AUD_VERSION        := 1;  
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('|| chr(39)||'APP_USER'|| chr(39)||'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('|| chr(39)||'USERENV'|| chr(39)||','|| chr(39)||'IP_ADDRESS'|| chr(39)||');
:NEW.AUD_VERSION                := NVL(:OLD.AUD_VERSION,0)+1;
END;
/';

  -- script_resultado VARCHAR2(32766);
  
BEGIN
  --  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.
  --
  --    MODIFIED   (YYYY/MM/DD)
  --      atroncoso 2015-05-03 - Created
  --
  --    VERSION 1.0
  /* Procedimiento que crea una salida en dbms.output  las instrucciones necesarias para agregar las columnas de auditor�a
  a las tablas que no las tienen y agregarles un default. Para ejecutarlo ver las lineas de ejemplo al final del script*/
  
  --Forma de uso
  --BEGIN
  --snw_script_auditoria_v1(ESQUEMA =>'CODENSA_GTH', TABLA_MODELO => 'TIPO');
  --END;
  ----------------------------------------------------------------------------------------------------------------
  FOR l_table IN
  (SELECT table_name
  FROM all_tables
  WHERE table_name NOT LIKE '%APEX%'
  AND owner IN (esquema)
  )
  LOOP
    dbms_output.put_line('--***************** TABLA: ' || l_table.table_name);
    FOR model_table IN
    (SELECT table_name,
      column_name,
      DECODE(data_type,'VARCHAR2','VARCHAR2('
      ||data_length
      ||')',data_type) data_type,
      data_length
    FROM user_tab_columns
    WHERE table_name IN
      (SELECT object_name
      FROM user_objects
      WHERE object_type IN ('TABLE')
      AND object_name   IN (tabla_modelo)
      )
      AND column_name LIKE 'AUD_%'
    )
    LOOP
      DECLARE
        tabla_objetivo user_tab_columns%rowtype;
      BEGIN
        SELECT *
        INTO tabla_objetivo
        FROM user_tab_columns
        WHERE table_name IN
          (SELECT object_name
          FROM user_objects
          WHERE object_type IN ('TABLE')
          AND object_name   IN (l_table.table_name)
          )
        AND column_name = model_table.column_name
        AND data_type   = model_table.data_type;
      EXCEPTION
      WHEN no_data_found THEN
        dbms_output.put_line('--AGREGANDO COLUMNA: '||model_table.column_name);
        dbms_output.put_line('ALTER TABLE '||l_table.table_name||' ADD ( '||model_table.column_name||' '|| model_table.data_type||' );');
        IF model_table.column_name = 'AUD_FECHA_CREACION' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' SYSDATE);');
        END IF;
        IF model_table.column_name = 'AUD_CREADO_POR' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' ''HISTORICO'');');
        END IF;
        IF model_table.column_name = 'AUD_ACTUALIZADO_POR' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' ''HISTORICO'');');
        END IF;
        IF model_table.column_name = 'AUD_FECHA_ACTUALIZACION' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' SYSDATE);');
        END IF;
        IF model_table.column_name = 'AUD_TERMINAL_ACTUALIZACION' THEN
          dbms_output.put_line('ALTER TABLE '||l_table.table_name||' MODIFY ( '||model_table.column_name||' DEFAULT '|| ' ''IP_ADDRESS'');');
        END IF;
      END;
    END LOOP; --Fin del loop de model_table
    DECLARE
      counter NUMBER;
    BEGIN
      SELECT COUNT(*)
      INTO counter
      FROM user_source
      WHERE name IN
        (SELECT trigger_name
        FROM all_triggers
        WHERE owner    IN (esquema)
        AND table_name IN
          (SELECT table_name
          FROM user_tables
          WHERE table_name NOT LIKE '%APEX%'
          AND table_name IN (l_table.table_name)
          )
        )
      AND upper(text) LIKE '%AUD_%'
      GROUP BY name
      HAVING COUNT(*) > 0;
      dbms_output.put_line('-- Existe triggger de auditoria para : '||l_table.table_name);
      NULL;
    EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('--- Creando triggger de auditoria para : '||l_table.table_name);
      dbms_output.put_line('CREATE OR REPLACE TRIGGER BIU_AUD_'||l_table.table_name||' 
      BEFORE INSERT OR UPDATE ON '||l_table.table_name||' FOR EACH ROW'||trigger_body);
      --EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER BIU_AUD_'||l_table.table_name||' BEFORE INSERT OR UPDATE ON '||l_table.table_name||' FOR EACH ROW
      --'||trigger_body;
    END;
    -- Poblar columnas de auditoria
    dbms_output.put_line('update '||l_table.table_name || ' set AUD_CREADO_POR=''HISTORICO'', AUD_FECHA_CREACION=SYSDATE, AUD_ACTUALIZADO_POR=''HISTORICO'', AUD_FECHA_ACTUALIZACION=SYSDATE, AUD_TERMINAL_ACTUALIZACION=''IP_ADDRESS'', AUD_VERSION=1 ;');
    -- Dejarlas not null
    dbms_output.put_line('alter table '||l_table.table_name || ' modify (AUD_FECHA_CREACION not null, AUD_CREADO_POR NOT NULL, AUD_ACTUALIZADO_POR NOT NULL, AUD_FECHA_ACTUALIZACION NOT NULL, AUD_TERMINAL_ACTUALIZACION NOT NULL, AUD_VERSION NOT NULL);');
  END LOOP; --Fin del loop de todas las tablas
  dbms_output.put_line('commit; ');
END snw_script_auditoria_v1;
/