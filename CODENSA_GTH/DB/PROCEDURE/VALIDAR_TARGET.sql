
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VALIDAR_TARGET" (
    i_trabajador  IN NUMBER,
    i_nuevo_target  IN NUMBER,
    i_mensaje OUT VARCHAR2)
IS

  l_conteo           NUMBER;
  l_periodo          NUMBER;
  l_target           NUMBER;
  l_pct_obj          NUMBER;
  l_pct_total        NUMBER;
  l_tipo_evaluacion  NUMBER;

BEGIN

    select ID_TARGET_OPR into l_target from trabajador where NUMERO_IDENTIFICACION = i_trabajador;
    
    -- Si el target en BD no cambio, no es necesario hacer la validaci�n de OPR
    if l_target = i_nuevo_target then
        i_mensaje := 'El target no cambia !!';
        return;
    end if;
            
    -- Se buscan los formularios del trabajador para periodos activos, y que esten en uno de los 3 estados se�alados
    select count(*) into l_conteo from BARS_FORMEVAOBJ 
    where EVALUADO = i_trabajador and 
    periodo in (select ano from BARS_PEROBJ WHERE ESTADO = 1) and -- Periodo activo
    estado in 
        (select ID from estado_bars where TIPO_ESTADO = 3 AND -- Objetivos 
        NOMBRE in ('CONCERTACION VALIDADA','CONCERTACION APROBADA','RESULTADO VALIDADO'));


    if l_conteo = 0 then
        i_mensaje := 'NO se encuentran formularios que se afecten. �Target actualizado!';
        return; 
    end if;

    -- Se encontro uno o mas formularios para los periodos activos. Se itera sobre cada uno el an�lisis
    FOR formulario IN
        (select id from BARS_FORMEVAOBJ 
        where EVALUADO = i_trabajador and 
        periodo in (select ano from BARS_PEROBJ WHERE ESTADO = 1) and -- Periodo activo
        estado in 
            (select ID from estado_bars where TIPO_ESTADO = 3 AND -- Objetivos 
            NOMBRE in ('CONCERTACION VALIDADA','CONCERTACION APROBADA','RESULTADO VALIDADO')))
    LOOP
        
        i_mensaje := 'SI HAY Objetivos por analizar';
    
        -- Objetivos abiertos 1010        
       -- FOR abierto IN
       --     (select id from BARS_OBJETIVO where FORMEVAOBJ = formulario.id and ID_TIPO in (1010))
       -- LOOP
        
        
            -- Se realiza la validacion de los objetivos actuales con el nuevo target           
            select periodo into l_periodo from bars_formevaobj where id = formulario.id;
            
            --Calcular ponderado de la concertaci�n para el tipo de evaluaci�n (tipo de objetivo). 
            -- La funci�n SUM retorna valores independiente del NO_DATA_FOUND
            select nvl( sum( nvl(ponderacion,0) ) , 0  )/100 into l_pct_obj
            from bars_objetivo 
            where formevaobj = formulario.id and bars_objetivo.id_tipo = 1010;
            
            -- Calcular ponderaci�n requerida para el tipo de evaluaci�n
            l_tipo_evaluacion := SNW_CONSTANTES.constante_tipo('OPR_ABI');
                                                  
            l_pct_total := VALIDACIONES_GUI.calc_peso_requerido_evaluacion(l_periodo, i_nuevo_target, l_tipo_evaluacion);
            
            --Validar con margen de tolerancia de 0.01
              if ( (l_pct_obj-0.01) <= l_pct_total and (l_pct_obj+0.01) >= l_pct_total ) then
                return;
              else
                return;
              end if;
        
 

    END LOOP;


END VALIDAR_TARGET;
/