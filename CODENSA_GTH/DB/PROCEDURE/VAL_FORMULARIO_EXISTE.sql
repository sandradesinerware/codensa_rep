
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."VAL_FORMULARIO_EXISTE" (pevaluado IN TRABAJADOR.NUMERO_IDENTIFICACION%type,  pano IN BARS_PEROBJ.ANO%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vmensaje VARCHAR2(4000);
BEGIN
 --Validar que el periodo no sea superior al a�o actual (eliminar si se va a concertar anticipado)
 if ( SUBSTR(pano,1,4) > extract( YEAR from sysdate ) ) then   preturn := 0;
   pmensaje := 'Aun no existen concertaciones para el periodo ' || pano || '. '; else --El periodo es igual o inferior al a�o actual
   --Determinar si el periodo es el activo (se divide para que el mensaje sea m�s apropiado)
    VAL_PEROBJ (pano, 1, vconteo_filas, vmensaje);
   if (vconteo_filas = 0) then --El a�o no es el activo.
     select count(*) into vconteo_filas
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano;
     if vconteo_filas = 0 then --La evaluaci�n no existe
       preturn := 0;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no fue evaluado en el periodo: '         || pano || '.';     else
       preturn := 1;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' fue evaluado en el periodo '         || pano || '.';     end if;
   else --El a�o es el activo
     select count(*) into vconteo_filas
       from BARS_FORMEVAOBJ
       where evaluado = pevaluado and periodo = pano;
     if vconteo_filas = 0 then --La evaluaci�n no existe
       preturn := 0;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' no ha concertado en el periodo '         || pano || '.';     else
       preturn := 1;
       pmensaje := 'El trabajador identificado con el n�mero ' || pevaluado || ' tiene una evaluaci�n en el periodo '         || pano || '.';     end if;
   end if;
 end if;

EXCEPTION
  WHEN OTHERS THEN
    preturn := 0;
    pmensaje := 'Error inesperado en VAL_FORMULARIO_EXISTE. ' || SQLCODE || ' - ' || SQLERRM || ' ';END;

/