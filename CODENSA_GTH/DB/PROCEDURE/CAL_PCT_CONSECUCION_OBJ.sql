
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."CAL_PCT_CONSECUCION_OBJ" ( i_formulario in BARS_FORMEVAOBJ.id%type) as 
    l_periodo BARS_FORMEVAOBJ.periodo%type;
    l_conteo_filas number;
    l_pctconspond NUMBER;
begin


select periodo into l_periodo from BARS_FORMEVAOBJ where id = i_formulario;

for i in (select * from BARS_OBJETIVO where FORMEVAOBJ in (i_formulario) and
        id_tipo not in (select id_tipo from bars_objetivo where formevaobj=i_formulario and pctconsind 
                       is null and id_tipo in SNW_CONSTANTES.constante_tipo('CERRADO'))
    )
loop
  
    l_pctconspond := OBTENER_PORCENTAJE_CONSECUCION(i.PCTCONSIND);
        
    /* ! ! ! Se cambia por:  https://sinerware.atlassian.net/browse/GTH2019-81 ! ! !  -- Estado anterior:
   SELECT count(*) into l_conteo_filas
    from PCT_CONSECUCION_BARS
    where tipo_id = 60 and periodo = l_periodo AND i.PCTCONSIND BETWEEN media_comp_inf AND media_comp_sup;

    if l_conteo_filas = 1 then -- Se determinó el porcentaje de consecución     
    SELECT pct_consecucion into l_pctconspond
      from PCT_CONSECUCION_BARS
      where tipo_id = 60 and periodo = l_periodo AND i.PCTCONSIND BETWEEN media_comp_inf AND media_comp_sup;  
      
      
  else --No se determinó un porcentaje de consecución
  --Dbms_Output.Put_Line('   Porcentaje no determinado! :(');
    l_pctconspond := NULL;
  end if;
    */
    

  --Dbms_Output.Put_Line('   Persistiendo...');
  update BARS_OBJETIVO set pctconspond = l_pctconspond where id = i.id;

 
end loop;
end cal_pct_consecucion_obj;
/