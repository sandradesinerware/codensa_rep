
  CREATE OR REPLACE PROCEDURE "CODENSA_GTH"."BARS_VALESTPERPRE" (pano IN BARS_PERPRE.ano%type,
  pestado IN ESTADO_BARS.NOMBRE%type, preturn OUT NUMBER, pmensaje OUT VARCHAR2)
IS
 vconteo_filas NUMBER;
 vtipo_estado NUMBER;
 vestado NUMBER;
 knombre_tipo_estado CONSTANT VARCHAR2(20) := 'PERIODO_BARS';
BEGIN
 --Validar que exista tipo estado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from TIPO_ESTADO_BARS where upper(nombre) = knombre_tipo_estado;
 if vconteo_filas = 0 then --El tipo estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vtipo_estado
     from TIPO_ESTADO_BARS;
   INSERT INTO TIPO_ESTADO_BARS (id, nombre) VALUES (vtipo_estado, knombre_tipo_estado);
 else --El tipo estado est� una o m�s veces
   SELECT id into vtipo_estado
     from TIPO_ESTADO_BARS
     where upper(nombre) = knombre_tipo_estado and rownum = 1;
 end if;

 --Validar que exista el estado solicitado para PERIODO_BARS
 SELECT count(*) into vconteo_filas
   from ESTADO_BARS where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado;
 if vconteo_filas = 0 then --El estado no existe
   SELECT nvl( max(id), 0 ) + 1 into vestado
     from ESTADO_BARS;
   INSERT INTO ESTADO_BARS (id, nombre, tipo_estado) VALUES (vestado, upper(pestado), vtipo_estado);
 else --El estado existe una o m�s veces
   SELECT id into vestado
     from ESTADO_BARS
     where upper(nombre) = upper(pestado) and tipo_estado = vtipo_estado and rownum = 1;
 end if;

 --Validar que el periodo est� en el estado solicitado
 SELECT count(*) into vconteo_filas
   from BARS_PERPRE
   where ano = pano and estado = vestado;
 if vconteo_filas = 0 then --El periodo no est� en el estado suministrado
   preturn := 0;
   pmensaje := 'El periodo de evaluaci�n no est� en el estado: ' || pestado || '. ';
 else
   preturn := 1;
   pmensaje := 'El periodo de evaluaci�n est� en el estado: ' || pestado || '. ';
 end if;

EXCEPTION
 WHEN OTHERS THEN
   RAISE_APPLICATION_ERROR (-20000, 'Error inesperado en BARS_VALESTPERPRE. ' || SQLCODE || ' - '
     || SQLERRM);
END;

/