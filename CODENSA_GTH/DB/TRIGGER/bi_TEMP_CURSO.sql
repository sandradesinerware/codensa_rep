
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."bi_TEMP_CURSO" 
  BEFORE INSERT ON "CODENSA_GTH"."TEMP_CURSO"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "TEMP_CURSO_SEQ".nextval into :new."ID" from dual;
  end if;
end;


/
ALTER TRIGGER "CODENSA_GTH"."bi_TEMP_CURSO" ENABLE;