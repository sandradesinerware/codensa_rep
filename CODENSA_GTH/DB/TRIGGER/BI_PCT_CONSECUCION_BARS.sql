
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_PCT_CONSECUCION_BARS" 
  BEFORE INSERT ON "CODENSA_GTH"."PCT_CONSECUCION_BARS"
  REFERENCING FOR EACH ROW
  begin
      /* ! ! ! TODO: eliminaR: https://sinerware.atlassian.net/browse/GTH2019-81 ! ! ! */
      if (:NEW.ID is null) then
      select PCT_CONSECUCION_BARS_SEQ.nextval into :NEW.ID from dual;
      end if;
end;
/
ALTER TRIGGER "CODENSA_GTH"."BI_PCT_CONSECUCION_BARS" ENABLE;