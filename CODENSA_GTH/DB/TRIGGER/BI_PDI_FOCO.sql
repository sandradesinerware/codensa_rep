
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_PDI_FOCO" 
  BEFORE INSERT OR UPDATE ON "CODENSA_GTH"."PDI_FOCO"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select PDI_FOCO_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;

/
ALTER TRIGGER "CODENSA_GTH"."BI_PDI_FOCO" ENABLE;