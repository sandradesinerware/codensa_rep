
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_BARS_POSICION" 
   before insert on "CODENSA_GTH"."BARS_POSICION" 
   for each row 
begin  
   if inserting then 
      if :NEW."ID" is null then 
         select BARS_POSICION_SEQ.nextval into :NEW."ID" from dual; 
      end if; 
   end if; 
end;

/
ALTER TRIGGER "CODENSA_GTH"."BI_BARS_POSICION" ENABLE;