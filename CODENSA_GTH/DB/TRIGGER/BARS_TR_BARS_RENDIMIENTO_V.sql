
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BARS_TR_BARS_RENDIMIENTO_V" 
  INSTEAD OF UPDATE ON "CODENSA_GTH"."BARS_RENDIMIENTO"
  REFERENCING FOR EACH ROW
  declare
vpct_calibrado bars_mesahom_trabajador.pct_calibrado%type;
  begin
    vpct_calibrado:=bars_get_niveles(:new.actuacion,:new.ajuste_vga,v('P45_PERIODO')) ;
    if(abs(:new.ajuste_vga)>0 and :new.actuacion=vpct_calibrado) then null;
    else update bars_mesahom_trabajador set ajuste=:new.ajuste_vga,pct_calibrado=vpct_calibrado where trabajador=:new.numero_identificacion and mesa=:new.mesa;
    end if;
  end;


/
ALTER TRIGGER "CODENSA_GTH"."BARS_TR_BARS_RENDIMIENTO_V" ENABLE;