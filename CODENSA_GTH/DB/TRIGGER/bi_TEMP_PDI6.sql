
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."bi_TEMP_PDI6" 
  BEFORE INSERT ON "CODENSA_GTH"."TEMP_PDI6"
  REFERENCING FOR EACH ROW
  begin  
  if :new."ID" is null then
    select "TEMP_PDI6_SEQ".nextval into :new."ID" from dual;
  end if;
end;


/
ALTER TRIGGER "CODENSA_GTH"."bi_TEMP_PDI6" ENABLE;