
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_BARS_OBJETIVO" 
  BEFORE INSERT ON "CODENSA_GTH"."BARS_OBJETIVO"
  REFERENCING FOR EACH ROW
  declare 
   counter NUMBER;
  begin
    if(:NEW.ID is null) then
    SELECT BARS_OBJETIVO_SEQ.NEXTVAL into :NEW.ID from dual;
    end if;
    if(:NEW.OBJNUM is null) then

    select count(*) into counter from bars_objetivo where formevaobj=:NEW.FORMEVAOBJ;
    if(counter>=1) then
    select max(objnum)+1 into :NEW.OBJNUM from bars_objetivo where formevaobj=:NEW.FORMEVAOBJ;
    else
    :NEW.OBJNUM:=1;
    end if;

    end if;

end;


/
ALTER TRIGGER "CODENSA_GTH"."BI_BARS_OBJETIVO" ENABLE;