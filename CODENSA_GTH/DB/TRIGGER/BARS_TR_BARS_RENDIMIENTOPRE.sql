
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BARS_TR_BARS_RENDIMIENTOPRE" 
  INSTEAD OF UPDATE ON "CODENSA_GTH"."BARS_RENDIMIENTOPRE"
  REFERENCING FOR EACH ROW
  begin
update bars_traevapre set pospre_final=:new.posicion_final_id where trabajador=:new.numero_identificacion and mesahompre=:new.mesa;
end;


/
ALTER TRIGGER "CODENSA_GTH"."BARS_TR_BARS_RENDIMIENTOPRE" ENABLE;