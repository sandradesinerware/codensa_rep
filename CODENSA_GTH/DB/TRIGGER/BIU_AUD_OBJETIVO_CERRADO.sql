
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BIU_AUD_OBJETIVO_CERRADO" 
  BEFORE INSERT OR UPDATE ON "CODENSA_GTH"."OBJETIVO_CERRADO"
  REFERENCING FOR EACH ROW
  BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria y mantiene la secuencia en el ID   
----------------------------------------------------------------------------------------------------------------  
IF :NEW.ID is null then
select OBJETIVO_CERRADO_SEQ.nextval into :NEW.ID from dual; 
END IF;

IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;      
:NEW.AUD_VERSION        := 1;  
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
:NEW.AUD_VERSION                := NVL(:OLD.AUD_VERSION,0)+1;
END;


/
ALTER TRIGGER "CODENSA_GTH"."BIU_AUD_OBJETIVO_CERRADO" ENABLE;