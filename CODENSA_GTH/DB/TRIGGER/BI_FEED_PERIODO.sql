
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_FEED_PERIODO" 
  BEFORE INSERT ON "CODENSA_GTH"."FEED_PERIODO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "FEED_PERIODO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;


/
ALTER TRIGGER "CODENSA_GTH"."BI_FEED_PERIODO" ENABLE;