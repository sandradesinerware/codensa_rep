
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."RESTRICCION_RESULTADOS_OPR_TRG" 
BEFORE INSERT ON RESTRICCION_RESULTADOS_OPR 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT RESTRICCION_RESULTADOS_OPR_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;
/
ALTER TRIGGER "CODENSA_GTH"."RESTRICCION_RESULTADOS_OPR_TRG" ENABLE;