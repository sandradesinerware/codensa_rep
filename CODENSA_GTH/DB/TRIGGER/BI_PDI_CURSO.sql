
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_PDI_CURSO" 
  BEFORE INSERT ON "CODENSA_GTH"."PDI_CURSO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_CURSO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;


/
ALTER TRIGGER "CODENSA_GTH"."BI_PDI_CURSO" ENABLE;