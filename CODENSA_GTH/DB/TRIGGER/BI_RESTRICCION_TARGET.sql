
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_RESTRICCION_TARGET" 
  BEFORE INSERT OR UPDATE ON "CODENSA_GTH"."RESTRICCION_TARGET"
  REFERENCING FOR EACH ROW
  begin
BEGIN
IF :NEW.ID is null then
select RESTRICCION_TARGET_SEQ.nextval into :NEW.ID from dual; 
END IF;
END;
end;


/
ALTER TRIGGER "CODENSA_GTH"."BI_RESTRICCION_TARGET" ENABLE;