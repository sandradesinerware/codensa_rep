
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_PONDERACION_PREG_360" 
  BEFORE INSERT ON "CODENSA_GTH"."PONDERACION_PREG_360"
  REFERENCING FOR EACH ROW
  begin
if :NEW.ID is null then
select PONDERACION_PREG_360_SEQ.nextval into :NEW.ID from dual;
end if;
end;


/
ALTER TRIGGER "CODENSA_GTH"."BI_PONDERACION_PREG_360" ENABLE;