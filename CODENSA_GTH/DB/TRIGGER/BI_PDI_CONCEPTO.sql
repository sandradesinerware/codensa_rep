
  CREATE OR REPLACE TRIGGER "CODENSA_GTH"."BI_PDI_CONCEPTO" 
  BEFORE INSERT ON "CODENSA_GTH"."PDI_CONCEPTO"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "PDI_CONCEPTO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if; 
end;

/
ALTER TRIGGER "CODENSA_GTH"."BI_PDI_CONCEPTO" ENABLE;