
  CREATE TABLE "CODENSA_GTH"."RESPUESTA_EVALUACION" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"VALOR" NUMBER, 
	"COMENTARIO" VARCHAR2(4000), 
	"AUD_FECHA_CREACION" DATE, 
	"AUD_CREADO_POR" VARCHAR2(4000), 
	"AUD_FECHA_ACTUALIZACION" DATE, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(4000), 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(4000), 
	"EVALUACION" NUMBER NOT NULL ENABLE, 
	"PREGUNTA" NUMBER, 
	 CONSTRAINT "RESPUESTA_EVALUACION_PK" PRIMARY KEY ("ID") ENABLE
   ) ;