
  CREATE TABLE "CODENSA_GTH"."PDI_HISTORICO" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"NUMERO_IDENTIFICACION" NUMBER, 
	"NOMBRE_TRABAJADOR" VARCHAR2(500), 
	"CURSO" VARCHAR2(500), 
	"PERIODO" NUMBER, 
	 CONSTRAINT "PDI_HISTORICO_PK" PRIMARY KEY ("ID") ENABLE
   ) ;