
  CREATE TABLE "CODENSA_GTH"."FORMULARIO_360" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"PLANTILLA" NUMBER NOT NULL ENABLE, 
	"EVALUADOR" NUMBER NOT NULL ENABLE, 
	"EVALUADO" NUMBER NOT NULL ENABLE, 
	"PERIODO" NUMBER NOT NULL ENABLE, 
	"ESTADO" NUMBER NOT NULL ENABLE, 
	"PCT_CONSECUCION" NUMBER, 
	"FECHA_EVALUACION" DATE, 
	"NOEDITABLE" NUMBER, 
	"TIPO" VARCHAR2(15) NOT NULL ENABLE, 
	"POSICION" VARCHAR2(25), 
	"MEDIA_COMPORTAMIENTOS" NUMBER, 
	"MEDIA_NORMALIZADA" NUMBER, 
	"ID_CARGO" NUMBER, 
	"ID_GERENCIA" NUMBER, 
	"ID_UNIDAD_ORGANIZATIVA" NUMBER, 
	"AUD_FECHA_CREACION" DATE DEFAULT SYSDATE, 
	"AUD_FECHA_ACTUALIZACION" DATE DEFAULT SYSDATE, 
	"AUD_CREADO_POR" VARCHAR2(30) DEFAULT USER, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(30) DEFAULT USER, 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(40) DEFAULT 'IP_ADDRESS', 
	"ID_TARGET_BPR" NUMBER, 
	 CONSTRAINT "FORMULARIO_360_PK" PRIMARY KEY ("ID") ENABLE
   ) ;