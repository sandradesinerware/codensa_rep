
  CREATE TABLE "CODENSA_GTH"."PERIODO_BARS" 
   (	"ANO" NUMBER NOT NULL ENABLE, 
	"ESTADO" NUMBER NOT NULL ENABLE, 
	"ACTIVO_PDI" CHAR(1), 
	"NOMBRE" VARCHAR2(30), 
	"AUD_FECHA_CREACION" DATE DEFAULT SYSDATE, 
	"AUD_FECHA_ACTUALIZACION" DATE DEFAULT SYSDATE, 
	"AUD_CREADO_POR" VARCHAR2(30) DEFAULT USER, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(30) DEFAULT USER, 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(40) DEFAULT 'IP_ADDRESS', 
	"ACTIVO_360" CHAR(1), 
	"EDITAR_FORM_BARS" VARCHAR2(2) DEFAULT 'N', 
	 CONSTRAINT "PERIODO_BARS_PK" PRIMARY KEY ("ANO") ENABLE
   ) ;