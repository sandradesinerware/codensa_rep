
  CREATE TABLE "CODENSA_GTH"."ERROR_LOG" 
   (	"ERROR_ID" NUMBER(11,0) NOT NULL ENABLE, 
	"MESSAGE" CLOB, 
	"ADDITIONAL_INFO" CLOB, 
	"DISPLAY_LOCATION" VARCHAR2(40), 
	"PAGE_ITEM_NAME" VARCHAR2(255), 
	"REGION_ID" NUMBER, 
	"COLUMN_ALIAS" VARCHAR2(255), 
	"ROW_NUM" NUMBER, 
	"IS_INTERNAL_ERROR" VARCHAR2(1), 
	"APEX_ERROR_CODE" VARCHAR2(255), 
	"ORA_SQLCODE" NUMBER, 
	"ORA_SQLERRM" CLOB, 
	"ERROR_BACKTRACE" CLOB, 
	"COMPONENT_TYPE" VARCHAR2(30), 
	"COMPONENT_ID" NUMBER, 
	"APPLICATION_ID" NUMBER, 
	"APP_PAGE_ID" NUMBER, 
	"APP_USER" VARCHAR2(255), 
	"BROWSER_LANGUAGE" VARCHAR2(30), 
	"TIMESTAMP" DATE, 
	"COMPONENT_NAME" VARCHAR2(255), 
	"ASSOCIATION_TYPE" VARCHAR2(40), 
	"AUD_FECHA_CREACION" DATE DEFAULT SYSDATE, 
	"AUD_FECHA_ACTUALIZACION" DATE DEFAULT SYSDATE, 
	"AUD_CREADO_POR" VARCHAR2(30) DEFAULT USER, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(30) DEFAULT USER, 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(40) DEFAULT 'IP_ADDRESS', 
	 CONSTRAINT "ERROR_LOG_PK" PRIMARY KEY ("ERROR_ID") ENABLE
   ) ;