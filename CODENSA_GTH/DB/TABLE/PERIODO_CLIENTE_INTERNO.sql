
  CREATE TABLE "CODENSA_GTH"."PERIODO_CLIENTE_INTERNO" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"ANO" NUMBER, 
	"ESTADO" VARCHAR2(2), 
	"AUD_FECHA_CREACION" DATE, 
	"AUD_CREADO_POR" VARCHAR2(4000), 
	"AUD_FECHA_ACTUALIZACION" DATE, 
	"AUD_ACTUALIZADO_POR" VARCHAR2(4000), 
	"AUD_TERMINAL_ACTUALIZACION" VARCHAR2(4000), 
	"PERIODO" VARCHAR2(4000), 
	 CONSTRAINT "PERIODO_CLIENTE_INTERNO_PK" PRIMARY KEY ("ID") ENABLE
   ) ;