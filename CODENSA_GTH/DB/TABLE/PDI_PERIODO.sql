
  CREATE TABLE "CODENSA_GTH"."PDI_PERIODO" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"ANO" NUMBER, 
	"NOMBRE" VARCHAR2(500), 
	"ESTADO" NUMBER, 
	"FECHA_INICIO" DATE, 
	"FECHA_FIN" DATE, 
	"ASISTENCIA" NUMBER, 
	 CONSTRAINT "PDI_PERIODO_PK" PRIMARY KEY ("ID") ENABLE
   ) ;