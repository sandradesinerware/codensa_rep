
  CREATE TABLE "CODENSA_GTH"."PEC_ESTADO" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"ID_PEC" NUMBER, 
	"ID_TIPO_ESTADO" NUMBER, 
	"FECHA" DATE, 
	"ID_TRABAJADOR_RESPONSABLE" NUMBER, 
	 CONSTRAINT "PEC_ESTADO_PK" PRIMARY KEY ("ID") ENABLE
   ) ;