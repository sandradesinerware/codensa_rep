--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body ADMINISTRACION_PDI_PCK
--------------------------------------------------------


CREATE OR REPLACE
PACKAGE BODY PRUEBA_REPOSITORIO_PCK AS

function periodo_activo_default return number is
    id_periodo_activo number;
  begin
    select id into id_periodo_activo from (select id from pdi_periodo where estado = 1 order by fecha_fin desc) where rownum = 1;
    return id_periodo_activo;
    exception when NO_DATA_FOUND then 
         return NULL;    
  end periodo_activo_default;
  
  function periodo_maximo_default return number is
    id_periodo_maximo number;
    id_correccion number;
  begin
    select id into id_periodo_maximo from (select id from pdi_periodo order by ano desc) where rownum = 1;
    return id_periodo_maximo;
    exception when NO_DATA_FOUND then 
         return NULL;    
  end periodo_maximo_default; 
  
END PRUEBA_REPOSITORIO_PCK;
