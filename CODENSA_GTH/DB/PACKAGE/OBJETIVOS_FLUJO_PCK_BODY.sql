--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body OBJETIVOS_FLUJO_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."OBJETIVOS_FLUJO_PCK" AS

--Subprogramas privados para utilizaci�n de las APIs de condicionales de visualizaci�n de elementos

/*Determina el n�mero asignado del trabajador asociado al usuario autenticado*/
FUNCTION get_asignado_username(i_user_name IN VARCHAR2) RETURN NUMBER AS
  l_asignado        NUMBER;

BEGIN
  select numero_identificacion into l_asignado from V_TRABAJADOR where usuario = i_user_name;
  RETURN l_asignado;

  EXCEPTION
    when NO_DATA_FOUND then RETURN NULL;

END get_asignado_username;


/*Determina el n�mero asignado del jefe de un trabajador.*/
FUNCTION get_asignado_jefe(i_asignado_trabajador IN NUMBER) RETURN NUMBER AS
  l_asignado_jefe   NUMBER;

BEGIN
  select jefe into l_asignado_jefe
    from TRABAJADOR
    where numero_identificacion = i_asignado_trabajador;
  RETURN l_asignado_jefe;

  EXCEPTION
  when NO_DATA_FOUND then
    RETURN NULL;

END get_asignado_jefe;


/*  Determina el target opr del trabajador */
FUNCTION get_asignado_target(i_asignado_trabajador IN NUMBER) RETURN NUMBER AS
  l_id_target_opr   NUMBER;

BEGIN
  
  select ID_TARGET_OPR into l_id_target_opr 
    from trabajador 
    where NUMERO_IDENTIFICACION = i_asignado_trabajador;
    
  RETURN l_id_target_opr;

  EXCEPTION
  when NO_DATA_FOUND then
    RETURN NULL;

END get_asignado_target;


/* Determina el porcentaje de consecuci�n correspondiente a un resultao dado */
FUNCTION get_porcentaje_consecucion(i_resultado IN NUMBER) RETURN NUMBER AS
    l_porcentaje_consecucion NUMBER;
begin

    l_porcentaje_consecucion := 0;

    if (i_resultado >= 6) then    
        l_porcentaje_consecucion := (i_resultado * 10) + 20;
    end if;

    return ROUND(l_porcentaje_consecucion, 1);

END get_porcentaje_consecucion;



/* Determina el porcentaje total que deben cumplir los objetivos dado el periodo, el target del trabajador y el tipo de evaluaci�n (objetivos abiertos o cerrados) */
FUNCTION get_porcentaje_total(i_periodo IN NUMBER, i_target IN NUMBER, i_tipo_evaluacion IN NUMBER) RETURN NUMBER AS
    l_pct_total number;
    
    begin
        select PORCENTAJE_TOTAL into l_pct_total 
        from restriccion_target 
        where periodo = i_periodo and id_target = i_target and id_tipo_evaluacion = i_tipo_evaluacion;
    
        return l_pct_total;
    
    exception
    when no_data_found then
        RETURN NULL;

END get_porcentaje_total;


/*Determina si bars_formevaobj se encuentra en los estados adecuados para el tipo de acci�n posible al usuario.*/
FUNCTION es_estado(i_tipo IN VARCHAR2, i_formevaobj_row IN BARS_FORMEVAOBJ%ROWTYPE, o_msj_error OUT VARCHAR2)
  RETURN BOOLEAN AS
  l_estado      VARCHAR2(30);

BEGIN
  select nombre into l_estado from V_OBJETIVOS_ESTADOS where id = i_formevaobj_row.estado;
  
  CASE i_tipo
    when k_grabable then
      if ( l_estado IN (k_nuevo, k_et) ) then 
        RETURN TRUE;
      else
        o_msj_error := 'No se encuentra en uno de los estados permitidos para ello: '||k_nuevo||', '||k_et;
        RETURN FALSE;
      end if;
      
    when k_grabable_aprobable then
      if ( l_estado IN (k_nuevo, k_et, k_conval) ) then
        RETURN TRUE;
      else
        o_msj_error := 'No se encuentra en uno de los estados permitidos para ello: '||k_nuevo||', '||k_et||', '||k_conval;
        RETURN FALSE;
      end if;

    when k_grabable_resultados then
      if ( l_estado IN (k_conap) ) then
        RETURN TRUE;
      else
        o_msj_error := 'No se encuentra en uno de los estados permitidos para ello: '||k_conap;
        RETURN FALSE;
      end if;

     when k_grabable_res_evaluable then
      if ( l_estado IN (k_resval, k_conap) ) then
        RETURN TRUE;
      else
        o_msj_error := 'No se encuentra en uno de los estados permitidos para ello: '||k_resval||', '||k_conap;
        RETURN FALSE;
      end if;
      
    else RETURN NULL;
  END CASE;

  EXCEPTION
    when NO_DATA_FOUND then
      o_msj_error := 'No se pudo determinar qu� hacer con la concertaci�n de acuerdo al estado.';
      RETURN NULL;

END es_estado;


/*Determina si el usuario puede realizar la acci�n del escenario de inter�s (ej: crear editar objetivos).*/
FUNCTION es_usuario(i_tipo IN VARCHAR2, i_formevaobj_row IN BARS_FORMEVAOBJ%ROWTYPE, i_user_name IN VARCHAR2,
  o_msj_error OUT VARCHAR2) RETURN BOOLEAN AS
  l_asignado_usuario        NUMBER;
  
BEGIN
  l_asignado_usuario := get_asignado_username(i_user_name);
  
  CASE i_tipo
    when k_grabable then
      if ( ( i_formevaobj_row.evaluado = l_asignado_usuario ) --el usuario es el evaluado
            OR ( get_asignado_jefe(i_formevaobj_row.evaluado) = l_asignado_usuario ) --el usuario es el jefe del evaluado
            OR ( AUTH_FUNCTION(i_user_name, k_funcion_admin, k_aplicacion_auth) ) --el usuario es administrador de OPR
          ) then
        RETURN TRUE;
      
      else
        o_msj_error := 'El usuario solicitante no es uno de los permitidos para ello:
          El evaluado, su jefe o el administrador.';
        RETURN FALSE;
      end if;
            
    when k_grabable_validable then
      if ( ( i_formevaobj_row.evaluado = l_asignado_usuario ) --el usuario es el evaluado
         OR ( AUTH_FUNCTION(i_user_name, k_funcion_admin, k_aplicacion_auth) ) --el usuario es administrador de OPR
         ) then
        RETURN TRUE;
       
      else
        o_msj_error := 'El usuario solicitante no es uno de los permitidos para ello: El evaluado o el administrador.';
        RETURN FALSE;
      end if;
    
    when k_grabable_aprobable then
      if ( ( get_asignado_jefe(i_formevaobj_row.evaluado) = l_asignado_usuario ) --el usuario es el jefe del evaluado
         OR ( AUTH_FUNCTION(i_user_name, k_funcion_admin, k_aplicacion_auth) ) --el usuario es administrador de OPR
         ) then
        RETURN TRUE;
       
      else
        o_msj_error := 'El usuario solicitante no es uno de los permitidos para ello: El jefe del evaluado o el administrador.';
        RETURN FALSE;
      end if;

    when k_grabable_resultados then
      if ( ( get_asignado_jefe(i_formevaobj_row.evaluado) = l_asignado_usuario ) --el usuario es el jefe del evaluado
         OR ( AUTH_FUNCTION(i_user_name, k_funcion_admin, k_aplicacion_auth) ) --el usuario es administrador de OPR
         ) then
        RETURN TRUE;

      else
        o_msj_error := 'El usuario solicitante para evaluar resultados no es uno de los permitidos para ello: El jefe del evaluado o el administrador.';
        RETURN FALSE;
      end if;

      
    else RETURN NULL;
   END CASE;
   
   EXCEPTION
     when OTHERS then 
       o_msj_error := 'No se pudo determinar qu� hacer con la concertaci�n de acuerdo al usuario.';
       RETURN NULL;

END es_usuario;


/*Determina si en la fecha dada se puede grabar la concertaci�n */
FUNCTION es_fecha_concertable(i_formevaobj_row IN BARS_FORMEVAOBJ%ROWTYPE, i_fecha IN DATE DEFAULT SYSDATE,
  o_msj_error OUT VARCHAR2) RETURN BOOLEAN AS
  l_fecha_max_concertacion   DATE;
  
BEGIN
  select fecha_concertacion into l_fecha_max_concertacion from BARS_PEROBJ where ano = i_formevaobj_row.periodo;
  
  if ( trunc(i_fecha) <= l_fecha_max_concertacion ) then
    RETURN TRUE;
  
  else
    o_msj_error := 'Se super� la fecha m�xima de concertaci�n: '||to_char(l_fecha_max_concertacion,'DD/MM/YYYY');
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when NO_DATA_FOUND then 
      o_msj_error := 'No se pudo determinar qu� hacer con la concertaci�n de acuerdo a la fecha.';
      RETURN NULL;
  
END es_fecha_concertable;

/*Determina si en la fecha dada se pueden evaluar resultados */
FUNCTION es_fecha_evaluable(i_formevaobj_row IN BARS_FORMEVAOBJ%ROWTYPE, i_fecha IN DATE DEFAULT SYSDATE,
  o_msj_error OUT VARCHAR2) RETURN BOOLEAN AS
  l_fecha_max_aprobacion   DATE;
  
BEGIN
  select fecha_aprobacion into l_fecha_max_aprobacion from BARS_PEROBJ where ano = i_formevaobj_row.periodo;
  
  if ( trunc(i_fecha) <= l_fecha_max_aprobacion ) then
    RETURN TRUE;
  
  else
    o_msj_error := 'Se super� la fecha m�xima de aprobaci�n: '||to_char(l_fecha_max_aprobacion,'DD/MM/YYYY');
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when NO_DATA_FOUND then 
      o_msj_error := 'No se pudo determinar qu� hacer con la aprobaci�n de acuerdo a la fecha.';
      RETURN NULL;
  
END es_fecha_evaluable;


/*Determina si el periodo de objetivos est� activo.*/
FUNCTION es_periodo_activo(i_formevaobj_id IN NUMBER) RETURN BOOLEAN AS
  l_estado      VARCHAR2(30);
  
BEGIN
  select e.nombre into l_estado 
    from BARS_PEROBJ p inner join ESTADO_BARS e on p.estado = e.id
    where p.ano = (select periodo from BARS_FORMEVAOBJ where id = i_formevaobj_id);
  
  if ( l_estado = 'ACTIVO' ) then
    RETURN TRUE;
    
  else
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when NO_DATA_FOUND then RETURN null;
  
END es_periodo_activo;



function es_valido_resultados(i_objetivo IN BARS_OBJETIVO.id%type, i_resultado IN BARS_OBJETIVO.id%type) RETURN VARCHAR2 AS
  
  l_target NUMBER;
  l_periodo NUMBER;
  l_curva NUMBER;
  l_EVALUADO NUMBER;
  
  l_min NUMBER;
  l_max NUMBER;
  
  l_conteo NUMBER;
  
  BEGIN
  
    -- Se busca el registro en RESTRICCION_RESULTADOS_OPR que contenga la terna (target,curva,periodo) dado el objetivo i_objetivo
    select id_tipo_curva into l_curva from BARS_OBJETIVO where id = i_objetivo;
    
    select periodo into l_periodo from BARS_FORMEVAOBJ where id = (select FORMEVAOBJ from BARS_OBJETIVO where id = i_objetivo);
    
     -- Se obtiene el TARGET_OPR del trabajador (ya no del formulario)
    select EVALUADO into l_evaluado from bars_formevaobj where id = (select FORMEVAOBJ from BARS_OBJETIVO where id = i_objetivo);
    select ID_TARGET_OPR into l_target from trabajador where NUMERO_IDENTIFICACION = l_evaluado;
        
    
    -- Se verifica cuantos registros en la tabla param�trica
    select count(*) into l_conteo from RESTRICCION_RESULTADOS_OPR where periodo = l_periodo and tipo_target = l_target and tipo_curva = l_curva;
    
    
        
    -- Se encuentran dos o mas parametrizaciones para la misma terna (curva,target,periodo)  
    if l_conteo > 1  then
        return 'Existen dos o m�s parametrizaciones para este objetivo, por favor comunicarse con el administrador OPR';
    
    -- No se encontr� ninguna parametrizaci�n para la terna (curva,target,periodo)      
    elsif l_conteo = 0 then
        return 'No existe ninguna parametrizaci�n para este objetivo, por favor comunicarse con el administrador OPR';
        
    -- Existe una sola parametrizaci�n, se procede con validar el rango minimo y maximo de resultados    
    else 
    
        select RESULTADO_MINIMO into l_min from RESTRICCION_RESULTADOS_OPR where periodo = l_periodo and tipo_target = l_target and tipo_curva = l_curva;
        select RESULTADO_MAXIMO into l_max from RESTRICCION_RESULTADOS_OPR where periodo = l_periodo and tipo_target = l_target and tipo_curva = l_curva;
        
        if i_resultado < l_min or i_resultado > l_max then
            return 'El resultado no es v�lido. Debe estar en el rango: ' || l_min || ' - ' || l_max;
        end if;
        
        return null;
    end if;
    
    END es_valido_resultados;



/* Determina si los objetivos abiertos cumplen con el peso ponderado para los resultados y finalizar el formulario */
FUNCTION es_valido_obj_abiertos(i_formevaobj_id IN NUMBER, o_msj_error IN OUT VARCHAR2) RETURN BOOLEAN AS
  
  l_evaluado number;
  l_periodo number;
  l_target_opr number;
  l_porcentaje_total number;
  l_porcentaje_abiertos number;
  l_tipo_abiertos number := SNW_CONSTANTES.constante_tipo('ABIERTO');
  l_tipo_evaluacion number := SNW_CONSTANTES.constante_tipo('OPR_ABI');
  
  
BEGIN

    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = i_formevaobj_id;
    
    l_target_opr := get_asignado_target(l_evaluado);    

    --Calcular ponderado de la concertaci�n para el tipo de evaluaci�n (tipo de objetivo). La funci�n SUM retorna valores independiente del NO_DATA_FOUND
    select nvl( sum( nvl(ponderacion,0) ) , 0  )/100 into l_porcentaje_abiertos
     from bars_objetivo 
     where bars_objetivo.formevaobj = i_formevaobj_id and bars_objetivo.id_tipo = l_tipo_abiertos;
                                                                                   
    l_porcentaje_total := get_porcentaje_total(l_periodo, l_target_opr, l_tipo_evaluacion);

  --Validar con margen de tolerancia de 0.01
  if ( (l_porcentaje_abiertos - 0.01) <= l_porcentaje_total and (l_porcentaje_abiertos + 0.01) >= l_porcentaje_total ) then
   return true;
  else
   return false;
  end if;
  
  
  EXCEPTION
    when NO_DATA_FOUND then 
      o_msj_error := 'No se pudo determinar la valides de los resultados de objetivos abiertos';
      RETURN NULL;
  
END es_valido_obj_abiertos;


/* Determina si los objetivos cerrados cumplen con el peso ponderado para los resultados y finalizar el formulario */
FUNCTION es_valido_obj_cerrados(i_formevaobj_id IN NUMBER, o_msj_error IN OUT VARCHAR2) RETURN BOOLEAN AS
  
  l_evaluado number;
  l_periodo number;
  l_target_opr number;
  l_porcentaje_total number;
  l_porcentaje_cerrados number;
  l_tipo_abiertos number := SNW_CONSTANTES.constante_tipo('CERRADO');
  l_tipo_evaluacion number := SNW_CONSTANTES.constante_tipo('OPR_CER'); 
  
BEGIN

    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = i_formevaobj_id;
    
    l_target_opr := get_asignado_target(l_evaluado);    
        
    -- Calcular ponderado de la concertaci�n para el tipo de evaluaci�n (tipo de objetivo). La funci�n SUM retorna valores independiente del NO_DATA_FOUND
    select nvl( sum( nvl(ponderacion,0) ) , 0  )/100 into l_porcentaje_cerrados
     from bars_objetivo 
     where bars_objetivo.formevaobj = i_formevaobj_id and bars_objetivo.id_tipo = l_tipo_abiertos;
                                                                                   
    l_porcentaje_total := get_porcentaje_total(l_periodo, l_target_opr, l_tipo_evaluacion);

  --Validar con margen de tolerancia de 0.01
  if ( (l_porcentaje_cerrados - 0.01) <= l_porcentaje_total and (l_porcentaje_cerrados + 0.01) >= l_porcentaje_total ) then
   return true;
  else
   return false;
  end if;
  
  
  EXCEPTION
    when NO_DATA_FOUND then 
      o_msj_error := 'No se pudo determinar la valides de los resultados de objetivos cerrados';
      RETURN NULL;
  
END es_valido_obj_cerrados;




/*Determina si bars_formevaobj es grabable (crearle o editarle objetivos) y por tanto 
se pueden mostrar los elementos de aplicaci�n que lo permiten, o se pueden ejecutar las acciones de grabar objetivos.*/
FUNCTION ES_GRABABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN AS
  l_formevaobj_row  BARS_FORMEVAOBJ%ROWTYPE;
  l_msj_error1      VARCHAR2(1000);
  l_msj_error2      VARCHAR2(1000);
  l_msj_error3      VARCHAR2(1000);
  
BEGIN
  select * into l_formevaobj_row from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  if (    ( es_estado(k_grabable, l_formevaobj_row, l_msj_error1) )
      and ( es_usuario(k_grabable, l_formevaobj_row, i_user_name, l_msj_error2) ) 
      and ( es_fecha_concertable(l_formevaobj_row, i_fecha, l_msj_error3) )
      and ( es_periodo_activo(l_formevaobj_row.id) )
     ) then
     o_msj_exito := 'La concertaci�n se puede grabar.';
     RETURN TRUE;

  else 
    o_msj_error := 'La concertaci�n no se puede grabar por el(los) siguiente(s) motivo(s): '||
      l_msj_error1||'; '||l_msj_error2||'; '||l_msj_error3;
    RETURN FALSE;
  end if;
  
  /*EXCEPTION
    when OTHERS then 
      raise_application_error(-20000, 'Un error inesperado ocurri� en OBJETIVOS_FLUJO_PCK.ES_GRABABLE.
        Por favor, inf�rmelo al administrador de la aplicaci�n. '|| sqlcode ||' - '|| sqlerrm);
    */
END ES_GRABABLE;


/*Determina si bars_formevaobj es grabable y validable (no que sea v�lida la concertaci�n sino si se puede intentar validar)
y por tanto se pueden mostrar los elementos de aplicaci�n que lo permiten, o se pueden ejecutar las acciones de grabar y validar
concertaci�n (por un asunto de roles, grabar puede hacerlo el gestor, pero validar no (para que no le salgan tantos botones)*/
FUNCTION ES_GRABABLE_VALIDABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN AS
  l_formevaobj_row  BARS_FORMEVAOBJ%ROWTYPE;
  l_msj_error1      VARCHAR2(1000);
  l_msj_error2      VARCHAR2(1000);
  l_msj_error3      VARCHAR2(1000);

BEGIN
  select * into l_formevaobj_row from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  if (    ( es_estado(k_grabable, l_formevaobj_row, l_msj_error1) )
      and ( es_usuario(k_grabable_validable, l_formevaobj_row, i_user_name, l_msj_error2) ) 
      and ( es_fecha_concertable(l_formevaobj_row, i_fecha, l_msj_error3) )
      and ( es_periodo_activo(l_formevaobj_row.id) )
     ) then
     o_msj_exito := 'La concertaci�n se puede grabar y validar.';
     RETURN TRUE;

  else 
    o_msj_error := 'La concertaci�n no se puede grabar o validar por el(los) siguiente(s) motivo(s): '||
      l_msj_error1||'; '||l_msj_error2||'; '||l_msj_error3;
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when OTHERS then 
      raise_application_error(-20000, 'Un error inesperado ocurri� en OBJETIVOS_FLUJO_PCK.ES_GRABABLE_VALIDABLE.
        Por favor, inf�rmelo al administrador de la aplicaci�n. '|| sqlcode ||' - '|| sqlerrm);
    
END ES_GRABABLE_VALIDABLE;



/*Determina si bars_formevaobj es grabable y aprobable (no que sea aprobada la concertaci�n sino si se puede intentar aprobar) 
y por tanto se pueden mostrar los elementos de aplicaci�n que lo permiten, o se pueden ejecutar las acciones de grabar y aprobar
concertaci�n*/
FUNCTION ES_GRABABLE_APROBABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN AS
  l_formevaobj_row  BARS_FORMEVAOBJ%ROWTYPE;
  l_msj_error1      VARCHAR2(1000);
  l_msj_error2      VARCHAR2(1000);
  l_msj_error3      VARCHAR2(1000);

BEGIN
  select * into l_formevaobj_row from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  if (    ( es_estado(k_grabable_aprobable, l_formevaobj_row, l_msj_error1) )
      and ( es_usuario(k_grabable_aprobable, l_formevaobj_row, i_user_name, l_msj_error2) ) 
      and ( es_fecha_concertable(l_formevaobj_row, i_fecha, l_msj_error3) )
      and ( es_periodo_activo(l_formevaobj_row.id) )
     ) then
     o_msj_exito := 'La concertaci�n se puede grabar y aprobar.';
     RETURN TRUE;

  else 
    o_msj_error := 'La concertaci�n no se puede grabar aprobar por el(los) siguiente(s) motivo(s): '||
      l_msj_error1||'; '||l_msj_error2||'; '||l_msj_error3;
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when OTHERS then 
      raise_application_error(-20000, 'Un error inesperado ocurri� en OBJETIVOS_FLUJO_PCK.ES_GRABABLE_APROBABLE.
        Por favor, inf�rmelo al administrador de la aplicaci�n. '|| sqlcode ||' - '|| sqlerrm);

END ES_GRABABLE_APROBABLE;


/*Determina si bars_formevaobj es grabable en su resultado (se le puede registrar resultados a los objetivos) y por tanto se pueden mostrar los elementos de aplicaci�n que lo permiten, o se pueden ejecutar las acciones de grabar resultado.*/
FUNCTION ES_GRABABLE_RES(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN AS
  l_formevaobj_row  BARS_FORMEVAOBJ%ROWTYPE;
  l_msj_error1      VARCHAR2(1000);
  l_msj_error2      VARCHAR2(1000);
  l_msj_error3      VARCHAR2(1000);

BEGIN
  select * into l_formevaobj_row from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  -- La validacion ES_VALIDO_RESULTADOS se realiza a nivel de validaciones de APEX
  if (    ( es_estado(k_grabable_resultados, l_formevaobj_row, l_msj_error1) )
      and ( es_usuario(k_grabable_resultados, l_formevaobj_row, i_user_name, l_msj_error2) ) 
      and ( es_fecha_evaluable(l_formevaobj_row, i_fecha, l_msj_error3) )
      and ( es_periodo_activo(l_formevaobj_row.id) )      
     ) then
     o_msj_exito := 'Los resultados se pueden grabar.';
     RETURN TRUE;

  else 
    o_msj_error := 'Los resultados no se pueden grabar por el(los) siguiente(s) motivo(s): '||
      l_msj_error1||'; '||l_msj_error2||'; '||l_msj_error3;
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when OTHERS then 
      raise_application_error(-20000, 'Un error inesperado ocurri� en OBJETIVOS_FLUJO_PCK.ES_GRABABLE_RES.
        Por favor, inf�rmelo al administrador de la aplicaci�n. '|| sqlcode ||' - '|| sqlerrm);

END ES_GRABABLE_RES;

/*Determina si bars_formevaobj es grabable el resultado y evaluable (se le puede registrar resultados a los objetivos e intentar validarlos) y por tanto se pueden mostrar los elementos de aplicaci�n que lo permiten, o se pueden ejecutar las acciones de grabar y validar resultado.*/
FUNCTION ES_GRABABLE_RES_EVALUABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN AS
  l_formevaobj_row  BARS_FORMEVAOBJ%ROWTYPE;
  l_msj_error1      VARCHAR2(1000);
  l_msj_error2      VARCHAR2(1000);
  l_msj_error3      VARCHAR2(1000);
  l_msj_error4      VARCHAR2(1000);

BEGIN
  select * into l_formevaobj_row from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  -- La validacion ES_VALIDO_RESULTADOS se realiza a nivel de validaciones de APEX
  if (    ( es_estado(k_grabable_res_evaluable, l_formevaobj_row, l_msj_error1) )
      and ( es_usuario(k_grabable_resultados, l_formevaobj_row, i_user_name, l_msj_error2) )       
      and ( es_fecha_evaluable(l_formevaobj_row, i_fecha, l_msj_error3) )
      and ( es_periodo_activo(l_formevaobj_row.id) )
      and ( es_valido_obj_abiertos(l_formevaobj_row.id, l_msj_error4) )
     ) then
     o_msj_exito := 'Los resultados se pueden grabar y evaluar.';
     RETURN TRUE;

  else 
    o_msj_error := 'Los resultados no se pueden grabar y evaluar por el(los) siguiente(s) motivo(s): '||
      l_msj_error1||'; '||l_msj_error2||'; '||l_msj_error3||'; '||l_msj_error4;
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when OTHERS then 
      raise_application_error(-20000, 'Un error inesperado ocurri� en OBJETIVOS_FLUJO_PCK.ES_GRABABLE_RES_EVALUABLE.
        Por favor, inf�rmelo al administrador de la aplicaci�n. '|| sqlcode ||' - '|| sqlerrm);

END ES_GRABABLE_RES_EVALUABLE;


/* Determina si bars_formevaobj es grabable el resultado y finalizable (se le puede registrar resultados a los objetivos y finalizarlo) y por tanto se pueden mostrar los elementos de aplicaci�n que lo permiten, o se pueden ejecutar las acciones de grabar y finalizar evaluaci�n. */
FUNCTION ES_FINALIZABLE(i_formevaobj_id IN NUMBER, i_user_name IN VARCHAR2, i_fecha IN DATE, o_msj_error OUT VARCHAR2,
  o_msj_exito OUT VARCHAR2) RETURN BOOLEAN AS
  l_formevaobj_row  BARS_FORMEVAOBJ%ROWTYPE;
  l_msj_error1      VARCHAR2(1000);
  l_msj_error2      VARCHAR2(1000);
  l_msj_error3      VARCHAR2(1000);
  l_msj_error4      VARCHAR2(1000);
  l_msj_error5      VARCHAR2(1000);

BEGIN
  select * into l_formevaobj_row from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  -- La validacion ES_VALIDO_RESULTADOS se realiza a nivel de validaciones de APEX
  if (    ( es_estado(k_grabable_res_evaluable, l_formevaobj_row, l_msj_error1) )
      and ( es_usuario(k_grabable_resultados, l_formevaobj_row, i_user_name, l_msj_error2) ) 
      and ( es_fecha_evaluable(l_formevaobj_row, i_fecha, l_msj_error3) )      
      and ( es_periodo_activo(l_formevaobj_row.id) )
      and ( es_valido_obj_abiertos(l_formevaobj_row.id, l_msj_error4) )
      and ( es_valido_obj_cerrados(l_formevaobj_row.id, l_msj_error5) )      
     ) then
     o_msj_exito := 'Los resultados se pueden grabar, evaluar y el formulario se puede finalizar.';
     RETURN TRUE;

  else 
    o_msj_error := 'Los resultados no se pueden grabar y por ende el formulario no se puede finalizar por el(los) siguiente(s) motivo(s): '||
      l_msj_error1||'; '||l_msj_error2||'; '||l_msj_error3||'; '||l_msj_error4||'; '||l_msj_error5;
    RETURN FALSE;
  end if;
  
  EXCEPTION
    when OTHERS then 
      raise_application_error(-20000, 'Un error inesperado ocurri� en OBJETIVOS_FLUJO_PCK.ES_FINALIZABLE.
        Por favor, inf�rmelo al administrador de la aplicaci�n. '|| sqlcode ||' - '|| sqlerrm);

END ES_FINALIZABLE;


--Subprogramas utilitarios para la utilizaci�n de las APIs de validaci�n y ejecuci�n del flujo por estados

/*Retorna el nombre del estado en que se encuentra la concertaci�n o evaluaci�n de objetivos*/
FUNCTION get_estado (i_formevaobj_id IN NUMBER) RETURN VARCHAR2 AS
  l_estado_nombre   VARCHAR2(30);
  
BEGIN
  select (select upper(nombre) from V_OBJETIVOS_ESTADOS where id = estado) into l_estado_nombre
    from BARS_FORMEVAOBJ where id = i_formevaobj_id;
  
  RETURN l_estado_nombre;
  
  EXCEPTION
    when NO_DATA_FOUND then RETURN NULL;

END get_estado;


/*Actualiza el estado del formulario de objetivos*/
PROCEDURE set_estado (i_formevaobj_id IN NUMBER, i_estado IN VARCHAR2) AS
  l_estado_id   NUMBER;

BEGIN
  UPDATE BARS_FORMEVAOBJ set estado = ( select id from V_OBJETIVOS_ESTADOS where upper(nombre) = upper(i_estado) ), fecheva = sysdate
    where id = i_formevaobj_id;
  
  EXCEPTION
    when NO_DATA_FOUND then
      raise_application_error(-20000, 'El cambio de estado de la concertaci�n no pudo ser realizada porque no se encuentra
        el estado o el formulario deseado. Favor contactar al administrador del sistema.');

END set_estado;


/* Actualiza el porcentaje de consecuci�n que deben cumplir los objetivos abiertos del formulario dado */
PROCEDURE set_consecucion_total(i_formevaobj_id IN NUMBER) AS
    l_periodo BARS_FORMEVAOBJ.periodo%type;
    l_conteo_filas number;
    l_porcentaje_total NUMBER;
begin


    select periodo into l_periodo from BARS_FORMEVAOBJ where id = i_formevaobj_id;
    
    for i in (select * from BARS_OBJETIVO where FORMEVAOBJ in (i_formevaobj_id) and
            id_tipo not in (select id_tipo from bars_objetivo where formevaobj=i_formevaobj_id and pctconsind 
                           is null and id_tipo in SNW_CONSTANTES.constante_tipo('CERRADO'))
        )
    loop
      
      l_porcentaje_total := get_porcentaje_consecucion(i.PCTCONSIND);
       
      --Dbms_Output.Put_Line('   Persistiendo...');
      update BARS_OBJETIVO set pctconspond = l_porcentaje_total where id = i.id;
    
     
    end loop;
END set_consecucion_total;


/*Ejecuta el procedimiento de flujo relacionado con grabar objetivos (el update del objetivo lo hace el autom�tico DML de Apex)*/
PROCEDURE GRABAR_OBJETIVOS (i_formevaobj_id IN NUMBER, o_msj_error OUT VARCHAR2, o_msj_exito OUT VARCHAR2) AS

BEGIN
  if ( upper(k_nuevo) = get_estado(i_formevaobj_id) ) then
    set_estado(i_formevaobj_id, k_et);
    o_msj_exito := 'Los objetivos se han guardado correctamente';
  end if;  

  EXCEPTION
    when others then
      o_msj_error := 'Ocurri� un problema inesperado con la actualizaci�n de los objetivos. Por favor intente nuevamente o
        inf�rmelo al administrador.';      
      
END GRABAR_OBJETIVOS;

/*Ejecuta el procedimiento de flujo relacionado con grabar los resultados de objetivos (el update del objetivo lo hace el autom�tico DML de Apex)*/
PROCEDURE GRABAR_RESULTADOS (i_formevaobj_id IN NUMBER, o_msj_error OUT VARCHAR2, o_msj_exito OUT VARCHAR2) AS

BEGIN

    UPDATE BARS_FORMEVAOBJ set fecheva = sysdate
    where id = i_formevaobj_id;
    o_msj_exito := 'Los resultados se han guardado correctamente';

  EXCEPTION
    when others then
      o_msj_error := 'Ocurri� un problema inesperado con la actualizaci�n de los resultados. Por favor intente nuevamente o
        inf�rmelo al administrador.';      
      
END GRABAR_RESULTADOS;



/*Ejecuta el procedimiento de flujo relacionado con grabar los resultados de objetivos (el update del objetivo lo hace el autom�tico DML de Apex)*/
PROCEDURE VALIDAR_EVALUACION (i_formevaobj_id IN NUMBER, o_msj_error OUT VARCHAR2, o_msj_exito OUT VARCHAR2) AS
    
l_evaluado number;
l_periodo number;
l_target_opr number;
l_porcentaje_cerrados number;
l_asignado_aprobador number;

BEGIN

    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = i_formevaobj_id;
    
    l_target_opr := get_asignado_target(l_evaluado);
    
    -- Se obtiene el porcentaje de obetivos cerrados
    -- l_porcentaje_cerrados := get_porcentaje_total(l_periodo, l_target_opr, SNW_CONSTANTES.constante_tipo('OPR_CER'));
        
    -- Se actualiza la consecuci�n de los objetivos de este formulario
    set_consecucion_total(i_formevaobj_id);    
    -- El estado del formulario pasa a: RESULTADO VALIDADO
    set_estado(i_formevaobj_id, k_resval);   
    -- Se obtiene el asignado del usuario (es el evaluador)  
    l_asignado_aprobador := get_asignado_username(NVL(upper(v('APP_USER')),'APEX'));
    
    -- Se actualiza el formulario
    UPDATE BARS_FORMEVAOBJ SET FECHEVA = sysdate, EVALUADOR_RESULTADO = l_asignado_aprobador  WHERE ID = i_formevaobj_id;

    o_msj_exito := 'Formulario diligenciado correctamente, muchas gracias ';       

  EXCEPTION
    when others then
      o_msj_error := 'Ocurri� un problema inesperado con la validaci�n de los resultados. Por favor intente nuevamente o
        inf�rmelo al administrador.';      
      
END VALIDAR_EVALUACION;

END OBJETIVOS_FLUJO_PCK;

/
