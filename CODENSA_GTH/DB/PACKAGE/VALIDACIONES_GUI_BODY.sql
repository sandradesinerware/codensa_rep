--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body VALIDACIONES_GUI
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."VALIDACIONES_GUI" as



function val_trans_estado_pdi 
(
  i_estado_new in number 
, i_estado_old in number 
) return number as 
begin
/**
 22 PLANEADO                                           
 23 CONCERTADO                                         
 24 FINALIZADO                                         
 25 ANULADO                                            
 26 NO APROBADO                                        
 30 APROBADO                                           
 31 PENDIENTE 

**/

if(i_estado_old = i_estado_new) then --Si el estado original es 'PLANEADO' puede ir a 'CONCERTADO' y 'ANULADO'
return 1;
end if;


if(i_estado_old in (22) and i_estado_new in (23,25)) then --Si el estado original es 'PLANEADO' puede ir a 'CONCERTADO','ANULADO' y 'PENDIENTE'
return 1;
end if;

if(i_estado_old in (23) and i_estado_new in (30,26)) then --Si el estado original es 'CONCERTADO' puede ir a 'APROBADO' y 'NO APROBADO'
return 1;
end if;

  return 0;
end val_trans_estado_pdi;


function val_trans_estado_pdi_bool(i_pdi in NUMBER, i_estado in NUMBER, o_msg OUT VARCHAR2 ) return boolean as
l_estado_old NUMBER;
l_boolean NUMBER;
begin
select id_estado into l_estado_old from bars_pdi where id=i_pdi;
l_boolean:= validaciones_gui.val_trans_estado_pdi(i_estado,l_estado_old);
if (l_boolean=1) then
return true;
else return false;
end if;

exception
when no_data_found then
null;
end val_trans_estado_pdi_bool;


function val_boton_create_p78 
(
  i_accion in varchar2 ,
  i_usuario in varchar2
) return boolean as 

l_usuario usuario.numero_identificacion%type;
l_numero_identificacion  trabajador.numero_identificacion%type;
begin

l_usuario:=identificar_usuario(i_usuario);
l_numero_identificacion:=cargar_evaluado_accion(i_accion);

 IF (( l_numero_identificacion = l_usuario ) ) then
    RETURN true;
  end if;
  return false;
end val_boton_create_p78;


function val_boton_save_p78 
(
  i_accion in varchar2 ,--Cambiar por i_accion
  i_usuario in varchar2
) return boolean as 


l_estado_formulario  NUMBER;

begin
  SELECT id_estado into l_estado_formulario
  FROM BARS_PDI F
  WHERE F.id = NVL(i_accion,0);

 IF ( l_estado_formulario in (22) ) then
    RETURN true;
  end if;
  return false;
end val_boton_save_p78;




function identificar_usuario (
i_usuario usuario.user_name%type
)
return usuario.numero_identificacion%type as
l_usuario usuario.numero_identificacion%type;
begin
SELECT numero_identificacion into l_usuario
  FROM USUARIO
  WHERE upper(user_name) = upper(i_usuario);

  return l_usuario;

  exception
  when no_data_found then
  raise_application_error(-20000,'Usuario '||i_usuario||' no existe!');
end identificar_usuario;



function cargar_evaluado_accion (i_accion bars_pdi.id%type) 
return bars_encpdi.evaluado%type as
l_evaluado bars_encpdi.evaluado%type;
begin

select bars_encpdi.evaluado into l_evaluado 
from bars_pdi left join bars_encpdi on (bars_pdi.id_encabezado=bars_encpdi.id) 
where bars_pdi.id in (i_accion);

return l_evaluado;

exception
when no_data_found then
raise_application_error(-20000,'La accion de PDI '||i_accion||' no tiene un encabezado o no existe!.');
end cargar_evaluado_accion;


-- Funcion que evalua si un usuario es empleado y su estado es menor que Concertacion Validada (Nuevo o en tratamiento)

function val_crud_formevaobj_evaluado(i_formevaobj in bars_formevaobj.id%type, i_username in usuario.user_name%type) 
return boolean as
l_estado bars_formevaobj.estado%type;
l_es_empleado boolean;
l_evaluado bars_formevaobj.evaluado%type;
l_evaluador bars_formevaobj.evaluador%type;
l_usuario usuario.numero_identificacion%type;
begin
select estado, evaluado, evaluador into l_estado,l_evaluado, l_evaluador from bars_formevaobj where id=i_formevaobj;--Carga informacion del formulario
l_es_empleado:= AUTH_FUNCTION(i_username,'DILIGENCIAR MI EVALUACIÓN DE OBJETIVOS','GTH') or AUTH_FUNCTION(i_username,'SER GESTOR','GTH');--Verifica que el usuario sea empleado
l_usuario:=identificar_usuario(i_username);--Identifica al usuario 
if(l_estado < 8 and l_es_empleado and (l_usuario=l_evaluado or l_usuario=l_evaluador)) then
return true;
else return false;
end if;
exception
when no_data_found then
raise_application_error(-20000,'Error en codensa_gth.validaciones_gui.val_crud_formevaobj_evaluado: no se han encontrado datos');
end val_crud_formevaobj_evaluado;

-- Evalua si un usuario es GESTOR y su estado es menor que CONCERTACION APROBADA (11)

FUNCTION val_crud_formevaobj_evaluador(
    i_formevaobj IN bars_formevaobj.id%type,
    i_username   IN usuario.user_name%type)
  RETURN BOOLEAN
AS
  l_estado bars_formevaobj.estado%type;
  l_es_gestor BOOLEAN;
BEGIN
  SELECT estado INTO l_estado FROM bars_formevaobj WHERE id=i_formevaobj;
  l_es_gestor:= AUTH_FUNCTION(i_username,'SER GESTOR','GTH') or AUTH_FUNCTION(i_username,'ADMINISTRAR OPR','GTH');--Es gestor
  IF(l_estado   < 14 AND l_es_gestor) THEN
    RETURN true;
  ELSE
    RETURN false;
  END IF;
EXCEPTION
WHEN no_data_found THEN
  raise_application_error(-20000,'Error en codensa_gth.validaciones_gui.val_crud_formevaobj_evaluador: no se han encontrado datos');
END val_crud_formevaobj_evaluador;


-- Evalua si no se ha superado la fecha maxima de concertacion y el usuario es GESTOR o TRABAJADOR
function val_crud_formevaobj_fch_concer(
    i_formevaobj in bars_formevaobj.id%type,
    i_username   in usuario.user_name%type)
  return BOOLEAN
as
  l_periodo bars_formevaobj.periodo%type;
  l_fecha_concertacion bars_perobj.fecha_concertacion%type;
  l_es_emp_gest BOOLEAN;
begin
  l_es_emp_gest:= AUTH_FUNCTION(i_username,'SER GESTOR','GTH') or AUTH_FUNCTION(i_username,'DILIGENCIAR MI EVALUACIÓN DE OBJETIVOS','GTH') /*or AUTH_FUNCTION(i_username,'ADMINISTRAR OPR','GTH')*/;--Es gestor o trabajador

  select periodo into l_periodo from bars_formevaobj where id=i_formevaobj;
  select fecha_concertacion into l_fecha_concertacion from bars_perobj where ano=l_periodo;
  if( sysdate < l_fecha_concertacion+1 and l_es_emp_gest) then
    return true;
  else
    return false;
  end if;
exception
when no_data_found then
  raise_application_error(-20000,'Error en codensa_gth.validaciones_gui.val_crud_formevaobj_fch_concer: no se han encontrado datos');
end val_crud_formevaobj_fch_concer;

-- Evalua si no se ha superado la fecha maxima de aprobacion y el usuario es GESTOR 
function val_crud_formevaobj_fch_aprob(
    i_formevaobj in bars_formevaobj.id%type,
    i_username   in usuario.user_name%type)
  return BOOLEAN
as
  l_periodo bars_formevaobj.periodo%type;
  l_fecha_aprob bars_perobj.fecha_aprobacion%type;
  l_es_gestor BOOLEAN;
begin
  l_es_gestor:= AUTH_FUNCTION(i_username,'SER GESTOR','GTH') or AUTH_FUNCTION(i_username,'ADMINISTRAR OPR','GTH'); --Es gestor o trabajador
  select periodo into l_periodo from bars_formevaobj where id=i_formevaobj;
  select fecha_aprobacion into l_fecha_aprob from bars_perobj where ano=l_periodo;
  if( sysdate < l_fecha_aprob+1 and l_es_gestor) then
    return true;
  else
    return false;
  end if;
exception
when no_data_found then
  raise_application_error(-20000,'Error en codensa_gth.validaciones_gui.val_crud_formevaobj_fch_aprob: no se han encontrado datos');
end val_crud_formevaobj_fch_aprob;


function calc_peso_requerido_evaluacion(i_periodo NUMBER, i_target NUMBER, i_tipo_evaluacion NUMBER)
    return RESTRICCION_TARGET.porcentaje_total%TYPE
    as
    l_pct_total RESTRICCION_TARGET.porcentaje_total%TYPE;
    
    begin
        select PORCENTAJE_TOTAL into l_pct_total 
        from restriccion_target 
        where periodo = i_periodo and id_target = i_target and id_tipo_evaluacion = i_tipo_evaluacion;
    
        return l_pct_total;
    
    exception
    when no_data_found then
        raise_application_error(-20000,'Error en codensa_gth.validaciones_gth.calc_peso_requerido_evaluacion: no se ha parametrizado el peso requerido');
end calc_peso_requerido_evaluacion;


function val_peso_total_objetivos (i_formulario BARS_FORMEVAOBJ.id%type, i_tipo_objetivo TIPO.ID%type)
 return BOOLEAN
 as 
     l_pct_total NUMBER;
     l_pct_obj NUMBER;
     l_periodo NUMBER;
     l_id_target_opr TIPO.ID%type;
     l_tipo_evaluacion NUMBER;
     l_evaluado NUMBER;
 
begin

    -- Se obtiene el TARGET_OPR del trabajador (ya no del formulario)
    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = i_formulario;
    select ID_TARGET_OPR into l_id_target_opr from trabajador where NUMERO_IDENTIFICACION = l_evaluado;
        

  --Calcular ponderado de la concertación para el tipo de evaluación (tipo de objetivo). La función SUM retorna valores independiente del NO_DATA_FOUND
    select nvl( sum( nvl(ponderacion,0) ) , 0  )/100 into l_pct_obj
     from bars_objetivo 
     where bars_objetivo.formevaobj = i_formulario and bars_objetivo.id_tipo = i_tipo_objetivo;

  --Calcular ponderación requerida para el tipo de evaluación
  l_tipo_evaluacion := CASE 
                            WHEN i_tipo_objetivo = SNW_CONSTANTES.constante_tipo('ABIERTO') then SNW_CONSTANTES.constante_tipo('OPR_ABI')
                            WHEN i_tipo_objetivo = SNW_CONSTANTES.constante_tipo('CERRADO') then SNW_CONSTANTES.constante_tipo('OPR_CER')
                       END;
                       
  l_pct_total := calc_peso_requerido_evaluacion(l_periodo, l_id_target_opr, l_tipo_evaluacion);

  --Validar con margen de tolerancia de 0.01
  if ( (l_pct_obj-0.01) <= l_pct_total and (l_pct_obj+0.01) >= l_pct_total ) then
   return true;
  else
   return false;
  end if;

exception
   when no_data_found then
    return false;
   when others then
    raise_application_error(-20000,'Error inesperado en validaciones_gui.val_peso_total_objetivos: '||SQLERRM);
end val_peso_total_objetivos;



 function val_peso_objetivo(i_formulario BARS_FORMEVAOBJ.id%type,
    i_tipo_objetivo TIPO.ID%type,
    i_ponderacion BARS_OBJETIVO.ponderacion%type)
     return BOOLEAN
     as 
         l_id_target_opr TIPO.ID%type;
         l_pct_ind_min NUMBER;
         l_pct_ind_max NUMBER;
         l_periodo NUMBER;
         l_evaluado NUMBER;
     
     begin
     
     -- Se obtiene el TARGET_OPR del trabajador (ya no del formulario)
    select EVALUADO, periodo into l_evaluado, l_periodo from bars_formevaobj where id = i_formulario;
    select ID_TARGET_OPR into l_id_target_opr from trabajador where NUMERO_IDENTIFICACION = l_evaluado;
    
     select pct_ind_min, pct_ind_max into l_pct_ind_min, l_pct_ind_max 
    from restriccion_target 
    where id_target = l_id_target_opr 
         and periodo = l_periodo
    and id_tipo_evaluacion = decode(i_tipo_objetivo,1011,1161,1162);
    dbms_output.put_line('MIN:'||(l_pct_ind_min*100)||' MAX:'||(l_pct_ind_max*100)||' POND:'||i_ponderacion);
    
    if( i_ponderacion >= (l_pct_ind_min*100) and  i_ponderacion <= (l_pct_ind_max*100)) then
    return true;
    else
    return false;
    end if;
    exception
    when no_data_found then
    return false;
    when others then
     raise_application_error(-20000,'Error inesperado en validaciones_gui.val_peso_objetivo: '||SQLERRM);
 end val_peso_objetivo;


 
 -------------------------------------------------------------------------------------
end validaciones_gui;

/
