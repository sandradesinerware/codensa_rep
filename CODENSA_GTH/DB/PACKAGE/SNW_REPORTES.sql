--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package SNW_REPORTES
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."SNW_REPORTES" AS 
    L_JNDI VARCHAR2(4000) := 'codensa_gth';
    L_URL VARCHAR2(4000) := 'http://54.164.83.200/AutomaticReports2/ReportsServlet';
    L_SUBFOLDER VARCHAR2(4000) := 'codensa';
    INTERNAL_URL CONSTANT varchar2(4000) := 'http://54.164.83.200/AutomaticReports2/ReportsServlet';
    FUNCTION CONSTRUIR_URL(I_TIPO_REPORTE VARCHAR2,I_PARAMETROS VARCHAR2) RETURN VARCHAR2;
END SNW_REPORTES;

/

  GRANT EXECUTE ON "CODENSA_GTH"."SNW_REPORTES" TO "SNW_CARGOS";
