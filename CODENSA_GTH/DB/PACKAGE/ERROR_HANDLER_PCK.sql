--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package ERROR_HANDLER_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."ERROR_HANDLER_PCK" AS 


FUNCTION fun_catch_error(p_error IN apex_error.t_error)
RETURN apex_error.t_error_result;
END ERROR_HANDLER_PCK;


/
