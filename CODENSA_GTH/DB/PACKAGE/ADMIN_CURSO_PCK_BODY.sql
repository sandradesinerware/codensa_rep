--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body ADMIN_CURSO_PCK--
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."ADMIN_CURSO_PCK" AS

--------------------------------------------
FUNCTION EXISTE_PDI_GRUPO (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
    l_conteo number;
BEGIN
    SELECT COUNT(*) INTO l_conteo FROM BARS_PDI WHERE ID_PDI_GRUPO=i_id_grupo AND ID_ESTADO IS NOT NULL;
    RETURN l_conteo > 0;    
END EXISTE_PDI_GRUPO;
--------------------------------------------
FUNCTION EXISTE_PDI_VIGENTE_GRUPO (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
    l_estado boolean;
BEGIN
    FOR PDI IN (SELECT * FROM BARS_PDI WHERE ID_PDI_GRUPO=i_id_grupo) LOOP
        l_estado := ADMINISTRACION_PDI_PCK.DETERMINAR_ESTADO_PDI(PDI.ID,22);
        IF (l_estado = FALSE) THEN
            l_estado := ADMINISTRACION_PDI_PCK.DETERMINAR_ESTADO_PDI(PDI.ID,23);
            IF(l_estado = FALSE) THEN
                l_estado := ADMINISTRACION_PDI_PCK.DETERMINAR_ESTADO_PDI(PDI.ID,30);
                RETURN l_estado;
            ELSE RETURN TRUE;
            END IF;
        ELSE RETURN TRUE;
        END IF;
    END LOOP;
    RETURN FALSE;
END EXISTE_PDI_VIGENTE_GRUPO;
--------------------------------------------
FUNCTION INSCRITOS_GRUPO (i_id_grupo IN NUMBER) RETURN NUMBER IS
    l_conteo number;
BEGIN
    SELECT COUNT(*) INTO l_conteo FROM BARS_PDI WHERE ID_PDI_GRUPO = i_id_grupo AND ID_ESTADO NOT IN (25,26);
    RETURN l_conteo;    
END INSCRITOS_GRUPO;
--------------------------------------------
PROCEDURE CANCELAR_GRUPO (i_id_grupo IN NUMBER, i_comentario IN VARCHAR2, i_responsable in PDI_CONCEPTO.RESPONSABLE_APROBACION%type)
IS
 BEGIN
    UPDATE PDI_GRUPO SET ESTADO=SNW_CONSTANTES.constante_tipo('CANCELADO_GRUPO') WHERE ID=i_id_grupo;
    FOR PDI IN (SELECT * FROM BARS_PDI WHERE ID_PDI_GRUPO=i_id_grupo)LOOP
        ADMINISTRACION_PDI_PCK.CAMBIAR_CONCEPTO_PDI(PDI.ID,26,i_comentario,i_responsable);
    END LOOP;
END CANCELAR_GRUPO;
--------------------------------------------
FUNCTION GRUPO_SIN_INSCRITOS_VALIDOS (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
    l_estado BOOLEAN;
    l_inscritos number;
    l_anulados number;
BEGIN
    l_estado:=ADMINISTRACION_PDI_PCK.DETERMINAR_ESTADO_GRUPO(i_id_grupo,snw_constantes.constante_tipo('CANCELADO_GRUPO'));
    SELECT COUNT(*) INTO l_inscritos FROM BARS_PDI WHERE ID_PDI_GRUPO= i_id_grupo;
    SELECT COUNT(*) INTO l_anulados FROM BARS_PDI WHERE ID_PDI_GRUPO= i_id_grupo AND ID_ESTADO IN (25,26);
    RETURN l_estado OR l_anulados=l_inscritos;    
END GRUPO_SIN_INSCRITOS_VALIDOS;
--------------------------------------------
PROCEDURE CAMBIAR_ESTADO_GRUPO
(i_id_grupo IN NUMBER, i_estado IN NUMBER)
IS
 BEGIN
    IF (i_estado is not null) THEN
        UPDATE PDI_GRUPO
        SET ESTADO=i_estado
        WHERE ID =i_id_grupo;
    END IF;
END CAMBIAR_ESTADO_GRUPO;
--------------------------------------------
FUNCTION GRUPO_RESULTADO_COMPLETO (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
l_inscritos number;
l_conceptos number;
BEGIN
    select count(*) into l_inscritos from v_inscritos where id_pdi_grupo=i_id_grupo;
    select count(*) into l_conceptos from pdi_concepto 
    where tipo_concepto=snw_constantes.constante_tipo('CONCEPTO_RESULTADO') 
    and id_pdi in (select id from v_inscritos where id_pdi_grupo=i_id_grupo);
    if (l_inscritos=l_conceptos) then 
        return true;
    end if;
    return false;
END GRUPO_RESULTADO_COMPLETO; 
--------------------------------------------
PROCEDURE FINALIZAR_GRUPO(i_id_grupo IN NUMBER) IS
    l_curso number;
    l_califica_asistencia pdi_curso.CALIFICABLE_ASIST%type;
    l_sesiones_completas boolean;
    l_grupo_resultado_completo boolean;
 BEGIN
    select ID_PDI_CURSO into l_curso from pdi_grupo where id = i_id_grupo;
    select CALIFICABLE_ASIST into l_califica_asistencia from pdi_curso where id = l_curso;
    l_sesiones_completas:=ADMIN_SESION_PCK.SESIONES_COMPLETAS_GRUPO(i_id_grupo);
    if (l_sesiones_completas) then 
        if (l_califica_asistencia='S')then        
            ADMIN_CURSO_PCK.CAMBIAR_ESTADO_GRUPO(i_id_grupo,snw_constantes.constante_tipo('FINALIZADO_GRUPO'));        
        else
            l_grupo_resultado_completo:=ADMIN_CURSO_PCK.GRUPO_RESULTADO_COMPLETO(i_id_grupo);
            if (l_grupo_resultado_completo) then
                ADMIN_CURSO_PCK.CAMBIAR_ESTADO_GRUPO(i_id_grupo,snw_constantes.constante_tipo('FINALIZADO_GRUPO'));      
            end if;
        end if;
    end if;
END FINALIZAR_GRUPO;
--------------------------------------------
PROCEDURE FINALIZAR_GRUPO_POR_RESULTADO(i_id_grupo IN NUMBER) IS
    L_RESULT_ASIST_INCOMPLETO number;
    l_sesiones_completas boolean;
 BEGIN
  /*SELECT COUNT(*) INTO L_RESULT_ASIST_INCOMPLETO
  FROM
    (SELECT bars_pdi.*,
     nvl((select ID_ESTADO_PDI from PDI_CONCEPTO WHERE ID_PDI=BARS_PDI.id and ID_ESTADO_PDI IN (50,51)),0)  RESULTADO,
     ADMINISTRACION_PDI_PCK.CALCULAR_AVANCE_IDEAL_PDI(bars_pdi.ID) ASISTENCIA_IDEAL
     FROM bars_pdi
     WHERE BARS_PDI.ID_PDI_GRUPO = i_id_grupo)
  WHERE RESULTADO <> 0 AND ASISTENCIA_IDEAL = 100;*/
  l_sesiones_completas:=ADMIN_SESION_PCK.SESIONES_COMPLETAS_GRUPO(i_id_grupo);

  IF /*L_RESULT_ASIST_INCOMPLETO > 0*/l_sesiones_completas THEN
    ADMIN_CURSO_PCK.CAMBIAR_ESTADO_GRUPO(i_id_grupo,snw_constantes.constante_tipo('FINALIZADO_GRUPO'));
  END IF;

END FINALIZAR_GRUPO_POR_RESULTADO;
--------------------------------------------
FUNCTION GRUPO_PRESENCIAL (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
l_conteo number;
BEGIN
    select count(*) into l_conteo from pdi_curso where tipo IN (snw_constantes.constante_tipo('PRESENCIAL'),snw_constantes.constante_tipo('SINCRONICO')) and
    id = (select id_pdi_curso from pdi_grupo where id = i_id_grupo);
    if (l_conteo > 0) then 
        return true;
    end if;
    return false;
END GRUPO_PRESENCIAL; 
--------------------------------------------
END ADMIN_CURSO_PCK;

/
