--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body ADMINISTRACION_PDI_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."ADMINISTRACION_PDI_PCK" AS


--------------------------------------------
FUNCTION DETERMINAR_ESTADO_PDI
(i_id_pdi IN NUMBER, i_estado IN NUMBER) RETURN BOOLEAN 
IS
l_conteo number;
 BEGIN
    SELECT COUNT(*) INTO l_conteo FROM BARS_PDI WHERE ID = i_id_pdi AND ID_ESTADO = i_estado;
    RETURN l_conteo > 0;
END DETERMINAR_ESTADO_PDI;
--------------------------------------------
PROCEDURE CAMBIAR_ESTADO_PDI
(i_id_pdi IN NUMBER, i_estado IN NUMBER)
IS
 BEGIN
    IF (i_estado is not null) THEN
        UPDATE BARS_PDI
        SET ID_ESTADO = i_estado
        WHERE ID = i_id_pdi;
    END IF;
END CAMBIAR_ESTADO_PDI;
--------------------------------------------
PROCEDURE REGISTRAR_PDI_CONCEPTO
(i_id_pdi IN NUMBER, i_estado IN NUMBER, i_comentario IN PDI_CONCEPTO.COMENTARIO%type, i_responsable in PDI_CONCEPTO.RESPONSABLE_APROBACION%type)
IS
l_conteo number;
l_tipo_concepto number;
BEGIN
    select tipo_concepto into l_tipo_concepto from estado_bars where id=i_estado;
    select count(*) into l_conteo from pdi_concepto where id_pdi=i_id_pdi and tipo_concepto=l_tipo_concepto;    
    IF (l_conteo >0) THEN
        UPDATE PDI_CONCEPTO 
        SET ID_ESTADO_PDI = i_estado,
        COMENTARIO = i_comentario,
        RESPONSABLE_APROBACION = i_responsable
        WHERE ID_PDI=i_id_pdi AND TIPO_CONCEPTO=l_tipo_concepto;
    ELSE
        INSERT INTO PDI_CONCEPTO (id_pdi,id_estado_pdi,comentario,TIPO_CONCEPTO,responsable_aprobacion)
        VALUES
        (i_id_pdi,i_estado,i_comentario,l_tipo_concepto,i_responsable);
    END IF;
END REGISTRAR_PDI_CONCEPTO;
--------------------------------------------
PROCEDURE INSERTAR_COMENTARIO
(i_id_pdi IN NUMBER, i_comentario IN BARS_PDI.COMENTARIOS_GESTOR%type, i_tipo_comentario IN VARCHAR2)
IS
 BEGIN
    IF (i_comentario is not null) THEN
        IF (i_tipo_comentario='gestor') THEN
            UPDATE BARS_PDI
            SET COMENTARIOS_GESTOR=i_comentario
            WHERE ID    =i_id_pdi;
        ELSE
            UPDATE BARS_PDI
            SET COMENTARIOS_RH=i_comentario
            WHERE ID    =i_id_pdi;
        END IF;
    END IF;
END INSERTAR_COMENTARIO;
--------------------------------------------
PROCEDURE CAMBIAR_CONCEPTO_PDI
(i_id_pdi IN NUMBER, i_estado IN NUMBER, i_comentario IN PDI_CONCEPTO.COMENTARIO%type DEFAULT NULL,i_responsable in PDI_CONCEPTO.RESPONSABLE_APROBACION%type )
IS
l_conteo number;
l_tipo_concepto number;
l_rol varchar2(500);
BEGIN    
    select tipo_concepto into l_tipo_concepto from estado_bars where id=i_estado;
    if (l_tipo_concepto = snw_constantes.constante_tipo('CONCEPTO_GESTOR')) then
        l_rol:='gestor';
    else 
        l_rol:='admin';
    end if;
    CAMBIAR_ESTADO_PDI(i_id_pdi, i_estado);
    if(i_comentario is not null) then
        INSERTAR_COMENTARIO(i_id_pdi, i_comentario,l_rol);  
    end if;
    select count(*) into l_conteo from pdi_concepto where id_pdi=i_id_pdi and tipo_concepto=l_tipo_concepto;    
    IF (l_conteo >0) THEN
        if(i_comentario is not null) then
            UPDATE PDI_CONCEPTO 
            SET ID_ESTADO_PDI = i_estado,
            COMENTARIO = i_comentario,
            RESPONSABLE_APROBACION = i_responsable  
            WHERE ID_PDI=i_id_pdi AND TIPO_CONCEPTO=l_tipo_concepto;
        else
            UPDATE PDI_CONCEPTO 
            SET ID_ESTADO_PDI = i_estado,
            RESPONSABLE_APROBACION = i_responsable  
            WHERE ID_PDI=i_id_pdi AND TIPO_CONCEPTO=l_tipo_concepto;
        end if;
    ELSE
        INSERT INTO PDI_CONCEPTO (id_pdi,id_estado_pdi,comentario,TIPO_CONCEPTO,responsable_aprobacion)
        VALUES
        (i_id_pdi,i_estado,i_comentario,l_tipo_concepto,i_responsable);
    END IF;
END CAMBIAR_CONCEPTO_PDI;
--------------------------------------------
FUNCTION DISPONIBILIDAD_CUPO_GRUPO (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
    cupo_maximo number;
    l_nro_inscritos number;
BEGIN
    if i_id_grupo is null then
        return false;
    end if;
    select nvl(cupo_maximo,0) into cupo_maximo from pdi_grupo where id = i_id_grupo;
    select count(*) into l_nro_inscritos from bars_pdi where id_pdi_grupo = i_id_grupo and id_estado not in (25/*Anulado por gestor*/,26/*No aprobado por RRHH*/);
    return (cupo_maximo - l_nro_inscritos) > 0;
END DISPONIBILIDAD_CUPO_GRUPO;
--------------------------------------------
FUNCTION NRO_CUPOS_DISP_GRUPO (i_id_grupo IN NUMBER) RETURN NUMBER IS
    cupo_maximo number;
    l_nro_inscritos number :=0 ;
BEGIN
    select cupo_maximo into cupo_maximo from pdi_grupo where id=i_id_grupo;
    select count(*) into l_nro_inscritos from bars_pdi where id_pdi_grupo=i_id_grupo and id_estado not in (25/*Anulado por gestor*/,26/*No aprobado por RRHH*/);
    RETURN cupo_maximo - l_nro_inscritos;
    exception when others then
        return 0;
END NRO_CUPOS_DISP_GRUPO;
--------------------------------------------
FUNCTION TRABAJADOR_GRUPO_CRUZADO (i_id_grupo IN NUMBER,i_id_bars_encpdi IN NUMBER) RETURN VARCHAR2 IS 
    l_conteo number;
    l_cruzado number;
    l_nombre_curso varchar2(4000);
    l_nombre_grupo varchar2(4000);
BEGIN
    select count(*) into l_conteo from pdi_grupo where id = i_id_grupo and (fecha_inicio is null or fecha_fin is null);
    if l_conteo > 0 or not admin_curso_pck.GRUPO_PRESENCIAL(i_id_grupo) then
        return '';
    end if;
    for i in (select pdi_grupo.id grupo,pdi_grupo.fecha_inicio, pdi_grupo.fecha_fin from bars_encpdi 
                        left join bars_pdi on (bars_encpdi.id = bars_pdi.id_encabezado and id_estado not in (25,26))
                        left join pdi_grupo on (pdi_grupo.id = bars_pdi.id_pdi_grupo)
                        where bars_encpdi.id = i_id_bars_encpdi and pdi_grupo.id <> i_id_grupo) loop
        select count(*) into l_cruzado 
        from pdi_grupo
        where id = i_id_grupo and (
        (fecha_inicio between i.fecha_inicio and i.fecha_fin) or 
        (fecha_fin between i.fecha_inicio and i.fecha_fin) or 
        (i.fecha_inicio between fecha_inicio and fecha_fin) or 
        (i.fecha_fin between fecha_inicio and fecha_fin));
        if l_cruzado > 0 then
            select nombre_grupo into l_nombre_grupo from pdi_grupo where id= i.grupo;
            select nombre_visible into l_nombre_curso from pdi_curso 
            where id = (select id_pdi_curso from pdi_grupo where id = i.grupo);
            RETURN 'El usuario ya se encuentra registrado en el grupo '||l_nombre_curso||' - '|| l_nombre_grupo||
            ', el cual tiene las fechas '||i.fecha_inicio||' - '||i.fecha_fin||' y se solapan con el que desea registar.';
        end if;
    end loop;
    RETURN '';
END TRABAJADOR_GRUPO_CRUZADO;
--------------------------------------------
FUNCTION GRUPO_CRUZADO_CAMBIO (i_id_grupo IN NUMBER,i_id_bars_pdi IN NUMBER) RETURN VARCHAR2 IS 
    l_conteo number;
    l_conteo2 number;
    l_cruzado number;
    l_nombre_grupo varchar2(4000);
    l_nombre_curso varchar2(4000);
    l_id_grupo number;
    l_id_bars_encpdi number;
BEGIN
    select id_pdi_grupo,id_encabezado into l_id_grupo,l_id_bars_encpdi from bars_pdi where id = i_id_bars_pdi;
    select count(*) into l_conteo from pdi_grupo where id = i_id_grupo and (fecha_inicio is null or fecha_fin is null);
    select count(*) into l_conteo2 from pdi_grupo where id = l_id_grupo and (fecha_inicio is null or fecha_fin is null);
    if l_conteo > 0 or l_conteo2 > 0 or not admin_curso_pck.GRUPO_PRESENCIAL(i_id_grupo) then
        return '';
    end if;
    for i in (select pdi_grupo.id grupo,pdi_grupo.fecha_inicio, pdi_grupo.fecha_fin from bars_encpdi 
                        left join bars_pdi on (bars_encpdi.id = bars_pdi.id_encabezado and id_estado not in (25,26))
                        left join pdi_grupo on (pdi_grupo.id = bars_pdi.id_pdi_grupo)
                        where bars_encpdi.id = l_id_bars_encpdi and pdi_grupo.id <> i_id_grupo and pdi_grupo.id <>l_id_grupo) loop
        select count(*) into l_cruzado 
        from pdi_grupo 
        where id = i_id_grupo and (
        (fecha_inicio between i.fecha_inicio and i.fecha_fin) or 
        (fecha_fin between i.fecha_inicio and i.fecha_fin) or 
        (i.fecha_inicio between fecha_inicio and fecha_fin) or 
        (i.fecha_fin between fecha_inicio and fecha_fin));
        if l_cruzado > 0 then
            select nombre_grupo into l_nombre_grupo from pdi_grupo where id= i.grupo;
            select nombre_visible into l_nombre_curso from pdi_curso 
            where id = (select id_pdi_curso from pdi_grupo where id = i.grupo);
            RETURN 'El usuario ya se encuentra registrado en el grupo '||l_nombre_curso||' - '|| l_nombre_grupo||
            ', el cual tiene las fechas '||i.fecha_inicio||' - '||i.fecha_fin||' y se solapan con el que desea registar.';
        end if;
    end loop;
    RETURN '';
END GRUPO_CRUZADO_CAMBIO;
--------------------------------------------
FUNCTION TRABAJADOR_DOBLE_GRUPO_CURSO (i_id_grupo IN NUMBER,i_id_bars_encpdi IN NUMBER) RETURN VARCHAR2 IS 
l_curso number;
BEGIN
    select id_pdi_curso into l_curso from pdi_grupo where id = i_id_grupo;
    for i in (select pdi_grupo.id grupo,pdi_grupo.id_pdi_curso curso, pdi_grupo.nombre_grupo nombre
                    from bars_encpdi 
                    left join bars_pdi on (bars_encpdi.id = bars_pdi.id_encabezado and bars_pdi.id_pdi_grupo is not null and id_estado not in (25,26))
                    left join pdi_grupo on (pdi_grupo.id = bars_pdi.id_pdi_grupo )
                    where bars_encpdi.id = i_id_bars_encpdi ) loop
        if (i.curso = l_curso) then
            return 'No se puede inscribir a este grupo porque ya est� inscrito en otro grupo del mismo curso.';
        end if;
    end loop;
    return '';
END TRABAJADOR_DOBLE_GRUPO_CURSO;
--------------------------------------------
FUNCTION DETERMINAR_ESTADO_GRUPO (i_id_grupo IN NUMBER,i_id_estado IN NUMBER) RETURN BOOLEAN IS 
l_estado number;
BEGIN
    select estado into l_estado from pdi_grupo where id= i_id_grupo;
    return l_estado = i_id_estado;
END DETERMINAR_ESTADO_GRUPO;
--------------------------------------------
FUNCTION GRUPO_ESTA_INICIADO (i_id_grupo IN NUMBER) RETURN BOOLEAN IS
    l_conteo number;
BEGIN
    select count(*) into l_conteo from pdi_grupo where id = i_id_grupo and sysdate >= fecha_inicio-7;
    return l_conteo > 0;
END GRUPO_ESTA_INICIADO;
--------------------------------------------
FUNCTION VALIDAR_ESTADO_PERIODO_PDI (i_id_periodo_pdi IN NUMBER,i_id_estado_pdi IN NUMBER) RETURN BOOLEAN IS
    l_conteo number;
BEGIN
    select count(*) into l_conteo from pdi_periodo where id = i_id_periodo_pdi and estado = i_id_estado_pdi;
    RETURN l_conteo > 0;
END VALIDAR_ESTADO_PERIODO_PDI;
--------------------------------------------
FUNCTION MAXIMO_NRO_ACCIONES (i_id_bars_encpdi IN NUMBER, i_id_accion IN NUMBER, i_usuario IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN IS
l_conteo NUMBER;
l_limite_acciones CONSTANT NUMBER := 3;
BEGIN
 --Exceptuar de la validaci�n al administrador de PDI o Formaci�n mediante la tenencia de la funci�n apropiada
 if ( AUTH_FUNCTION(NVL(i_usuario,-1),'Crear acci�n de PDI como administrador','GTH') ) then
   return TRUE;
 else --No es administrador de PDI o formaci�n entonces se valida el n�mero
    SELECT COUNT(*) INTO l_conteo FROM BARS_PDI 
    WHERE ID_ENCABEZADO = i_id_bars_encpdi AND 
    PDI_RECHAZADO(BARS_PDI.id) = 0 AND 
    ID_ACCION = i_id_accion and
    id_pdi_grupo in (select id from pdi_grupo where id_pdi_curso in 
    (select id from pdi_curso where tipo in (snw_constantes.constante_tipo('PRESENCIAL'))));
    
    RETURN l_conteo < l_limite_acciones;
 end if;
 
END MAXIMO_NRO_ACCIONES;
--------------------------------------------

FUNCTION MAX_NRO_ACCI_SINCRONICAS (i_id_bars_encpdi IN NUMBER, i_id_accion IN NUMBER, i_usuario IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN IS
l_conteo NUMBER;
l_limite_acciones CONSTANT NUMBER := 4;
BEGIN
 --Exceptuar de la validaci�n al administrador de PDI o Formaci�n mediante la tenencia de la funci�n apropiada
 if ( AUTH_FUNCTION(NVL(i_usuario,-1),'Crear acci�n de PDI como administrador','GTH') ) then
   return TRUE;
 else --No es administrador de PDI o formaci�n entonces se valida el n�mero
    SELECT COUNT(*) INTO l_conteo FROM BARS_PDI 
    WHERE ID_ENCABEZADO = i_id_bars_encpdi AND 
    PDI_RECHAZADO(BARS_PDI.id) = 0 AND 
    ID_ACCION = i_id_accion and
    id_pdi_grupo in (select id from pdi_grupo where id_pdi_curso in 
    (select id from pdi_curso where tipo in (snw_constantes.constante_tipo('SINCRONICO'))));
    
    RETURN l_conteo < l_limite_acciones;
 end if;
 
END MAX_NRO_ACCI_SINCRONICAS;
--------------------------------------------
FUNCTION LISTA_ESPERA (i_id_grupo IN NUMBER, i_id_periodo IN NUMBER DEFAULT NULL, 
  i_id_jurisdiccion_trabajador IN NUMBER DEFAULT NULL) RETURN VARCHAR2 IS

l_periodo NUMBER;
l_curso NUMBER;
l_tipo_grupo NUMBER;

BEGIN

    -- Si se trata de un grupo que es tipo ABIERTO, entonces si se muestra
    select tipo_grupo into l_tipo_grupo from pdi_grupo where id = i_id_grupo;
    
    if l_tipo_grupo = SNW_CONSTANTES.constante_tipo('CONFORMADO_GRUPO') then
        return 'S';
    end if;   
    

  --Determinar curso que se intenta inscribir
  select id_pdi_curso into l_curso from pdi_grupo where id = i_id_grupo;
  --Determinar periodo actual si LISTA_ESPERA fue llamado sin periodo (compatibilidad hacia atr�s)
  if (i_id_periodo IS NULL) then
    select max(id) into l_periodo from PDI_PERIODO where estado = 1 /*Activo*/;
  else
    l_periodo := i_id_periodo;
  end if;
  
  --Determinar si hay cupo en alguno de los grupos, de periodo y jurisdicci�n, del curso que se intenta inscribir
    for i in ( select id grupo from pdi_grupo
                where id_pdi_curso = l_curso 
                and id <> i_id_grupo
                and tipo_grupo = SNW_CONSTANTES.constante_tipo('CONFORMADO_GRUPO')
                and id_periodo = l_periodo 
                and NVL(id_tipo_jurisdiccion,0) = NVL(i_id_jurisdiccion_trabajador,0) ) loop
        if ADMINISTRACION_PDI_PCK.DISPONIBILIDAD_CUPO_GRUPO(i.grupo) then
            return 'F';
        end if;
    end loop;
RETURN 'S';

END LISTA_ESPERA;
--------------------------------------------
FUNCTION CURSO_ANTERIOR (i_id_pdi IN NUMBER) RETURN VARCHAR2 IS
l_curso_actual varchar2(500);
l_curso_anterior varchar2(500);
l_contador_curso number;
BEGIN
    select count(*) into l_contador_curso from v_reporte_pdi where id=i_id_pdi and id_curso is not null;
    if (l_contador_curso>0)then
        select curso into l_curso_actual from v_reporte_pdi where id=i_id_pdi;
        select nombre_curso_anterior into l_curso_anterior from pdi_equivalencia where UPPER(TRIM(nombre_curso_actual))=UPPER(TRIM(l_curso_actual));
        return l_curso_anterior;
    else 
        return '-';
    end if;
exception when others then return '-';
END CURSO_ANTERIOR;
--------------------------------------------
FUNCTION HIZO_CURSO (i_id_pdi IN NUMBER) RETURN NUMBER IS
l_curso_anterior varchar2(500);
l_identificacion number;
l_contador_curso number;
l_periodo number;
BEGIN
    select count(*) into l_contador_curso from v_reporte_pdi where id=i_id_pdi and id_curso is not null;
    if (l_contador_curso>0)then
        select evaluado into l_identificacion from v_reporte_pdi where id=i_id_pdi;
        l_curso_anterior:=CURSO_ANTERIOR(i_id_pdi);
        select periodo_anterior into l_periodo from pdi_equivalencia where nombre_curso_anterior=l_curso_anterior;
        FOR CURSOS IN (SELECT * FROM PDI_HISTORICO WHERE numero_identificacion=l_identificacion) loop
            if (cursos.curso=l_curso_anterior and cursos.periodo=l_periodo) then
                return 1;
            end if;
        end loop;
    end if;
    RETURN 0;
exception when others then return 0;
END HIZO_CURSO;
--------------------------------------------
PROCEDURE REPROBAR_PDI_REPETICION_CURSO
(i_id_pdi IN NUMBER, i_responsable IN NUMBER)
IS
l_grupo number;
 BEGIN 
    CAMBIAR_CONCEPTO_PDI(i_id_pdi,26, 'Rechazado por repetici�n de curso',i_responsable);    
    SELECT ID_PDI_GRUPO INTO l_grupo FROM BARS_PDI WHERE ID=i_id_pdi;
    IF (ADMIN_CURSO_PCK.GRUPO_SIN_INSCRITOS_VALIDOS(l_grupo)) THEN
        ADMIN_CURSO_PCK.CAMBIAR_ESTADO_GRUPO(l_grupo,snw_constantes.constante_tipo('CANCELADO_GRUPO'));
    END IF;

END REPROBAR_PDI_REPETICION_CURSO;
--------------------------------------------
FUNCTION CALCULAR_AVANCE_PDI (i_id_bars_pdi IN NUMBER) RETURN NUMBER IS
    l_asistencias number;
    l_sesiones number;
    l_avance number;
    l_id_pdi_grupo number;
BEGIN
    select id_pdi_grupo into l_id_pdi_grupo from bars_pdi where id= i_id_bars_pdi;
    l_asistencias:=ADMIN_ASISTENCIA_PCK.ASISTENCIAS_SESIONES(i_id_bars_pdi);
    l_sesiones:= ADMIN_SESION_PCK.SESIONES_GRUPO(l_id_pdi_grupo);
    l_avance:= (l_asistencias*100)/l_sesiones;
    RETURN l_avance;    
END CALCULAR_AVANCE_PDI;

--------------------------------------------
FUNCTION CALCULAR_AVANCE_IDEAL_PDI (i_id_bars_pdi IN NUMBER) RETURN NUMBER IS
    l_sesiones_totales number;
    l_sesiones_fecha number;
    l_id_pdi_grupo number;
BEGIN
    select id_pdi_grupo into l_id_pdi_grupo from bars_pdi where id= i_id_bars_pdi;
    l_sesiones_totales := ADMIN_SESION_PCK.SESIONES_GRUPO(l_id_pdi_grupo);
    select count(*) into l_sesiones_fecha from pdi_sesion 
    where id_pdi_grupo = l_id_pdi_grupo and asistio = 'S';
    return round((l_sesiones_fecha*100)/l_sesiones_totales); 
    exception when others then
        return 0;
END;
--------------------------------------------
PROCEDURE GUARDAR_AVANCE_PDI(i_id_bars_pdi IN NUMBER) is
l_avance number;
begin
l_avance:=CALCULAR_AVANCE_PDI(i_id_bars_pdi);
    UPDATE BARS_PDI SET AVANCE_PORCENTAJE_PDI = l_avance WHERE ID= i_id_bars_pdi;
end GUARDAR_AVANCE_PDI;
--------------------------------------------
FUNCTION PORCENTAJE_REST_PDI (i_id_pdi IN NUMBER) RETURN NUMBER IS
l_grupo number;
l_asistencia number;
l_sesiones number;
l_avance number;
BEGIN
    SELECT ID_PDI_GRUPO INTO l_grupo FROM BARS_PDI WHERE ID= i_id_pdi;
    l_asistencia:=ADMIN_SESION_PCK.SESIONES_CON_ASISTENCIA(i_id_pdi);
    l_sesiones:=ADMIN_SESION_PCK.SESIONES_GRUPO(l_grupo);
    l_avance:=((l_sesiones-l_asistencia)*100)/l_sesiones;
    RETURN l_avance;
END PORCENTAJE_REST_PDI;
--------------------------------------------
PROCEDURE CAMBIAR_RESULTADO_PDI(i_id_pdi IN NUMBER ,i_responsable PDI_CONCEPTO.RESPONSABLE_APROBACION%type) IS
l_grupo number;
l_periodo number;
l_avance_pdi number;
l_avance_rest number;
l_asistencia_periodo number;
l_estado number;
l_existe_concepto number;
l_califica_asistencia pdi_curso.CALIFICABLE_ASIST%type;
BEGIN
    select ID_ESTADO INTO l_estado FROM BARS_PDI WHERE ID=i_id_pdi;
    select ID_PDI_GRUPO into l_grupo from bars_pdi where id=i_id_pdi;
    select ID_PERIODO into l_periodo from pdi_grupo where id=l_grupo;
    select ASISTENCIA into l_asistencia_periodo from pdi_periodo where id=l_periodo;
    select CALIFICABLE_ASIST into l_califica_asistencia from pdi_curso where id in (select id_pdi_curso from pdi_grupo where id=l_grupo);
    l_avance_pdi:= ADMINISTRACION_PDI_PCK.CALCULAR_AVANCE_PDI(i_id_pdi);
    l_avance_rest:=ADMINISTRACION_PDI_PCK.PORCENTAJE_REST_PDI(i_id_pdi);
    if ((l_estado=30 or l_estado=50 or l_estado=51)  and l_califica_asistencia='S')then
        if (l_avance_pdi<l_asistencia_periodo) then
            if (l_avance_pdi+l_avance_rest) < l_asistencia_periodo then 
                ADMINISTRACION_PDI_PCK.CAMBIAR_CONCEPTO_PDI(i_id_pdi,51,'Reprobado por asistencia',i_responsable);
                dbms_output.put_line('No aprobado'||'-'||l_avance_pdi||'-'||l_avance_rest);
            else 
                ADMINISTRACION_PDI_PCK.CAMBIAR_CONCEPTO_PDI(i_id_pdi,30,null,i_responsable);
                select count(*) into l_existe_concepto from pdi_concepto where id_pdi=i_id_pdi and tipo_concepto=snw_constantes.constante_tipo('CONCEPTO_RESULTADO');
                if (l_existe_concepto > 0) then
                    delete from pdi_concepto where id_pdi=i_id_pdi and tipo_concepto=snw_constantes.constante_tipo('CONCEPTO_RESULTADO');
                end if;
                dbms_output.put_line('Null'||l_avance_pdi||'-'||l_avance_rest);
            end if;
        else 
            ADMINISTRACION_PDI_PCK.CAMBIAR_CONCEPTO_PDI(i_id_pdi,50,'Aprobado por asistencia',i_responsable);
            dbms_output.put_line('Aprobado'||l_avance_pdi||'-'||l_avance_rest);
        end if;
    else dbms_output.put_line('No aplica'||l_avance_pdi||'-'||l_avance_rest);
    end if;
END CAMBIAR_RESULTADO_PDI;
--------------------------------------------
FUNCTION DETERMINAR_ENCABEZADO_PDI (i_id_periodo IN NUMBER,i_gestor in number,i_colaborador in number) RETURN NUMBER IS
L_ID NUMBER;
BEGIN 
    SELECT ID  INTO L_ID FROM BARS_ENCPDI WHERE PERIODO = i_id_periodo AND EVALUADOR = i_gestor AND EVALUADO = i_colaborador;
    return l_id;
    exception when others then
        return null;
END DETERMINAR_ENCABEZADO_PDI;
--------------------------------------------
FUNCTION CREAR_ENCABEZADO(i_id_periodo IN NUMBER,i_gestor in number,i_colaborador in number)  RETURN NUMBER IS
L_ID_CARGO NUMBER;
L_ID_GERENCIA NUMBER;
L_ID NUMBER;
BEGIN
    IF DETERMINAR_ENCABEZADO_PDI(i_id_periodo,i_gestor,i_colaborador) IS NULL THEN
        SELECT ID_CARGO,ID_GERENCIA INTO L_ID_CARGO,L_ID_GERENCIA FROM TRABAJADOR WHERE NUMERO_IDENTIFICACION = i_colaborador;
        INSERT INTO BARS_ENCPDI (PERIODO,EVALUADOR,EVALUADO,ID_CARGO,ID_GERENCIA) 
        VALUES(i_id_periodo,i_gestor,i_colaborador,L_ID_CARGO,L_ID_GERENCIA) RETURNING ID INTO L_ID;
        RETURN L_ID;
    ELSE
        RETURN DETERMINAR_ENCABEZADO_PDI(i_id_periodo,i_gestor,i_colaborador);
    END IF;
    exception when others then
        return 0;
END CREAR_ENCABEZADO;
--------------------------------------------
FUNCTION CONTEO_PDI_GESTOR(i_evaluado IN NUMBER,i_evaluador IN NUMBER,i_id_periodo IN NUMBER,
                            i_pdi_negocio IN VARCHAR2,i_estado_negocio IN VARCHAR2) RETURN NUMBER IS
    l_conteo number;
BEGIN
    SELECT count(*) into l_conteo 
    FROM BARS_ENCPDI 
    INNER JOIN BARS_PDI ON (BARS_PDI.ID_ENCABEZADO = BARS_ENCPDI.ID)
    where  BARS_ENCPDI.EVALUADO = i_evaluado and 
    BARS_ENCPDI.EVALUADOR = i_evaluador and BARS_ENCPDI.PERIODO = i_id_periodo and 
    DECODE(ID_ACCION,57,'FORMACION','DESARROLLO') = i_pdi_negocio and 
    (select case count(*) when 0 then 'Pendiente'
    when 1 then 'Realizado'
    else'Pendiente' end from pdi_concepto 
    where id_pdi = BARS_PDI.id and 
    tipo_concepto = SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_RESULTADO')) = i_estado_negocio and
    BARS_PDI.ID IN (SELECT ID FROM V_EXISTE_CONCEPTO WHERE CONCEPTO_RRHH = 1);
    RETURN l_conteo;
END CONTEO_PDI_GESTOR;
--------------------------------------------
FUNCTION PORCENTAJE_PDI_GESTOR(i_evaluado IN NUMBER,i_evaluador IN NUMBER,i_id_periodo IN NUMBER) RETURN NUMBER IS
    l_total number;
    l_conteo number;
BEGIN
    SELECT nvl(count(*),0) into l_total 
    from BARS_ENCPDI 
    INNER JOIN BARS_PDI ON (BARS_PDI.ID_ENCABEZADO = BARS_ENCPDI.ID)
    where
        evaluado=i_evaluado and  
        evaluador=i_evaluador and  
        BARS_ENCPDI.PERIODO  =i_id_periodo and
        BARS_PDI.ID IN (SELECT ID FROM V_EXISTE_CONCEPTO WHERE CONCEPTO_RRHH = 1 AND CONCEPTO_RRHH_RESULTADO = 1);
    SELECT nvl(count(*),0) into l_conteo 
    from BARS_ENCPDI 
    INNER JOIN BARS_PDI ON (BARS_PDI.ID_ENCABEZADO = BARS_ENCPDI.ID)
    where
        evaluado=i_evaluado and  
        evaluador=i_evaluador and  
        BARS_ENCPDI.PERIODO  =i_id_periodo and
        (select case count(*) when 0 then 'Pendiente'
        when 1 then 'Realizado'
        else'Pendiente' end from pdi_concepto 
        where id_pdi = BARS_PDI.id and 
        tipo_concepto = SNW_CONSTANTES.CONSTANTE_TIPO('CONCEPTO_RESULTADO')) = 'Realizado' and
        BARS_PDI.ID IN (SELECT ID FROM V_EXISTE_CONCEPTO WHERE CONCEPTO_RRHH = 1 AND CONCEPTO_RRHH_RESULTADO = 1);
    if l_total = 0 then return 100; end if;
    return (l_conteo/l_total)*100;
END PORCENTAJE_PDI_GESTOR;
--------------------------------------------
FUNCTION OBTENER_RESULTADO_ADMIN(i_id_pdi IN NUMBER) RETURN NUMBER 
IS
    l_pdi_concepto  NUMBER;
    l_id_result     NUMBER;
BEGIN

    select count(*) into l_pdi_concepto from pdi_concepto where id_pdi = i_id_pdi;

    if l_pdi_concepto > 0 then
      select id_estado_pdi into l_id_result from pdi_concepto where id_pdi = i_id_pdi and id_estado_pdi in(50,51);    
      return l_id_result;
    end if;

    return 0;

    exception when others then
      return 0;

END OBTENER_RESULTADO_ADMIN;
--------------------------------------------                            
FUNCTION ACCION_FECHA_CRUZADA (i_id_pdi IN NUMBER) RETURN NUMBER
IS
l_id_bars_encpdi number;
l_cruzado number;
l_id_pdi_grupo number;
l_fecha_inicio date;
l_fecha_fin date;
BEGIN
    select id_encabezado,id_pdi_grupo into l_id_bars_encpdi,l_id_pdi_grupo from bars_pdi where id = i_id_pdi;
    select fecha_inicio, fecha_fin into l_fecha_inicio,l_fecha_fin from pdi_grupo 
        where id = l_id_pdi_grupo;
    select count(bars_pdi.id) into l_cruzado 
    from bars_pdi 
    left join pdi_grupo on (pdi_grupo.id = bars_pdi.id_pdi_grupo)
    left join pdi_curso on (pdi_curso.id = pdi_grupo.id_pdi_curso)
    where id_encabezado = l_id_bars_encpdi and
        id_accion = 57 and id_estado = 30 and bars_pdi.id <> i_id_pdi and
        ((pdi_grupo.fecha_inicio between l_fecha_inicio and l_fecha_fin) or 
        (pdi_grupo.fecha_fin between l_fecha_inicio and l_fecha_fin) or 
        (l_fecha_inicio between pdi_grupo.fecha_inicio and pdi_grupo.fecha_fin) or 
        (l_fecha_fin between pdi_grupo.fecha_inicio and pdi_grupo.fecha_fin)) and
        pdi_curso.tipo in (SNW_CONSTANTES.CONSTANTE_TIPO('PRESENCIAL'),snw_constantes.constante_tipo('SINCRONICO'));
    if l_cruzado > 0 then
        return 1;
    else
        return 0;
    end if;
END;

--------------------------------------------                            
FUNCTION CREAR_PDI_BASICO (I_TIPO_ACCION IN NUMBER, I_ID_ENCABEZADO IN NUMBER, 
                            I_ID_ESTADO IN NUMBER, I_JUSTIFICACION IN VARCHAR2, 
                            I_ID_PDI_GRUPO IN NUMBER) 
RETURN NUMBER
IS
L_ID_ACCION NUMBER;
BEGIN
  INSERT 
  INTO BARS_PDI(ID_ACCION, ID_ENCABEZADO, ID_ESTADO, JUSTIFICACION, ID_PDI_GRUPO) 
  VALUES(I_TIPO_ACCION, I_ID_ENCABEZADO, I_ID_ESTADO, I_JUSTIFICACION, I_ID_PDI_GRUPO)
  RETURNING ID INTO L_ID_ACCION;

  RETURN L_ID_ACCION;

END CREAR_PDI_BASICO;

--------------------------------------------       
FUNCTION PDI_RECHAZADO (i_id_pdi IN NUMBER) RETURN NUMBER
IS 
 l_anulado_gestor CONSTANT NUMBER := 25;
 l_rechazado_rh CONSTANT NUMBER := 26;
BEGIN
--TODO GCARRILLO 28/01/2020: Hacer m�s robusto para corregir inconsistencias entre PDI_CONCEPTO y BARS_PDI.estado 
 if ( determinar_estado_pdi(i_id_pdi, l_anulado_gestor) OR determinar_estado_pdi(i_id_pdi, l_rechazado_rh) ) then
   return 1;
 else
   return 0;
 end if;

END PDI_RECHAZADO;

---------------------------------------------------------------------------
PROCEDURE MIGRAR_ENCABEZADO (i_evaluado IN NUMBER, i_periodo IN NUMBER) AS

CURSOR c_encabezados_deprecated IS
  SELECT id
    from BARS_ENCPDI 
    where evaluado = i_evaluado and periodo = i_periodo and evaluador not in (
      select jefe from trabajador where numero_identificacion = i_evaluado );

l_encabezado_destino NUMBER;
l_gestor_actual NUMBER;
l_existe_jefe NUMBER;

BEGIN
  --El objetivo es que se migren las acciones del(los) encabezado(s) deprecated al vigente, entendiendo
  --el vigente como aquel que tiene el gestor asociado al trabajador
  SELECT jefe into l_gestor_actual from trabajador where numero_identificacion = i_evaluado;
  SELECT COUNT(*) INTO l_existe_jefe FROM trabajador WHERE numero_identificacion = l_gestor_actual;

  IF l_existe_jefe > 0 THEN
    l_encabezado_destino := CREAR_ENCABEZADO( i_id_periodo => i_periodo,
                                              i_gestor => l_gestor_actual, 
                                              i_colaborador => i_evaluado );

      FOR i in c_encabezados_deprecated LOOP

        IF l_encabezado_destino IS NOT NULL THEN

            --TODO: Validar que las acciones migradas cumplan las reglas de negocio (num maximo acciones por ejemplo)            
            UPDATE BARS_PDI set id_encabezado = l_encabezado_destino
                where id_encabezado = i.id;
            DBMS_OUTPUT.PUT_LINE( 'ENC_ORIGEN: ' || i.id || ' ENC_DESTINO: ' || l_encabezado_destino );
        ELSE
            DBMS_OUTPUT.PUT_LINE(  'ENCABEZADO NULO: GESTOR => ' || l_gestor_actual || 'EVALUADO: ' || i_evaluado );
        END IF;
      END LOOP;        

  ELSE
    DBMS_OUTPUT.PUT_LINE( 'EVALUADOR INEXISTENTE: ' || l_gestor_actual || ' EVALUADO: ' || i_evaluado );
  END IF;    


EXCEPTION
  WHEN OTHERS THEN
    raise_application_error (-20001, 'Un error inesperado ocurri� en MIGRAR_ENCABEZADO. Por favor, inf�rmelo al administrador de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM);

END MIGRAR_ENCABEZADO;

-------------------------------------------------------------------------------
PROCEDURE MIGRAR_ENCABEZADOS (i_periodo IN NUMBER) AS
CURSOR c_evaluados_con_inconsistencia IS
 SELECT enc.evaluado evaluado
    from bars_encpdi enc inner join trabajador t on (enc.evaluado = t.numero_identificacion)
    where enc.periodo = i_periodo and enc.evaluador != t.jefe;
BEGIN
  --El objetivo es migrar los encabezados del periodo que no tienen por evaluador al gestor actual del trabajador
  FOR i in c_evaluados_con_inconsistencia LOOP
    IF i.evaluado IS NOT NULL THEN
        MIGRAR_ENCABEZADO (i_evaluado => i.evaluado, i_periodo => i_periodo);
    END IF;
  END LOOP;
EXCEPTION
  WHEN OTHERS THEN
    raise_application_error (-20001, 'Un error inesperado ocurri� en MIGRAR_ENCABEZADOS. Por favor, inf�rmelo al administrador de la aplicaci�n. ' || SQLCODE || ' - ' || SQLERRM);
END MIGRAR_ENCABEZADOS;

-------------------------------------------- 
FUNCTION VALIDAR_USUARIO (i_id_grupo IN NUMBER,i_numero_identificacion IN Number) RETURN VARCHAR IS
    l_id_jurisdiccion number;
    l_juris_trabajador number;
    l_id_trabajador number;
    l_id_rol number;
    l_tipo_curso number;
    l_nombre_juris varchar2(200);
    l_bandera varchar2(4000) := '';
BEGIN
    select tipo into l_tipo_curso from pdi_curso where id in (select id_pdi_curso from pdi_grupo where id = i_id_grupo);
    if l_tipo_curso = snw_constantes.constante_tipo('VIRTUAL') then
        return '';
    end if;
    if l_tipo_curso = snw_constantes.constante_tipo('ASINCRONICO') then
        return '';
    end if;
    select id,id_tipo_jurisdiccion into l_id_trabajador,l_juris_trabajador from trabajador where numero_identificacion = i_numero_identificacion;
    select id_tipo_jurisdiccion,id_rol into l_id_jurisdiccion,l_id_rol from pdi_grupo where id = i_id_grupo;
    
    if ADMINISTRACION_PDI_PCK.TRABAJADOR_ROL(i_numero_identificacion,l_id_rol) and l_id_rol is not null then
        return '';
    end if;
    if  not ADMINISTRACION_PDI_PCK.TRABAJADOR_ROL(i_numero_identificacion,l_id_rol) and l_id_rol is not null then
        l_bandera := '<ul>No puede realizar la inscripci�n a este curso porque ser� dictado solo para l�deres</ul>';
    end if;
    if not ADMINISTRACION_PDI_PCK.TRABAJADOR_JURISDICCION(l_id_trabajador,l_id_jurisdiccion) then
        if l_id_jurisdiccion is null then
            select nombre into l_nombre_juris from tipo where id = l_juris_trabajador;
            l_bandera := l_bandera||'<ul>No puede realizar la inscripci�n a este curso porque el curso no ser� dictado en la central de '||l_nombre_juris||'</ul>';
        else
            select nombre into l_nombre_juris from tipo where id = l_id_jurisdiccion;
            l_bandera := l_bandera||'<ul>No puede realizar la inscripci�n a este curso porque ser� dictado en la central de '||l_nombre_juris||'</ul>';
        end if;
    end if;
    return l_bandera;
END VALIDAR_USUARIO;

-------------------------------------------- 
FUNCTION TRABAJADOR_JURISDICCION (i_id_trabajador IN NUMBER,i_id_jurisdiccion IN NUMBER) RETURN BOOLEAN IS
    l_conteo number;
    l_tipo_jurisdiccion number;
BEGIN
    select id_tipo_jurisdiccion  into l_tipo_jurisdiccion from trabajador where id = i_id_trabajador;
    if i_id_jurisdiccion is null and l_tipo_jurisdiccion is null then return true; end if;
    if i_id_jurisdiccion is  null and l_tipo_jurisdiccion is not null then return false; end if;
    if i_id_jurisdiccion is not null and l_tipo_jurisdiccion is null then return false; end if;
    select count(*)  into l_conteo from trabajador where id = i_id_trabajador and id_tipo_jurisdiccion = i_id_jurisdiccion;
    return l_conteo > 0;
END TRABAJADOR_JURISDICCION;

-------------------------------------------- 
FUNCTION TRABAJADOR_ROL (i_numero_identificacion IN number,i_id_rol IN NUMBER) RETURN BOOLEAN IS
    l_id_usuario number;
BEGIN

    -- Se obtiene primero el id de usuario a partir de la identificacion
    

    select id into l_id_usuario from snw_auth.usuario where NUMERO_IDENTIFICACION = i_numero_identificacion;
    return AUTORIZACION.pertenece_usu_grupo(l_id_usuario,i_id_rol);
END TRABAJADOR_ROL;


END ADMINISTRACION_PDI_PCK;

/
