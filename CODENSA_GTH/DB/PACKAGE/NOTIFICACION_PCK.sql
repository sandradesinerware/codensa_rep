--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package NOTIFICACION_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."NOTIFICACION_PCK" AS 
  l_enviados_diario number := 0;
  l_dia_vigente date := sysdate;
  
  ADMIN_EMAIL_SERVER CONSTANT varchar2(3000) := 'ip-172-31-80-167.ec2.internal:8888';
  
  PROCEDURE INSERTAR_LOG(I_REMITENTE IN VARCHAR, 
                            I_DESTINATARIO IN VARCHAR,
                            I_CUERPO IN VARCHAR, 
                            I_ASUNTO IN VARCHAR, 
                            I_FECHA IN DATE,
                            I_ERRORES IN VARCHAR);


  PROCEDURE ENVIAR_MENSAJE_INSCRITO_RRHH(I_ID_BARS_PDI IN NUMBER);
  PROCEDURE ENVIAR_MENSAJE_INSCRITO_GESTOR(I_ID_BARS_PDI IN NUMBER);
  PROCEDURE NOTIFICAR_CAMBIO_GRUPO(I_ID_BARS_PDI IN NUMBER,I_NUEVO_ID_PDI_GRUPO IN NUMBER);
  PROCEDURE ENVIAR_CORRREO( I_DESTINATARIO IN VARCHAR,I_ASUNTO IN VARCHAR,I_CUERPO IN VARCHAR);
  PROCEDURE ENVIAR_CORRREO( I_DESTINATARIO IN VARCHAR,I_ASUNTO IN VARCHAR,I_CUERPO IN VARCHAR,I_FECHA DATE);
  PROCEDURE NOTIFICAR_CAMBIO_CLAVE(i_token in varchar2);
  
-----------------------NOTIFICACIONES DE PEC-------------------------------  
  PROCEDURE Notificar_Trabajador_PEC(i_id_pec IN NUMBER, i_o_mensaje in out varchar2);
  PROCEDURE Notificar_Evaluacion_PEC(i_id_pec IN NUMBER, i_o_mensaje in out varchar2);
  PROCEDURE Alertar_PEC_No_Planeado(i_pec IN PEC%ROWTYPE); 
  PROCEDURE Alertar_PEC_Sin_Evaluar(i_pec IN PEC%ROWTYPE);
  
-----------------------NOTIFICACIONES DE DC--------------------------------
  PROCEDURE NOTIFICAR_BP_DESDE_GESTOR(i_id_cargo IN NUMBER);
  PROCEDURE NOTIFICAR_POC_DESDE_BP(i_id_cargo IN NUMBER);
  PROCEDURE NOTIFICAR_BP_DESDE_POC(i_id_cargo IN NUMBER);    
  PROCEDURE NOTIFICAR_GESTOR_DESDE_BP(i_id_cargo IN NUMBER);
  PROCEDURE NOTIFICAR_ACEPTACION_DC(i_id_cargo IN NUMBER);
  
  PROCEDURE DISPARADOR_CORREO;
END NOTIFICACION_PCK;

/

  GRANT EXECUTE ON "CODENSA_GTH"."NOTIFICACION_PCK" TO "SNW_CARGOS";
