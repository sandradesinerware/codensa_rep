--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package PARAMETRIZACION_CLIENTE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "CODENSA_GTH"."PARAMETRIZACION_CLIENTE" AS 
  procedure sincronizar_agentes;
  procedure parametrizacion_servicios;
  procedure parametrizacion_dependencias;
  procedure parametrizacion_contratistas;
  procedure parametrizacion_trabajadores;
  procedure parametrizacion_evaluaciones(i_id_periodo in number);
  function buscar_tipo(i_gerencia in varchar2, 
                        i_subgerencia in varchar2, 
                        i_division in varchar2, 
                        i_departamento in varchar2, 
                        i_nivel in number) return number;
END PARAMETRIZACION_CLIENTE;


/
