--------------------------------------------------------
-- Archivo creado  - lunes-diciembre-21-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body ERROR_HANDLER_PCK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "CODENSA_GTH"."ERROR_HANDLER_PCK" AS

FUNCTION fun_log_error(p_error IN apex_error.t_error)
RETURN error_log.error_id%TYPE AS
  PRAGMA AUTONOMOUS_TRANSACTION;
  l_error_id error_log.error_id%TYPE;
  l_is_internal_error error_log.is_internal_error%TYPE;
BEGIN
  IF p_error.is_internal_error THEN
    l_is_internal_error := 'Y';
  ELSE
    l_is_internal_error := 'N';
  END IF;
  --
  INSERT
    INTO error_log (message, additional_info, display_location, association_type
                  , page_item_name, region_id, column_alias, row_num
                  , is_internal_error, apex_error_code, ora_sqlcode, ora_sqlerrm
                  , error_backtrace, component_type, component_id, component_name
                  -- substitution strings
                  , application_id, app_page_id, app_user, browser_language
                  , timestamp)
  VALUES (p_error.message, p_error.additional_info, p_error.display_location, p_error.association_type
        , p_error.page_item_name, p_error.region_id, p_error.column_alias, p_error.row_num
        , l_is_internal_error, p_error.apex_error_code, p_error.ora_sqlcode, p_error.ora_sqlerrm
        , p_error.error_backtrace, p_error.component.type, p_error.component.id, p_error.component.name
        -- substitution strings
        , v('APP_ID'), v('APP_PAGE_ID'), v('APP_USER'), v('BROWSER_LANGUAGE')
        , SYSDATE)
  RETURNING error_id
       INTO l_error_id;
  COMMIT;
  --
  RETURN l_error_id;
END fun_log_error;

FUNCTION fun_build_gen_error_message(p_reference_id IN error_log.error_id%TYPE)
RETURN VARCHAR2 AS
BEGIN
  RETURN 'Un error inesperado ha ocurrido. ' ||
         'El Administrador del Sistema ha sido notificado de los detalles y  ' ||
         'pronto se pondr� en contacto!, Incidente registrado con ERROR_ID : <b>#' || p_reference_id || '</b>.';
END fun_build_gen_error_message;


FUNCTION fun_catch_error(p_error IN apex_error.t_error)
RETURN apex_error.t_error_result AS
  l_result apex_error.t_error_result;
  l_reference_id error_log.error_id%TYPE;
  l_constraint_name constraint_lookup.constraint_name%TYPE;
BEGIN
  l_result := apex_error.init_error_result(p_error => p_error);

IF p_error.is_internal_error AND p_error.apex_error_code <> 'APEX.AUTHORIZATION.ACCESS_DENIED' THEN
  -- log the error and get the generated PK
  l_reference_id := fun_log_error(p_error => p_error);
  -- notify app. admin. via e-mail
  --pro_send_email(p_reference_id => l_reference_id);
  -- display a generic error message
  l_result.message := fun_build_gen_error_message(l_reference_id);
  l_result.additional_info := NULL;
  -- set the display location
  l_result.display_location := apex_error.c_inline_in_notification;
ELSE

IF p_error.ora_sqlcode IN (-1, -2091, -2290, -2291, -2292) THEN
  -- get the constraint name
  l_constraint_name := apex_error.extract_constraint_name(p_error => p_error);
  --
  BEGIN
    SELECT error_message
         , page_item_name
         , tab_form_col_alias
      INTO l_result.message
         , l_result.page_item_name
         , l_result.column_alias
      FROM constraint_lookup
     WHERE upper(constraint_name) = upper(l_constraint_name);
  EXCEPTION
    -- not every constraint has to be in our lookup table
    WHEN no_data_found THEN
      l_reference_id := fun_log_error(p_error => p_error);
      -- notify app. admin. via e-mail
      --pro_send_email(p_reference_id => l_reference_id);
      -- display a generic error message
      l_result.message := fun_build_gen_error_message(l_reference_id);
  END;
  l_result.additional_info := NULL;
  -- set the display location
  l_result.display_location := apex_error.c_inline_in_notification;
END IF;
IF p_error.ora_sqlcode IS NOT NULL AND l_result.message = p_error.message THEN
      l_result.message := apex_error.get_first_ora_error_text(p_error => p_error);
    END IF;
    --
    IF l_result.page_item_name IS NULL AND l_result.column_alias IS NULL THEN
      apex_error.auto_set_associated_item(p_error        => p_error
                                        , p_error_result => l_result);
    END IF;
  END IF;
  --
  RETURN l_result;
END fun_catch_error;





END ERROR_HANDLER_PCK;


/
