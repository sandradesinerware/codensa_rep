
  CREATE OR REPLACE FUNCTION "SNW_AUTH"."OBTENER_USUARIO" (APP_USR in varchar2)
return number
is 
id_agente number;
begin 
select id into id_agente from SNW_AUTH.USUARIO where UPPER(SNW_AUTH.USUARIO.user_name) = UPPER(APP_USR);
return id_agente;
exception when no_data_found then
return null;
end;

/