
  CREATE OR REPLACE FUNCTION "SNW_AUTH"."AUTH_CONCATENATE_LIST" (p_cursor IN  SYS_REFCURSOR)
  RETURN  CLOB
IS
  l_return  CLOB; 
  l_temp    CLOB;
BEGIN

  LOOP
    FETCH p_cursor
    INTO  l_temp;
    EXIT WHEN p_cursor%NOTFOUND;
    l_return := l_return || ',' || l_temp;
  END LOOP;
  close p_cursor;
  RETURN LTRIM(l_return, ',');
END;

/