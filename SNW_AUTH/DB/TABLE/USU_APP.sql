
  CREATE TABLE "SNW_AUTH"."USU_APP" 
   (	"ID" NUMBER, 
	"USUARIO" NUMBER, 
	"APLICACION" NUMBER, 
	 CONSTRAINT "USU_APP_CON" UNIQUE ("USUARIO", "APLICACION") ENABLE, 
	 CONSTRAINT "USU_APP_PK" PRIMARY KEY ("ID") ENABLE
   ) ;