
  CREATE TABLE "SNW_AUTH"."MD_TRIGGERS" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"TABLE_OR_VIEW_ID_FK" NUMBER NOT NULL ENABLE, 
	"TRIGGER_ON_FLAG" CHAR(1) NOT NULL ENABLE, 
	"TRIGGER_NAME" VARCHAR2(4000), 
	"TRIGGER_TIMING" VARCHAR2(4000), 
	"TRIGGER_OPERATION" VARCHAR2(4000), 
	"TRIGGER_EVENT" VARCHAR2(4000), 
	"NATIVE_SQL" CLOB, 
	"NATIVE_KEY" VARCHAR2(4000), 
	"LANGUAGE" VARCHAR2(40) NOT NULL ENABLE, 
	"COMMENTS" VARCHAR2(4000), 
	"LINECOUNT" NUMBER, 
	"SECURITY_GROUP_ID" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	"CREATED_ON" DATE DEFAULT sysdate NOT NULL ENABLE, 
	"CREATED_BY" VARCHAR2(255), 
	"LAST_UPDATED_ON" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(255), 
	 CONSTRAINT "MD_TRIGGERS_PK" PRIMARY KEY ("ID") ENABLE
   ) ;