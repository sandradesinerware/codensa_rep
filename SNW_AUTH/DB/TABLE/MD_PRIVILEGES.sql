
  CREATE TABLE "SNW_AUTH"."MD_PRIVILEGES" 
   (	"ID" NUMBER NOT NULL ENABLE, 
	"SCHEMA_ID_FK" NUMBER NOT NULL ENABLE, 
	"PRIVILEGE_NAME" VARCHAR2(4000) NOT NULL ENABLE, 
	"PRIVELEGE_OBJECT_ID" NUMBER, 
	"PRIVELEGEOBJECTTYPE" VARCHAR2(4000) NOT NULL ENABLE, 
	"PRIVELEGE_TYPE" VARCHAR2(4000) NOT NULL ENABLE, 
	"ADMIN_OPTION" CHAR(1), 
	"NATIVE_SQL" CLOB NOT NULL ENABLE, 
	"NATIVE_KEY" VARCHAR2(4000), 
	"COMMENTS" VARCHAR2(4000), 
	"SECURITY_GROUP_ID" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	"CREATED_ON" DATE DEFAULT sysdate NOT NULL ENABLE, 
	"CREATED_BY" VARCHAR2(255), 
	"LAST_UPDATED_ON" DATE, 
	"LAST_UPDATED_BY" VARCHAR2(255), 
	 CONSTRAINT "MD_PRIVILEGES_PK" PRIMARY KEY ("ID") ENABLE
   ) ;