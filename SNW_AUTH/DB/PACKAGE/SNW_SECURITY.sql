
  CREATE OR REPLACE PACKAGE "SNW_AUTH"."SNW_SECURITY" 
IS
  --------------------------------------------------------------------------------
  --
  --------------------------------------------------------------------------------
  FUNCTION sentry_basic_auth
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  --
  --------------------------------------------------------------------------------
  PROCEDURE invalid_session_basic_auth;
  --------------------------------------------------------------------------------
  --
  --------------------------------------------------------------------------------
  FUNCTION snw_authentication(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2 )
    RETURN BOOLEAN;
  --------------------------------------------------------------------------------
  --
  --------------------------------------------------------------------------------
  PROCEDURE snw_reset_password(
      p_username IN VARCHAR2);
  --------------------------------------------------------------------------------
  --
  ---------------------------------------------------------------------
  PROCEDURE snw_update_password(
      p_username VARCHAR2,
      p_password VARCHAR2 );
  -------------------------------

 FUNCTION VALIDAR_GRUPO(
      p_user IN VARCHAR2,
      p_grupo IN VARCHAR2,
      p_aplicacion IN VARCHAR2)
    RETURN BOOLEAN;
-------------------------------------------------------------------------
END;

/
CREATE OR REPLACE PACKAGE BODY "SNW_AUTH"."SNW_SECURITY" 
IS
  --------------------------------------------------------------------------------
  --
  --------------------------------------------------------------------------------
FUNCTION sentry_basic_auth
  RETURN BOOLEAN
IS
  c_auth_header CONSTANT VARCHAR2(4000) := owa_util.get_cgi_env('AUTHORIZATION');
  l_user_pass   VARCHAR2(4000);
  l_separator_pos pls_integer;
BEGIN
  IF apex_application.g_user <> 'nobody' THEN
    RETURN true;
  END IF;
  IF c_auth_header LIKE 'Basic %' THEN
    l_user_pass       := utl_encode.text_decode ( buf => SUBSTR(c_auth_header, 7), encoding => utl_encode.base64 );
    l_separator_pos   := instr(l_user_pass, ':');
    IF l_separator_pos > 0 THEN
      apex_authentication.login ( p_username => SUBSTR(l_user_pass, 1, l_separator_pos-1), p_password => SUBSTR(l_user_pass, l_separator_pos+1) );
      RETURN true;
    END IF;
  END IF;
  RETURN false;
END sentry_basic_auth;
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
PROCEDURE invalid_session_basic_auth
IS
BEGIN
  owa_util.status_line ( nstatus => 401, creason => 'Basic Authentication required', bclose_header => false);
  htp.p('WWW-Authenticate: Basic realm="protected realm"');
  apex_application.stop_apex_engine;
END invalid_session_basic_auth;
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
FUNCTION snw_authentication(
    p_username IN VARCHAR2,
    p_password IN VARCHAR2 )
  RETURN BOOLEAN
IS
  l_user usuario.user_name%type := upper(p_username);
  l_pwd usuario.password%type;
  l_id usuario.id%type;
BEGIN
  SELECT id ,
    password
  INTO l_id,
    l_pwd
  FROM usuario
  WHERE user_name = l_user;
  RETURN l_pwd   = rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password||l_id||l_user), 2 ));
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN false;
END;
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
PROCEDURE snw_reset_password(p_username IN VARCHAR2)
IS
  l_user usuario.user_name%type := upper(p_username);
  l_pwd usuario.password%type;
  l_id usuario.id%type;
BEGIN
  SELECT id ,
    password
  INTO l_id,
    l_pwd
  FROM usuario
  where user_name = l_user;
  update usuario set first_time = 'Y', password = rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(lower(l_user)||l_id||l_user), 2 ))
  where user_name = p_username;
/*EXCEPTION
when no_data_found then
  return false;*/
END;

--------------------------------------------------------------------------------
--
--------------------------------------------------
PROCEDURE snw_update_password
  (
    p_username VARCHAR2,
    p_password VARCHAR2
  )
IS
BEGIN
  UPDATE usuario
  SET password=rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password
    ||id
    ||p_username), 2 )),
    first_time  ='N'
  WHERE user_name=p_username;
  --rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password||id||p_username), 2 ))
END;
--------------------------------------------------------------------------------------------

-------------------------------

 FUNCTION VALIDAR_GRUPO(
      p_user IN VARCHAR2,
      p_grupo IN VARCHAR2,
      p_aplicacion IN VARCHAR2)
    RETURN BOOLEAN
    IS 
    l_contador NUMBER;

    BEGIN
     BEGIN

  SELECT 1
    INTO l_contador
    FROM GRUPO G
    JOIN USU_GRUPO UG
    ON (UG.GRUPO=G.ID)
    JOIN USUARIO U
    ON (U.NUMERO_IDENTIFICACION=UG.USUARIO)
    JOIN APLICACION AP
    ON(AP.ID                =G.APLICACION)
    WHERE  TRIM(UPPER(U.USER_NAME))=TRIM(UPPER(p_user))
    AND TRIM(UPPER(G.NOMBRE)) = TRIM(UPPER(p_grupo))
    AND TRIM(UPPER(AP.NOMBRE) )    = TRIM(UPPER(p_aplicacion)) ;

  EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN FALSE;
   WHEN TOO_MANY_ROWS THEN
     RETURN TRUE;
   WHEN OTHERS THEN
    RETURN FALSE;
  END;
    RETURN TRUE;
    END  VALIDAR_GRUPO;
-------------------------------------------------------------------------
END;

/