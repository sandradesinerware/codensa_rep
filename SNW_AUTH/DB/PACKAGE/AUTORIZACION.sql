
  CREATE OR REPLACE PACKAGE "SNW_AUTH"."AUTORIZACION" 
AS

  FUNCTION AUTH(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION AUTH_ALUMNOS(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION cambiar_contrasena(
      i_usuario    IN VARCHAR2,
      i_contrasena IN VARCHAR2)
    RETURN BOOLEAN;


  FUNCTION verificar_grupo(
      i_grupo      IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN RESULT_CACHE;
  FUNCTION verificar_grupo(
      i_grupo   IN NUMBER,
      i_fecha   IN DATE,
      i_usuario IN VARCHAR2)
    RETURN BOOLEAN RESULT_CACHE;
  FUNCTION verificar_privilegio(
      i_privilegio IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  FUNCTION verificar_privilegio_usuario(
      i_privilegio IN NUMBER,
      i_fecha      IN DATE,
      i_usuario    IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION wrap_verificar_grupo(
      i_grupo   IN NUMBER,
      i_fecha   IN DATE,
      i_usuario IN VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION wrap_verificar_grupo_agente(
      i_grupo      IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION AUTH_HASH(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN VARCHAR2 ;
  FUNCTION nombre_grupo(
      i_grupo IN grupo.id%type)
    RETURN VARCHAR2;
  FUNCTION existe_usuario(
      i_usuario VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION existe_usuario(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  PROCEDURE asociar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER);
  PROCEDURE quitar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER);
  FUNCTION obtener_nombre_usuario(
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION cambiar_fecha_expira_usuario(
      i_id_usuario       IN NUMBER,
      i_fecha_expiracion IN DATE)
    RETURN BOOLEAN;
  PROCEDURE activar_usuario(
      i_id_usuario IN NUMBER);
  FUNCTION usuario_activo(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN;
  PROCEDURE ELIMINAR_USUARIO(
      i_id_agente IN NUMBER);
  FUNCTION VERIFICAR_USUARIO_ACTIVO(
      i_nombre_usuario IN VARCHAR2)
    RETURN BOOLEAN;
  FUNCTION OBTENER_CONTRASENA(
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2 ;
  FUNCTION VALIDAR_PRIV_ACCESO_DATOS(
      i_usuario IN VARCHAR2)
    RETURN BOOLEAN ;
  FUNCTION restablecer_contrasena(
      i_usuario             IN VARCHAR2, 
      i_passAutogenerada    IN VARCHAR2,
      i_passNueva           IN VARCHAR2,
      i_passNuevaConfirmada IN VARCHAR2)
    RETURN VARCHAR2;

  FUNCTION obtener_agente_grupo(
      i_NivelAccesoProyecto IN NUMBER,
      i_GrupoAcceso         IN NUMBER,
      i_TipoAcceso          IN NUMBER)
      RETURN VARCHAR2;  

END AUTORIZACION;
/
CREATE OR REPLACE PACKAGE BODY "SNW_AUTH"."AUTORIZACION" 
IS
  ----------------------------------------------------------------------------------
  FUNCTION AUTH_HASH(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN VARCHAR2
  AS
    l_password VARCHAR2(4000);
    l_salt     VARCHAR2(4000) := '8KK2B6LIH21ZZ6JWKDAB1TJZVLUZ60';
  BEGIN
    l_password := rawtohex(sys.dbms_crypto.hash ( sys.utl_raw.cast_to_raw(p_password ||upper(p_username)||l_salt), 2 ));
    RETURN l_password;
  END AUTH_HASH;
--------------------------------------------------------------------------------
  FUNCTION AUTH(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN BOOLEAN
  IS
    l_usuario usuario%rowtype;
    l_hash usuario.password%type;
  BEGIN
    SELECT *
    INTO l_usuario
    FROM USUARIO
    WHERE UPPER(user_name) = upper(p_username);
    IF l_usuario.ESTADO = 0 OR NOT(VALIDAR_PRIV_ACCESO_DATOS(upper(p_username))) THEN
      RETURN FALSE;
    END IF; --Si el estado del usuario es inactivo se niega el acceso
    l_hash:=AUTH_HASH(UPPER(p_username),p_password);
    RETURN l_hash=l_usuario.password;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN false;
  END AUTH;
--------------------------------------------------------------------------------
  FUNCTION AUTH_ALUMNOS(
      p_username IN VARCHAR2,
      p_password IN VARCHAR2)
    RETURN BOOLEAN
  IS
    l_usuario usuario%rowtype;
    l_hash usuario.password%type;
    l_existe_rol NUMBER;
  BEGIN
    SELECT *
    INTO l_usuario
    FROM USUARIO
    WHERE UPPER(user_name) LIKE upper(p_username);
    IF l_usuario.ESTADO = 0 THEN
      RETURN FALSE;
    END IF; --Si el estado del usuario es inactivo se niega el acceso
    SELECT COUNT(*)
    INTO l_existe_rol
    FROM usu_grupo
    WHERE l_usuario.id=USUARIO
    AND GRUPO     IN(261,263,267,268);
    IF l_existe_rol   = 0 THEN
      RETURN FALSE;
    END IF; --Si el usuario no tiene el rol alumno,Monitor,Asistence del Proyecto,Director del proyecto se le niega el acceso
      l_hash:=AUTH_HASH(UPPER(p_username),p_password);
      RETURN l_hash=l_usuario.password;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN false;
  END AUTH_ALUMNOS;
--------------------------------------------------------------------------------
  FUNCTION cambiar_contrasena(
      i_usuario    IN VARCHAR2,
      i_contrasena IN VARCHAR2)
    RETURN BOOLEAN
  IS
    l_hash usuario.password%type;
  BEGIN
    l_hash:= AUTH_HASH(UPPER(i_usuario),i_contrasena);
    UPDATE usuario SET password = l_hash WHERE upper(user_name)=upper(i_usuario);
    RETURN true;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN false;
  END cambiar_contrasena;


--------------------------------------------------------------------------------
  FUNCTION verificar_grupo(
      i_grupo      IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN RESULT_CACHE RELIES_ON(usu_grupo)
  IS
    l_count_usuario NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_count_usuario
    FROM usu_grupo
    WHERE usu_grupo.usuario =i_id_usuario
    AND usu_grupo.GRUPO     =i_grupo;
    RETURN l_count_usuario     > 0;
  END verificar_grupo;
--------------------------------------------------------------------------------
  FUNCTION verificar_privilegio(
      i_privilegio IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN
  IS
    l_conteo NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_conteo
    FROM usuario
    LEFT JOIN usu_grupo
    ON (usu_grupo.usuario=usuario.id)
    LEFT JOIN grupo_privilegios
    ON (usu_grupo.grupo              =grupo_privilegios.grupo)
    WHERE usuario.id                    =i_id_usuario
    AND grupo_privilegios.funcion=i_privilegio;
    RETURN l_conteo                     > 0;
  END verificar_privilegio;
------------------------------------------------------------------------------
  FUNCTION verificar_privilegio_usuario(
      i_privilegio IN NUMBER,
      i_fecha      IN DATE,
      i_usuario    IN VARCHAR2)
    RETURN BOOLEAN
  IS
    l_usuario NUMBER;
  BEGIN
    SELECT id
    INTO l_usuario
    FROM usuario
    WHERE upper(user_name) = upper(i_usuario);
    RETURN verificar_privilegio( i_privilegio, i_fecha , l_usuario );
  EXCEPTION
  WHEN no_data_found THEN
    RETURN false;
  END verificar_privilegio_usuario;
------------------------------------------------------------------------------
  FUNCTION verificar_grupo(
      i_grupo   IN NUMBER,
      i_fecha   IN DATE,
      i_usuario IN VARCHAR2)
    RETURN BOOLEAN RESULT_CACHE RELIES_ON(usu_grupo)
  IS
    l_count_usuario NUMBER;
    
  BEGIN
    
    SELECT COUNT(*)
    INTO l_count_usuario
    FROM usuario
    INNER JOIN usu_grupo
    ON (usuario.id = usu_grupo.usuario)
    WHERE UPPER(user_name) = UPPER(i_usuario)
    AND GRUPO = i_grupo;
    RETURN l_count_usuario > 0 ;
  END verificar_grupo;
------------------------------------------------------------------------------
  FUNCTION wrap_verificar_grupo(
      i_grupo   IN NUMBER,
      i_fecha   IN DATE,
      i_usuario IN VARCHAR2)
    RETURN VARCHAR2
  IS
  BEGIN
    IF(verificar_grupo(i_grupo,i_fecha,i_usuario) ) THEN
      RETURN 'TRUE';
    ELSE
      RETURN 'FALSE';
    END IF;
  END wrap_verificar_grupo;
------------------------------------------------------------------------------
  FUNCTION wrap_verificar_grupo_agente(
      i_grupo      IN NUMBER,
      i_fecha      IN DATE,
      i_id_usuario IN NUMBER)
    RETURN VARCHAR2
  IS
  BEGIN
    IF(verificar_grupo(i_grupo,i_fecha,i_id_usuario) ) THEN
      RETURN 'TRUE';
    ELSE
      RETURN 'FALSE';
    END IF;
  END wrap_verificar_grupo_agente;
------------------------------------------------------------------------------
  FUNCTION nombre_grupo(
      i_grupo IN grupo.id%type)
    RETURN VARCHAR2
  IS
    o_nombre grupo.nombre%type;
  BEGIN
    SELECT NOMBRE INTO o_nombre FROM grupo WHERE id=i_grupo;
    RETURN o_nombre;
  EXCEPTION
  WHEN no_data_found THEN
    RETURN '';
  END nombre_grupo;
---------------------------------------------------------------------------------
  FUNCTION existe_usuario(
      i_usuario VARCHAR2)
    RETURN BOOLEAN
  IS
    l_contador_usuario NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_contador_usuario
    FROM USUARIO
    WHERE UPPER(user_name) LIKE UPPER (i_usuario);
    IF(l_contador_usuario=0) THEN
      RETURN false;
    ELSE
      RETURN true;
    END IF;
  END existe_usuario;

---------------------------------------------------------------------------------
  FUNCTION existe_usuario(
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN
  IS
    l_contador_usuario NUMBER;
  BEGIN
    SELECT COUNT(*) INTO l_contador_usuario FROM USUARIO WHERE id=i_id_usuario;
    IF(l_contador_usuario=0) THEN
      RETURN false;
    ELSE
      RETURN true;
    END IF;
  END existe_usuario;
-----------------------------------------------------------------------------------
  FUNCTION asociar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER)
    RETURN BOOLEAN
  IS
    l_contador_usu_grupo NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_contador_usu_grupo
    FROM USU_GRUPO
    WHERE USUARIO       =i_id_usuario
    AND GRUPO           =i_grupo;
    IF(l_contador_usu_grupo=0)THEN
      INSERT INTO USU_GRUPO
        (USUARIO,GRUPO
        ) VALUES
        (i_id_usuario,i_grupo
        );
    END IF;
    RETURN true;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN false;
  END asociar_usuario_grupo;
  --------------------------------------------------------------------------------
    PROCEDURE asociar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER)
      AS
    l_contador_usu_grupo NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_contador_usu_grupo
    FROM USU_GRUPO
    WHERE USUARIO       =i_id_usuario
    AND GRUPO           =i_grupo;
    IF(l_contador_usu_grupo=0)THEN
      INSERT INTO USU_GRUPO
        (USUARIO,GRUPO
        ) VALUES
        (i_id_usuario,i_grupo
        );
    END IF;
      END asociar_usuario_grupo;
      --------------------------------------------------------------------------------
  PROCEDURE quitar_usuario_grupo(
      i_grupo      IN NUMBER,
      i_id_usuario IN NUMBER) AS
    l_contador_usu_grupo NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO l_contador_usu_grupo
    FROM USU_GRUPO
    WHERE USUARIO       =i_id_usuario
    AND GRUPO           =i_grupo;
    IF(l_contador_usu_grupo=1)THEN
      DELETE USU_GRUPO WHERE USUARIO =i_id_usuario  AND GRUPO =i_grupo;
    END IF;
      END quitar_usuario_grupo;
--------------------------------------------------------------------------------
  FUNCTION obtener_nombre_usuario
    (
      i_id_usuario IN NUMBER
    )
    RETURN VARCHAR2
  IS
    l_nombre_usuario VARCHAR2 (1000);
  BEGIN
    SELECT user_name INTO l_nombre_usuario FROM USUARIO WHERE id=i_id_usuario;
    RETURN l_nombre_usuario;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN NULL;
  END obtener_nombre_usuario;
--------------------------------------------------------------------------------
  FUNCTION cambiar_fecha_expira_usuario(
      i_id_usuario       IN NUMBER,
      i_fecha_expiracion IN DATE)
    RETURN BOOLEAN
  IS
  BEGIN
    UPDATE snw_auth.usuario
    SET FECHA_EXPIRACION = i_fecha_expiracion
    WHERE id             =i_id_usuario;
    RETURN true;
  EXCEPTION
  WHEN OTHERS THEN
    RETURN false;
  END cambiar_fecha_expira_usuario;

--------------------------------------------------------------------------------
PROCEDURE activar_usuario(
    i_id_usuario IN NUMBER)
IS
BEGIN
  UPDATE usuario SET estado = 1 WHERE id = i_id_usuario;
END activar_usuario;
--------------------------------------------------------------------------------
FUNCTION usuario_activo(
    i_id_usuario IN NUMBER)
  RETURN BOOLEAN
IS
  l_conteo NUMBER;
BEGIN
  SELECT COUNT(*)
  INTO l_conteo
  FROM usuario
  WHERE id       =i_id_usuario
  AND estado     = 1;
  RETURN l_conteo>0;
END usuario_activo;
--------------------------------------------------------------------------------
PROCEDURE ELIMINAR_USUARIO(
    i_id_agente IN NUMBER)
AS
BEGIN
  CONTROL_ACCESO_DATOS.revocar_total_permiso_agente(i_id_agente);
  DELETE FROM USUARIO WHERE ID = i_id_agente;
END;
--------------------------------------------------------------------------------
FUNCTION VERIFICAR_USUARIO_ACTIVO(
    i_nombre_usuario IN VARCHAR2)
  RETURN BOOLEAN
IS
  l_existe_usuario NUMBER;
  l_estado_usuario NUMBER;
BEGIN
  SELECT COUNT(*)
  INTO l_existe_usuario
  FROM USUARIO
  WHERE UPPER(user_name) LIKE upper(i_nombre_usuario);
  IF l_existe_usuario > 0 THEN
    SELECT ESTADO
    INTO l_estado_usuario
    FROM USUARIO
    WHERE upper(user_name) LIKE upper(i_nombre_usuario);
    IF l_estado_usuario = 0 THEN
      RETURN FALSE;
    END IF;
  END IF;
  RETURN TRUE;
EXCEPTION
WHEN no_data_found THEN
  RETURN false;
END;
--------------------------------------------------------------------------------
FUNCTION OBTENER_CONTRASENA(
    i_id_usuario IN NUMBER)
  RETURN VARCHAR2
IS
  l_contrasena VARCHAR2(500);
BEGIN
  SELECT password INTO l_contrasena FROM USUARIO WHERE ID = i_id_usuario;
  RETURN l_contrasena;
EXCEPTION
WHEN NO_DATA_FOUND THEN
  RETURN '';
END;
--------------------------------------------------------------------------------
FUNCTION VALIDAR_PRIV_ACCESO_DATOS(
    i_usuario IN VARCHAR2)
  RETURN BOOLEAN
IS
  l_id_agente       NUMBER;
  l_tiene_persmisos NUMBER;
BEGIN
  SELECT id
  INTO l_id_agente
  FROM usuario
  WHERE UPPER(user_name) LIKE UPPER(i_usuario);
  SELECT COUNT(*)
  INTO l_tiene_persmisos
  FROM PERMISO_NIVEL_ACCESO
  WHERE ID_AGENTE         =l_id_agente
  AND ESTADO              =123;
  RETURN l_tiene_persmisos>0;
EXCEPTION
WHEN no_data_found THEN
  RETURN false;
END VALIDAR_PRIV_ACCESO_DATOS;
---------------------------------------------------
FUNCTION restablecer_contrasena(
    i_usuario             IN VARCHAR2, 
    i_passAutogenerada    IN VARCHAR2, 
    i_passNueva           IN VARCHAR2, 
    i_passNuevaConfirmada IN VARCHAR2)
RETURN VARCHAR2
IS
l_autenticacionCorrecta     BOOLEAN;
l_contrasenasCoinciden      BOOLEAN;
l_cambioContrasenaExitoso   BOOLEAN;
l_tamanoContrasenaCorrecto  BOOLEAN;
BEGIN
--LENGTH('Tech on the Net')

l_autenticacionCorrecta := AUTH_ALUMNOS(TRIM(i_usuario), i_passAutogenerada);
l_tamanoContrasenaCorrecto := LENGTH(i_passNueva) >= 6;
l_contrasenasCoinciden := i_passNueva LIKE i_passNuevaConfirmada;

IF (l_tamanoContrasenaCorrecto AND l_autenticacionCorrecta AND l_contrasenasCoinciden) THEN

  l_cambioContrasenaExitoso := cambiar_contrasena(i_usuario, i_passNuevaConfirmada);

  IF l_cambioContrasenaExitoso THEN
    RETURN 'La contrase�a se ha actualizado correctamente';  
  ELSE
    RETURN 'Se ha presentado un error con el cambio de su contrase�a, por favor intentarlo nuevamente.';
  END IF;  

ELSE

  IF (NOT l_autenticacionCorrecta) THEN
    RETURN 'El usuario o la contrase�a autogenerada son incorrectos';
  ELSIF (NOT l_contrasenasCoinciden) THEN
    RETURN 'La nueva contrase�a y su confirmaci�n no coinciden';
  ELSIF (NOT l_tamanoContrasenaCorrecto) THEN
    RETURN 'Tama�o de contrase�a incorrecto';
  END IF;

END IF;

END restablecer_contrasena;

--------------------------------------------------------------------------------
FUNCTION obtener_agente_grupo(
    i_NivelAccesoProyecto IN NUMBER,
    i_GrupoAcceso         IN NUMBER,
    i_TipoAcceso          IN NUMBER)
RETURN VARCHAR2
IS
l_agentesDisponibles   VARCHAR2(32767)  := '0';
BEGIN
-- Encuentra el �tlimo agente creado que tiene un rol espec�fico para un nivel de acceso determinado.
-- Ejm: El �ltimo validador FIC creado de la facultad a la que pertenece un proyecto.

for nivel_acessos in (select nivel_acceso.*, level
               from nivel_acceso 
               start with nivel_acceso.id = i_NivelAccesoProyecto
               connect by
               prior nivel_acceso.nivel_superior = nivel_acceso.id )
loop
  if nivel_acessos.tipo_acceso = i_TipoAcceso then

    SELECT auth_concatenate_list( cursor( select id_agente 
                                          from permiso_nivel_acceso 
                                          where ID_NIVEL_ACCESO = nivel_acessos.id 
                                                and id_grupo = 121 
                                                and estado = 123) ) 
    INTO l_agentesDisponibles FROM dual;      

    exit;

  end if;
end loop nivel_acessos;

IF l_agentesDisponibles IS NULL THEN
  RETURN '0';
ELSE
  RETURN l_agentesDisponibles;
END IF;

EXCEPTION
WHEN no_data_found THEN
  RETURN '0';

END obtener_agente_grupo;
--------------------------------------------------------------------------------

END AUTORIZACION;
/