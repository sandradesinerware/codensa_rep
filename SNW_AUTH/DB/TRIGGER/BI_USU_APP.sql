
  CREATE OR REPLACE TRIGGER "SNW_AUTH"."BI_USU_APP" 
  BEFORE INSERT ON "SNW_AUTH"."USU_APP"
  REFERENCING FOR EACH ROW
  begin   
  if :NEW."ID" is null then 
    select "USU_APP_SEQ".nextval into :NEW."ID" from dual; 
  end if; 
end;


/
ALTER TRIGGER "SNW_AUTH"."BI_USU_APP" ENABLE;