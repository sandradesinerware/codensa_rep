
  CREATE OR REPLACE TRIGGER "SNW_AUTH"."BIU_FUNCION" 
  BEFORE INSERT OR UPDATE ON "SNW_AUTH"."FUNCION"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "FUNCION_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;


/
ALTER TRIGGER "SNW_AUTH"."BIU_FUNCION" ENABLE;