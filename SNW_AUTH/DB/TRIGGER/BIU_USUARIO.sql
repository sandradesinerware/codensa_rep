
  CREATE OR REPLACE TRIGGER "SNW_AUTH"."BIU_USUARIO" 
  BEFORE INSERT OR UPDATE ON "SNW_AUTH"."USUARIO"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "USUARIO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;


/
ALTER TRIGGER "SNW_AUTH"."BIU_USUARIO" ENABLE;