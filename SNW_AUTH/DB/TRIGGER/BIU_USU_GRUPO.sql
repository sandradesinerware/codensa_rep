
  CREATE OR REPLACE TRIGGER "SNW_AUTH"."BIU_USU_GRUPO" 
  BEFORE INSERT OR UPDATE ON "SNW_AUTH"."USU_GRUPO"
  REFERENCING FOR EACH ROW
  BEGIN

if inserting then
  if :NEW."ID" is null then 
    select "USU_GRUPO_SEQ".nextval into :NEW."ID" from sys.dual; 
  end if;
    end if;
END;


/
ALTER TRIGGER "SNW_AUTH"."BIU_USU_GRUPO" ENABLE;