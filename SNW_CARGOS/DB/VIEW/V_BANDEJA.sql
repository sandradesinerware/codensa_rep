
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_BANDEJA" ("ID", "CODIGO_FUNCION", "DENOMINACION", "JUSTIFICACION", "ID_INSTANCIA_PROCESO", "ELABORO", "ESTADO", "ID_ESTADO", "TIPO_UBICACION", "FECHA_CREACION", "ID_ACTIVIDAD_VIGENTE", "ACTIVIDAD_ACTUAL", "NOMBRE_UNIDAD", "NOMBRE_GERENCIA", "NOMBRE_SUBGERENCIA", "NOMBRE_DIVISION", "NOMBRE_DEPARTAMENTO", "FECHA_ACTIVIDAD", "ALERTA", "ID_UNIDAD_ORGANIZATIVA") AS 
  select 
cargo.id,
cargo.codigo_funcion,
cargo.denominacion,
cargo.justificacion,
cargo.ID_INSTANCIA_PROCESO,
cargo.ELABORO,
(select nombre from tipo where id = CARGO.ESTADO) estado,
CARGO.ESTADO id_estado,
cargo.tipo_ubicacion,
CARGO.FECHA_INICIO FECHA_CREACION,
snw_flow.v_bandeja.id_actividad_vigente,
snw_flow.flujo.nombre_actividad(snw_flow.flujo.determinar_instancia_activa(cargo.id_instancia_proceso)) ACTIVIDAD_ACTUAL,
v_rama_unidad_nombre.unidad nombre_unidad,
v_rama_unidad_nombre.gerencia nombre_gerencia,
v_rama_unidad_nombre.subgerencia nombre_subgerencia,
v_rama_unidad_nombre.division nombre_division,
v_rama_unidad_nombre.departamento nombre_departamento,
(select max(fecha) from snw_flow.historia_proceso where id_instancia_proceso =snw_flow.v_bandeja.id_instancia_activa and id_actividad = snw_flow.v_bandeja.id_actividad_vigente) fecha_actividad,
VERSIONADOR_PCK.ALERTA_BANDEJA(cargo.id) alerta,
cargo.id_unidad_organizativa
from cargo
left join snw_flow.v_bandeja on (snw_flow.v_bandeja.id_instancia_activa = snw_flow.flujo.determinar_instancia_activa(cargo.id_instancia_proceso))
left join v_rama_unidad_nombre on (v_rama_unidad_nombre.id = cargo.id_unidad_organizativa)
where id_proceso = 141;