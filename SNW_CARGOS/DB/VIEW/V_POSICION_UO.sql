
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_POSICION_UO" ("NUMERO_IDENTIFICACION", "ID_POSICION", "POSICION", "ID_UNIDAD_ORGANIZATIVA", "UNIDAD_ORGANIZATIVA") AS 
  select 
    trabajador.numero_identificacion,
    posicion.id id_posicion,
    posicion.nombre posicion,
    posicion_uo.id_unidad_organizativa id_unidad_organizativa, 
    (select nombre from unidad_organizativa where id = posicion_uo.id_unidad_organizativa) unidad_organizativa
from posicion  
left join posicion_uo on (posicion_uo.id_posicion = posicion.id)
left join posicion_trabajador on (posicion_trabajador.id_posicion = posicion.id)
left join TRABAJADOR on (TRABAJADOR.id = posicion_trabajador.id_trabajador)
where ((sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null) and
(sysdate between posicion_uo.fecha_inicio and posicion_uo.fecha_fin or posicion_uo.fecha_fin is null));