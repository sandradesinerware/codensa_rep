
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_UNIDADES_ORGANIZATIVAS" ("ID", "NIVEL", "NOMBRE", "TIPO_UNIDAD", "TIPO_UNIDAD_NOMBRE", "RAIZ", "CODIGO_CODENSA", "CODIGO_EMGESA") AS 
  select id, level nivel, 
  CONCAT ( LPAD (
         ' ',
         LEVEL*3-3
      ),
      nombre
   ) nombre, 
   tipo_unidad, (select nombre from tipo where id = tipo_unidad) tipo_unidad_nombre,
  SYS_CONNECT_BY_PATH(nombre,'|') raiz,
  CODIGO_CODENSA, CODIGO_EMGESA
from unidad_organizativa
start with id_padre is null
connect by prior id = id_padre;