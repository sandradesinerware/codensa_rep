
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_CARGO_PROCESO" ("ID", "ESTADO", "FECHA_ACTIVIDAD", "TIEMPO_ACTIVIDAD", "CODIGO_FUNCION", "DENOMINACION", "NOMBRE_GERENCIA", "NOMBRE_SUBGERENCIA", "NOMBRE_DIVISION", "NOMBRE_DEPARTAMENTO", "SOLICITANTE") AS 
  select 
ID,
ESTADO,
FECHA_ACTIVIDAD,
trunc(to_date(sysdate,'DD/MM/YYYY'))-trunc(to_date(FECHA_ACTIVIDAD,'DD/MM/YYYY')) as TIEMPO_ACTIVIDAD,
CODIGO_FUNCION,
DENOMINACION,
NOMBRE_GERENCIA,
NOMBRE_SUBGERENCIA,
NOMBRE_DIVISION,
NOMBRE_DEPARTAMENTO,
FORMULARIO.DENOMINACION_DE_USUARIO(elaboro) SOLICITANTE
from V_BANDEJA
WHERE ID_ACTIVIDAD_VIGENTE in (325, 345, 346)
order by fecha_actividad asc;