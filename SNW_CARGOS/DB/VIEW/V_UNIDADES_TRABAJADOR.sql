
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_UNIDADES_TRABAJADOR" ("ID_UNIDAD_ORGANIZATIVA", "NUMERO_IDENTIFICACION", "ID_TRABAJADOR", "USUARIO") AS 
  select 
posicion_uo.id_unidad_organizativa,
codensa_gth.trabajador.numero_identificacion,
posicion_trabajador.id_trabajador,
(select user_name from snw_auth.usuario where numero_identificacion = codensa_gth.trabajador.numero_identificacion) usuario
from posicion_uo   
left join posicion_trabajador on (posicion_uo.id_posicion = posicion_trabajador.id_posicion)
left join codensa_gth.trabajador on (codensa_gth.trabajador.id = posicion_trabajador.id_trabajador)
where (sysdate between posicion_trabajador.fecha_inicio and posicion_trabajador.fecha_fin or posicion_trabajador.fecha_fin is null)
and (sysdate between posicion_uo.fecha_inicio and posicion_uo.fecha_fin or posicion_uo.fecha_fin is null)
union all
select 
business_partner.id_unidad_organizativa,
codensa_gth.trabajador.numero_identificacion,
business_partner.id_trabajador,
(select user_name from snw_auth.usuario where numero_identificacion = codensa_gth.trabajador.numero_identificacion) usuario
from business_partner
left join codensa_gth.trabajador on (codensa_gth.trabajador.id = business_partner.id_trabajador);