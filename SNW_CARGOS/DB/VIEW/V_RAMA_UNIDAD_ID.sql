
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_RAMA_UNIDAD_ID" ("ID", "ID_GERENCIA", "ID_SUB_GERENCIA", "ID_DIVISION", "ID_DEPARTAMENTO") AS 
  select 
id,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('GERENCIA')) id_gerencia,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('SUBGERENCIA')) id_sub_gerencia,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('DIVISION')) id_division,
admin_unidad_organizativa.buscar_unidad(id,snw_constantes.constante_tipo('DEPARTAMENTO')) id_departamento
from unidad_organizativa;