
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_POSICION_VIGENTE" ("POS_ID", "POS_COD", "POS_UO_TEO_VIG", "POS_UO_TEO_VIG_COD", "POS_UO_TEO_VIG_NOM", "POS_UO_VIG", "POS_UO_VIG_COD", "POS_UO_VIG_NOM", "POS_UO_LAST", "POS_UO_LAST_COD", "POS_UO_LAST_NOM", "CARGO_PADRE_VIG", "CARGO_PADRE_CODIGO_DC_VIG", "CARGO_PADRE_DENOMINACION_VIG", "CARGO_PADRE_VERSION_VIG", "CARGO_PADRE_TIPO_VIG", "CARGO_PADRE_UO_VIG", "CARGO_PADRE_UO_VIG_NOM", "ASIGNADO_VIG", "ASIGNADO_VIG_NOMBRES", "ASIGNADO_INICIO", "ASIGNADO_LAST", "ASIGNADO_LAST_NOMBRES", "ASIGNADO_LAST_FIN") AS 
  select 
p.id pos_id, p.codigo pos_cod, p.id_unidad_organizativa pos_uo_teo_vig, (select NVL(codigo_emgesa,codigo_codensa) from unidad_organizativa where id = p.id_unidad_organizativa) pos_uo_teo_vig_cod
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(p.id_unidad_organizativa) pos_uo_teo_vig_nom
, puo.id_unidad_organizativa pos_uo_vig, (select NVL(codigo_emgesa,codigo_codensa) from unidad_organizativa where id = puo.id_unidad_organizativa) pos_uo_vig_cod
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(puo.id_unidad_organizativa) pos_uo_vig_nom
, puo_last.id_posicion pos_uo_last, (select NVL(codigo_emgesa, codigo_codensa) from unidad_organizativa where id = puo_last.id_unidad_organizativa) pos_uo_last_cod
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(puo_last.id_unidad_organizativa) pos_uo_last_nom
, c_padre.id cargo_padre_vig, c_padre.codigo_funcion cargo_padre_codigo_dc_vig, c_padre.denominacion cargo_padre_denominacion_vig
, c_padre.version_cargo cargo_padre_version_vig
, SNW_CONSTANTES.get_nombre_tipo(c_padre.tipo_ubicacion) cargo_padre_tipo_vig, c_padre.id_unidad_organizativa cargo_padre_uo_vig
, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(c_padre.id_unidad_organizativa) cargo_padre_uo_vig_nom
, t.numero_identificacion asignado_vig, t.nombres ||' '|| t.apellidos asignado_vig_nombres
, to_char(pt.fecha_inicio,'DD/MM/YYYY HH24:MI') asignado_inicio
, t_last.numero_identificacion asignado_last, t_last.nombres ||' '|| t_last.apellidos asignado_last_nombres
, to_char(pt_last.fecha_fin,'DD/MM/YYYY HH24:MI') asignado_last_fin
from posicion p 
left join posicion_uo puo on (p.id = puo.id_posicion and puo.fecha_fin is null)
left join (
  select id, id_posicion, id_unidad_organizativa, fecha_inicio, fecha_fin
     ,rank() over ( PARTITION BY id_posicion ORDER BY fecha_fin desc ) rango_desc
  from posicion_uo
  where fecha_fin is not null
  ) puo_last on ( puo_last.id_posicion = p.id and puo_last.rango_desc = 1 )
left join posicion_cargo pc on (p.id = pc.id_posicion and pc.fecha_fin is null)
left join cargo c_padre on (pc.id_cargo = c_padre.id)
left join posicion_trabajador pt on (p.id = pt.id_posicion and pt.fecha_fin is null)
left join trabajador t on (pt.id_trabajador = t.id)
left join (
  select id, id_posicion, id_trabajador, fecha_inicio, fecha_fin
    ,rank() over ( PARTITION BY id_posicion ORDER BY fecha_fin desc ) rango_desc
  from posicion_trabajador
  where fecha_fin is not null
  ) pt_last on (p.id = pt_last.id_posicion and pt_last.rango_desc = 1)
left join trabajador t_last on (pt_last.id_trabajador = t_last.id);