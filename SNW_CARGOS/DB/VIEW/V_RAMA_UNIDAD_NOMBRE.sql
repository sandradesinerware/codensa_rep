
  CREATE OR REPLACE FORCE VIEW "SNW_CARGOS"."V_RAMA_UNIDAD_NOMBRE" ("ID", "UNIDAD", "GERENCIA", "SUBGERENCIA", "DIVISION", "DEPARTAMENTO") AS 
  select 
id, 
admin_unidad_organizativa.NOMBRE_UNIDAD(id) Unidad,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_gerencia) gerencia,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_sub_gerencia) subgerencia,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_division) division,
admin_unidad_organizativa.NOMBRE_UNIDAD(id_departamento) departamento 
from V_RAMA_UNIDAD_ID;