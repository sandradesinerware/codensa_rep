
  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."FORMULARIO" AS 

     PROCEDURE DUPLICAR_FORMULARIO(I_CARGO NUMBER); 
     PROCEDURE DUPLICAR_FORMULARIO(I_CARGO IN NUMBER, I_CARGO_DUPLICADO IN OUT NUMBER); 
     PROCEDURE RESPONSABILIDADES_OBLIGATORIAS(I_CARGO NUMBER); 
     PROCEDURE REORDENAR_RESPONSABILIDADES(I_CARGO NUMBER); 
     PROCEDURE dep_ORDENAR_RESPONSABILIDADES(I_CARGO NUMBER); 
     PROCEDURE ELIMINAR_NO_APLICA(I_CARGO NUMBER); 

    --Estas funciones retornan la denominación del cargo vigente, de un usuario o de un trabajador
     FUNCTION  DENOMINACION_DE_USUARIO(I_USER_NAME VARCHAR2) RETURN VARCHAR2; 
     FUNCTION  DENOMINACION_CARGO_TRABAJADOR(i_numero_asignado VARCHAR2) RETURN VARCHAR2;

     FUNCTION ID_CARGO_DE_USUARIO(I_USER_NAME VARCHAR2) RETURN NUMBER;      
     PROCEDURE GENERAR_CONCEPTO(I_CONCEPTO_CARGO NUMBER, I_CARGO NUMBER, I_CONCEPTO NUMBER, I_TIPO NUMBER, I_COMENTARIO VARCHAR, I_INSTANCIA_PROCESO NUMBER,I_USUARIO VARCHAR2);
     FUNCTION DETERMINAR_DIGITALSK_NOMBRE(I_CARGO NUMBER) RETURN VARCHAR2;
     FUNCTION DETERMINAR_DIGITALSK_ESPEC(I_CARGO NUMBER) RETURN VARCHAR2;

END FORMULARIO;
/
CREATE OR REPLACE PACKAGE BODY "SNW_CARGOS"."FORMULARIO" AS
---------------------------------------------------------------------------------------------------
 
 PROCEDURE DUPLICAR_FORMULARIO(I_CARGO NUMBER) IS
    comprobar_vigencia char;
    comprobar_version number;
    l_conteo number;
    l_id_cargo number;
 BEGIN
    select count(*) into l_conteo from cargo where 
    id_cargo_padre = (select id_cargo_padre from cargo where id = I_CARGO) and vigencia is null;

    if l_conteo = 0 then
        INSERT INTO CARGO(DENOMINACION, 
                          CATEGORY_ENEL, 
                          COMPLEMENTARIO_NIVEL, 
                          FUNCTIONAL_AREA, 
                          SUBGRUPO_PROFESIONAL, 
                          ID_CARGO_PADRE,
                          CODIGO_FUNCION, 
                          CODIGO_HOMOLOGACION, 
                          ROLES, 
                          HOMOLOGACION,  
                          TIPO_UBICACION, 
                          ID_UNIDAD_ORGANIZATIVA, 
                          COMPLEMENTARIO_ESPECIFICACION, 
                          MISION, 
                          EXPERIENCIA, 
                          BASICO_ESPECIFICACION, 
                          IDIOMA_EXTRA_ESPECIFICACION, 
                          DIGITAL_ESPECIFICA,                            
                          OTROS,
                          ELABORO,
                          PERSONAS_DIRECTAS,                               
                          PERSONAS_INDIRECTAS,
                          RECURSOS_EMPRESAS,
                          RECURSOS_PERSONAS,
                          JUSTIF_SOFTWARE,
                          JUSTIF_LICENCIAS,                                                  
                          JUSTIF_ECONOMICA,
                          JUSTIF_CUALITATIVA,
                          JUSTIF_DES_TOMAR,
                          JUSTIF_DES_PROPONER,
                          JUSTIF_INTERNAS,
                          JUSTIF_EXTERNAS,
                          CARGO_SUPERIOR_JERARQUICO,
                          BASICO_NIVEL,
                          NIVEL_COMPLEMENTARIO,
                          IDIOMA_NIVEL,
                          IDIOMA_ESPECIFICACION,
                          DIGITAL_TRANSVERSAL,
                          ACTIVO,
                          JUSTIF_COMPETENCIA_DIG,                          
                          TELETRABAJO,
                          DETALLES_TELETRABAJO
                          ) 
        (SELECT DENOMINACION, 
                          CATEGORY_ENEL, 
                          COMPLEMENTARIO_NIVEL, 
                          FUNCTIONAL_AREA, 
                          SUBGRUPO_PROFESIONAL, 
                          ID_CARGO_PADRE,
                          CODIGO_FUNCION, 
                          CODIGO_HOMOLOGACION, 
                          ROLES, 
                          HOMOLOGACION,  
                          TIPO_UBICACION, 
                          ID_UNIDAD_ORGANIZATIVA, 
                          COMPLEMENTARIO_ESPECIFICACION, 
                          MISION, 
                          EXPERIENCIA, 
                          BASICO_ESPECIFICACION, 
                          IDIOMA_EXTRA_ESPECIFICACION, 
                          DIGITAL_ESPECIFICA,                            
                          OTROS,
                          ELABORO,
                          PERSONAS_DIRECTAS,                               
                          PERSONAS_INDIRECTAS,
                          RECURSOS_EMPRESAS,
                          RECURSOS_PERSONAS,
                          JUSTIF_SOFTWARE,
                          JUSTIF_LICENCIAS,                          
                          JUSTIF_ECONOMICA,
                          JUSTIF_CUALITATIVA,
                          JUSTIF_DES_TOMAR,
                          JUSTIF_DES_PROPONER,
                          JUSTIF_INTERNAS,
                          JUSTIF_EXTERNAS,
                          CARGO_SUPERIOR_JERARQUICO,
                          BASICO_NIVEL,
                          NIVEL_COMPLEMENTARIO,
                          IDIOMA_NIVEL,
                          IDIOMA_ESPECIFICACION,
                          DIGITAL_TRANSVERSAL,
                          ACTIVO,
                          JUSTIF_COMPETENCIA_DIG,                          
                          TELETRABAJO,
                          DETALLES_TELETRABAJO
        FROM CARGO
        WHERE ID = I_CARGO);
        
        select id into l_id_cargo from cargo where 
        id_cargo_padre = (select id_cargo_padre from cargo where id =I_CARGO) and vigencia is null;

        insert into decision (descripcion,tipo,id_cargo) 
        (select descripcion,tipo,l_id_cargo from decision where id_cargo = I_CARGO);

        insert into dimension (tipo_variable,variable,impacto,descripcion,meta_anual,id_cargo) 
        (select tipo_variable,variable,impacto,descripcion,meta_anual,l_id_cargo from dimension where id_cargo = I_CARGO);

        insert into licencia_matricula (descripcion,tipo,id_cargo) 
        (select descripcion,tipo,l_id_cargo from licencia_matricula where id_cargo = I_CARGO);
                
        insert into cargo_competencia_especifica (id_digital_skill,especificacion,id_cargo) 
        (select id_digital_skill,especificacion, l_id_cargo from cargo_competencia_especifica where id_cargo = I_CARGO);                

        insert into responsabilidad_contactos (tipo,interaccion,proposito,frecuencia,id_cargo) 
        (select tipo,interaccion,proposito,frecuencia,l_id_cargo from responsabilidad_contactos where id_cargo = I_CARGO);

        insert into responsabilidad_estrategia (descripcion,id_cargo) 
        (select descripcion,l_id_cargo from responsabilidad_estrategia where id_cargo = I_CARGO);

        insert into responsabilidad_principal (tipo,descripcion,numero,id_cargo) 
        (select tipo,descripcion,numero,l_id_cargo from responsabilidad_principal where id_cargo = I_CARGO);

        insert into software (descripcion,id_cargo) 
        (select descripcion,l_id_cargo from software where id_cargo = I_CARGO);

    ELSE

        UPDATE CARGO
        SET     DENOMINACION =  (SELECT DENOMINACION FROM CARGO WHERE ID = I_CARGO), 
                CATEGORY_ENEL = (SELECT CATEGORY_ENEL FROM CARGO WHERE ID = I_CARGO), 
                COMPLEMENTARIO_NIVEL = (SELECT COMPLEMENTARIO_NIVEL FROM CARGO WHERE ID = I_CARGO) , 
                FUNCTIONAL_AREA = (SELECT FUNCTIONAL_AREA FROM CARGO WHERE ID = I_CARGO), 
                SUBGRUPO_PROFESIONAL = (SELECT  SUBGRUPO_PROFESIONAL FROM CARGO WHERE ID = I_CARGO),
                CODIGO_FUNCION = (SELECT CODIGO_FUNCION FROM CARGO WHERE ID = I_CARGO), 
                CODIGO_HOMOLOGACION = (SELECT CODIGO_HOMOLOGACION FROM CARGO WHERE ID = I_CARGO), 
                ROLES = (SELECT ROLES FROM CARGO WHERE ID = I_CARGO), 
                HOMOLOGACION = (SELECT HOMOLOGACION FROM CARGO WHERE ID = I_CARGO), 
                JUSTIFICACION = (SELECT JUSTIFICACION FROM CARGO WHERE ID = I_CARGO), 
                TIPO_UBICACION = (SELECT TIPO_UBICACION FROM CARGO WHERE ID = I_CARGO), 
                ID_UNIDAD_ORGANIZATIVA = (SELECT ID_UNIDAD_ORGANIZATIVA FROM CARGO WHERE ID = I_CARGO), 
                COMPLEMENTARIO_ESPECIFICACION = (SELECT COMPLEMENTARIO_ESPECIFICACION FROM CARGO WHERE ID = I_CARGO), 
                ID_INSTANCIA_PROCESO = (SELECT ID_INSTANCIA_PROCESO FROM CARGO WHERE ID = I_CARGO), 
                ESTADO = (SELECT ESTADO FROM CARGO WHERE ID = I_CARGO), 
                MISION = (SELECT MISION FROM CARGO WHERE ID = I_CARGO), 
                EXPERIENCIA = (SELECT  EXPERIENCIA FROM CARGO WHERE ID = I_CARGO), 
                BASICO_ESPECIFICACION = (SELECT BASICO_ESPECIFICACION FROM CARGO WHERE ID = I_CARGO), 
                IDIOMA_EXTRA_ESPECIFICACION = (SELECT IDIOMA_EXTRA_ESPECIFICACION FROM CARGO WHERE ID = I_CARGO), 
                DIGITAL_ESPECIFICA = (SELECT  DIGITAL_ESPECIFICA FROM CARGO WHERE ID = I_CARGO),                  
                OTROS = (SELECT OTROS FROM CARGO WHERE ID = I_CARGO)
        WHERE ID = i_CARGO;
    END IF;

 END DUPLICAR_FORMULARIO;

 -------------------------------------------------------------------------------------------------------------
 PROCEDURE GENERAR_CONCEPTO(I_CONCEPTO_CARGO NUMBER, I_CARGO NUMBER, I_CONCEPTO NUMBER, I_TIPO NUMBER, I_COMENTARIO VARCHAR, I_INSTANCIA_PROCESO NUMBER,I_USUARIO VARCHAR2) IS
   i_cargo_consultado NUMBER;

 BEGIN
    SELECT ID INTO i_cargo_consultado FROM CONCEPTO_CARGO WHERE ID_CARGO = I_CARGO AND TIPO = I_TIPO;  
        UPDATE CONCEPTO_CARGO
        SET CONCEPTO = i_concepto, TIPO = i_tipo, COMENTARIO = i_comentario, USUARIO = i_usuario
        WHERE ID = i_cargo_consultado;

       EXCEPTION WHEN NO_DATA_FOUND THEN
        INSERT INTO CONCEPTO_CARGO(ID_CARGO, CONCEPTO, TIPO, COMENTARIO,USUARIO)
        VALUES ( i_cargo, i_concepto, i_tipo, i_comentario,i_usuario);

 END GENERAR_CONCEPTO;

  --------------------------------------------------------------------------------------------------
  PROCEDURE ELIMINAR_NO_APLICA(I_CARGO NUMBER) IS   

    l_conteo  number;

 BEGIN

    --TODO: RESPONSABILIDAD_PRINCIPAL.TIPO REFERECNIAR A TIPO


        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_SOFTWARE is not null;

        if l_conteo > 0 then                
            delete from software where ID_CARGO = I_CARGO;    
        end if;  

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_LICENCIAS is not null;        

        if l_conteo > 0 then  
            delete from licencia_matricula where ID_CARGO = I_CARGO;    
        end if;  
        
        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_COMPETENCIA_DIG is not null;        

        if l_conteo > 0 then  
            delete from cargo_competencia_especifica where ID_CARGO = I_CARGO;    
        end if;  

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_ECONOMICA is not null;                

        if l_conteo > 0 then                 
            delete from dimension where TIPO_VARIABLE = SNW_CONSTANTES.CONSTANTE_TIPO('ECONOMICA') and ID_CARGO = I_CARGO;    
        end if;  

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_CUALITATIVA is not null;                

        if l_conteo > 0 then                          
            delete from dimension where TIPO_VARIABLE = SNW_CONSTANTES.CONSTANTE_TIPO('CUALITATIVA') and ID_CARGO = I_CARGO;    
        end if;  

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_DES_TOMAR is not null;                

        if l_conteo > 0 then                                          
            delete from decision where TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('TOMAR') and ID_CARGO = I_CARGO;    
        end if;  

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_DES_PROPONER is not null;                

        if l_conteo > 0 then              
            delete from decision where TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('PROPONER') and ID_CARGO = I_CARGO;    
        end if; 

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_INTERNAS is not null;                

        if l_conteo > 0 then                          
            delete from responsabilidad_contactos where TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('INTERNAS') and ID_CARGO = I_CARGO;    
        end if;  

        select count(*) into l_conteo from CARGO WHERE ID = I_CARGO and JUSTIF_EXTERNAS is not null;                

        if l_conteo > 0 then            
            delete from responsabilidad_contactos where TIPO = SNW_CONSTANTES.CONSTANTE_TIPO('EXTERNAS') and ID_CARGO = I_CARGO;    
        end if;  

 END ELIMINAR_NO_APLICA;

 --------------------------------------------------------------------------------------------------
  PROCEDURE RESPONSABILIDADES_OBLIGATORIAS(I_CARGO NUMBER) IS   
   l_conteo_transversales NUMBER;
   l_id_responsabilidad NUMBER;

 BEGIN

        --TODO: RESPONSABILIDAD_PRINCIPAL.TIPO REFERECNIAR A TIPO

        -- Se eliminan las transversales (para cualquier efecto: modificacion, eliminacion o duplicacion via formulario)
        DELETE FROM RESPONSABILIDAD_PRINCIPAL WHERE TIPO = 'Transversal' AND ID_CARGO = I_CARGO;

        -- Se insertan las dos transversales obligatorias con numeros por defecto en 1 y 2               
        INSERT INTO RESPONSABILIDAD_PRINCIPAL (descripcion,id_cargo,tipo,numero)
        VALUES (  'Apropiar y dar cumplimiento a las políticas, documentación y estándares establecidos en los Sistemas Integrados de Gestión de la organización; así como participar de manera activa en las actividades orientadas a la prevención del riesgo, la cultura del autocuidado, cuidado del medio ambiente y el mejoramiento continuo de los diferentes sistemas de gestión.' ,
        I_CARGO, 'Transversal', 1);                            

        INSERT INTO RESPONSABILIDAD_PRINCIPAL (descripcion,id_cargo,tipo,numero)
        VALUES ( 'Desarrollar las demás actividades relacionadas e inherentes al cargo y aquellas que le sean asignadas, orientadas al cumplimiento de los objetivos, proyectos e iniciativas de la unidad y de la Organización.' ,
        I_CARGO, 'Transversal', 2);

 END RESPONSABILIDADES_OBLIGATORIAS;

  --------------------------------------------------------------------------------------------------
  PROCEDURE REORDENAR_RESPONSABILIDADES(I_CARGO NUMBER) IS
   l_numeradas NUMBER;
   l_inicial NUMBER;
 BEGIN

        -- << Procedimiento nuevo para el numero EDITABLE por el usuario >>

        -- * Se respetara la numeracion que el usuario haga, por ejemplo si coloca (3,1,20,5) apareceran ordenadas (1,3,5,20)
        -- * Las responsabilidades con numero null (omitido por el usuario) automaticamente seguiran con el maximo valor de las especificas
        -- * En este caso (21,22,23...)

        -- El maximo de las especificas para asignar a partir de este, a las que esten nulas   
        select max(numero) INTO l_inicial from RESPONSABILIDAD_PRINCIPAL where numero is not null and TIPO <> 'Transversal' and ID_CARGO = I_CARGO;            

        if l_inicial is null then
            l_inicial := 0;
        end if;

        -- Se asigna un numero a las especificas nulas
        for i in (select id from RESPONSABILIDAD_PRINCIPAL where numero is null and TIPO <> 'Transversal' and ID_CARGO = I_CARGO) loop

            UPDATE RESPONSABILIDAD_PRINCIPAL SET NUMERO = l_inicial+1 where id = i.id;
            l_inicial := l_inicial + 1;

        end loop;

        -- Se asigna los ultimos dos numeros a las transversales
        for i in (select id from RESPONSABILIDAD_PRINCIPAL where TIPO = 'Transversal' and ID_CARGO = I_CARGO) loop

            UPDATE RESPONSABILIDAD_PRINCIPAL SET NUMERO = l_inicial+1 where id = i.id;
            l_inicial := l_inicial + 1;

        end loop;


 END REORDENAR_RESPONSABILIDADES;


 --------------------------------------------------------------------------------------------------
 PROCEDURE dep_ORDENAR_RESPONSABILIDADES(I_CARGO NUMBER) IS
   l_numeradas NUMBER;
   l_inicial NUMBER := 1;

 BEGIN 

        -- << Procedimiento que se ejecutaba cuando el numero de las responsabilidades NO ERA EDITABLE >>

        for i in (select id from RESPONSABILIDAD_PRINCIPAL where numero is not null and ID_CARGO = I_CARGO order by numero) loop

                UPDATE RESPONSABILIDAD_PRINCIPAL SET NUMERO = l_inicial where id = i.id;
                l_inicial := l_inicial + 1;

        end loop;


        select count(*) into l_numeradas from RESPONSABILIDAD_PRINCIPAL where numero is not null and TIPO <> 'Transversal' and ID_CARGO = I_CARGO;



        for i in (select id from RESPONSABILIDAD_PRINCIPAL where numero is null and TIPO <> 'Transversal' and ID_CARGO = I_CARGO) loop

                UPDATE RESPONSABILIDAD_PRINCIPAL SET NUMERO = l_numeradas+1 where id = i.id;
                l_numeradas := l_numeradas + 1;

        end loop;

        for i in (select id from RESPONSABILIDAD_PRINCIPAL where TIPO = 'Transversal' and ID_CARGO = I_CARGO) loop

                UPDATE RESPONSABILIDAD_PRINCIPAL SET NUMERO = l_numeradas+1 where id = i.id;
                l_numeradas := l_numeradas + 1;

        end loop;


 END dep_ORDENAR_RESPONSABILIDADES;


 --------------------------------------------------------------------------------------------------
 FUNCTION  DENOMINACION_DE_USUARIO(I_USER_NAME VARCHAR2) RETURN VARCHAR2 AS 

  l_denominacion varchar2(4000);
  l_conteo number;

  BEGIN

    select count(*) into l_conteo from cargo where id = 
    (select id_cargo from posicion_Cargo where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and id_posicion = 
    (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
    (select id from trabajador where NUMERO_IDENTIFICACION = 
    (select numero_identificacion from usuario where UPPER(user_name) = UPPER(I_USER_NAME)))));

    if l_conteo > 0 then

        select denominacion into l_denominacion from cargo where id = 
        (select id_cargo from posicion_Cargo where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and id_posicion = 
        (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
        (select id from trabajador where NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where UPPER(user_name) = UPPER(I_USER_NAME)))));    

    else

        select nombre denominacion into l_denominacion from posicion where id =     
        (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
        (select id from trabajador where NUMERO_IDENTIFICACION = 
        (select numero_identificacion from usuario where UPPER(user_name) = UPPER(I_USER_NAME))));    

    end if;

    RETURN l_denominacion;    

    exception 

    when no_data_found then 
         return 'Cargo sin legalizar en sistema';

    when others THEN
         return 'Inconsistencia en la posicion del trabajador o el cargo de la posicion';

  END DENOMINACION_DE_USUARIO;

--------------------------------------------------------------------------------------------------
FUNCTION  DENOMINACION_CARGO_TRABAJADOR(i_numero_asignado VARCHAR2) RETURN VARCHAR2 AS 

  l_denominacion varchar2(4000);
  l_conteo number;

BEGIN
 
    -- Se verifica si el trabajador tiene un cargo asociado
    select count(*) into l_conteo from cargo where id = 
    (select id_cargo from posicion_cargo where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and id_posicion = 
    (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
    (select id from trabajador where numero_identificacion = i_numero_asignado)));

    if l_conteo > 0 then

        select denominacion into l_denominacion from cargo where id = 
        (select id_cargo from posicion_Cargo where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and id_posicion = 
        (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
        (select id from trabajador where numero_identificacion = i_numero_asignado)));    

    else

        -- Se verifica si el trabajador tiene una posicion asociada
        select count(*) into l_conteo from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
        (select id from trabajador where numero_identificacion = i_numero_asignado); 

         if l_conteo > 0 then

            select nombre into l_denominacion from posicion where id =     
            (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin or fecha_fin is null) and ID_TRABAJADOR = 
            (select id from trabajador where numero_identificacion = i_numero_asignado));   
        
        else
        
            -- Trabajador sin cargo y sin posicion
            l_denominacion := 'Cargo sin legalizar en sistema';
        
        end if;
        
    end if;

    RETURN l_denominacion;    

EXCEPTION 
    when no_data_found then 
        return 'Cargo sin legalizar en sistema';
    when too_many_rows then
		return 'Inconsistencia en la posición del trabajador o el cargo de la posición';
    when others then
        return 'Cargo imposible de determinar';

END DENOMINACION_CARGO_TRABAJADOR;

--------------------------------------------------------------------------------------------------
 FUNCTION  ID_CARGO_DE_USUARIO(I_USER_NAME VARCHAR2) RETURN NUMBER AS 

  l_id_cargo NUMBER;
  l_conteo NUMBER;

  BEGIN
    select id_cargo into l_id_cargo from posicion_Cargo where (sysdate between fecha_inicio and fecha_fin  or fecha_fin is null) and id_posicion = 
    (select id_posicion from posicion_trabajador where (sysdate between fecha_inicio and fecha_fin  or fecha_fin is null) and ID_TRABAJADOR = 
    (select id from trabajador where NUMERO_IDENTIFICACION = 
    (select numero_identificacion from usuario where user_name = I_USER_NAME)));

    select count(*) into l_conteo from cargo where id = l_id_cargo;

    if l_conteo = 0 then
        return null;
    end if;

    RETURN l_id_cargo;

    exception when no_data_found then return null;

  END ID_CARGO_DE_USUARIO;   

--------------------------------------------------------------------------------------------------  
  PROCEDURE DUPLICAR_FORMULARIO(I_CARGO NUMBER, I_CARGO_DUPLICADO IN OUT NUMBER) IS
    comprobar_vigencia char;
    comprobar_version number;
    l_conteo number;
    l_id_cargo number;
    
 BEGIN
    select count(*) into l_conteo from cargo where 
    id_cargo_padre = (select id_cargo_padre from cargo where id = I_CARGO) and vigencia is null;
    
    if l_conteo = 0 then
    
        insert into cargo(id) values (null) returning id into I_CARGO_DUPLICADO;
        
        update CARGO set (DENOMINACION, 
                          CATEGORY_ENEL, 
                          COMPLEMENTARIO_NIVEL, 
                          FUNCTIONAL_AREA, 
                          SUBGRUPO_PROFESIONAL, 
                          ID_CARGO_PADRE,
                          CODIGO_FUNCION, 
                          CODIGO_HOMOLOGACION, 
                          ROLES, 
                          HOMOLOGACION,  
                          TIPO_UBICACION, 
                          ID_UNIDAD_ORGANIZATIVA, 
                          COMPLEMENTARIO_ESPECIFICACION, 
                          MISION, 
                          EXPERIENCIA, 
                          BASICO_ESPECIFICACION, 
                          IDIOMA_EXTRA_ESPECIFICACION, 
                          DIGITAL_ESPECIFICA,  
                          OTROS,
                          ELABORO,
                          PERSONAS_DIRECTAS,                               
                          PERSONAS_INDIRECTAS,
                          RECURSOS_EMPRESAS,
                          RECURSOS_PERSONAS,
                          JUSTIF_SOFTWARE,
                          JUSTIF_LICENCIAS,                        
                          JUSTIF_COMPETENCIA_DIG,
                          JUSTIF_ECONOMICA,
                          JUSTIF_CUALITATIVA,
                          JUSTIF_DES_TOMAR,
                          JUSTIF_DES_PROPONER,
                          JUSTIF_INTERNAS,
                          JUSTIF_EXTERNAS,
                          CARGO_SUPERIOR_JERARQUICO,
                          BASICO_NIVEL,
                          NIVEL_COMPLEMENTARIO,
                          IDIOMA_NIVEL,
                          IDIOMA_ESPECIFICACION,
                          DIGITAL_TRANSVERSAL
                          ) =
            (SELECT DENOMINACION, 
                          CATEGORY_ENEL, 
                          COMPLEMENTARIO_NIVEL, 
                          FUNCTIONAL_AREA, 
                          SUBGRUPO_PROFESIONAL, 
                          ID_CARGO_PADRE,
                          CODIGO_FUNCION, 
                          CODIGO_HOMOLOGACION, 
                          ROLES, 
                          HOMOLOGACION,  
                          TIPO_UBICACION, 
                          ID_UNIDAD_ORGANIZATIVA, 
                          COMPLEMENTARIO_ESPECIFICACION, 
                          MISION, 
                          EXPERIENCIA, 
                          BASICO_ESPECIFICACION, 
                          IDIOMA_EXTRA_ESPECIFICACION, 
                          DIGITAL_ESPECIFICA,                            
                          OTROS,
                          ELABORO,
                          PERSONAS_DIRECTAS,                               
                          PERSONAS_INDIRECTAS,
                          RECURSOS_EMPRESAS,
                          RECURSOS_PERSONAS,
                          JUSTIF_SOFTWARE,
                          JUSTIF_LICENCIAS,
                          JUSTIF_COMPETENCIA_DIG,
                          JUSTIF_ECONOMICA,
                          JUSTIF_CUALITATIVA,
                          JUSTIF_DES_TOMAR,
                          JUSTIF_DES_PROPONER,
                          JUSTIF_INTERNAS,
                          JUSTIF_EXTERNAS,
                          CARGO_SUPERIOR_JERARQUICO,
                          BASICO_NIVEL,
                          NIVEL_COMPLEMENTARIO,
                          IDIOMA_NIVEL,
                          IDIOMA_ESPECIFICACION,
                          DIGITAL_TRANSVERSAL
                          FROM CARGO
                          WHERE ID = I_CARGO)
            WHERE ID = I_CARGO_DUPLICADO;
        
                    
            select id into l_id_cargo from cargo where 
            id_cargo_padre = (select id_cargo_padre from cargo where id = I_CARGO) and vigencia is null;
    
            insert into decision (descripcion,tipo,id_cargo) 
            (select descripcion,tipo,l_id_cargo from decision where id_cargo = I_CARGO);
    
            insert into dimension (tipo_variable,variable,impacto,descripcion,meta_anual,id_cargo) 
            (select tipo_variable,variable,impacto,descripcion,meta_anual,l_id_cargo from dimension where id_cargo = I_CARGO);
    
            insert into licencia_matricula (descripcion,tipo,id_cargo) 
            (select descripcion,tipo,l_id_cargo from licencia_matricula where id_cargo = I_CARGO);
                    
            insert into cargo_competencia_especifica (id_digital_skill,especificacion,id_cargo) 
            (select id_digital_skill,especificacion, l_id_cargo from cargo_competencia_especifica where id_cargo = I_CARGO);                
    
            insert into responsabilidad_contactos (tipo,interaccion,proposito,frecuencia,id_cargo) 
            (select tipo,interaccion,proposito,frecuencia,l_id_cargo from responsabilidad_contactos where id_cargo = I_CARGO);
    
            insert into responsabilidad_estrategia (descripcion,id_cargo) 
            (select descripcion,l_id_cargo from responsabilidad_estrategia where id_cargo = I_CARGO);
    
            insert into responsabilidad_principal (tipo,descripcion,numero,id_cargo) 
            (select tipo,descripcion,numero,l_id_cargo from responsabilidad_principal where id_cargo = I_CARGO);
    
            insert into software (descripcion,id_cargo) 
            (select descripcion,l_id_cargo from software where id_cargo = I_CARGO);

    ELSE

        UPDATE CARGO
        SET     DENOMINACION =  (SELECT DENOMINACION FROM CARGO WHERE ID = I_CARGO), 
                CATEGORY_ENEL = (SELECT CATEGORY_ENEL FROM CARGO WHERE ID = I_CARGO), 
                COMPLEMENTARIO_NIVEL = (SELECT COMPLEMENTARIO_NIVEL FROM CARGO WHERE ID = I_CARGO) , 
                FUNCTIONAL_AREA = (SELECT FUNCTIONAL_AREA FROM CARGO WHERE ID = I_CARGO), 
                SUBGRUPO_PROFESIONAL = (SELECT  SUBGRUPO_PROFESIONAL FROM CARGO WHERE ID = I_CARGO),
                CODIGO_FUNCION = (SELECT CODIGO_FUNCION FROM CARGO WHERE ID = I_CARGO), 
                CODIGO_HOMOLOGACION = (SELECT CODIGO_HOMOLOGACION FROM CARGO WHERE ID = I_CARGO), 
                ROLES = (SELECT ROLES FROM CARGO WHERE ID = I_CARGO), 
                HOMOLOGACION = (SELECT HOMOLOGACION FROM CARGO WHERE ID = I_CARGO), 
                JUSTIFICACION = (SELECT JUSTIFICACION FROM CARGO WHERE ID = I_CARGO), 
                TIPO_UBICACION = (SELECT TIPO_UBICACION FROM CARGO WHERE ID = I_CARGO), 
                ID_UNIDAD_ORGANIZATIVA = (SELECT ID_UNIDAD_ORGANIZATIVA FROM CARGO WHERE ID = I_CARGO), 
                COMPLEMENTARIO_ESPECIFICACION = (SELECT COMPLEMENTARIO_ESPECIFICACION FROM CARGO WHERE ID = I_CARGO), 
                ID_INSTANCIA_PROCESO = (SELECT ID_INSTANCIA_PROCESO FROM CARGO WHERE ID = I_CARGO), 
                ESTADO = (SELECT ESTADO FROM CARGO WHERE ID = I_CARGO), 
                MISION = (SELECT MISION FROM CARGO WHERE ID = I_CARGO), 
                EXPERIENCIA = (SELECT  EXPERIENCIA FROM CARGO WHERE ID = I_CARGO), 
                BASICO_ESPECIFICACION = (SELECT BASICO_ESPECIFICACION FROM CARGO WHERE ID = I_CARGO), 
                IDIOMA_EXTRA_ESPECIFICACION = (SELECT IDIOMA_EXTRA_ESPECIFICACION FROM CARGO WHERE ID = I_CARGO), 
                DIGITAL_ESPECIFICA = (SELECT  DIGITAL_ESPECIFICA FROM CARGO WHERE ID = I_CARGO),                  
                OTROS = (SELECT OTROS FROM CARGO WHERE ID = I_CARGO)
        WHERE ID = i_CARGO;
        
        I_CARGO_DUPLICADO := I_CARGO;
    END IF;
    
    dbms_output.put_line(' origen:' || I_CARGO || ' duplicado: ' || I_CARGO_DUPLICADO || ' DUPLICAR FORMULARIO NUEVO !!');            
    
    exception when others then
        dbms_output.put_line(sqlerrm || ' origen:' || I_CARGO || ' duplicado: ' || I_CARGO_DUPLICADO || ' E.R.R.O.R en DUPLICAR FORMULARIO NUEVO !!');            

 END DUPLICAR_FORMULARIO;
 
 
 FUNCTION  DETERMINAR_DIGITALSK_NOMBRE(I_CARGO NUMBER) RETURN VARCHAR2 AS
    l_digital_skills VARCHAR2(4000) := '';    
    
    BEGIN
    
        for i in (select (SELECT NOMBRE FROM DIGITAL_SKILL where id = ID_DIGITAL_SKILL) nombre 
            from cargo_competencia_especifica where id_cargo = I_CARGO)
        loop 
            begin                 
             l_digital_skills := l_digital_skills  || ', ' ||  i.nombre;
            exception
            when no_data_found then
                null;    
            end;
    
        end loop;
            l_digital_skills := SUBSTR(l_digital_skills,3);
    
        RETURN  l_digital_skills;
    
    END DETERMINAR_DIGITALSK_NOMBRE;

 FUNCTION  DETERMINAR_DIGITALSK_ESPEC(I_CARGO NUMBER) RETURN VARCHAR2 AS
    l_digital_skills VARCHAR2(4000) := '';    
    
    BEGIN
    
        for i in (select ESPECIFICACION from cargo_competencia_especifica where id_cargo = I_CARGO)
        loop 
            begin                 
             l_digital_skills := l_digital_skills  || ', ' ||  i.ESPECIFICACION;
            exception
            when no_data_found then
                null;    
            end;
    
        end loop;
            l_digital_skills := SUBSTR(l_digital_skills,3);
    
        RETURN  l_digital_skills;
    
    END DETERMINAR_DIGITALSK_ESPEC;

END FORMULARIO;
/