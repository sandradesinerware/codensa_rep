
  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."ADMIN_VALIDACION_PCK" AS 

function completitud_identificacion(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function completitud_ubicacion(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  

function completitud_responsabilidades(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  
function completitud_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  

function completitud_otros(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 
    
--------------------------------------------------------------------  

function completitud_digital_skills(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------  

function completitud_dimensiones(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

-------------------------------------------------------------------- 

function completitud_decisiones(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

-------------------------------------------------------------------- 

function completitud_contactos(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

-------------------------------------------------------------------- 

function existe_concepto_bp(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function existe_concepto_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function concepto_bp(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function concepto_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

function cancela_dc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN; 

--------------------------------------------------------------------    

END ADMIN_VALIDACION_PCK;
/
CREATE OR REPLACE PACKAGE BODY "SNW_CARGOS"."ADMIN_VALIDACION_PCK" AS

  function completitud_identificacion(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';
  select count(*) into l_conteo from cargo where id = i_number and DENOMINACION is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Denominaci�n<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and SUBGRUPO_PROFESIONAL is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Subgrupo profesional<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and mision is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Misi�n<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and CATEGORY_ENEL is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Category ENEL<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and FUNCTIONAL_AREA is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Functional Area<br>';
  end if;
  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_identificacion;

  ----------------------------------------------------------------------------------------------------------------------------


  function completitud_ubicacion(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';
  select count(*) into l_conteo from cargo where id = i_number and id_UNIDAD_ORGANIZATIVA is null and
  tipo_ubicacion = snw_constantes.constante_tipo('ESPECIFICO');
  if l_conteo > 0 then 
    l_bandera := true;
    l_error := l_error||'- Unidad organizativa<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and personas_directas is null and subgrupo_profesional in
  (select id from subgrupo_profesional where jefe='S') and tipo_ubicacion = snw_constantes.constante_tipo('ESPECIFICO');
  if l_conteo = 1 then 
    l_bandera := true;
    l_error := l_error||'- Personas indirectas<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and personas_indirectas is null and subgrupo_profesional in
  (select id from subgrupo_profesional where jefe='S') and tipo_ubicacion = snw_constantes.constante_tipo('ESPECIFICO');
  if l_conteo = 1 then 
    l_bandera := true;
    l_error := l_error||'- Personas indirectas<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and Recursos_empresas is null and subgrupo_profesional in
  (select id from subgrupo_profesional where jefe='S') and tipo_ubicacion = snw_constantes.constante_tipo('ESPECIFICO');
  if l_conteo = 1 then 
    l_bandera := true;
    l_error := l_error||'- Recursos empresas<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and Recursos_personas is null and subgrupo_profesional in
  (select id from subgrupo_profesional where jefe='S') and tipo_ubicacion = snw_constantes.constante_tipo('ESPECIFICO');
  if l_conteo = 1 then 
    l_bandera := true;
    l_error := l_error||'- Recursos personas<br>';
  end if;
  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_ubicacion;


  ----------------------------------------------------------------------------------------------------------------------------


  function completitud_responsabilidades(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  l_limite_especificas number := 8; -- Este ser� el m�ximo de responsabilidades espec�ficas permitidas por la aplicaci�n
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';
  select count(*) into l_conteo from responsabilidad_principal where id_cargo = i_number and tipo='Espec�fica';
  if l_conteo > l_limite_especificas then 
    l_bandera := true;
    l_error := l_error||'- El campo Responsabilidades del Cargo excede el numero de filas establecido<br>';
  end if;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- El campo Responsabilidades del Cargo debe tener al menos una responsabilidad de tipo espec�fica.<br>';  
  else 
    select min(numero) into l_conteo from responsabilidad_principal where id_cargo = i_number and tipo <> 'Transversal';
      for i in (
            select numero from responsabilidad_principal where id_cargo = i_number and tipo <> 'Transversal' order by numero )
      LOOP
          if l_conteo <> i.numero then 
            l_bandera := true;
            l_error := l_error||'- El campo Responsabilidades del Cargo tiene n�meros asignados repetidos, o no respeta el orden requerido<br>';
            exit;
          end if;
          l_conteo := l_conteo + 1;  
      end LOOP;
  end if;

  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_responsabilidades;

   ----------------------------------------------------------------------------------------------------------------------------

  function completitud_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
    l_conteo number;
    l_conteo_codigo_usado number;
    l_codigo number;
    BEGIN
        select count(*) into l_conteo from cargo where id = i_number and homologacion is not null 
        and codigo_funcion is not null and codigo_homologacion is not null;
        
    if l_conteo > 0 then
    
        select count(*) into l_conteo_codigo_usado from cargo where id = i_number and 
        codigo_funcion in (select codigo_funcion from v_cargo_vigente);
        
        if l_conteo_codigo_usado = 0 then
            RETURN TRUE;
        else
            select codigo_funcion into l_codigo from cargo where id = i_number;
            Raise_Application_Error(-20000,'El C�digo DC: ' || l_codigo || ' ya existe en una Descripci�n de Cargo vigente');
        end if;
    
    else
        Raise_Application_Error(-20000,'El campo Homologaci�n, C�digo homologaci�n y C�digo DC debe diligenciarlos.');
    end if;
    
    END completitud_poc;

--------------------------------------------------------------------  
  function completitud_otros(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  l_subgrupo number;
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';    
  select count(*) into l_conteo from cargo where id = i_number and basico_especificacion is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Educaci�n b�sica: Especificaci�n<br>';
  end if;
  select count(*) into l_conteo from cargo where id = i_number and experiencia is not null;
  if l_conteo = 0 then 
    l_bandera := true;
    l_error := l_error||'- Experiencia<br>';
  end if;  
  select count(*) into l_conteo from cargo where id = i_number and complementario_especificacion is not null;
  select subgrupo_profesional into l_subgrupo from cargo where id = i_number;
  if l_conteo = 0 and l_subgrupo in (1,2,3,4,6,20) then 
    l_bandera := true;
    l_error := l_error||'- Educaci�n complementaria: Especificaci�n<br>'; 
  end if;    
  select count(*) into l_conteo from software where id_cargo = i_number;
  select decode(justif_software,'','S','N') into l_aplica from cargo where id = i_number;
  if l_conteo > 6 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Software excede el n�mero de filas establecido<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Software debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if;
  select decode(justif_licencias,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from licencia_matricula where id_cargo = i_number;
  if l_conteo > 3 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Licencias/Matriculas/Certificaciones excede el n�mero de filas establecido.<br>';
  end if;
   if l_conteo = 0  and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Licencias/Matriculas/Certificaciones debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if; 
   if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_otros;
  

  ----------------------------------------------------------------------------------------------------------------------------
  
  function completitud_digital_skills(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_digital_otra number := 24;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';  
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';
  select decode(justif_competencia_dig,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from cargo_competencia_especifica where id_cargo = i_number;
  -- if l_conteo > l_limite_especificas then 
  --  l_bandera := true;
  --  l_error := l_error||'- El campo Responsabilidades del Cargo excede el numero de filas establecido<br>';
  -- end if;
  if l_aplica = 'S' then
      if l_conteo = 0 then 
        l_bandera := true;
        l_error := l_error||'- El campo Digital Skills debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n.<br>';  
      else 
        -- Revisar cada registro y para cada competencia del tipo Otra, debe haber una especificacion No nulla
          for i in ( 
                select especificacion from cargo_competencia_especifica where id_cargo = i_number and id_digital_skill = l_digital_otra)
          LOOP
              if i.especificacion is null then 
                l_bandera := true;
                l_error := l_error||'- Los campos Digital Skills de tipo "Otra" deben tener una especificaci�n NO nula.<br>';
                exit;
              end if;          
          end LOOP;
      end if;
  end if;

  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_digital_skills;

   ----------------------------------------------------------------------------------------------------------------------------


  function completitud_dimensiones(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';  
  select decode(justif_economica,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from dimension where tipo_variable = snw_constantes.constante_tipo('ECONOMICA') and id_cargo=i_number;
  if l_conteo > 4 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Dimensiones Variable Cuantitativa excede el n�mero de filas establecido<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Dimensiones Variable Cuantitativa debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if;
  select decode(justif_cualitativa,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from dimension where tipo_variable = snw_constantes.constante_tipo('CUALITATIVA') and id_cargo=i_number;
  if l_conteo > 5 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Dimensiones Variable Cualitativa excede el n�mero de filas establecido<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Dimensiones Variable Cualitativa debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if;
  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_dimensiones;

  ----------------------------------------------------------------------------------------------------------------------------


  function completitud_decisiones(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';    
  select decode(justif_des_tomar,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from decision where tipo = snw_constantes.constante_tipo('TOMAR') and id_cargo=i_number;
  if l_conteo > 4 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Decisiones que puede y debe tomar excede el n�mero de filas establecido<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Decisiones que puede y debe tomar debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if;  
  select decode(justif_des_proponer,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from decision where tipo = snw_constantes.constante_tipo('PROPONER') and id_cargo=i_number;
  if l_conteo > 4 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Decisiones que puede y debe proponer excede el n�mero de filas establecido<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Decisiones que puede y debe proponer debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if; 
  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_decisiones;


    ----------------------------------------------------------------------------------------------------------------------------


  function completitud_contactos(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  l_error varchar2(4000);
  l_bandera boolean := false;
  l_aplica char := 'N';
  BEGIN
  l_error := 'Requiere diligenciar los siguientes campos:<br>';    
  select decode(justif_internas,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from responsabilidad_contactos where tipo = snw_constantes.constante_tipo('INTERNAS') and id_cargo=i_number;
  if l_conteo > 4 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Relaciones internas que inciden con el cargo excede el n�mero de filas establecido<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Relaciones internas que inciden con el cargo debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if;
  select decode(justif_externas,'','S','N') into l_aplica from cargo where id = i_number;
  select count(*) into l_conteo from responsabilidad_contactos where tipo = snw_constantes.constante_tipo('EXTERNAS') and id_cargo=i_number;
  if l_conteo > 4 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Relaciones externas que inciden con el cargo excede el n�mero de filas establecido.<br>';
  end if;
  if l_conteo = 0 and l_aplica = 'S' then 
    l_bandera := true;
    l_error := l_error||'- El campo Relaciones externas que inciden con el cargo debe tener al menos un registro. Si no aplica, debe diligenciar la Justificaci�n<br>';
  end if;
  if l_bandera then
    Raise_Application_Error(-20000,l_error);
  end if;
    RETURN true;
  END completitud_contactos;

  function existe_concepto_bp(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
    l_conteo number;
  BEGIN
    select count(*) into l_conteo from concepto_cargo where id_cargo = i_number and tipo = snw_constantes.constante_tipo('CONCEPTO_BP') and comentario is not null;
    RETURN l_conteo > 0;
  END existe_concepto_bp;

  function existe_concepto_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
    l_conteo number;
  BEGIN
    select count(*) into l_conteo from concepto_cargo 
    where id_cargo = i_number and tipo = snw_constantes.constante_tipo('CONCEPTO_POC') and comentario is not null;
    RETURN l_conteo > 0;
  END existe_concepto_poc;

  function concepto_bp(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  l_conteo number;
  BEGIN
    select count(*) into l_conteo from concepto_cargo 
    where id_cargo = i_number and tipo = snw_constantes.constante_tipo('CONCEPTO_BP') and concepto = snw_constantes.constante_tipo('APROBADO_BP');
    RETURN l_conteo > 0;
  END concepto_bp;

  function concepto_poc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
   l_conteo number;
  BEGIN
    select count(*) into l_conteo from concepto_cargo 
    where id_cargo = i_number and tipo = snw_constantes.constante_tipo('CONCEPTO_POC') and concepto = snw_constantes.constante_tipo('APROBADO_POC');
    RETURN l_conteo > 0;
  END concepto_poc;

  function cancela_dc(
    i_number IN NUMBER,
    i_fecha  IN DATE,
    i_texto  IN VARCHAR2)
    RETURN BOOLEAN AS
  BEGIN
    -- TAREA: Se necesita implantaci�n para function ADMIN_VALIDACION_PCK.cancela_dc
    RETURN NULL;
  END cancela_dc;

END ADMIN_VALIDACION_PCK;
/