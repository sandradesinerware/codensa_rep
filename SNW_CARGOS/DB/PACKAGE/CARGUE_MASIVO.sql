
  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."CARGUE_MASIVO" AS 

    PROCEDURE ORQUESTADOR;
    PROCEDURE PREPROCESAR_REGISTROS;
    PROCEDURE ACTUALIZAR_UNIDAD_ORGANIZATIVA( i_codigo NUMBER );
    PROCEDURE CREAR_UNIDAD_ORGANIZATIVA( i_codigo NUMBER );
    PROCEDURE ACTUALIZAR_CARGO;
    PROCEDURE ACTUALIZAR_POSICION(I_TEMP NUMBER );
    PROCEDURE ACTUALIZAR_TRABAJADOR(I_TEMP NUMBER );
    PROCEDURE ACTUALIZAR_POSICION_TRABAJADOR( I_TEMP NUMBER );
    PROCEDURE ACTUALIZAR_POSICION_UO( I_TEMP NUMBER );
    PROCEDURE ACTUALIZAR_POSICION_CARGO( I_TEMP NUMBER );
    FUNCTION BUSCAR_PADRE_UO( i_codigo NUMBER,i_tipo NUMBER ) RETURN NUMBER;
    FUNCTION BUSCAR_TIPO_UO(i_codigo NUMBER ) RETURN NUMBER;
    PROCEDURE ACTUALIZAR_NIVEL_ACCESO;
    PROCEDURE EVALUAR_REGISTRO(I_TEMP NUMBER);
    PROCEDURE PERMISOS_JEFE_OBJETIVOS;
    PROCEDURE actualizar_nivel;
    PROCEDURE actualizar_permiso_nivel;
END CARGUE_MASIVO;
/
CREATE OR REPLACE PACKAGE BODY "SNW_CARGOS"."CARGUE_MASIVO" AS 
----------------------------------------------------------------------------------------------------------------------
PROCEDURE ORQUESTADOR AS
    l_padre number;
    l_temp number;
    l_existe_uo number;
    l_log varchar2(4000);
  BEGIN 
  
  update tempo_plantilla_cargo set ultimo_cargue = null where REGISTRO_MANIPULADO = 'Y';
  update tempo_plantilla_cargo set ultimo_cargue = 'S' where REGISTRO_MANIPULADO is null;
  
  -- Limpiar espacios inutiles
  PREPROCESAR_REGISTROS;
  
    FOR i IN (SELECT id,UNIDAD_ORGANIZATIVA_CODIGO,asignado FROM v_temp_plantilla_posicion WHERE REGISTRO_MANIPULADO is null 
    order by orden) LOOP
    begin
        cargue_masivo.EVALUAR_REGISTRO(i.id);
        select log into l_log from v_temp_plantilla_posicion where id = i.id;
        if l_log is null then
            select count(*) into l_existe_uo from unidad_organizativa
            where i.UNIDAD_ORGANIZATIVA_CODIGO in (codigo_codensa,codigo_emgesa);
            begin
            if l_existe_uo = 0 then 
                CREAR_UNIDAD_ORGANIZATIVA(I.UNIDAD_ORGANIZATIVA_CODIGO);
            else
                ACTUALIZAR_UNIDAD_ORGANIZATIVA(i.UNIDAD_ORGANIZATIVA_CODIGO);
            end if;
            exception when others then 
                    dbms_output.put_line('>>> UO <<< Error creando/actualizando uo de asignado:  ' || i.asignado || ' ' || sqlerrm);
                    update tempo_plantilla_cargo set log = log||' Error creando/actualizando uo de asignado: '||i.asignado where id = i.id;
            end;
            begin
                ACTUALIZAR_POSICION(i.ID); 
                exception when others then 
                    dbms_output.put_line('Error actualizando posicion de asignado:  ' || i.asignado || ' ' || sqlerrm);
                    update tempo_plantilla_cargo set log = log||' Error actualizando posicion de asignado: '||i.asignado where id = i.id;
            end;
            begin
                ACTUALIZAR_TRABAJADOR(i.ID);
                exception when others then 
                    dbms_output.put_line('Error Actualizando trabajador ' || i.asignado || ' ' || sqlerrm);
                    update tempo_plantilla_cargo set log = log||' Error actualizando trabajador de asignado: '||i.asignado where id = i.id;
            end;
            begin
                ACTUALIZAR_POSICION_TRABAJADOR(i.ID); 
                exception when others then 
                    dbms_output.put_line('Error actualizando posicion_trabajador de asignado:  ' || i.asignado || ' ' || sqlerrm);
                    update tempo_plantilla_cargo set log = log||' Error actualizando posicion_trabajador de asignado: '||i.asignado where id = i.id;
            end;
            begin
                ACTUALIZAR_POSICION_CARGO(i.ID);
                exception when others then 
                    dbms_output.put_line('Error actualizando posicion_cargo de asignado:  ' || i.asignado || ' ' || sqlerrm);
                    update tempo_plantilla_cargo set log = log||' Error actualizando posicion_cargo de asignado: '||i.asignado where id = i.id;
            end;
            begin
                ACTUALIZAR_POSICION_UO(i.ID); 
                exception when others then 
                    dbms_output.put_line('Error actualizando posicion_uo de asignado:  ' || i.asignado || ' ' || sqlerrm);
                    update tempo_plantilla_cargo set log = log||' Error actualizando posicion_uo de asignado: '||i.asignado where id = i.id;
            end;
        end if;
        UPDATE TEMPO_PLANTILLA_CARGO
        SET REGISTRO_MANIPULADO = 'Y' 
        WHERE ID =  i.id;
        exception when others then 
            dbms_output.put_line('Error procesando registro asignado:   ' || i.asignado || ' ' || sqlerrm);
            update tempo_plantilla_cargo set log = log||' Error procesando registro asignado: '||i.asignado where id = i.id;
    end;
    END LOOP;  
    
    ACTUALIZAR_CARGO; 
    ACTUALIZAR_NIVEL_ACCESO;
    PERMISOS_JEFE_OBJETIVOS;
  END ORQUESTADOR;

---------------------------------------------------------------------------------------------------------------------

PROCEDURE PREPROCESAR_REGISTROS AS
  l_nombre_unidad varchar2(4000);
  l_id_cargo number;

  BEGIN
     UPDATE TEMPO_PLANTILLA_CARGO set POS_RESP = TRIM(POS_RESP);
     UPDATE TEMPO_PLANTILLA_CARGO set NOMBRE_PILA = TRIM(NOMBRE_PILA);
     UPDATE TEMPO_PLANTILLA_CARGO set P_APELLIDOS = TRIM(P_APELLIDOS);
     UPDATE TEMPO_PLANTILLA_CARGO set S_APELLIDOS = TRIM(S_APELLIDOS);
  END PREPROCESAR_REGISTROS;
---------------------------------------------------------------------------------------------------------------------
PROCEDURE CREAR_UNIDAD_ORGANIZATIVA( i_codigo NUMBER ) AS
    l_codigo_unidad number;
    l_nombre_unidad varchar2(2000);
    l_cod_gerencia number;
    l_nombre_gerencia varchar2(2000);
    l_cod_subgerencia  number;
    l_nombre_subgerencia varchar2(2000);
    l_cod_division number;
    l_nombre_division varchar2(2000);
    l_cod_departamento number;
    l_nombre_departamento varchar2(2000);
    l_tipo_uo number;
    l_padre number;
    l_existe_uo number;

BEGIN
    
    SELECT cod_unidad,nombre_unidad,cod_gerencia,nombre_gerencia,cod_subgerencia,nombre_subgerencia,
    cod_division,nombre_division, cod_departamento,nombre_departamento
    INTO l_codigo_unidad,l_nombre_unidad,l_cod_gerencia,l_nombre_gerencia, l_cod_subgerencia,l_nombre_subgerencia,
    l_cod_division,l_nombre_division, l_cod_departamento,l_nombre_departamento
    FROM temp_unidad_organizativa
    WHERE cod_unidad = i_codigo;
        
    if l_cod_gerencia is not null then
        l_tipo_uo:= BUSCAR_TIPO_UO(l_cod_gerencia);        
        select count(*) into l_existe_uo from unidad_organizativa where l_cod_gerencia in (codigo_emgesa,codigo_codensa)
        or (upper(nombre) = upper(l_nombre_gerencia) and TIPO_UNIDAD = l_tipo_uo );
        if l_existe_uo = 0 then
            dbms_output.put_line('CREANDO GERENCIA ! ! ! ! ' || l_codigo_unidad);
            INSERT INTO UNIDAD_ORGANIZATIVA (nombre, tipo_unidad, codigo_emgesa,estado) 
            VALUES (upper(l_nombre_gerencia), l_tipo_uo, l_cod_gerencia,'A'); 
        end if;
        if  l_existe_uo = 1 then
            update UNIDAD_ORGANIZATIVA set codigo_codensa = l_cod_gerencia where
            upper(nombre) = upper(l_nombre_gerencia) and tipo_unidad = l_tipo_uo  and codigo_emgesa <> l_cod_gerencia;
        end if;
    end if;

    if l_cod_subgerencia is not null then
        l_tipo_uo:= BUSCAR_TIPO_UO(l_cod_subgerencia);
        l_padre:= BUSCAR_PADRE_UO(l_cod_subgerencia,l_tipo_uo);
        select count(*) into l_existe_uo from unidad_organizativa where l_cod_subgerencia in (codigo_emgesa,codigo_codensa)
        or (upper(nombre) = upper(l_nombre_subgerencia) and TIPO_UNIDAD = l_tipo_uo and ID_PADRE = l_padre);
        if l_existe_uo = 0 then
            dbms_output.put_line('CREANDO SUB_GERENCIA ! ! ! ! ' || l_codigo_unidad);
            INSERT INTO UNIDAD_ORGANIZATIVA (nombre, tipo_unidad, codigo_emgesa,id_padre,estado) 
            VALUES (upper(l_nombre_subgerencia), l_tipo_uo, l_cod_subgerencia,l_padre,'A'); 
        end if;
        if  l_existe_uo = 1 then
            update UNIDAD_ORGANIZATIVA set codigo_codensa = l_cod_subgerencia where
            nombre = upper(l_nombre_subgerencia) and tipo_unidad = l_tipo_uo and id_padre = l_padre and codigo_emgesa<>l_cod_subgerencia;
        end if;
    end if;

    if l_cod_division is not null then
        l_tipo_uo:= BUSCAR_TIPO_UO(l_cod_division);
        l_padre:= BUSCAR_PADRE_UO(l_cod_division,l_tipo_uo);
        select count(*) into l_existe_uo from unidad_organizativa where l_cod_division in (codigo_emgesa,codigo_codensa)
        or (upper(nombre) = upper(l_nombre_division) and TIPO_UNIDAD = l_tipo_uo and ID_PADRE =l_padre);
        if l_existe_uo = 0 then
            dbms_output.put_line('CREANDO DIVISION ! ! ! ! ' || l_codigo_unidad);
            INSERT INTO UNIDAD_ORGANIZATIVA (nombre, tipo_unidad, codigo_emgesa,id_padre,estado) 
            VALUES (upper(l_nombre_division), l_tipo_uo, l_cod_division,l_padre,'A'); 
        end if;
        if  l_existe_uo = 1 then
            update UNIDAD_ORGANIZATIVA set codigo_codensa = l_cod_division where
            nombre = upper(l_nombre_division) and tipo_unidad = l_tipo_uo and id_padre = l_padre and codigo_emgesa<>l_cod_division;
        end if;
    end if;

    if l_cod_departamento is not null then
        l_tipo_uo:= BUSCAR_TIPO_UO(l_cod_departamento);
        l_padre:= BUSCAR_PADRE_UO(l_cod_departamento,l_tipo_uo);
        select count(*) into l_existe_uo from unidad_organizativa where l_cod_departamento in (codigo_emgesa,codigo_codensa)
        or (upper(nombre) = upper(l_nombre_departamento) and TIPO_UNIDAD = l_tipo_uo and ID_PADRE =l_padre);
        if l_existe_uo = 0 then
            dbms_output.put_line('CREANDO DEPARTAMENTO ! ! ! ! ' || l_codigo_unidad);
            INSERT INTO UNIDAD_ORGANIZATIVA (nombre, tipo_unidad, codigo_emgesa,id_padre,estado) 
            VALUES (upper(l_nombre_departamento), l_tipo_uo, l_cod_departamento,l_padre,'A'); 
        end if;
        if  l_existe_uo = 1 then
            update UNIDAD_ORGANIZATIVA set codigo_codensa = l_cod_departamento where
            nombre = upper(l_nombre_departamento) and tipo_unidad = l_tipo_uo and id_padre = l_padre and codigo_emgesa<>l_cod_departamento;
        end if;
    end if;
            
END CREAR_UNIDAD_ORGANIZATIVA;


---------------------------------------------------------------------------------------------------------------------
PROCEDURE ACTUALIZAR_UNIDAD_ORGANIZATIVA( i_codigo NUMBER ) AS
    codigo_unidad number;
    nombre_unidad varchar2(2000);
    nombre_unidad_anterior varchar2(2000); 
    nombre_unidad_nuevo varchar2(4000);
    tipo_uo number;
    conteo number;
    l_id_unidad number;
    l_cod_gerencia number;
    l_nombre_gerencia varchar2(2000);
    l_cod_subgerencia  number;
    l_nombre_subgerencia varchar2(2000);
    l_cod_division number;
    l_nombre_division varchar2(2000);
    l_cod_departamento number;
    l_nombre_departamento varchar2(2000);
  BEGIN
    SELECT 
    cod_unidad, 
    nombre_unidad,
    nombre_gerencia||nombre_subgerencia||nombre_division||nombre_departamento,
    cod_gerencia,nombre_gerencia,
    cod_subgerencia,nombre_subgerencia,
    cod_division,nombre_division, 
    cod_departamento,nombre_departamento
    INTO codigo_unidad, nombre_unidad, nombre_unidad_nuevo , 
    l_cod_gerencia,l_nombre_gerencia, l_cod_subgerencia,l_nombre_subgerencia,
    l_cod_division,l_nombre_division, l_cod_departamento,l_nombre_departamento
    FROM temp_unidad_organizativa
    WHERE cod_unidad = i_codigo;

    SELECT id 
    INTO  l_id_unidad
    FROM UNIDAD_ORGANIZATIVA
    WHERE codigo_unidad in (codigo_codensa,codigo_emgesa);

    select
    gerencia||subgerencia||division||departamento
    into nombre_unidad_anterior
    from v_rama_unidad_nombre where id = l_id_unidad;

    if  nombre_unidad_nuevo <> nombre_unidad_anterior then 
        if l_nombre_gerencia is not null then
            UPDATE UNIDAD_ORGANIZATIVA
            SET nombre = upper(l_nombre_gerencia)
            WHERE l_cod_gerencia in (codigo_codensa,codigo_emgesa); 
        end if;
        if l_nombre_subgerencia is not null then
            UPDATE UNIDAD_ORGANIZATIVA
            SET nombre = upper(l_nombre_subgerencia)
            WHERE l_cod_subgerencia in (codigo_codensa,codigo_emgesa); 
        end if;
        if l_nombre_division is not null then
            UPDATE UNIDAD_ORGANIZATIVA
            SET nombre = upper(l_nombre_division)
            WHERE l_cod_division in (codigo_codensa,codigo_emgesa); 
        end if;
        if l_nombre_departamento is not null then
            UPDATE UNIDAD_ORGANIZATIVA
            SET nombre = upper(l_nombre_departamento)
            WHERE l_cod_departamento in (codigo_codensa,codigo_emgesa); 
        end if;
    end if; 
  END ACTUALIZAR_UNIDAD_ORGANIZATIVA;

----------------------------------------------------------------------------------------------------------------------


PROCEDURE ACTUALIZAR_CARGO AS
  l_nombre_unidad varchar2(4000);
  l_id_cargo number;
  l_nombre_denominacion1 varchar2(4000);
  l_nombre_denominacion2 varchar2(4000);
  l_id_padre number;
  BEGIN
     for i in (select id,codigo_funcion,id_unidad_organizativa,nombre_gerencia||nombre_subgerencia||nombre_division||nombre_departamento unidad from cargo 
                where VERSIONADOR_PCK.CARGO_VIGENTE(cargo.id) = 'S' and tipo_ubicacion = snw_constantes.constante_tipo('ESPECIFICO')) loop
                begin
                    select gerencia||subgerencia||division||departamento into l_nombre_unidad from v_rama_unidad_nombre 
                    where id = i.id_unidad_organizativa;
                    if i.unidad <> l_nombre_unidad then
                        formulario.duplicar_formulario(i.ID,l_id_cargo);           
                        dbms_output.put_line('Punto 1');
                        versionador_pck.ACTUALIZAR_VERSION(l_id_cargo);
                        versionador_pck.ACTUALIZAR_VIGENCIA(l_id_cargo);
                        dbms_output.put_line('Punto 2');
                        if VERSIONADOR_PCK.CARGO_POS_RES(l_id_cargo) then
                            dbms_output.put_line('Punto 3');
                            l_nombre_denominacion1 := versionador_pck.CARGO_SUBGRUPO(l_id_cargo);
                            select nombre into l_nombre_denominacion2 from unidad_organizativa where id = i.id_unidad_organizativa;
                            update cargo set denominacion = l_nombre_denominacion1||' '||l_nombre_denominacion2 where id = l_id_cargo;
                            dbms_output.put_line('Punto 4');
                        end if;
                        select id_cargo_padre into l_id_padre from cargo where id = l_id_cargo;
                        dbms_output.put_line('Punto 5');
                        update cargo set estado = snw_constantes.constante_tipo('CARGO_NO_VIGENTE') where id_cargo_padre = l_id_padre;
                        update cargo set estado = snw_constantes.constante_tipo('CARGO_VIGENTE') where id = l_id_cargo;
                        dbms_output.put_line('Punto 6');
                    end if;
                exception when others then
                    dbms_output.put_line(sqlerrm || ' ' || l_id_cargo || ' C.A.R.G.O  Error actualizando el cargo, Codigo funcion:' || i.codigo_funcion);
                    update tempo_plantilla_cargo set log = log || ' Error actualizando el cargo: ' || ' Codigo funcion:' || i.codigo_funcion where ocupacion_codigo = i.codigo_funcion;
                end;
     end loop;
  END ACTUALIZAR_CARGO;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE ACTUALIZAR_POSICION(I_TEMP NUMBER ) AS
  i_posicion number; 
  i_pos_resp varchar(200);
  codigo_posicion_plantilla number;
  nombre_posicion varchar(2000); 
  i_unidad number;
  codigo_unidad number;
  conteo number;

  BEGIN
    SELECT posicion_codigo, posicion_nombre, unidad_organizativa_codigo, pos_resp
    INTO codigo_posicion_plantilla, nombre_posicion, codigo_unidad, i_pos_resp 
    FROM v_temp_plantilla_posicion
    WHERE ID = i_temp;
    
    SELECT ID INTO i_unidad
    FROM UNIDAD_ORGANIZATIVA
    WHERE codigo_unidad in (codigo_codensa,codigo_emgesa);

    SELECT COUNT(*) INTO conteo
    FROM POSICION
    WHERE codigo = codigo_posicion_plantilla;

    if conteo = 0 then 
        INSERT INTO POSICION(codigo, nombre, id_unidad_organizativa, pos_res) 
        VALUES (codigo_posicion_plantilla, nombre_posicion, i_unidad, i_pos_resp);
    else

        SELECT ID INTO i_posicion
        FROM POSICION
        WHERE CODIGO = codigo_posicion_plantilla;
--------pendiente: se puede pasar esta parte de la actualizacion a actualizar_posicion_uo
        UPDATE POSICION
        SET nombre = nombre_posicion,
        id_unidad_organizativa = i_unidad  
        WHERE ID = i_posicion;


    end if;
            --dbms_output.put_line(sqlerrm||' ACTUALIZAR posicion check');

  END ACTUALIZAR_POSICION;
----------------------------------------------------------------------------------------------------------------------

PROCEDURE ACTUALIZAR_TRABAJADOR( I_TEMP NUMBER ) AS
  i_trabajador number; 
  l_asignado number;
  conteo number;
  codigo_cargo number;
  nombre_trabajador varchar (4000);
  apellido_trabajador varchar (4000);
  identificacion_trabajador varchar(4000);
  l_conteo_usuario number;
  l_id_usuario number;
  l_gestor varchar(200);
  l_prom_cargo varchar(20);
  l_SG varchar(20);
  id_SG number;
  l_fecha_inicio date;
  l_fecha_antiguedad date; 
  l_email VARCHAR2(100); 
  l_asignado_jefe_obj NUMBER;
  l_target_opr_nombre VARCHAR2(100);
  l_target_opr_id NUMBER;
  
  l_estado_activo NUMBER := 1; -- El trabajador se crea activo
  l_grupo_evaluados NUMBER := 15; -- El trabajador se crea y se asocia al grupo de evaluados
  
  BEGIN

    SELECT asignado, nombre_pila, p_apellidos ||' '|| s_apellidos, ocupacion_codigo, co, POS_RESP, promocion_cargo, SG, 
    EMAIL, ASIGNADO_JEFE_OBJ, TARGET_OPR, 
    to_date(FECHA_INICIO,'dd/mm/yyyy'), to_date(FECHA_ANTIGUEDAD,'dd/mm/yyyy')
    INTO l_asignado, nombre_trabajador, apellido_trabajador, codigo_cargo, identificacion_trabajador, l_gestor, 
    l_prom_cargo, l_SG, l_email, l_asignado_jefe_obj, l_target_opr_nombre,
    l_fecha_inicio, l_fecha_antiguedad
    FROM TEMPO_PLANTILLA_CARGO
    WHERE ID = i_temp;

    SELECT COUNT(*) INTO conteo
    FROM codensa_gth.TRABAJADOR
    WHERE numero_identificacion = l_asignado;

    id_SG := SGI.DETERMINAR_SUBGRUPO(l_SG);
    l_target_opr_id := CODENSA_GTH_SNW_CONSTANTES.get_id_tipo(l_target_opr_nombre, 'TARGET_OPR');   

    if conteo = 0 and l_asignado is not null then 
        INSERT INTO codensa_gth.TRABAJADOR (numero_identificacion, nombres, apellidos, identificacion, 
        id_subgrupo_profesional,
        INICIO_LABORES, ANTIGUEDAD,
        jefe, id_target_opr, email, id_estado) 
        VALUES (l_asignado, nombre_trabajador, apellido_trabajador, identificacion_trabajador, id_SG,
        l_fecha_inicio, 
        l_fecha_antiguedad,
        l_asignado_jefe_obj, l_target_opr_id, l_email, l_estado_activo);        
        
    else
        begin
        SELECT ID INTO i_trabajador
        FROM codensa_gth.TRABAJADOR
        WHERE numero_identificacion = l_asignado;
        exception when no_data_found then null;
        end;               
        UPDATE TRABAJADOR
        SET nombres = nombre_trabajador, 
        apellidos = apellido_trabajador, 
        id_subgrupo_profesional = id_SG, 
        INICIO_LABORES = l_fecha_inicio, 
        ANTIGUEDAD = l_fecha_antiguedad, 
        jefe = l_asignado_jefe_obj, 
        id_target_opr = l_target_opr_id, 
        email = l_email 
        WHERE  id = i_trabajador;
    end if;
    
    
    -- https://sinerware.atlassian.net/browse/GTH2020-89: 
    -- Ya NO se crea el PEC segun antiguedad del trabajador. Solo por la marca de promoci�n cargo.
    --if EXTRACT(YEAR FROM to_date(l_fecha_antiguedad)) = EXTRACT(YEAR FROM SYSDATE) and
    --    EXTRACT(MONTH FROM to_date(l_fecha_antiguedad)) = EXTRACT(MONTH FROM SYSDATE)THEN
                    
    if l_prom_cargo is not null then
        -- Se debe crear el PEC Predeterminado
        CODENSA_GTH.ADMIN_PEC_PCK.CREAR_PEC_PREDETERMINADO(l_asignado, l_fecha_inicio, 
        codensa_gth.snw_constantes.constante_tipo('PEC_MOTIVO_PROMOCION'));        
    end if;            
            
    select count(*) into l_conteo_usuario from snw_auth.usuario where numero_identificacion = l_asignado;
    if l_conteo_usuario = 0 then
        snw_auth.snw_gestion_usuarios.snw_create_user(l_asignado);
    end if;

    begin
    select id into l_id_usuario from snw_auth.usuario where numero_identificacion = l_asignado;
    
    -- Se le asocia el grupo de evaluados al trabajador
    snw_auth.autorizacion.asociar_usuario_grupo(l_grupo_evaluados,l_id_usuario);        
        
    if l_gestor is not null then
        snw_auth.autorizacion.asociar_usuario_grupo(14,l_id_usuario);
    else
        snw_auth.autorizacion.quitar_usuario_grupo(14,l_id_usuario);
    end if;    
    end;
            

  END ACTUALIZAR_TRABAJADOR;
----------------------------------------------------------------------------------------------------------------------
PROCEDURE ACTUALIZAR_POSICION_TRABAJADOR( I_TEMP NUMBER ) AS
    i_trabajador number; 
    l_asignado number;
    codigo_posicion number;
    i_posicion number;
    i_posicion_anterior number;
    conteo number;
    conteo2 number;
    conteo3 number;

  BEGIN
    SELECT  asignado, posicion_codigo
    INTO l_asignado, codigo_posicion
    FROM v_temp_plantilla_posicion
    WHERE ID = i_temp; 

    ---busca la posicion en la tabla POSICION
    SELECT ID INTO i_posicion
    FROM POSICION
    WHERE CODIGO = codigo_posicion;
    ----busca el trabajador en la tabla TRABAJADOR
    begin
    SELECT ID INTO i_trabajador
    FROM TRABAJADOR
    WHERE numero_identificacion = l_asignado;
    exception when no_data_found then
                null;
    end;
    ------busca si existe la relacion posicion-trabajador
    SELECT COUNT(*) INTO conteo
    FROM POSICION_TRABAJADOR
    WHERE ID_TRABAJADOR = i_trabajador AND ID_POSICION = i_posicion AND (SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN OR FECHA_FIN is null); 

    -----busca si la posicion cambio de trabajador 
    SELECT COUNT(*) INTO conteo2
    FROM POSICION_TRABAJADOR
    WHERE ID_POSICION = i_posicion AND (SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN OR FECHA_FIN is null); 

    -----busca si el trabajador cambi� de posicion
    SELECT COUNT(*) INTO conteo3
    FROM POSICION_TRABAJADOR
    WHERE ID_TRABAJADOR = i_trabajador AND (SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN OR FECHA_FIN is null); 



    if  conteo = 0 and i_trabajador is not null  then
            if  conteo2 = 0 and i_trabajador is not null  then  
                if conteo3 = 0 and i_trabajador is not null then 
                    INSERT INTO POSICION_TRABAJADOR (ID_TRABAJADOR, ID_POSICION, FECHA_INICIO) 
                    VALUES (i_trabajador, i_posicion, SYSDATE); 
                else
                    UPDATE POSICION_TRABAJADOR
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_TRABAJADOR = i_trabajador AND FECHA_FIN is null ;
                    INSERT INTO POSICION_TRABAJADOR (ID_TRABAJADOR, ID_POSICION, FECHA_INICIO) 
                    VALUES (i_trabajador, i_posicion, SYSDATE()); 

                end if;
            else 
                if conteo3 = 0 then 
                    UPDATE POSICION_TRABAJADOR
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
                    INSERT INTO POSICION_TRABAJADOR (ID_TRABAJADOR, ID_POSICION, FECHA_INICIO) 
                    VALUES (i_trabajador, i_posicion, SYSDATE()); 
                else
                    UPDATE POSICION_TRABAJADOR
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
                    UPDATE POSICION_TRABAJADOR
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_TRABAJADOR = i_trabajador AND FECHA_FIN is null ;
                    INSERT INTO POSICION_TRABAJADOR (ID_TRABAJADOR, ID_POSICION, FECHA_INICIO) 
                    VALUES (i_trabajador, i_posicion, SYSDATE()); 
                end if;


            end if;

    end if;
              --dbms_output.put_line(sqlerrm||' ACTUALIZAR posicion-trabajador check');
    if i_trabajador is null AND i_posicion is not null then
         UPDATE POSICION_TRABAJADOR
         SET FECHA_FIN = SYSDATE()-1/1440
         WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
    end if;
  END ACTUALIZAR_POSICION_TRABAJADOR;
------------------------------------------------------------------------------------------------------------------------------------------
PROCEDURE ACTUALIZAR_POSICION_CARGO( I_TEMP NUMBER ) AS
    i_cargo number; 
    i_cargo_padre number; 
    codigo_cargo number;
    i_cargo_anterior number;
    i_posicion number;
    codigo_posicion number;
    i_posicion_anterior number;
    conteo number;
    conteo2 number;
    conteo3 number;
    i_posicion_cargo number;
    l_conteo_cargo number;
    i_pos_trabajador number;
    --contemplar cambio de nombre en posicion y cambio de nombre en cartgo

  BEGIN


    SELECT ocupacion_codigo, posicion_codigo
    INTO codigo_cargo,  codigo_posicion
    FROM TEMPO_PLANTILLA_CARGO
    WHERE ID = i_temp; 

    SELECT COUNT(ID) INTO l_conteo_cargo
    FROM CARGO
    WHERE CODIGO_FUNCION = codigo_cargo and vigencia = 'S';

    if l_conteo_cargo>0 then 

        SELECT ID INTO i_posicion
        FROM POSICION
        WHERE CODIGO = codigo_posicion;
    
        SELECT ID, ID_CARGO_PADRE INTO i_cargo, i_cargo_padre
        FROM CARGO
        WHERE codigo_funcion = codigo_cargo and vigencia ='S'; 
    
         ------busca si existe la relacion cargo-posicion
        SELECT COUNT(*) INTO conteo
        FROM POSICION_CARGO
        WHERE ID_CARGO = i_cargo_padre AND ID_POSICION = i_posicion AND (SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN OR FECHA_FIN is null); 
    
        -----busca si la posicion cambio de cargo 
        SELECT COUNT(*) INTO conteo2
        FROM POSICION_CARGO
        WHERE ID_POSICION = i_posicion AND (SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN OR FECHA_FIN is null); 
    
        -----busca si el cargo cambi� de posicion
        /*SELECT COUNT(*) INTO conteo3
        FROM POSICION_CARGO
        WHERE ID_CARGO = i_cargo_padre AND (SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN OR FECHA_FIN is null); */
    
        if  conteo = 0  then
                if  conteo2 = 0 then  
                   -- if conteo3 = 0 then 
                        INSERT INTO POSICION_CARGO (ID_CARGO, ID_POSICION, FECHA_INICIO) 
                        VALUES (i_cargo_padre, i_posicion, SYSDATE); 
                   /* else
                        UPDATE POSICION_CARGO
                        SET FECHA_FIN = SYSDATE()-1/1440
                        WHERE ID_CARGO = i_cargo_padre AND FECHA_FIN is null ;
                        INSERT INTO POSICION_CARGO (ID_CARGO, ID_POSICION, FECHA_INICIO) 
                        VALUES (i_cargo_padre, i_posicion, SYSDATE()); 
    
                    end if;*/
                else 
                    --if conteo3 = 0 then 
                        UPDATE POSICION_CARGO
                        SET FECHA_FIN = SYSDATE()-1/1440
                        WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
                        INSERT INTO POSICION_CARGO (ID_CARGO, ID_POSICION, FECHA_INICIO) 
                        VALUES (i_cargo_padre, i_posicion, SYSDATE()); 
                   -- else
                  /*      UPDATE POSICION_CARGO
                        SET FECHA_FIN = SYSDATE()-1/1440
                        WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
                        UPDATE POSICION_CARGO
                        SET FECHA_FIN = SYSDATE()-1/1440
                        WHERE ID_CARGO = i_cargo_padre AND FECHA_FIN is null ;
                        INSERT INTO POSICION_CARGO (ID_CARGO, ID_POSICION, FECHA_INICIO) 
                        VALUES (i_cargo_padre, i_posicion, SYSDATE()); */
                   -- end if;
    
    
                end if;
    
        end if;

    end if;
    --aqu� debo actualizar version???    

            --dbms_output.put_line(sqlerrm||' ACTUALIZAR posicion_cargo check');

  END ACTUALIZAR_POSICION_CARGO;
----------------------------------------------------------------------------------------------------------------------

PROCEDURE ACTUALIZAR_POSICION_UO( I_TEMP NUMBER ) AS
    i_unidad number; 
    codigo_unidad number;
    codigo_unidad_anterior number;
    nombre_unidad varchar (4000); 
    nombre_unidad_anterior varchar (4000); 
    i_unidad_anterior number;
    codigo_posicion number;
    i_posicion number;
    i_posicion_anterior number;
    conteo number;
    conteo2 number;
    conteo3 number;

  BEGIN
------trae los datos de la plantilla localizados en tabla temporal
    SELECT unidad_organizativa_codigo, posicion_codigo, unidad_organizativa_nombre
    INTO codigo_unidad, codigo_posicion, nombre_unidad
    FROM v_temp_plantilla_posicion
    WHERE ID = i_temp; 

-------buscar el id en tabla POSICION de la posici�n en plantilla
    SELECT ID INTO i_posicion
    FROM POSICION
    WHERE CODIGO = codigo_posicion;
-------buscar el id en la tabla UNIDAD_ORGANIZATIVA de la uo en plantilla
    SELECT ID INTO i_unidad
    FROM UNIDAD_ORGANIZATIVA
    WHERE codigo_unidad in (codigo_codensa,codigo_emgesa);
--------verificar si existe registro de la posicion con esa unidad en la tabla POSICION_UO
    SELECT COUNT(ID) INTO conteo
    FROM POSICION_UO
    WHERE ID_POSICION = i_posicion and ID_UNIDAD_ORGANIZATIVA = i_unidad AND ((SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN) OR FECHA_FIN is null);

    -----busca si la posicion cambio de unidad 
    SELECT COUNT(*) INTO conteo2
    FROM POSICION_UO
    WHERE ID_POSICION = i_posicion AND ((SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN) OR FECHA_FIN is null); 

    -----busca si la unidad cambi� de posicion
    /*SELECT COUNT(*) INTO conteo3
    FROM POSICION_UO
    WHERE ID_UNIDAD_ORGANIZATIVA = i_unidad AND ((SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN) OR FECHA_FIN is null); */

---------Si no existe la relacion posicion-uo, se crea
    if  conteo = 0  then
            if  conteo2 = 0 then  
                --if conteo3 = 0 then 
                    INSERT INTO POSICION_UO (ID_UNIDAD_ORGANIZATIVA, ID_POSICION, FECHA_INICIO, NOMBRE_UNIDAD) 
                    VALUES (i_unidad, i_posicion, SYSDATE,nombre_unidad ); 
                /*else
                    UPDATE POSICION_UO
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_UNIDAD_ORGANIZATIVA = i_unidad AND FECHA_FIN is null ;
                    INSERT INTO POSICION_UO (ID_UNIDAD_ORGANIZATIVA, ID_POSICION, FECHA_INICIO, NOMBRE_UNIDAD) 
                    VALUES (i_unidad, i_posicion, SYSDATE(), nombre_unidad); 
                end if;*/
            else 
               -- if conteo3 = 0 then 
                    UPDATE POSICION_UO
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
                    INSERT INTO POSICION_UO (ID_UNIDAD_ORGANIZATIVA, ID_POSICION, FECHA_INICIO, NOMBRE_UNIDAD) 
                    VALUES (i_unidad, i_posicion, SYSDATE(), nombre_unidad); 
               /* else
                    UPDATE POSICION_UO
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_POSICION = i_posicion AND FECHA_FIN is null ;
                    UPDATE POSICION_UO
                    SET FECHA_FIN = SYSDATE()-1/1440
                    WHERE ID_UNIDAD_ORGANIZATIVA = i_unidad AND FECHA_FIN is null ;
                    INSERT INTO POSICION_UO (ID_UNIDAD_ORGANIZATIVA, ID_POSICION, FECHA_INICIO, NOMBRE_UNIDAD) 
                    VALUES (i_unidad, i_posicion, SYSDATE(), nombre_unidad); 
                end if;*/


            end if;
    else
        SELECT id_unidad_organizativa, id_posicion, nombre_unidad 
        INTO i_unidad_anterior, i_posicion_anterior, nombre_unidad_anterior
        FROM POSICION_UO
        WHERE id_posicion = i_posicion  AND ID_UNIDAD_ORGANIZATIVA = i_unidad AND ((SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN) OR FECHA_FIN is null);

        if nombre_unidad = nombre_unidad_anterior then
             null;
            else
              UPDATE POSICION_UO
              SET FECHA_FIN = SYSDATE()-1/1440
              WHERE ID_POSICION = i_posicion_anterior AND ID_UNIDAD_ORGANIZATIVA = i_unidad_anterior AND ((SYSDATE BETWEEN FECHA_INICIO AND FECHA_FIN) OR FECHA_FIN is null) ;
              INSERT INTO POSICION_UO (ID_POSICION, ID_UNIDAD_ORGANIZATIVA, FECHA_INICIO, NOMBRE_UNIDAD) 
              VALUES (i_posicion, i_unidad, SYSDATE(), nombre_unidad); 
            end if;

    end if;

            --dbms_output.put_line(sqlerrm||' ACTUALIZAR posicion_uo check');

  END ACTUALIZAR_POSICION_UO;

-----------------------------------------------------------------------------------------------------------------------

FUNCTION BUSCAR_PADRE_UO(i_codigo NUMBER,i_tipo NUMBER ) RETURN NUMBER AS
    l_padre number;
    nombre_padre varchar2(4000); 
    tipo_uo number;
    l_gerencia varchar2(2000);
    l_subgerencia varchar2(2000);
    l_division varchar2(2000);
    l_cod_division number;
    l_departamento varchar2(2000);
  BEGIN
SELECT nombre_gerencia, nombre_subgerencia, cod_division,nombre_division, nombre_departamento
    INTO l_gerencia, l_subgerencia,l_cod_division,l_division, l_departamento
    FROM temp_unidad_organizativa
    WHERE cod_unidad = i_codigo;
    if i_tipo = snw_constantes.constante_tipo('GERENCIA') then
        return null;
    end if;
    if i_tipo = snw_constantes.constante_tipo('SUBGERENCIA') then
        select id into l_padre from unidad_organizativa where
        upper(nombre) = upper(l_gerencia) and tipo_unidad = snw_constantes.constante_tipo('GERENCIA');
        return l_padre;
    end if;
    if i_tipo = snw_constantes.constante_tipo('DIVISION') then
        if l_subgerencia is null then
            select id into l_padre from unidad_organizativa where
            upper(nombre) = upper(l_gerencia) and tipo_unidad = snw_constantes.constante_tipo('GERENCIA');
            return l_padre;
        else
            select id into l_padre from unidad_organizativa where
            upper(nombre) = upper(l_subgerencia) and tipo_unidad = snw_constantes.constante_tipo('SUBGERENCIA')
            and id_padre = (
                        select id from unidad_organizativa where
                        upper(nombre) = upper(l_gerencia) and tipo_unidad = snw_constantes.constante_tipo('GERENCIA'));
            return l_padre;
        end if;
    end if;
    if i_tipo = snw_constantes.constante_tipo('DEPARTAMENTO') then
        if l_division  is null then
            if l_subgerencia is null then
                select id into l_padre from unidad_organizativa where
                upper(nombre) = upper(l_gerencia) and tipo_unidad = snw_constantes.constante_tipo('GERENCIA');
                return l_padre;
            else
                select id into l_padre from unidad_organizativa where
                upper(nombre) = upper(l_subgerencia) and tipo_unidad = snw_constantes.constante_tipo('SUBGERENCIA')
                and id_padre = (
                select id from unidad_organizativa where
                upper(nombre) = upper(l_gerencia) and tipo_unidad = snw_constantes.constante_tipo('GERENCIA'));
                return l_padre;
            end if;
        else
            select id into l_padre from unidad_organizativa where
            upper(nombre) = upper(l_division) and  tipo_unidad = snw_constantes.constante_tipo('DIVISION') 
            and l_cod_division in (codigo_codensa,codigo_emgesa);
            return l_padre;
        end if;
    end if;  
    exception when others then return null;
    return null;
  END BUSCAR_PADRE_UO;
------------------------------------------------------------------------------------------------------------------------
FUNCTION BUSCAR_TIPO_UO(i_codigo NUMBER ) RETURN NUMBER AS
    l_tipo_uo number;
    l_gerencia varchar2(2000);
    l_subgerencia varchar2(2000);
    l_division varchar2(2000);
    l_departamento varchar2(2000);
    l_nombre_uo varchar2(2000);
  BEGIN
    SELECT nvl(nombre_gerencia,''), nvl(nombre_subgerencia,''), nvl(nombre_division,''), nvl(nombre_departamento,''),nvl(nombre_unidad,'')
    INTO l_gerencia, l_subgerencia,l_division, l_departamento,l_nombre_uo
    FROM temp_unidad_organizativa
    WHERE cod_unidad = i_codigo;

    CASE
    WHEN l_gerencia is null THEN l_tipo_uo := null;
    WHEN l_subgerencia is null and l_division is null and  l_departamento is null and l_gerencia is not null THEN l_tipo_uo := 78;
    WHEN l_division is null and l_departamento is null  and l_subgerencia is not null THEN l_tipo_uo := 79;
    WHEN l_departamento is null and l_division is not null THEN l_tipo_uo := 80;
    WHEN l_departamento is not null then l_tipo_uo := 81;
    else l_tipo_uo :=  null;
    END CASE;
    RETURN l_tipo_uo;

  END BUSCAR_TIPO_UO;
----------------------------------------------------------------------------------------------------------------------

PROCEDURE ACTUALIZAR_NIVEL_ACCESO IS
BEGIN
    actualizar_nivel;
    actualizar_permiso_nivel; 
END ACTUALIZAR_NIVEL_ACCESO;

----------------------------------------------------------------------------------------------------------------------

PROCEDURE EVALUAR_REGISTRO(I_TEMP NUMBER) IS
 l_conteo number;
 l_conteo2 number;
 l_conteo3 number;
BEGIN
    select count(*) into l_conteo from v_temp_plantilla_posicion where id = i_temp
    and (cod_gerencia in (cod_subgerencia,cod_division,cod_departamento) or
    cod_subgerencia in (cod_division,cod_departamento) or cod_division = cod_departamento);
    if l_conteo > 0 then
        update tempo_plantilla_cargo set log = log||'Error procesando c�digo de la unidad organizativa del registro.;'
        where id = i_temp;
    end if;
    select count(*) into l_conteo2 from tipo where upper(nombre) like 
    (select rol from v_temp_plantilla_posicion where id = i_temp);
    if l_conteo > 0 then
        update tempo_plantilla_cargo set log = log||'Error procesando rol del cargo.;'
        where id = i_temp;
    end if;
    select count(*) into l_conteo3 from temp_unidad_organizativa where cod_unidad =
    (select unidad_organizativa_codigo from tempo_plantilla_cargo where id= i_temp);
    if l_conteo3 = 0 then
        update tempo_plantilla_cargo set log = log||'Error unidad organizativa no encontrada.;'
        where id = i_temp;
    end if;
END;

----------------------------------------------------------------------------------------------------------------------

PROCEDURE PERMISOS_JEFE_OBJETIVOS is
    l_id_usuario number;
    l_asignado number;
begin
    for i in (select  ASIGNADO_JEFE_OBJ  from tempo_plantilla_cargo) loop
        begin
            select id into l_id_usuario from snw_auth.usuario where numero_identificacion = i.ASIGNADO_JEFE_OBJ;
            if l_id_usuario is not null then
                snw_auth.autorizacion.asociar_usuario_grupo(14,l_id_usuario);
            else
                snw_auth.autorizacion.quitar_usuario_grupo(14,l_id_usuario);
            end if;
            exception when no_data_found then
                null;
        end;
    end loop;
end PERMISOS_JEFE_OBJETIVOS;

----------------------------------------------------------------------------------------------------------------------

  PROCEDURE actualizar_nivel is
    l_conteo number;
  begin
      --TODO VDIAGO: Quitar grant sobre snw_auth.nivel_acceso, usar api 15/08/19
    for i in (select * from snw_cargos.unidad_organizativa order by id) loop
        select count(*) into l_conteo from snw_auth.nivel_acceso where id = i.id;
        if l_conteo = 0 then
            insert into snw_auth.nivel_acceso (id,nivel_superior,nombre,tipo_acceso) values
            (i.id,i.id_padre,i.nombre,i.tipo_unidad);
        else
            update snw_auth.nivel_acceso set nivel_superior = i.id_padre,
                                    nombre = i.nombre, tipo_acceso = i.tipo_unidad
                                    where id = i.id;
        end if;
    end loop;
  end actualizar_nivel;
  PROCEDURE actualizar_permiso_nivel is
    l_id_usuario number; 
    l_pos_resp varchar(20);
    l_unidad_temp number;
    l_conteo number;
  begin
    for i in (select 
                    id_trabajador,
                    (select numero_identificacion from codensa_gth.trabajador where id = id_trabajador) numero_identificacion,
                    (select id_unidad_organizativa from snw_cargos.posicion where id=posicion_trabajador.id_posicion) id_nivel_acceso
                    from snw_cargos.posicion_trabajador where id_trabajador in (
                    select id from codensa_gth.trabajador where identificacion in (select co from snw_cargos.tempo_plantilla_cargo))
                    and (sysdate between fecha_inicio and fecha_fin or fecha_fin is null)) loop
        begin
            select id into l_id_usuario from usuario where numero_identificacion = i.numero_identificacion;
            select pos_resp,unidad_organizativa_codigo into l_pos_resp,l_unidad_temp from snw_cargos.tempo_plantilla_cargo where asignado = i.numero_identificacion;
            select count(*) into l_conteo from snw_cargos.unidad_organizativa where l_unidad_temp in (codigo_emgesa,codigo_codensa);
            if l_conteo > 0 then
                if l_pos_resp is not null  then
                    snw_auth.control_acceso_datos.conceder_permiso_agente(i.id_nivel_acceso,l_id_usuario,14);
                else
                    snw_auth.control_acceso_datos.revocar_permiso_agente(i.id_nivel_acceso,l_id_usuario,14);
                end if;
            end if;
            exception when others then
                update snw_cargos.tempo_plantilla_cargo set log = log||' Error asignando permiso trabajador: '||i.numero_identificacion
                where asignado = i.numero_identificacion;
        end;
    end loop;
  end actualizar_permiso_nivel;

----------------------------------------------------------------------------------------------------------------------

END CARGUE_MASIVO;
/