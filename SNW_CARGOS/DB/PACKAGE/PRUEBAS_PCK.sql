
  CREATE OR REPLACE PACKAGE "SNW_CARGOS"."PRUEBAS_PCK" AS 
  
  --Esta funci�n retorna el id, nombre y user_name de los BPs que pueden avalar una DC
  function bp_from_cargo(i_id_cargo CARGO.id%type) return VARCHAR2;

  --Este procedimiento asigna a un trabajador a una UO (mediante su posici�n vigente), creando la posici�n si es necesario
  procedure trabajador_a_unidad(i_id_trabajador number, i_id_unidad number);

  --funci�n para ejecutar una eliminaci�n de cargo una vez se valide que se puede eliminar
  function ejecutar_eliminacion_cargo(i_id_cargo CARGO.id%type) return BOOLEAN;

  --Este procedimiento elimina una versi�n de una descripci�n de cargo
  procedure eliminar_version_cargo(i_id_cargo CARGO.id%type);

END PRUEBAS_PCK;

/
CREATE OR REPLACE PACKAGE BODY "SNW_CARGOS"."PRUEBAS_PCK" AS

function bp_from_cargo(i_id_cargo CARGO.id%type) return VARCHAR2 AS
  
  l_unidad_cargo UNIDAD_ORGANIZATIVA.id%type; --Consulta la UO a la que hay que buscarle el BP perimetral
  l_unidad_bp UNIDAD_ORGANIZATIVA.id%type;
  l_tipo_ubicacion CARGO.tipo_ubicacion%type;
  l_usuario_creador_proceso SNW_AUTH.USUARIO.user_name%type;
  l_bps VARCHAR2(1000);

  --Esta funci�n retorna el id, nombre y user_name de los BPs que pueden avalar una DC
  BEGIN
  --Consultar la UO de la DC o su creador si es transversal
  select id_unidad_organizativa, tipo_ubicacion into l_unidad_cargo, l_tipo_ubicacion
    from CARGO
    where id = i_id_cargo;
  if (l_tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') and l_unidad_cargo is not null) then
    return 'Error en cargo. Es transversal y tiene UO';
  elsif (l_tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('TRANSVERSAL') and l_unidad_cargo is null) then
    --la unidad es la del usuario que cre� la DC
    select aud_creado_por into l_usuario_creador_proceso
      from CARGO
      where id = i_id_cargo;
    l_unidad_cargo := ADMIN_UNIDAD_ORGANIZATIVA.USUARIO_PERTENECE_UNIDAD(l_usuario_creador_proceso);
  elsif (l_tipo_ubicacion = SNW_CONSTANTES.CONSTANTE_TIPO('ESPECIFICO') and l_unidad_cargo is null) then
    return 'Error en cargo. Es espec�fica y no tiene UO';
  end if;
  --Teniendo la UO ahora se busca la l�nea padre de dicha UO
  l_unidad_bp := ADMIN_UNIDAD_ORGANIZATIVA.BUSCAR_UNIDAD(l_unidad_cargo, SNW_CONSTANTES.CONSTANTE_TIPO('LINEA_BP'));
  --Con la UO del(los) bp ahora busco los bps  
  select LISTAGG( T.id ||'|'|| T.nombres || ' ' || T.apellidos ||'|'|| U.user_name , ', ' ) WITHIN GROUP (order by T.id) into l_bps
  from BUSINESS_PARTNER BP inner join TRABAJADOR T on (BP.id_trabajador = T.id)
    left join USUARIO U on (T.numero_identificacion = U.numero_identificacion)
  where BP.id_unidad_organizativa = l_unidad_bp;
  RETURN l_bps;

END bp_from_cargo;

procedure trabajador_a_unidad(i_id_trabajador number, i_id_unidad number) AS
  l_posicion NUMBER;
  l_unidad NUMBER;

  BEGIN
    --Determinar posici�n vigente
    BEGIN
    select ID_POSICION into l_posicion
      from POSICION_TRABAJADOR
      where ID_TRABAJADOR = i_id_trabajador and
        sysdate between fecha_inicio and NVL(fecha_fin,sysdate);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN 
        insert into POSICION(codigo, nombre, id_unidad_organizativa)
          values ( to_number(to_char(sysdate, 'yyyymmddhh24miss')), 'Posicion de prueba', i_id_unidad )
          returning id into l_posicion;
        insert into POSICION_UO (id_posicion, id_unidad_organizativa, fecha_inicio, fecha_fin, nombre_unidad)
          values ( l_posicion, i_id_unidad, sysdate, null, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(i_id_unidad) );
        insert into POSICION_TRABAJADOR(id_trabajador, id_posicion, fecha_inicio, fecha_fin)
          values (i_id_trabajador, l_posicion, sysdate, null);
    END;

    --Cambiar posici�n trabajador a nueva unidad organizativa (cuando cambia por ser nuevo id_unidad)
    --y actualizar fecha_fin de la anterior (bajo el supuesto que es la que tiene fecha_fin null
    select id_unidad_organizativa into l_unidad
      from POSICION
      where id = l_posicion;
    if (l_unidad != i_id_unidad) then
      UPDATE POSICION_UO set fecha_fin = sysdate-1/24
        where id_posicion = l_posicion AND fecha_fin is null;
      insert into POSICION_UO (id_posicion, id_unidad_organizativa, fecha_inicio, fecha_fin, nombre_unidad)
        values ( l_posicion, i_id_unidad, sysdate, null, ADMIN_UNIDAD_ORGANIZATIVA.nombre_unidad(i_id_unidad) );
      UPDATE POSICION set id_unidad_organizativa = i_id_unidad
        where id = l_posicion;
    end if;

END trabajador_a_unidad;


--funci�n para ejecutar una eliminaci�n de cargo una vez se valide que se puede eliminar
function ejecutar_eliminacion_cargo(i_id_cargo CARGO.id%type) return BOOLEAN as
l_instancia_proceso NUMBER;
BEGIN
    delete from decision where id_cargo = i_id_cargo;
    delete from dimension where id_cargo = i_id_cargo;
    delete from licencia_matricula where id_cargo = i_id_cargo;
    delete from responsabilidad_contactos where id_cargo = i_id_cargo;
    delete from responsabilidad_estrategia where id_cargo = i_id_cargo;
    delete from responsabilidad_principal where id_cargo = i_id_cargo;
    delete from software where id_cargo = i_id_cargo;
    delete from posicion_cargo where id_cargo = i_id_cargo;
    delete from concepto_cargo where id_cargo = i_id_cargo;
    delete from cargos_reportan where id_cargo = i_id_cargo;
    select id_instancia_proceso into l_instancia_proceso from cargo where id = i_id_cargo;
    SNW_FLOW_FLUJO.eliminar_proceso(l_instancia_proceso);
    delete from cargo where id = i_id_cargo;
    return true;
EXCEPTION
    when others then 
      dbms_output.put_line('Error en ejecutar_eliminacion_cargo. ' || SQLCODE || '-' || SQLERRM);
      return false;
END ejecutar_eliminacion_cargo;


procedure eliminar_version_cargo(i_id_cargo CARGO.id%type) AS

  l_version_cargo NUMBER;
  l_cargo_inicial NUMBER;
  l_vigencia CHAR(1);
  l_ultima_version NUMBER;
  l_cargo_anterior NUMBER;
  l_bool BOOLEAN;

BEGIN
  select id_cargo_padre, version_cargo, vigencia into l_cargo_inicial, l_version_cargo, l_vigencia
    from cargo
    where id = i_id_cargo;

  select version_cargo into l_ultima_version --puede ser nulo si ninguna tiene versi�n pero ese deber�a ser el caso de versi�n en proceso
    from cargo
    where id_cargo_padre = l_cargo_inicial
        and nvl(version_cargo,0) = ( select max(nvl(version_cargo,0)) 
                            from cargo 
                            where id_cargo_padre = l_cargo_inicial ); 
  --caso1: no tiene vigencia -> es version en proceso por tanto se puede borrar
  --caso2: tiene vigencia y es el �ltimo y es >1 -> se vuelve vigente el anterior y se elimina
  --caso3: tiene vigencia,y es el �ltimo y es 1 -> es �nica vesi�n, se borran los que est�n en proceso y se elimina
  --caso4: tiene vigencia,y no es el �ltimo y es >1 -> se disminuyen las siguientes versiones y se elimina
  --caso5: tiene vigencia,y no es el �ltimo y es 1 -> se eliminan todos

  if (l_version_cargo is null) then --el cargo est� en proceso por tanto se puede borrar
    l_bool := ejecutar_eliminacion_cargo(i_id_cargo);
    if (l_bool) then dbms_output.put_line('Cargo en proceso eliminado');
    else dbms_output.put_line('Cargo en proceso no eliminado');
    end if;

  elsif ( l_version_cargo = l_ultima_version and l_version_cargo > 1 ) then --se vuelve vigente el anterior y se elimina
    select id into l_cargo_anterior
      from cargo
      where id_cargo_padre = l_cargo_inicial and version_cargo = (l_version_cargo-1);
    l_bool := ejecutar_eliminacion_cargo(i_id_cargo);
    if (l_bool) then update cargo set vigencia = 'S' where id = l_cargo_anterior; dbms_output.put_line('Cargo �ltima versi�n eliminado');
    else dbms_output.put_line('Cargo �ltima versi�n no eliminado');
    end if;

  elsif ( l_version_cargo = l_ultima_version and l_version_cargo = 1 ) then --es �nica vesi�n, se borran los que est�n en proceso y se elimina
    for i in (select id from cargo where id_cargo_padre = l_cargo_inicial and version_cargo is null) loop
      l_bool := ejecutar_eliminacion_cargo(i.id);
    end loop;
    l_bool := ejecutar_eliminacion_cargo(i_id_cargo);
    if (l_bool) then dbms_output.put_line('Cargo �nica versi�n y en proceso eliminados');
    else dbms_output.put_line('Cargo �nica versi�n y en proceso no eliminados');
    end if;

  elsif ( l_version_cargo != l_ultima_version and l_version_cargo > 1 ) then --se disminuyen las siguientes versiones y se elimina
    l_bool := ejecutar_eliminacion_cargo(i_id_cargo);
    if (l_bool) then 
        for i in (select id from cargo where id_cargo_padre = l_cargo_inicial and version_cargo > l_version_cargo) loop
            update cargo set version_cargo = version_cargo-1 where id = i.id;
        end loop;
        dbms_output.put_line('Cargo versi�n intermedia eliminado');
    else dbms_output.put_line('Cargo versi�n intermedia no eliminado');
    end if;

  elsif ( l_version_cargo != l_ultima_version and l_version_cargo = 1 ) then  --se eliminan todos
    for i in ( select id from cargo where id_cargo_padre = l_cargo_inicial and (version_cargo is null or version_cargo>l_version_cargo) ) loop
      l_bool := ejecutar_eliminacion_cargo(i.id);
    end loop;
    l_bool := ejecutar_eliminacion_cargo(i_id_cargo);
    if (l_bool) then dbms_output.put_line('Cargo primera versi�n y siguientes eliminados');
    else dbms_output.put_line('Cargo primera versi�n y siguientes no eliminados');
    end if;

  end if;

EXCEPTION
  WHEN OTHERS THEN 
    dbms_output.put_line('Error en eliminar_version_cargo. '||SQLCODE||SQLERRM);
    dbms_output.put_line('Versi�n de cargo no eliminado');

END eliminar_version_cargo;

END PRUEBAS_PCK;
/