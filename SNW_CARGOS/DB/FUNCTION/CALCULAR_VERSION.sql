
  CREATE OR REPLACE FUNCTION "SNW_CARGOS"."CALCULAR_VERSION" (I_CARGO NUMBER) 
    RETURN NUMBER IS
    temp NUMBER;
    version_cargo NUMBER;

  BEGIN
    SELECT max(version_cargo) INTO temp FROM cargo WHERE ID_CARGO_PADRE = (SELECT ID_CARGO_PADRE FROM cargo WHERE ID = I_CARGO);
    version_cargo := temp + 1;
    --DBMS_OUTPUT.PUT_LINE(version_cargo);
    /*UPDATE cargo
    SET version = version_cargo
    WHERE id = I_CARGO;*/

    RETURN version_cargo;

  END CALCULAR_VERSION;
/