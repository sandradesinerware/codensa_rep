
  CREATE OR REPLACE PROCEDURE "SNW_CARGOS"."MIGRAR_DIGITAL_SKILLS" 
is

BEGIN


for i in ( select id, digital_especifica from cargo ) 

LOOP

    if i.digital_especifica is null or upper(i.digital_especifica) = upper('no aplica') then
        -- Ingresar texto en el campo de justificacion de la tabla cargo                    
        UPDATE cargo set justif_competencia_dig = 'Actualización automática en cambio de formulario de competencias digitales' where id = i.id;
    else
        -- Crear una nueva digital skill de tipo Otra con el texto de digital especifica
        INSERT into cargo_competencia_especifica values (null, i.id, 24, i.digital_especifica);
    end if;
end loop;

END MIGRAR_DIGITAL_SKILLS;
/