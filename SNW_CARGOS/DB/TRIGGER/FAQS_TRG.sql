
  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."FAQS_TRG" 
BEFORE INSERT ON FAQS 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT FAQS_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;

/
ALTER TRIGGER "SNW_CARGOS"."FAQS_TRG" ENABLE;