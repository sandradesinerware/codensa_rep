
  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."POSICION_UO_TRG1" 
BEFORE INSERT ON POSICION_UO 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT POSICION_UO_SEQ1.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;

/
ALTER TRIGGER "SNW_CARGOS"."POSICION_UO_TRG1" ENABLE;