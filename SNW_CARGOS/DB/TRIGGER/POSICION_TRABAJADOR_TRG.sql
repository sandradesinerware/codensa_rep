
  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."POSICION_TRABAJADOR_TRG" 
BEFORE INSERT ON POSICION_TRABAJADOR 
FOR EACH ROW 
BEGIN
  <<COLUMN_SEQUENCES>>
  BEGIN
    IF INSERTING AND :NEW.ID IS NULL THEN
      SELECT POSICION_TRABAJADOR_SEQ.NEXTVAL INTO :NEW.ID FROM SYS.DUAL;
    END IF;
  END COLUMN_SEQUENCES;
END;

/
ALTER TRIGGER "SNW_CARGOS"."POSICION_TRABAJADOR_TRG" ENABLE;