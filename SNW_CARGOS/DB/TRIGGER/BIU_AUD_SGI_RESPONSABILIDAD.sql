
  CREATE OR REPLACE TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_RESPONSABILIDAD" 
BEFORE INSERT OR UPDATE ON SGI_RESPONSABILIDAD FOR EACH ROW 
BEGIN    
--  Copyright (c) Sinerware SAS 2005-2015. All Rights Reserved.    
--    
--    MODIFIED   (YYYY/MM/DD)    
--      atroncoso 2015-05-03 - Created    
--    
--    VERSION 1.0    
--    Trigger que pobla las columnas de auditoria    
----------------------------------------------------------------------------------------------------------------  
IF inserting THEN   
:NEW.AUD_CREADO_POR     := NVL(V('APP_USER'),USER);   
:NEW.AUD_FECHA_CREACION := SYSDATE;       
END IF;
:NEW.AUD_FECHA_ACTUALIZACION    := SYSDATE;
:NEW.AUD_ACTUALIZADO_POR        := NVL(V('APP_USER'),USER);
:NEW.AUD_TERMINAL_ACTUALIZACION := SYS_CONTEXT('USERENV','IP_ADDRESS');
END;

/
ALTER TRIGGER "SNW_CARGOS"."BIU_AUD_SGI_RESPONSABILIDAD" ENABLE;